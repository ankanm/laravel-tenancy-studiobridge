<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Studio Bridge">
  <meta name="author" content="Ashar Babar">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

  <link rel="shortcut icon" href="{{ URL::asset('assets/global/images/favicon.png')}}" type="image/png">
  <title>{{ config('app.name') }} | {{ config('client.client.name') }}</title>

 	<link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/icons/icomoon/icomoon.css') }}">
  <link href="{{ URL::asset('assets/global/css/style.css')}}" rel="stylesheet">

  <link href="{{ URL::asset('assets/global/css/theme.css')}}" rel="stylesheet">
  <link href="{{ URL::asset('assets/global/css/ui.min.css')}}" rel="stylesheet">
  <link href="{{ URL::asset('assets/admin/layout1/css/layout.css')}}" rel="stylesheet">
  <!-- BEGIN PAGE STYLE -->
  <link href="{{ URL::asset('assets/global/plugins/maps-amcharts/ammap/ammap.css')}}" rel="stylesheet">
  <!-- END PAGE STYLE -->

  <!-- BEGIN CUSTOM STYLE -->
  <link href="{{ URL::asset('css/pages-icons.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ URL::asset('css/plugins.css') }}" type="text/css" rel="stylesheet" >
  <link href="{{ URL::asset('css/color-system.css')}}" rel="stylesheet">

  <link href="{{ URL::asset('assets/icons/linearicons/linearicons.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ URL::asset('assets/icons/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

  <link href="{{ URL::asset('css/core.css') }}" type="text/css" rel="stylesheet" >
 	<link href="{{ URL::asset('css/bootstrap-extended.css') }}" type="text/css" rel="stylesheet" >
  <link href="{{ URL::asset('css/liveshoot.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ URL::asset('css/fancybox/jquery.fancybox.css') }}" type="text/css" rel="stylesheet">

  <script src="{{ URL::asset('assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>


  {{-- <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/global/plugins/datatables/dataTables.min.css')}}"> --}}
  @yield('page-styles')
  <!-- END CUSTOM STYLE -->
</head>
<script>
    var __baseUrl = "{{url('/')}}"
</script>
  @yield('body')
</html>
