@extends('layouts.plane')

@section('body')

  <body class="fixed-topbar fixed-sidebar dashboard color-default theme-sdtl bg-lighter sidebar-collapsed">
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    {!! survivor() !!}
    <section>
      <input type="hidden" value="{{ Auth::id() }}" id="current_user_id">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar">
        <div class="logopanel">
          <h1>
            <a href="/"></a>
          </h1>
        </div>
        <div class="sidebar-inner">
          <div class="sidebar-top">
            <form action="search-result.html" method="post" class="searchform" id="search-results">
              <input type="text" class="form-control" name="keyword" placeholder="Search...">
            </form>

          </div>
          <div class="menu-title">
            Navigation
          </div>
          @include('navigation.base')
          <!-- SIDEBAR WIDGET FOLDERS -->

          <div class="sidebar-footer clearfix">
            <a class="pull-left footer-settings" href="#" data-rel="tooltip" data-placement="top" data-original-title="Settings">
              <i class="icon-settings"></i></a>
              <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
              <i class="icon-size-fullscreen"></i></a>
              <a class="pull-left" href="" data-rel="tooltip" data-placement="top" data-original-title="Lockscreen">
              <i class="icon-lock"></i></a>
              <a class="pull-left btn-effect" href="/logout" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Logout">
              <i class="icon-power"></i></a>
          </div>
        </div>
      </div>
        <!-- END SIDEBAR -->
        <div class="main-content">
          <!-- BEGIN TOPBAR -->
          <div class="topbar">
            <div class="header-left">
              <div class="topnav">
                <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
                {{-- <ul class="nav nav-icons">
                <li> <img src="/assets/images/altayer-logo.png" alt=""> </li>
              </ul> --}}
            </div>
          </div>
          <div class="header-right">
            <ul class="header-menu nav navbar-nav">
              <!-- BEGIN NOTIFICATION DROPDOWN -->
              <li class="dropdown" id="notifications-header">
                <a href="#" data-toggle="dropdown" data-lick="dropdown" data-close-others="true">
                  <i class="icon-bell"></i>
                  <span class="badge badge-danger badge-header"><unread-notification :unreadcount="unreadcount"></unread-notification></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="dropdown-header clearfix">
                    <p class="pull-left"><unread-notification :unreadcount="unreadcount"></unread-notification> Pending Notifications</p>
                  </li>
                  <li>
                    <list-notifications :notificationstream="notificationstream"></list-notifications>
                  </li>
                  <li class="dropdown-footer clearfix">
                    <a href="#" class="pull-left">See all notifications</a>
                    <a href="#" class="pull-right">
                      <i class="icon-settings"></i>
                    </a>
                  </li>
                </ul>
              </li>
              <!-- END NOTIFICATION DROPDOWN -->
              <!-- BEGIN USER DROPDOWN -->
              <li class="dropdown" id="user-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                  <img class="userimage" src="{{ Auth::user()->avatar }}"  alt="user image">
                  <span class="username">Hi, {{Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="/user/{{Auth::user()->id}}"><i class="icon-user"></i><span>My Profile</span></a>
                  </li>
                  <li>
                    <a href="/logout"><i class="icon-logout"></i><span>Logout</span></a>
                  </li>
                </ul>
              </li>
              <!-- END USER DROPDOWN -->
              <!-- CHAT BAR ICON -->
              <li id="quickview-toggle"><a href="#"><i class="icon-bubbles"></i></a></li>
            </ul>
          </div>
          <!-- header-right -->
        </div>
        <!-- END TOPBAR -->
        <!-- BEGIN PAGE CONTENT -->
        @yield('main-content')
        <!-- END PAGE CONTENT -->
      </div>
      <!-- END MAIN CONTENT -->
      <!-- BEGIN BUILDER -->
      @yield('session-info')
            <!-- END BUILDER -->
          </section>
          <!-- BEGIN QUICKVIEW SIDEBAR -->
          <div id="quickview-sidebar">
            <div class="quickview-header">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#chat" data-toggle="tab">Chat</a></li>
                <li><a href="#notes" data-toggle="tab">Notes</a></li>
              </ul>
            </div>
            <div class="quickview">
              <div class="tab-content">
                @include('sidebar.chat')
                @include('sidebar.notes')


              </div>
            </div>
          </div>
          <!-- END QUICKVIEW SIDEBAR -->
          <!-- BEGIN SEARCH -->

          @include('search.search')
          <!-- END SEARCH -->
          <!-- BEGIN PRELOADER -->
          <div class="loader-overlay">
            {{-- <div class="spinner spinner-large">
            <img class="confer-overlay-loader" src="/assets/images/puff.svg" >
          </div> --}}
          <div class="cssload-container">
            <div class="loader">Loading...</div>
          </div>

          </div>

        <!-- END PRELOADER -->
        <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>

        <script src="{{ URL::asset('js/SB.js') }}"></script>
        <script src="{{ URL::asset('js/CoreAssets.js') }}"></script>
        <!-- BEGIN CUSTOM SCRIPT -->

        <script src="{{ URL::asset('assets/global/js/plugins.js')}}"></script> <!-- Main Plugin Initialization Script -->
        <script src="{{ URL::asset('assets/global/js/widgets/notes.js')}}"></script> <!-- Notes Widget -->
        <script src="{{ URL::asset('assets/global/js/quickview.js')}}"></script> <!-- Chat Script -->
        <script src="{{ URL::asset('assets/global/js/pages/search.js')}}"></script> <!-- Search Script -->
        <script src="{{ URL::asset('assets/global/plugins/noty/jquery.noty.packaged.min.js')}}"></script>
        <script src="{{ URL::asset('js/ripple.min.js') }}"></script>


        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};</script>
        {{-- <script src="/js/app.js"></script> --}}
        <script src="{{ URL::asset('assets/admin/layout1/js/layout.js')}}"></script>
        <script src="{{ URL::asset('js/studiobridge/globalfunctions.js') }}"></script>
        <script src="{{ URL::asset('js/extensions/countdown/jquery.plugin.js') }}"></script>
        <script src="{{ URL::asset('assets/global/plugins/moment/moment.min.js')}}"></script>
        <script src="{{ URL::asset('assets/global/plugins/slick/slick.min.js')}}"></script>


        <!-- END PAGE SCRIPT -->
        @yield('page-scripts')

      </body>
    @stop
