<style>
#chartdiv {
  width: 100%;
  height: 235px;
}
.amcharts-graph-g2 .amcharts-graph-stroke {
  stroke-dasharray: 3px 3px;
  stroke-linejoin: round;
  stroke-linecap: round;
  -webkit-animation: am-moving-dashes 1s linear infinite;
  animation: am-moving-dashes 1s linear infinite;
}
</style>

  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-body p-5">
        <div class="panel-header">
          <h3>Shoot Trend</h3>
        </div>
        <div id="chartdiv2"></div>
        <canvas id="chartdiv" height="200px"></canvas>
      </div>
    </div>
  </div>



{{--
<div class="row m-t-10">
<div class="col-md-6 equalheight">
<div class="panel panel-flat">
<div class="panel-header">
<h3>Recent Activities</h3>
</div>
<div class="p-15">
<div class="streamline">
@foreach($recent_activity as $activity)

@php

$type_class = 'sl-info';

switch ($activity->subject) {
case 'Note':
$activity_text = 'added a new note in ';
break;
case 'Complete the look':
$activity_text = 'added a new complete the look in ';
break;
case 'QC Product Rejected':
$type_class = 'sl-danger';
$activity_text = 'rejected a product in session';
break;
case 'QC Product Approved':
$type_class = 'sl-success';
$activity_text = 'approved a product in session';
break;
default:
$activity_text = '';
$type_class = 'sl-info';
break;
}

$activity_text = $activity_text . $activity->productcomment->psession_id;

@endphp
<div class="sl-item {{$type_class}}">
<div class="sl-content">
<small class="text-muted"><i class="icon-pencil5 position-left"></i>{{Carbon\Carbon::parse($activity->created_at)->diffForHumans()}}</small>
<p>{{$activity->user->name}} {{$activity_text}}</p>
</div>
</div>
@endforeach
@if(count($recent_activity) == 0)
<h3>No recent activity</h3>
@endif

</div>
</div>
</div>
</div>

<div class="col-md-6 equalheight">
<div class="panel panel-flat">
<div class="panel-header p-b-5">
<h3>Recent Comments</h3>
</div>

<ul class="media-list media-list-linked">

@foreach($recent_comments as $comment)


<li class="media">
<a  href="/product/{{$comment->productcomment->psession_id}}/{{$comment->productcomment->product_id}}" class="media-link">
<div class="media-left">
<img alt="" src="{{$comment->user->avatar}}" data-name="{{$comment->user->name}}" class="img-circle initials-profile">
</div>

<div class="media-body">
<h6 class="media-heading">{{$comment->user->name}} <span class="media-annotation dotted">{{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</span></h6>
{!! $comment->body !!}
</div>
</a>
</li>

@endforeach
@if(count($recent_comments) == 0)
<h3 class="p-15">No recent comments</h3>
@endif
</ul>

</div>
</div>
</div> --}}


<script src="{{ URL::asset('assets/global/plugins/noty/jquery.noty.packaged.min.js')}}"></script>  <!-- Notifications -->
<script src="{{ URL::asset('assets/global/plugins/charts-chartjs/Chart.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js')}}"></script>

<!-- Resources -->
<script src="{{ URL::asset('js/charts/amcharts/amcharts.js')}}"></script>
<script src="{{ URL::asset('js/charts/amcharts/serial.js')}}"></script>
<script src="{{ URL::asset('js/charts/amcharts/amcharts.js')}}"></script>
<script src="{{ URL::asset('js/charts/amcharts/themes/light.js')}}"></script>


<!-- END PAGE SCRIPT -->
{{-- <script src="{{ URL::asset('assets/global/js/pages/dashboard.js')}}"></script> --}}
<script>

$(document).ready(function(){
  liveTile();
  animateNumber();
  handleProgress();

  // panel-stat-chart, visitors-chart
  var parentHeight = $('.panel-stat').height();
  var pstatheight = parentHeight;
  $('.panel-stat-chart').css('height', pstatheight);
  var clockHeight = $('.jquery-clock ').height();
  var widgetProgressHeight = $('.widget-progress-bar').height();


});




</script>
<script>

$(document).ready(function() {

  var ctx = document.getElementById("chartdiv").getContext('2d');
  ctx.height = 300;

  const daysWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday",]
  const shotProducts = [];
  const approvedProducts = [];
  const copyProducts = [];

  @foreach($graph_shot as $graph_val)
  shotProducts.push({{$graph_val}})
  @endforeach

  @foreach($graph_approved as $graph_val)
  approvedProducts.push({{$graph_val}})
  @endforeach

  @foreach($graph_copy as $graph_val)
  copyProducts.push({{$graph_val}})
  @endforeach


  var gradient = ctx.createLinearGradient(0, 0, 0, 200);
  gradient.addColorStop(0, 'rgba(34,40,49, 0.3)');
  gradient.addColorStop(1, 'rgba(34,40,49, 0)');

  var gradient2 = ctx.createLinearGradient(0, 0, 0, 200);
  gradient2.addColorStop(0, 'rgba(0, 173, 181, 0.3)');
  gradient2.addColorStop(1, 'rgba(0, 173, 181, 0)');

  var gradient3 = ctx.createLinearGradient(0, 0, 0, 200);
  gradient3.addColorStop(0, 'rgba(104,38,102, 0.3)');
  gradient3.addColorStop(1, 'rgba(104,38,102, 0)');

  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: daysWeek,
      datasets: [{
        label: 'Shot Products',
        data: shotProducts,
        backgroundColor: gradient,
        borderColor: '#222831',
        strokeColor : "#222831",
        pointColor : "#222831",
        pointStrokeColor : "#222831",
        borderWidth: 0
      },
      {
        label: 'Approved Products',
        data: approvedProducts,
        backgroundColor: gradient2,
        borderColor: '#00ADB5',
        strokeColor : "#00ADB5",
        pointColor : "#00ADB5",
        pointStrokeColor : "#00ADB5",
        borderWidth: 0
      },
      {
        label: 'Copywritten Products',
        data: copyProducts,
        backgroundColor: gradient3,
        borderColor: '#682666',
        strokeColor : "#682666",
        pointColor : "#682666",
        pointStrokeColor : "#682666",
        borderWidth: 0
      }]
    },
    options: {
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          stacked: false,
          ticks: {
            beginAtZero:true
          }
        }]
      }
    },
    xAxes: [{
      gridLines: {
        display: false
      },
      ticks: {
        fontFamily: "Open Sans",
      }
    }],
  });

});

</script>
