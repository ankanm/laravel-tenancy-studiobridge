<ul class="chat-container">
  @if(isset($chat_messages))
    @foreach($chat_messages as $chat_message)
      @if($chat_message->sender->id == auth()->user()->id)
        <li class="img" id="message-{{$chat_message->id}}">
          <span>
            <div class="chat-detail chat-right">
              <span class="chat-date">{{$chat_message->humans_time}} ago</span>
              <img src="../assets/global/images/avatars/avatar1.png" data-retina-src="../assets/global/images/avatars/avatar1_2x.png">
              <div class="chat-detail">
                <div class="chat-bubble">{{$chat_message->message}}</div>
              </div>
            </div>
          </span>
        </li>
      @else

        <li class="img" id="message-{{$chat_message->id}}">
          <div class="chat-detail">
            <span class="chat-date">{{$chat_message->humans_time}} ago</span>
            <div class="conversation-img">
              <img src="../assets/global/images/avatars/avatar13.png" alt="avatar 4">
            </div>
            <div class="chat-bubble">
              <span>{{$chat_message->message}}</span>
            </div>
          </div>
        </li>


      @endif
    @endforeach
  @endif
</ul>
