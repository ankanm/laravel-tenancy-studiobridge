<div id="status-qc" class="text-center" style="">
  <div class="loader">
    <div class="loader-inner ball-pulse">
      <div class="bg-blue"></div>
      <div class="bg-amber"></div>
    </div>
  </div>
</div>
@php
$username = '';
$useravatar = '';

@endphp

@foreach ($result as $products)
  @php
  if(isset($products->photographer)){
    $useravatar = App\User::where('id', $products->photographer)->first()->avatar;
  }
  @endphp


    <li class="media" id="qc-product-{{$products->pid}}">
      <input type="hidden" class="requires-extra-shot-{{$products->pid}}" value="{{$products->requires_extra_shot}}">
      <a href="#" class="media-link qc-product-a" data-pid="{{ $products->pid }}"
      data-sid="{{$products->sid}}" data-title="{{ product_identifier($products->base_product_id, $products->identifier, 0) }}">
      <div class="media-left">
        <img src="{{$useravatar}}" data-name = "" src="" class="img-circle initials-profile-qc" alt="">
      </div>

      <div class="media-body">
        <h6 class="media-heading">{!! product_identifier($products->base_product_id, $products->identifier) !!}

        </h6>
        <span class="media-annotation">{{ Carbon\Carbon::parse($products->created_at)->diffForHumans() }}</span>
        <h6 class="no-margin m-t-5 no-padding-bottom current-product-sid">
          Session {{$products->sid }}</h6>


        </div>
      </a>

      <div class="media-actions pull-right">
        @if ($products->requires_extra_shot == 1)
          <span style="font-size: 27px !important;font-weight: 900;color: #48c126;">+1</span>
        @endif

        <i class="fa fa-lock hidden lock-product-{{ $products->pid }}"></i>
      </div>


    </li>

@endforeach
