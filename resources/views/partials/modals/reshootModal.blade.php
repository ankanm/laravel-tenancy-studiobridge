<div id="reshoot-product-model" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reshoot Product</h4>
      </div>
      <div class="modal-body" >
        <p>Click on the session tab you wish to import images from. The lastest session tab is automatically selected but you can toggle between other sessions if they exist.
          <p>
            <div class="tabbable" id="reshoot-product-model-tabs">
            </div>
          </div>
          <div class="modal-footer no-padding-top">
            <button type="button" class="ripple btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
            <a  class="ripple btn btn bg-blue btn-sm btn-labeled reshoot-import-btn" >Import Images</a>
          </div>
        </div>
      </div>
    </div>
