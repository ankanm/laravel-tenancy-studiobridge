<div id="add-loc" class="modal fade modal-slideright">
<input type="hidden" id="location-org-id">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="p-l-10">Add <strong>Location</strong></h2>
        <span class="text-danger shiperror"></span>
        <form class="form-horizontal">
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Short name <span class="text-danger">*</span></label>
            <div class="col-lg-6">
              <input type class="form-control ui-wizard-content" id="short_name" name="short_name" placeholder="Short Name">
            </div>
          </div>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Address 1 <span class="text-danger">*</span></label>
            <div class="col-lg-6">
              <textarea name="address1" rows="3" cols="5" id="address1" class="form-control" placeholder="Address 1"></textarea>
            </div>
          </div>
           <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Address 2</label>
            <div class="col-lg-6">
              <textarea id="address2" name="address2" rows="3" cols="5" class="form-control" 
              placeholder="Address 2"></textarea>
            </div>
          </div>
           <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Select Country <span class="text-danger">*</span></label>
            <div class="col-lg-6">
              <select id="country" class="form-control" name="country">
                <option value="">Select Country</option>
                @foreach($countryList as $country)
                  <option value="{{ $country->id }}">{{ $country->name }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Select Province <span class="text-danger">*</span></label>
            <div class="col-lg-6">
             <select id="state" name="province" class="form-control">
                <option value="">Select Province</option>
            </select>
            </div>
          </div>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Select City <span class="text-danger">*</span></label>
            <div class="col-lg-6">
            <select id="city" name="city" class="form-control">
               <option value="">Select City</option>
            </select>
            </div>
          </div>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">ZipCode</label>
            <div class="col-lg-6">
              <input type class="form-control ui-wizard-content" id="zipcode" name="zipcode" placeholder="ZipCode">
            </div>
          </div>

          <div class="form-wizard-actions">
            <button type="button" class="ripple btn btn-default ui-wizard-content ui-formwizard-button reset-button" data-dismiss="modal">Cancel</button>
            <a class="ripple btn btn-info" id="add-location-bt">Add Location</a>
          </div>
        </form>
      </div>
    </div>
  </div>
