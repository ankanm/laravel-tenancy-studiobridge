<div id="flag-product" class="modal fade">
<input type="hidden" id="flag-product-id">
<input type="hidden" id="flag-box-id">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="p-l-10">Add <strong>Flag</strong></h2>
      </div>
      <div class="modal-body">
        <div class="form-group m-b-5">
          <select name="flag-options" id="live-shoot-modal-flag-options" class="form-control select2">
            <option value="0">Please select an option from the list</option>
            <option value="Product Mismatch">Product Mismatch</option>
            <option value="Information Incorrect">Information Incorrect</option>
            <option value="Complete the look">Complete the look</option>
            <option value="Note">Note</option>
            <option value="Other">Other</option>
          </select>
        </div>
        <div class="form-group">
          <textarea class="basic-editor" name="flag-message" id="live-shoot-modal-flag-message"></textarea>
        </div>
      </div>
      <div class="modal-footer no-padding-top">
        <button type="button" class="ripple btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
        <button type="button" class="ripple btn btn-success btn-sm btn-labeled" id="liveshoot-flag-product-bt">Flag Product</button>
      </div>
    </div>
  </div>
</div>
