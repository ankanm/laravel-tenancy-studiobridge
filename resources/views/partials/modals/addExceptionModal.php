 <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="p-l-10">Add <strong>Exception</strong></h2>
      </div>
      <div class="modal-body">
        <div class="form-group m-b-5">
          <select name="flag-options" id="product-modal-flag-options" class="form-control select2">
            <option value="0">Please select an option from the list</option>
            <option value="1">Add a Box which is received but not on list</option>
            <option value="2">Add a Product if not on list is received</option>
            <option value="3">Add quantity if more quantity is received than is expected</option>
            

          </select>
        </div>
        <div class="form-group m-b-5"">
          <div class="input input--fumi col-sm-12">
            <input class="input__field input__field--fumi" type="text" id="add-exception-identifier">
            <label class="input__label input__label--fumi" for="add-exception-identifier">
              <i id="icon-identifier-search" class="fa fa-fw fa-search icon icon--fumi "></i>
              <span class="input__label-content input__label-content--fumi exception-identifier">Scan <strong>Box ID or Product ID</strong></span>
            </label>
          </div>
        </div>
      </div>
      <div class="modal-footer no-padding-top">
        <button type="button" class="ripple btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
        <button type="button" class="ripple btn btn-danger btn-sm btn-labeled" id="liveshoot-add-exception-bt"><b><i class="icon-paperplane"></i></b>Add Exception</button>
      </div>
    </div>
  </div>
</div>