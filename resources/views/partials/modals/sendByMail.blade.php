<div id="send-email" class="modal fade">
<input type="hidden" id="send-box-id">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="p-l-10">Add <strong>Email Address</strong></h2>
      </div>
      <div class="modal-body">
        <div class="form-group">
         <input type="text" name="send-email-address" id="send-email-address" class="form-control">
         <div class="mail_error text-danger"></div>
        </div>
      </div>
      <div class="modal-footer no-padding-top">
        <button type="button" class="ripple btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
        <button type="button" class="ripple btn btn-success btn-sm btn-labeled" id="asn-send-mail-btn"><b><i class="icon-paperplane"></i></b>Send Email</button>
      </div>
    </div>
  </div>
</div>
