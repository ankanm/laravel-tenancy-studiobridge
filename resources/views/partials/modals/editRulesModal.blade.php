<div id="edit_rule" class="modal fade modal-slideright edit-rule">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="p-l-10">Edit <strong>Rule</strong></h2>
        <span class="text-danger shiperror"></span>
        <form class="form-horizontal" id="edit_rule_form">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="id_sequence_convention" value="">
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Short Name <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <input type="text" class="form-control ui-wizard-content" id="edit_short_name" name="short_name" placeholder="Short Name">
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Applies When <span class="text-danger">*</span></label>
            <div class="form-group col-lg-10">
              <select id="edit_channel" class="form-control" name="channel">
                <option value="0">Select Channel</option>
                @if($channels->count() > 0)
                  @foreach($channels as $channel)
                    <option value="{{$channel->channel_code}}">{{$channel->channel_name}}</option>
                  @endforeach
                @endif
              </select>
            </div>
            <div class="form-group col-lg-10">
              <select id="edit_shoot_type" class="form-control" name="shoot_type">
                <option value="0">Select Shoot Type</option>
                <option value="product">Product</option>
                <option value="model">Model</option>
                <option value="pk">PK</option>
              </select>
            </div>
            <div class="form-group col-lg-10">
              <select id="edit_gender" class="form-control" name="gender">
                <option value="0">Select Gender</option>
                @if($genders->count() > 0)
                  @foreach($genders as $gender)
                    <option value="{{$gender->gender}}">{{$gender->gender}}</option>
                  @endforeach
                @endif
              </select>
            </div>
            <div class="form-group col-lg-10">
              <select id="edit_category" class="form-control" name="category">
                <option value="0">Select Category</option>
                @if($categories->count() > 0)
                  @foreach($categories as $category)
                    <option value="{{$category->category}}">{{$category->category}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Sequence <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <input type="text" class="form-control ui-wizard-content" id="edit_sequence" name="sequence" placeholder="Sequence">
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Naming Convention <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select id="edit_convention_id" class="form-control" name="convention_id">
                <option value="0">Select Naming Convention</option>
                @if($namingConventions->count() > 0)
                  @foreach($namingConventions as $namingConvention)
                    @php
                      $parameter_1 = ($namingConvention->parameter_1)? $namingConvention->parameter_1:'';
                      $parameter_2 = ($namingConvention->parameter_2)? '_'.$namingConvention->parameter_2:'';
                      $parameter_3 = ($namingConvention->parameter_3)? '_'.$namingConvention->parameter_3:'';
                      $parameter_4 = ($namingConvention->parameter_4)? '_'.$namingConvention->parameter_4:'';
                    @endphp
                    <option value="{{$namingConvention->id_naming_convention}}">{{$parameter_1.$parameter_2.$parameter_3.$parameter_4}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-wizard-actions col-lg-10">
            <button type="button" class="ripple btn btn-default ui-wizard-content ui-formwizard-button reset-button" data-dismiss="modal">Cancel</button>
            <a class="ripple btn btn-info" id="edit_rule_bt">Update</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
