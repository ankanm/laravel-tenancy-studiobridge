<div id="edit_copyheader" class="modal fade modal-slideright">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="p-l-10">Edit <strong>Copy Header</strong></h2>
        <span class="text-danger shiperror"></span>
        <form class="form-horizontal" id="edit_ch_form">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="id" value="" id="edit_id">
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Header Name <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <input type="text" class="form-control ui-wizard-content" id="header_name_edit" name="header_name" placeholder="Header Name">
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Header Type <span class="text-danger">*</span></label>
            <div class="form-group col-lg-10">
              <select id="header_type_edit" class="form-control" name="header_type">
                <option value="">Select Type</option>
                <option value="text">Text</option>
                <option value="textarea">Textarea</option>
                <option value="dropdown">Dropdown</option>
               
              </select>
            </div>
            
          </div>
          <div class="form-group p-r-16 hide">
            <label class="control-label col-lg-6">Template <span class="text-danger">*</span></label>
            <div class="form-group col-lg-10">
              <select id="copy_template_id_edit" class="form-control" name="copy_template_id">
                <option value="">Select Template</option>
                @foreach($template_all as $row)
                <option value="{{$row->id}}">{{$row->template_name}}</option>
                @endforeach
               
              </select>
            </div>
            
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Default Value </label>
            <div class="col-lg-10">
              <input type="text" class="form-control ui-wizard-content" id="default_value_edit" name="default_value" placeholder="Default Value">
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Required </label>
            <div class="col-lg-10">
              <input type="radio" id="required_edit1" name="is_required" value="1"> Yes
              <input type="radio" id="required_edit0" name="is_required" value="0"> No
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Visible </label>
            <div class="col-lg-10">
              <input type="radio" id="visible_edit1" name="is_visible" value="1"> Yes
              <input type="radio" id="visible_edit0" name="is_visible" value="0" checked="true"> No
            </div>
          </div>
          <div class="form-group p-r-16 hide">
            <label class="control-label col-lg-6">Channels </label>
            <div class="col-lg-10">
              <input type="text" class="form-control ui-wizard-content" id="channels_edit" name="channels" placeholder="Default Value">
            </div>
          </div>
          <div class="form-group p-r-16 hide">
            <label class="control-label col-lg-6">Weight </label>
            <div class="col-lg-10">
              <input type="text" class="form-control ui-wizard-content" id="weight_edit" name="weight" placeholder="Weight">
            </div>
          </div>
          
          <div class="form-wizard-actions col-lg-10">
            <button type="button" class="ripple btn btn-default ui-wizard-content ui-formwizard-button reset-button" data-dismiss="modal">Cancel</button>
            <a class="ripple btn btn-info" id="edit_ch_bt">Update</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
