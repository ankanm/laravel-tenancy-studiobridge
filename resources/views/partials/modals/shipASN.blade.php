<div id="ship-asn" class="modal fade modal-slideright">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="p-l-10">Ship <strong>ASN</strong></h2>
        <span class="text-danger shiperror"></span>
        <form class="form-horizontal">
          <h6 class="form-wizard-title">
            <span>Carrier</span>
            <small class="display-block">Select a carrier </small>
          </h6>
          <div class="form-group p-10">
            <select name="carrier_id" class="form-control" id="carrier_id">
              @php
              if($asn->carrier_id != '' || $asn->carrier_id != null){
                $carrier_selected = $asn->carrier_id;
              } else{
                $carrier_selected = '';
              }
                $all_carriers = App\Models\Carrier::all();


              @endphp
              <option value="">Select Carrier</option>
              @foreach($all_carriers as $carrier)
                <option value="{{ $carrier->id }}" {{ ($carrier_selected == $carrier->id ? 'selected' : null)}}>{{ $carrier->name }}
                </option>
              @endforeach
            </select>
          </div>
          <h6 class="form-wizard-title">
            <span>Shipping Details</span>
            <small class="display-block">What does this box look like? </small>
          </h6>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Shipping Method</label>
            <div class="col-lg-6">
              <input type="text" id="shipping_method" name="shipping_method" value="" class="form-control ui-wizard-content" placeholder="Shipping Method">
            </div>
          </div>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Ship Date</label>
            <div class="col-lg-6">
              <div class="prepend-icon">
                <input type class="form-control datetimepicker" id="estimated_ship_date"
                name="estimated_ship_date" placeholder="Ship Date" autocomplete='off' readonly="readonly">
                <i class="icon-clock"></i>
              </div>
            </div>
          </div>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Tracking Number</label>
            <div class="col-lg-6">
              <input type="text" name="tracking_number" id="tracking_number" value="" class="form-control ui-wizard-content" placeholder="Tracking Number">
            </div>
          </div>

          <div class="form-wizard-actions">
            <button type="button" class="ripple btn btn-default ui-wizard-content ui-formwizard-button reset-button" data-dismiss="modal">Cancel</button>
            <a class="ripple btn btn-info ship-asn">Ship ASN</a>
          </div>
        </form>
      </div>
    </div>
  </div>
