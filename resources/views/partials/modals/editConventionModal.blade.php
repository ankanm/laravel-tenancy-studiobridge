<div id="edit_naming" class="modal fade modal-slideright">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="p-l-10">Edit <strong>Convention</strong></h2>
        <span class="text-danger shiperror"></span>
        <form class="form-horizontal" id="edit_naming_form">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="id_naming_convention" value="">
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Short Name <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <input type="text" class="form-control ui-wizard-content" id="edit_short_name_con" name="short_name" placeholder="Short Name">
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Parameter 1 <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select id="edit_parameter_1" class="form-control" name="parameter_1">
                <option value="0">Select Parameter 1</option>
                @if(sizeof($headers))
                  @foreach($headers as $header)
                    <option value="{{$header}}">{{$header}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Parameter 2 <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select id="edit_parameter_2" class="form-control" name="parameter_2">
                <option value="0">Select Parameter 2</option>
                @if(sizeof($headers))
                  @foreach($headers as $header)
                    <option value="{{$header}}">{{$header}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Parameter 3 <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select id="edit_parameter_3" class="form-control" name="parameter_3">
                <option value="0">Select Parameter 3</option>
                @if(sizeof($headers))
                  @foreach($headers as $header)
                    <option value="{{$header}}">{{$header}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Parameter 4 <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select id="edit_parameter_4" class="form-control" name="parameter_4">
                <option value="0">Select Parameter 4</option>
                @if(sizeof($headers))
                  @foreach($headers as $header)
                    <option value="{{$header}}">{{$header}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-wizard-actions col-lg-10">
            <button type="button" class="ripple btn btn-default ui-wizard-content ui-formwizard-button reset-button" data-dismiss="modal">Cancel</button>
            <a class="ripple btn btn-info" id="edit_convention_bt">Update</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
