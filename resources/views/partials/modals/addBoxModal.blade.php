<div id="add-box" class="modal fade modal-slideright">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="p-l-10">Add <strong>Box</strong></h2>
        <form class="form-horizontal">
          <h6 class="form-wizard-title">
            <span>Box Template (Optional)</span>
            <small class="display-block">Select a template </small>
          </h6>
          <div class="form-group p-10">
            <select name="flag-options" class="form-control" id="box_size">
              <option value="0">Please select template from the list</option>
              <option value="small_box">Small Box</option>
              <option value="medium_box">Medium Box</option>
              <option value="large_box">Large Box</option>
              <option value="flat_box">Flat Box</option>
            </select>
          </div>
          <h6 class="form-wizard-title">
            <span>Box Dimensions</span>
            <small class="display-block">What does this box look like? </small>
          </h6>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Length</label>
            <div class="col-lg-6">
              <input type="text" id="dimension_l" name="dimension_l" value="" class="form-control ui-wizard-content" placeholder="L">
            </div>
          </div>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Width</label>
            <div class="col-lg-6">
              <input type="text" id="dimension_w" name="dimension_w" value="" class="form-control ui-wizard-content" placeholder="W">
            </div>
          </div>
          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Height</label>
            <div class="col-lg-6">
              <input type="text" name="dimension_h" id="dimension_h" value="" class="form-control ui-wizard-content" placeholder="H">
            </div>
          </div>

          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Unit</label>
            <div class="col-lg-6">
              <select name="flag-options" class="form-control " id="dimension_unit">
                <option value="0">Unit</option>
                <option value="cm">centimeters</option>
                <option value="in">Inches</option>
                <option value="ft">Feet</option>
              </select>
            </div>
          </div>

          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Weight</label>
            <div class="col-lg-6">
              <input type="text" id="weight" name="weight"  class="form-control ui-wizard-content" placeholder="Weight">
            </div>
          </div>

          <div class="form-group p-r-10">
            <label class="control-label col-lg-6">Unit</label>
            <div class="col-lg-6">
              <select name="flag-options" class="form-control" id="weight_unit">
                <option value="0">Unit</option>
                <option value="kg">Kilograms</option>
                <option value="lb">Lbs</option>
              </select>
            </div>
          </div>

          <div class="form-wizard-actions">
            <button type="button" class="ripple btn btn-default ui-wizard-content ui-formwizard-button reset-button" data-dismiss="modal">Cancel</button>
            <button id="add-asnbox" type="button" class="ripple btn btn-info ">Add Box</button>


          </div>
        </form>

      </div>
    </div>
  </div>
