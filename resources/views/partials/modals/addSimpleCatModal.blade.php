<div id="add_simplecat" class="modal fade modal-slideright">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="p-l-10">Add <strong>Simple Category</strong></h2>
        <span class="text-danger shiperror"></span>
        <form class="form-horizontal" id="add_sc_form">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Calss <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select class="form-control ui-wizard-content" id="add_sc_class" name="class" required>
              <option value="0">Select</option>
                @if($classes->count() > 0)
                  @foreach($classes as $class)
                    <option value="{{$class->class}}">{{$class->class}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Sub Class <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select class="form-control ui-wizard-content" id="add_sc_subclass" name="subclass" required disabled>
              <option value="0">Select</option>
                @if($subclasses->count() > 0)
                  @foreach($subclasses as $subclass)
                    @if($subclass->subclass)
                    <option value="{{$subclass->subclass}}">{{$subclass->subclass}}</option>
                    @endif
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Category <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select class="form-control ui-wizard-content" id="add_sc_category" name="category" required>
              <option value="0">Select</option>
                @if($simple_cats->count() > 0)
                  @foreach($simple_cats as $simple_cat)
                    <option value="{{$simple_cat->category}}">{{$simple_cat->category}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-wizard-actions col-lg-10">
            <button type="button" class="ripple btn btn-default ui-wizard-content ui-formwizard-button reset-button" data-dismiss="modal">Cancel</button>
            <a class="ripple btn btn-info" id="add_sc_bt">Save</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
