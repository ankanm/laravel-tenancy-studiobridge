<div id="add_convention" class="modal fade modal-slideright">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="p-l-10">Add <strong>Convention</strong></h2>
        <span class="text-danger shiperror"></span>
        <form class="form-horizontal" id="add_naming_form">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Short Name <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <input type="text" class="form-control ui-wizard-content" id="short_name_con" name="short_name" placeholder="Short Name">
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Parameter 1 <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select id="parameter_1" class="form-control" name="parameter_1">
                <option value="0">Select Parameter 1</option>
                @if(sizeof($headers))
                  @foreach($headers as $header)
                    <option value="{{$header}}">{{$header}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Parameter 2 <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select id="parameter_2" class="form-control" name="parameter_2" disabled>
                <option value="0">Select Parameter 2</option>
              </select>
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Parameter 3 <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select id="parameter_3" class="form-control" name="parameter_3" disabled>
                <option value="0">Select Parameter 3</option>
              </select>
            </div>
          </div>
          <div class="form-group p-r-16">
            <label class="control-label col-lg-6">Parameter 4 <span class="text-danger">*</span></label>
            <div class="col-lg-10">
              <select id="parameter_4" class="form-control" name="parameter_4" disabled>
                <option value="0">Select Parameter 4</option>
              </select>
            </div>
          </div>
          <div class="form-wizard-actions col-lg-10">
            <button type="button" class="ripple btn btn-default ui-wizard-content ui-formwizard-button reset-button" data-dismiss="modal">Cancel</button>
            <a class="ripple btn btn-info" id="add_convention_bt">Add Convention</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
