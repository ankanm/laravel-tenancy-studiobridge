<aside class="rightbar"id="pinned-sidebar" >
  <div class="rightbar-container">
    <div class="chat-user-toolbar clearfix">
      <div class="chat-user-search pull-left text-white text-center">
        <h5>Pinned Images</h5>
      </div>
    </div>
    <div class="chat-user-container">
      <ul class="chat-user-list liveshoot-pinblock-ul">
        @foreach ($pin_images as $image)
          <li id="liveshoot-pinblock-li-{{$image->id}}">
            <span class="chat-avatar"><img data-src="{{ URL::asset($image->thumbpath) }}" alt="Avatar" style="width:100px;height:100px;"></span><span class="chat-u-info"></span>
            <span class="chat-u-status">
              <button data-id="{{$image->id}}" class="ripple btn-primary btn-sm liveshoot-pin-to-session">Add</button>
              <button data-id="{{$image->id}}" class="ripple btn-danger btn-sm liveshoot-unpin">Unpin</button>
            </span>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
</aside>
