
<div class=" item-comment" id="flags-{{$product_id}}">
  @foreach($comments as $comment)
    <div class="row">
      <div class="col-sm-2">
        <div class="thumbnail p-0 ">
          <img class="img-responsive user-photo comment-thumbnail" src="{{$comment->user->avatar}}" >
          <br>
          <span class="text-center block fs-12 " style="width:100%">{{$comment->user->name}}</span>
        </div><!-- /thumbnail -->
      </div><!-- /col-sm-1 -->

      <div class="col-sm-10 p-l-0">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #fff !important;">
            <span class="text-muted">added {{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</span>
            {!! $comment->body !!}
          </div>

          @php
          $qc_operation_ids = DB::table('comment_qc_operation')
          ->where('comment_id', $comment->id)
          ->limit(1)
          ->get(['qc_operation_id']);

          foreach ($qc_operation_ids as $qc_operation_id) {
            if($qc_operation_id != '' || $qc_operation_id != null){
              $qc_ops = App\Models\QcOperation::where('id', $qc_operation_id->qc_operation_id)->get();
              echo '<div class="panel-body" style="background-color:#f8f8fa;">';
              if($qc_ops[0]->status == 1){
                echo "QC Operation: " . "<span class='text-bold text-success'>Approved</span>
                <br>
                Session ID:" . $qc_ops[0]->psession_id;
              } else{
                echo "QC Operation: " . "<span class='text-bold text-danger'>Rejected</span>
                <br>
                Session ID:" . $qc_ops[0]->psession_id;
              }
              echo '</div>';
            }
          }



        @endphp

      </div><!-- /panel panel-default -->
    </div><!-- /col-sm-5 -->
  </div>
@endforeach
</div>
