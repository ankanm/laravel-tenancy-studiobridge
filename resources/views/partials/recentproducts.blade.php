
@foreach ($products as $pid => $product)


  <li class="media recent-product-click" data-pid="{{$product['product']['products']->identifier}}" id="recent-product-{{$product['product']['products']->id}}">

    <a href="#" class="media-link " >
      <div class="media-left">
        @if(isset($product['thumb']->image['thumbpath']))
          <img data-src="/{{$product['thumb']->image['thumbpath']}}" style="width:48px!important;height:48px!important;">
        @else
          <img data-src="http://www.kamacatchme.com/wp-content/themes/cozy/images/noimage_2.gif" style="width:48px!important;height:48px!important;">
        @endif
      </div>
      <div class="media-body">
        <h6 class="media-heading">{{ $product['product']['products']->identifier }}

        </h6>
        <span class="media-annotation ">
          @if($product['product']['qc_done'] == 1)
            @if($product['product']['qc_state'] == 1)
              <span class="text-success">Product Approved</span>
            @else
              <span class="text-danger">Product Rejected</span>
            @endif
          @else
            <span class="text-amber-darkest">QC Pending</span>
          @endif
        </span>

      </div>
    </a>

  </li>

@endforeach
