<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Studio Bridge</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="Ashar Babar" name="author" />
    <link rel="shortcut icon" href="{{ URL::asset('assets/global/images/favicon.png')}}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/fonts/proxima/fonts.css') }}">
    <link href="{{ URL::asset('assets/global/css/style.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/global/css/ui.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/global/plugins/bootstrap-loading/lada.min.css')}}" rel="stylesheet">
</head>
<body class="account2" data-page="login">
    <!-- BEGIN LOGIN BOX -->
    <div class="container" id="login-block">
        <i class="user-img icons-faces-users-03"></i>
        <div class="account-info">
            <a class="logo"></a>
            <h3>Intelligent.Studio.Management</h3>
            <ul>
                <li><i class="icon-camera c-orange"></i> Receive</li>
                <li><i class="icon-camera c-red"></i> Shoot</li>
                <li><i class="icon-magic-wand c-yellow"></i> Write</li>
                <li><i class="icon-check c-green"></i> Done</li>
            </ul>
        </div>
        <div class="account-form">
            <form class="form-signin" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <h3><strong>Sign in</strong> to your account</h3>
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Oops!</strong> {{ Session::get('error') }}
                    </div>
                @endif
               
                @if(!empty($message))
                    <div class="alert alert-danger">
                        <strong>Oops!</strong> {{ $message }}
                    </div>
                @endif
                <div class="append-icon">
                    <input type="text" name="email" id="email" class="form-control form-white username" placeholder="Username" value="{{ old('email') }}" required>
                    <i class="icon-user"></i>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="append-icon m-b-20">
                    <input type="password" name="password" class="form-control form-white password" placeholder="Password" required>
                    <i class="icon-lock"></i>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" id="submit-form" class="ripple btn btn-lg btn-dark btn-rounded ladda-button" data-style="expand-left">Sign In</button>
                <span class="forgot-password"><a id="password" href="account-forgot-password.html">Forgot password?</a></span>
                <div class="form-footer">

                    <div class="clearfix">
                        <p class="new-here"><a>Can't Login? Contact Us</a></p>
                    </div>
                </div>
            </form>
            <form class="form-password" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <h3><strong>Reset</strong> your password</h3>
                <div class="append-icon m-b-20">
                    <input type="text" name="email" id="email" class="form-control form-white username-reset" placeholder="Username" value="{{ old('email') }}" required>
                    <i class="icon-user"></i>
                </div>
                <button type="submit" id="submit-password" class="ripple btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left">Send Password Reset Link</button>
                <div class="clearfix m-t-60">
                    <p class="pull-left m-t-20 m-b-0"><a id="login" href="#">Have an account? Sign In</a></p>
                </div>
            </form>

        </div>

    </div>
    <!-- END LOCKSCREEN BOX -->
    <p class="account-copyright">
        <span>{{config('studiobridge.display.copyright')}}</span>
    </p>
    <script src="{{ URL::asset('assets/global/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
    <script src="{{ URL::asset('assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js')}}"></script>
    <script src="{{ URL::asset('assets/global/plugins/gsap/main-gsap.min.js')}}"></script>
    <script src="{{ URL::asset('assets/global/plugins/tether/js/tether.min.js')}}"></script>
    <script src="{{ URL::asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ URL::asset('assets/global/plugins/backstretch/backstretch.min.js')}}"></script>
    <script src="{{ URL::asset('assets/global/plugins/bootstrap-loading/lada.min.js')}}"></script>
    <script src="{{ URL::asset('assets/global/js/pages/login-v2.js')}}"></script>
</body>
</html>