<div class="tab-pane fade" id="notes">
     <div class="list-notes current withScroll">
       <div class="notes ">
         <div class="row">
           <div class="col-md-12">
             <div id="add-note">
               <i class="fa fa-plus"></i>ADD A NEW NOTE
             </div>
           </div>
         </div>
         <div id="notes-list">
           <div class="note-item media current fade in">
             <button class="close">×</button>
             <div>
               <div>
                 <p class="note-name">Check camera</p>
               </div>
               <p class="note-desc hidden">Break security reasons.</p>
               <p><small>Tuesday 6 May, 3:52 pm</small></p>
             </div>
           </div>

         </div>
       </div>
     </div>
     <div class="detail-note note-hidden-sm">
       <div class="note-header clearfix">
         <div class="note-back">
           <i class="icon-action-undo"></i>
         </div>
         <div class="note-edit">Edit Note</div>
         <div class="note-subtitle">title on first line</div>
       </div>
       <div id="note-detail">
         <div class="note-write">
           <textarea class="form-control" placeholder="Type your note here"></textarea>
         </div>
       </div>
     </div>
   </div>
