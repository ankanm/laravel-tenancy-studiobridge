
<div class="tab-pane fade active in " id="chat">
     <div class="chat-body current">
       <div class="chat-search">
         <form class="form-inverse" action="#" role="search">
           <div class="append-icon">
             <input type="text" class="form-control" placeholder="Search contact...">
             <i class="icon-magnifier"></i>
           </div>
         </form>
       </div>
       <div class="chat-list">
         <div class="title">Contacts</div>
         Chat is not enabled
         {{-- @php
         $users = App\User::where('id', '!=', Auth::id())->get();
         @endphp
         <ul>
           @foreach($users as $user)
             @if($user->name != '' || $user->name != 'StudioBridge Super Admin')
               <li class="clearfix">
                 <div class="user-img">
                   <img src="{{$user->avatar}}" alt="avatar" class="img-sm img-circle"/>
                 </div>
                 <div class="user-details">
                   <div class="user-name text-bold" data-id="{{$user->id}}"><a href="{{route('message.read', ['id'=>$user->id])}}">{{$user->name}}</a></div>
                   <div class="user-txt">On the road again...</div>
                 </div>
                 <div class="user-status">
                   <i class="online"></i>
                 </div>
               </li>
             @endif
           @endforeach --}}
         </ul>
       </div>
     </div>
     <div class="chat-conversation ">
       <div class="conversation-header">
         <div class="user clearfix">
           <div class="chat-back">
             <i class="icon-action-undo"></i>
           </div>
           <div class="user-details">
             <div class="user-name">Ashar Babar</div>
             <div class="user-txt">On the road again...</div>
           </div>
         </div>
       </div>
       <div class="conversation-body ">
            @include('partials.chatbody')
       </div>
       <div class="conversation-message">

          <form action="" method="post" id="talkSendMessage">
                <input type="text" placeholder="Your message..." class="form-control form-white send-message" name="message-data" id="message-data" />
                <input type="hidden" name="_id" value="{{@request()->route('id')}}">
             </form>


         <div class="item-footer clearfix">
           <div class="footer-actions">
             <i class="icon-rounded-marker"></i>
             <i class="icon-rounded-camera"></i>
             <i class="icon-rounded-paperclip-oblique"></i>
             <i class="icon-rounded-alarm-clock"></i>
           </div>
         </div>
       </div>
     </div>
   </div>

   <script>
      var show = function(data) {
           alert(data.sender.name + " - '" + data.message + "'");
      }

      var msgshow = function(data) {
         console.log(data);
           var html = '<li class="img" id="message-'+data.id+'">'+
           '            <div class="chat-detail">'+
           '              <span class="chat-date">Just now</span>'+
           '            <div class="conversation-img">'+
           '             <img src="../assets/global/images/avatars/avatar13.png" alt="avatar 4">'+
           '            </div>'+
           '              <div class="chat-detail">'+
           '                <div class="chat-bubble">'+data.message +'</div>'+
           '              </div>'+
           '            </div>'+
           '        </li>';

           $('.conversation-body ul').append(html);
           $('.chat-container').animate({scrollTop: $('.chat-container').prop("scrollHeight")}, 500);
      }


   </script>
   {{-- {!! talk_live(['user'=>["id"=>auth()->user()->id, 'callback'=>['msgshow']]]) !!} --}}
