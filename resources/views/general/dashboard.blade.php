@extends('layouts.body')

@section('main-content')

	<link href="https://www.google.com/uds/api/visualization/1.0/84dc8f392c72d48b78b72f8a2e79c1a1/ui+en.css" type="text/css" rel="stylesheet">


	<!--Page Container-->
	<div class="page-content">
		@if(Session::has('error'))
			<div class="alert alert-danger">
				<strong>Oops!</strong> {{ Session::get('error') }}
			</div>
		@endif

		<div class="row m-b-20">
			<div class="col-sm-6">
				<h2>Hi there, <strong>{{Auth::user()->name}}</strong></h2>
				<p>Here is whats happening @ {{ config('client.client.name') }} today</p>
			</div>

			{{-- <div class="col-md-6">
			<div class="widget-cash-in-hand m-t-5 pull-right">
			<div class="cash">
			<div class="number c-primary">{{$total_shot}}</div>
			<div class="txt">Total Products</div>
		</div>
		<div class="cash">
		<div class="number c-green">{{$total_approved}}</div>
		<div class="txt">Approved</div>
	</div>
	<div class="cash">
	<div class="number c-red">{{$total_rejected}}</div>
	<div class="txt">Rejected</div>
</div>
<div class="cash">
<div class="number ">{{$total_dropped}}</div>
<div class="txt">Dropped</div>
</div>
</div>
</div> --}}
</div>
{{--
<div class="row m-b-20 flex-container">
<div class="flex-item text-center">
<img src="/assets/images/channels/ounass.svg" height="24px" alt="">
<div class="cash">
<h1 class="number c-primary">{{@$ounass_count}}</h1>
<div class="txt">Total Products</div>
</div>
</div>
<div class="flex-item  text-center">
<img src="/assets/images/channels/nass.svg" height="24px" alt="">
<div class="cash">
<h1 class="number c-primary">{{@$nass_count}}</h1>
<div class="txt">Total Products</div>
</div>
</div>
<div class="flex-item  text-center">
<img src="/assets/images/channels/mamasandpapas.svg" height="24px" alt="">
<div class="cash">
<h1 class="number c-primary">{{@$map_count}}</h1>
<div class="txt">Total Products</div>
</div>
</div>
<div class="flex-item  text-center">
<img src="/assets/images/channels/gap.svg" height="24px" alt="">
<div class="cash">
<h1 class="number c-primary">{{@$nisnass_count}}</h1>
<div class="txt">Total Products</div>
</div>
</div>
<div class="flex-item  text-center">
<img src="/assets/images/channels/nisnass.webp" height="24px" alt="">
<div class="cash">
<h1 class="number c-primary">{{@$nisnass_count}}</h1>
<div class="txt">Total Products</div>
</div>
</div>
</div> --}}
<div class="col-sm-12">
	<div class="text-center">
		<svg width="20em" height="16em" viewBox="0 0 300 240"><g fill="none"><ellipse fill="#000" opacity=".05" cx="150" cy="197" rx="135" ry="30"></ellipse><g transform="translate(191 129)"><rect fill="#9484E0" x=".47" y="12.26" width="76.05" height="51.72" rx="3.65"></rect><path d="M3.32 31.12H37.6v27.61a2.6 2.6 0 0 1-2.6 2.6H5.92a2.6 2.6 0 0 1-2.6-2.6V31.12zm36.45 0H74v27.61a2.6 2.6 0 0 1-2.6 2.6H42.37a2.6 2.6 0 0 1-2.6-2.6V31.12zM5.92 14.78h65.53a2.6 2.6 0 0 1 2.6 2.6v11.31H3.32V17.38a2.6 2.6 0 0 1 2.6-2.6z" fill="#A08EED"></path><path d="M19.17 16.95h8.64v6a3.25 3.25 0 0 1-3.25 3.25h-2.13a3.25 3.25 0 0 1-3.25-3.25v-6h-.01zm31.68 0h8.64v6a3.25 3.25 0 0 1-3.25 3.25h-2.13a3.25 3.25 0 0 1-3.25-3.25v-6h-.01z" fill="#44BCE6"></path><path d="M25.915 12.6A7.385 7.385 0 0 1 33.3 5.215h12a7.385 7.385 0 0 1 7.385 7.385v4.47h4.75V12.6C57.435 5.898 52.002.465 45.3.465h-12c-6.702 0-12.135 5.433-12.135 12.135v4.47h4.75V12.6z" fill="#44BCE6"></path></g><g transform="translate(33 117)"><path d="M61.6 7.37a.57.57 0 0 1-.57-.57v9.31a1.38 1.38 0 0 1-1.38 1.38H44.11a1.38 1.38 0 0 1-1.38-1.38V7.68a.31.31 0 0 1 .306-.31H61.6zm-25.29.31v8.43a7.8 7.8 0 0 0 7.8 7.8h15.54a7.8 7.8 0 0 0 7.8-7.8V6.8A5.85 5.85 0 0 0 61.6.95H43c-3.723.042-6.69 3.043-6.69 6.73z" fill="#00C79E"></path><rect fill="#00C79E" x=".29" y="10.71" width="102.82" height="69.92" rx="4.94"></rect><path d="M4.63 10.71h94.15a4.33 4.33 0 0 1 4.33 4.33v43.44a6.16 6.16 0 0 1-6.16 6.16H6.45a6.16 6.16 0 0 1-6.16-6.16V15a4.33 4.33 0 0 1 4.34-4.29z" fill="#00D4A8"></path><path d="M19.1 9.61H26v60a3.08 3.08 0 0 1-3 3.03h-.78a3.08 3.08 0 0 1-3.08-3.08v-60l-.04.05z" fill="#FFCE3E"></path><path d="M19.408 62.13h6.314c.138 0 .248.11.248.248v4.534c0 .14-.11.248-.248.248h-6.314a.246.246 0 0 1-.248-.248v-4.534c0-.14.11-.248.248-.248zm-1.988 4.782c0 1.1.89 1.988 1.988 1.988h6.314c1.1 0 1.988-.89 1.988-1.988v-4.534c0-1.1-.89-1.988-1.988-1.988h-6.314c-1.1 0-1.988.89-1.988 1.988v4.534z" fill="#F1F1F3"></path><path d="M77.12 9.61h6.94v60a3.08 3.08 0 0 1-3.08 3.08h-.78a3.08 3.08 0 0 1-3.08-3.08v-60z" fill="#FFCE3E"></path><path d="M77.438 62.13h6.314c.138 0 .248.11.248.248v4.534c0 .14-.11.248-.248.248h-6.314a.246.246 0 0 1-.248-.248v-4.534c0-.14.11-.248.248-.248zm-1.988 4.782c0 1.1.89 1.988 1.988 1.988h6.314c1.1 0 1.988-.89 1.988-1.988v-4.534c0-1.1-.89-1.988-1.988-1.988h-6.314c-1.1 0-1.988.89-1.988 1.988v4.534z" fill="#F1F1F3"></path></g><g transform="translate(79 95)"><path fill="#428AE0" d="M13.1 14.61h19.87v2.97H13.1zm99.81 0h19.87v2.97h-19.87zM62.418 9.87h21.894a1.93 1.93 0 0 1 1.928 1.93v10.35c0 1.07-.86 1.93-1.928 1.93H62.418a1.93 1.93 0 0 1-1.928-1.93V11.8c0-1.07.86-1.93 1.928-1.93zM51.47 22.15c0 6.047 4.9 10.95 10.948 10.95h21.894c6.052 0 10.948-4.9 10.948-10.95V11.8c0-6.047-4.9-10.95-10.948-10.95H62.418C56.366.85 51.47 5.75 51.47 11.8v10.35z"></path><rect fill="#428AE0" x=".89" y="17.35" width="144.4" height="98.19" rx="6.93"></rect><path d="M6.3 17.35h133.57a5.42 5.42 0 0 1 5.42 5.42v36c0 10.24-8.3 18.54-18.54 18.54H19.43C9.19 77.31.89 69.01.89 58.77v-36a5.42 5.42 0 0 1 5.41-5.42z" fill="#4996F2"></path><rect fill="#00D4A8" x="64.92" y="68.89" width="16.9" height="16.9" rx="2.47"></rect></g></g></svg>
		<p>You have no metrics defined for your dashboard</p>
	</div>
</div>

<div id="dashboard_metrics"></div>


</div>
@stop

@section('page-scripts')

	<!-- Page scripts -->
	<!-- BEGIN PAGE SCRIPT -->
	<script src="{{ URL::asset('js/charts/chartjs/chart.js') }}"></script>



	<script src="https://www.google.com/jsapi"></script>
	<script src="https://www.google.com/uds/?file=visualization&amp;v=1&amp;packages=corechart" type="text/javascript"></script>
	<script src="https://www.google.com/uds/api/visualization/1.0/84dc8f392c72d48b78b72f8a2e79c1a1/format+en,default+en,ui+en,corechart+en.I.js" type="text/javascript"></script>
	<script src="{{ URL::asset('js/studiobridge/DashboardFunctions.js') }}"></script>

	<!-- /page scripts -->
	<script>


	$(function () {
		'use strict';
		// Line chart
		// ------------------------------
		// Initialize chart
		google.load("visualization", "1", {packages:["corechart"]});
		google.load("visualization", "1", {packages:["corechart"]});

		var timeframe = 'This Week';
		// renderDashboard(timeframe);

	});


	$(function () {
		// Resize chart on sidebar width change and window resize
		$(window).on('resize', resize);
		$(".sidebar-control").on('click', resize);
		equalheight('.equalheight');
		// Resize function
		function resize() {
			// drawLineChart();
			// drawPie();
		}
	});


	</script>

@stop
