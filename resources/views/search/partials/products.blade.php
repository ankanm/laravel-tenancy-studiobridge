@if(isset($product_result))
  @foreach($product_result as $product)
    <a class="dummy-media-object" href="/product/0/{{$product->id}}">
      <img src="{{ URL::asset('assets/global/images/gallery/1.jpg')}}" alt="1"/>
      <h3>{{$product->identifier}}</h3>
    </a>
  @endforeach
@endif
