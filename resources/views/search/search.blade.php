<div id="morphsearch" class="morphsearch">
  <form class="morphsearch-form">
    <input class="morphsearch-input" type="search" placeholder="Search..."/>
    <span class="morphsearch-submit" type="submit">Search</span>
  </form>
  <div class="morphsearch-content withScroll">
    {{-- <div class="dummy-column user-column">
      <h2>Users</h2>
      <a class="dummy-media-object" href="#">
        <img src="{{ URL::asset('assets/global/images/avatars/avatar1_big.png')}}" alt="Avatar 1"/>
        <h3>Ashar Babar</h3>
      </a>
      <a class="dummy-media-object" href="#">
        <img src="{{ URL::asset('assets/global/images/avatars/avatar2_big.png')}}" alt="Avatar 2"/>
        <h3>Yoshita Bhatia</h3>
      </a>
    </div> --}}
    <div class="dummy-column">
      <h2>Products</h2>
      <div class="product-search">
        @include('search.partials.products')
      </div>
    </div>
    {{-- <div class="dummy-column">
      <h2>Messages</h2>
      <a class="dummy-media-object" href="#">
        <img src="{{ URL::asset('assets/global/images/gallery/7.jpg')}}" alt="7"/>
        <h3>Check 972399797978234. not right...</h3>
      </a>
      <a class="dummy-media-object" href="#">
        <img src="{{ URL::asset('assets/global/images/gallery/8.jpg')}}" alt="8"/>
        <h3>What happened to shoot?</h3>
      </a>
      <a class="dummy-media-object" href="#">
        <img src="{{ URL::asset('assets/global/images/gallery/9.jpg')}}" alt="9"/>
        <h3>Lunch?</h3>
      </a>

    </div> --}}
  </div>
  <!-- /morphsearch-content -->
  <span class="morphsearch-close"></span>
</div>
