<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Client config
    |--------------------------------------------------------------------------
    |
    | Here you can specify studiobridge's client specific settings
    |
    */


    'client' => [
        'name' => env('CLIENT_NAME', 'SB'),
    ],

    'bucket' => [
        'bucket_name'  => env('BUCKET_NAME', 'studiobridgebucket'),
        'folder_name' => env('FOLDER_NAME', 'zappos'),
        'with_folder' => 0,
    ],

    'rules' => [
        'naming_convention' => "Default",
        'min_images' => "1",
        'max_images' => "7",
        'export_as' => env('EXPORT_AS', 'tif'),
    ],

    'image_naming' => [
        'seperator' => "_",
        '1' => "in",
        '2' => "fr",
        '3' => "bk",
        '4' => "cu",
        '5' => "e1",
        '6' => "e2",
        '7' => "e3",
        '8' => "e4"
    ],

    'naming_rules' => [

      'rule_1' => [
          'applies_when' => ['ounass', 'fashion'],
          'seperator' => "_",
          '1' => "in",
          '2' => "fr",
          '3' => "bk",
          '4' => "cu",
          '5' => "e1",
          '6' => "e2",
          '7' => "e3",
          '8' => "e4"
      ],
      'rule_2' => [
          'applies_when' => ['nass'],
          'seperator' => "_",
          '1' => "in",
          '2' => "fr",
          '3' => "bk",
          '4' => "cu",
          '5' => "e1",
          '6' => "e2",
          '7' => "e3",
          '8' => "e4"
      ],
      'rule_3' => [
          'applies_when' => ['mamasandpapas'],
          'seperator' => "_",
          '1' => "in",
          '2' => "fr",
          '3' => "bk",
          '4' => "cu",
          '5' => "e1",
          '6' => "e2",
          '7' => "e3",
          '8' => "e4"
      ],
      'rule_4' => [
          'applies_when' => ['ounass'],
          'seperator' => "_",
          '1' => "in",
          '2' => "fr",
          '3' => "bk",
          '4' => "cu",
          '5' => "e1",
          '6' => "e2",
          '7' => "e3",
          '8' => "e4"
      ],
    ],

    'qc_can_delete_images' => 'yes',

    'qc_can_rearrange_images' => 'yes',

    'dropdowns' => [
        'copywriting' => [
            'material' => [
                'Cotton' => 'Cotton',
                'Polyester' => 'Polyester',
                'Silk' => 'Silk',
            ],
            'care_instructions' => [
              'Machine Wash' => 'Machine Wash',
              'Dry Wash' => 'Dry Wash',
              'Dry Clean' => 'Dry Clean',
          ],
            'frame_type' => [
                'Aviator' => 'Aviator',
                'Cat Eye' => 'Cat Eye',
                'Oversized' => 'Oversized',
                'Round' => 'Round',
                'Square' => 'Square'
            ],
            'lens_type' => [
                'Mirrored' => 'Mirrored',
                'Tinted' => 'Tinted'
            ],
            '100ml_restriction' => [
                'Yes' => 'Yes',
                'No' => 'No'
            ],
            'hair_concern' => [
                'Colour Treated' => 'Colour Treated',
                'Scalp Care' => 'Scalp Care',
                'Dry Hair' => 'Dry Hair',
                'Fine Hair' => 'Fine Hair'
            ],
            'heel_height' => [
                'Flat' => 'Flat',
                'Mid Heel' => 'Mid Heel',
                'High Heel' => 'High Heel'
            ],
            'heel_style' => [
                'Stiletto' => 'Stiletto',
                'Wedge' => 'Wedge',
                'Chunky Heel' => 'Chunky Heel'
            ],
            'skin_concern' => [
                'Anti-Ageing' => 'Anti-Ageing',
                'Sensitive Skin' => 'Sensitive Skin',
                'Dry Skin' => 'Dry Skin'
            ],
        ]
    ]

];
