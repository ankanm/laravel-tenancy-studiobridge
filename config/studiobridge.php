<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Display config
    |--------------------------------------------------------------------------
    |
    | Here you can specify studiobridge's display settings
    |
    */

    'display' => [
        'logo'          => "/assets/images/logo-light.png",
        'background'    => "assets/images/Background-SB.jpg",
        'copyright'    => "© 2018 StudioBridge"
    ],

    'settings' => [
      'cache_navigation' => env('CACHE_NAVIGATION', 'YES'),
      'cache_query_duration' => '30'
    ],

    /* Admin Email */

    'email' => [
        'admin_email'  => "superadmin@teamstudiobridge.com"
    ],

    'system_emails' => [
    'organization_created'  => [
        "from" => "superadmin@teamstudiobridge.com",
        "subject_admin" => "Account Created for Organization Admin",
        "subject_user" => "Account Created for Organization User",
        "subject_vendor_admin" => "Account Created for Vendor Admin",
        "subject_vendor_user" => "Account Created for Vendor User",
        "subject_agency_admin" => "Account Created for Agency Admin",
        "subject_agency_user" => "Account Created for Agency User",
        "reply_to" => "No reply",
        "headers" => "",
        "token_valid_upto" => "+1 day"

     ]
  ],



];
