//All functions related to copy memory

//Attach the add button to all elements that are data_memory_addable



//To render the copy memory
$(document).on("click", ".copy-memory",function(){

  var memory_type = $(this).attr('data-memory-type');
  var csrfToken = $('[name=_token]').val();

  $.ajax({
    url: '/copywriting/memory',
    method: 'POST',
    headers: {
      'X-CSRF-Token': csrfToken
    },
    data: {
      "memory_type": memory_type,
    },
    success: function (node) {
      $('.copy-memory-container').html(node.html)
      $('#add-from-memory').modal('show');
    },
    error: function(){
      alert('Failed! **');
    }

  });
});

//Add selected copy to relevant container
$(document).on("click", ".addcopytocontainer", function(){
  var csrfToken = $('[name=_token]').val();
  var checked_items = [];
  var target_container = $(this).attr('data-target-container');

  $("input:checkbox[name=check-memory-item]:checked").each(function(){
      checked_items.push($(this).val());
  });

  if(checked_items.length == 0){
    swal({
      title: "No Items Selected",
      text: "You have not selected any items, please select atleast one item to add.",
      type: "warning"
    });
  } else{

    checked_items.forEach(function(entry) {
      var text_to_insert = '<p>'+entry+'</p>'
      tinyMCE.get(String(target_container)).execCommand( 'mceInsertContent', false, text_to_insert );
    });

        swal({
          title: "Imported from Memory",
          text: "You have successfully added items from copy memory",
          timer: 500,
          type: "success",
          showConfirmButton: false
        });

        $('#add-from-memory').modal('hide');

  }
});

//Filter funtions for the memory items
$(document).on('keyup', '.filter-memory-items', function(){
// $('.filter-memory-items').keyup(function(){
  var valThis = $(this).val().toLowerCase();
  filterProducts(valThis)
});

function filterProducts(val) {
  var filtered = 0;
  if(val == ""){
    $('.memory-master-list > li').show();
    filtered_products = total_products
  } else {
    $('.memory-master-list > li').each(function(){
      var text = $(this).text().toLowerCase();
      if(text.indexOf(val) >= 0) {
        $(this).show()
        filtered++;
      } else {
        $(this).hide();
      }
    });
  };
}
