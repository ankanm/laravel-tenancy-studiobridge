var qcStartButton = ['.rc-start'];
var selectedProductStatus = $('.qc-product-status').html();
//----------------------------------
// QC Operations start for the image
//----------------------------------

$(".rc-start").on("click",function(){
  swal({
    title: "Start QC?",
    text: "Are you sure you want to QC this image?.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: 'btn-success',
    confirmButtonText: 'Start',
    cancelButtonText: "Cancel",
    closeOnConfirm: true,
    closeOnCancel: true
  },
  function(isConfirm){
    if (isConfirm) {
      //hide startQC button and unhide all controls
      $.each( qcStartButton, function( index, value ){
        $(value).addClass('hidden');
      });

      //enable all image controls

      $( ".qcControls" ).each( function( index, element ){
        $(element).removeClass('hidden');
      });
            
    }
  });

});

//----------------------------------
// QC Operations submit for the product
//----------------------------------

$(".qc-submit").on("click",function(){

  var setMesage;
  alert(selectedProductStatus);
  if(selectedProductStatus == 'Pending'){
    swal({
      title: "Image Still Pending",
      text: "This image is still pending for review. Please read the QC guidelines for assistance.",
      type: "error",
      showCancelButton: false,
      confirmButtonClass: 'btn-success',
      confirmButtonText: 'Ok',
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    })

  } else{

    if(selectedProductStatus == 'Approved'){
      setMessage = 'This product has been approved by QC';
    }else{
      setMessage = 'This product has been rejected by QC';
    }

    //tinyMCE.get('0').setContent(setMessage);

    $('#qc-submit-modal').modal('show');
  }


});

//----------------------------------
// This function will mark image as selected
//----------------------------------

$(".qc-approve-all").on("click",function(){
  $( ".bulkviewfilesqc" ).each( function( index, element ){
    alert($(this).attr('data-status'));
    $(this).attr('data-status', 'approved');
    $(element).addClass('border-selected-approved').removeClass('border-selected-rejected');
  });

  //UpdateStatus();
});

//----------------------------------
// Update the status of QC for the product
//----------------------------------

function UpdateStatus(approvalType) {

  totalCount = 0;
  selectedProductStatus = 'Pending';

  //Get total images in the product
  totalCount = $( ".bulkviewfilesqc" ).length;

  //Get individual image status
  imagesRejected = $(".bulkviewfilesqc[data-status='rejected']").length;
  imagesApproved = $(".bulkviewfilesqc[data-status='approved']").length;

  if(totalCount !=0 && imagesApproved == totalCount){
    selectedProductStatus = 'Approved';
  }

  if(totalCount ==0 || imagesRejected > 0){
    selectedProductStatus = 'Rejected';
  }

  switch(selectedProductStatus){
    case 'Approved':
    $('.qc-product-status').removeClass('text-amber-darker').removeClass('text-danger').addClass('text-success');
    break;
    case 'Rejected':
    $('.qc-product-status').removeClass('text-amber-darker').removeClass('text-success').addClass('text-danger');
    break;
    case 'Pending':
    $('.qc-product-status').removeClass('text-success').removeClass('text-danger').addClass('text-amber-darker');
    break;
  }

  $('.qc-product-status').html(selectedProductStatus);
  console.log(imagesRejected + " " + imagesApproved);
  console.log(selectedProductStatus);
}