/**
 * Created by krishna on 28/12/16.
 */

(function ($) {

    $(document).on("click", ".liveshoot-img-pin", function () {

        var fid = $(this).attr('data-id');
        var csrfToken = $('[name=_token]').val();
        var psession_id = $('#liveshoot-psession-id').val();
        var psession_uid = $('#liveshoot-psession-uid').val();
        var product_id = $('#liveshoot-pid').val();

        var data = {
            "product_id": product_id,
            "psession_id": psession_id,
            "psession_uid": psession_uid,
            "fid": fid
        };

        if (fid) {
            $.ajax({
                url: '/psession/image/pin',
                method: 'post',
                headers: {
                    'Content-Type': 'application/hal+json',
                    'X-CSRF-Token': csrfToken
                },
                data: JSON.stringify(data),
                success: function (file) {
                    console.log(file);

                    if (file.err) {
                        swal({
                            title: "Cannot pin this image",
                            text: "This image has already been pinned, you cannot pin this image again",
                            type: "error",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    } else {
                        swal({
                            title: "Success",
                            text: "This image has been pinned to current session. You can view it by clicking on Add Pinned button",
                            timer: 1000,
                            type: "success",
                            showConfirmButton: false
                        });

                        addImageToPinBlock(file);

                    }
                },
                error: function () {
                    swal({
                        title: "Pin Image Error",
                        text: "There was an error, please try again.",
                        type: "error",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }

            });
        }

    });

    $(document).on("click", ".liveshoot-pin-to-session", function () {
        var fid = $(this).attr('data-id');
        var csrfToken = $('[name=_token]').val();
        var psession_id = $('#liveshoot-psession-id').val();
        var psession_uid = $('#liveshoot-psession-uid').val();
        var product_id = $('#liveshoot-pid').val();

        var data = {
            "product_id": product_id,
            "psession_id": psession_id,
            "psession_uid": psession_uid,
            "fid": fid
        };


        if (fid) {
            $.ajax({
                url: '/psession/image/addtosession',
                method: 'post',
                headers: {
                    'Content-Type': 'application/hal+json',
                    'X-CSRF-Token': csrfToken
                },
                data: JSON.stringify(data),
                success: function (file) {
                    console.log(file);
                    if(!file.err){
                        appendImageLiveShoot(file);
                        swal({
                            title: "Pin Image Added",
                            text: "Pinned image has been added to current product.",
                            timer: 2000,
                            type: "success",
                            showConfirmButton: false
                        });
                    }else{
                        swal({
                            title: "Pin Image Error",
                            text: "You cannot add this image to the product because it already exists.",
                            type: "error",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                },
                error: function () {
                    swal({
                        title: "Error",
                        text: "There was an error, please try again.",
                        type: "error",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }

            });
        }



    });
    //liveshoot-unpin

    $(document).on("click", ".liveshoot-unpin", function () {

        var fid = $(this).attr('data-id');
        var csrfToken = $('[name=_token]').val();
        var psession_id = $('#liveshoot-psession-id').val();
        var psession_uid = $('#liveshoot-psession-uid').val();
        var product_id = $('#liveshoot-pid').val();

        var data = {
            "product_id": product_id,
            "psession_id": psession_id,
            "psession_uid": psession_uid,
            "fid": fid
        };


        if (fid) {
            $.ajax({
                url: '/psession/image/unpin',
                method: 'post',
                headers: {
                    'Content-Type': 'application/hal+json',
                    'X-CSRF-Token': csrfToken
                },
                data: JSON.stringify(data),
                success: function (file) {
                    console.log(file);
                    removeImageFromPinBlock(fid)
                    swal({
                        title: "Un pinned!",
                        text: "Image Unpinned",
                        timer: 2000,
                        type: "success",
                        showConfirmButton: false
                    });
                },
                error: function () {
                    swal({
                        title: "Failed!",
                        text: "There was an error, please try again.",
                        type: "error",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }

            });
        }


    });

    function addImageToPinBlock(file){
        var image_id = file.image_id;
        //var src = 'https://cdn1.iconfinder.com/data/icons/material-design-icons-light/24/pin-off-128.png';
        var src = document.getElementById('img-' + image_id).getAttribute("src"); //$('img-' + image_id).attr('scr');

        var markup = '<li id="liveshoot-pinblock-li-'+image_id+'">';
        markup += '<div><span class="chat-avatar"><img data-src='+src+' alt="Avatar" style="width:100px;height:100px;"></span><span class="chat-u-info"></span></div>';
        markup += '<span class="chat-u-status"><button data-id='+image_id+' class="ripple btn-primary btn-sm liveshoot-pin-to-session">Add</button><button data-id='+image_id+' class="ripple btn-danger btn-sm liveshoot-unpin">Unpin</button></span>';
        markup += '</li>';

        $( ".liveshoot-pinblock-ul" ).append( markup );

    }

    function removeImageFromPinBlock(fid){
        document.getElementById('liveshoot-pinblock-li-'+fid).remove();
    }



})(jQuery);
