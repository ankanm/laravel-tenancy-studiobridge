/**
* Created by krishna on 28/12/16.
*/

(function ($) {

  $("#imagecontainer").sortable({
    tolerance: 'pointer',
    start: function(event, ui){
      ui.placeholder.html("<div class='bulkviewfiles file gray-bkground' style='width: 200px; height: 200px; background: #D2D2D2; border: 4px dotted #fff;'></div>");
    },
    update: function (event, ui){
      // $("#sequence-check").removeClass('fa-square-check-o');
      // $("#sequence-check").addClass('fa-square-o');
      // $("#sequence-check").removeClass('text-success');
    },
    stop: function(event, ui){
      ui.placeholder.html("");
      var imgs = update_w();
      if(imgs.length > 1){
        reseq();
      }
    }
  });


  function reseq(){
    //console.log($('[name=_token]').val());
    var csrfToken = $('[name=_token]').val();
    var psession_id = $('#liveshoot-psession-id').val();
    var psession_uid = $('#liveshoot-psession-uid').val();
    var product_id = $('#liveshoot-pid').val();

    var img = update_w();
    var data = {"product_id":product_id, "psession_id": psession_id, "psession_uid": psession_uid, "img": img};

    $.ajax({
      url: '/images/resequence',
      method: 'POST',
      headers: {
        //'Content-Type': 'application/hal+json',
        'X-CSRF-Token': csrfToken
      },
      //data: JSON.stringify(data),
      data: data,
      success: function (node) {
        // Update image wrappers.
        container = document.getElementById('imagecontainer');
        var inputs2 = container.getElementsByClassName("form-checkbox");
        for (index2 = 0; index2 < inputs2.length; ++index2) {
          // deal with inputs[index] element.
          document.getElementById('seq-'+inputs2[index2].value).innerHTML = "" + tempRename(index2 + 1);
        }
      },
      error: function(){
        swal({
          title: "Resequence Error",
          text: "There was an issue trying to requeuence the images. Please refresh the page and try again",
          timer: 1000,
          type: "error",
          showConfirmButton: false
        });
      }

    });
  }

  $( "#imagecontainer" ).disableSelection();


  function update_w() {

    var container, inputs, index;
    var imgs = [];
    var dup_holder = [];

    // Get the container element
    container = document.getElementById('imagecontainer');

    // Find its child `input` elements
    inputs = container.getElementsByTagName('input');
    for (index = 0; index < inputs.length; ++index) {
      // deal with inputs[index] element.
      //console.log(inputs[index].value);
      if(inputs[index].type == 'hidden'){
        if(dup_holder.indexOf(inputs[index].value) == '-1'){
          imgs.push({"id": inputs[index].value});
        }
        dup_holder.push(inputs[index].value);
      }
    }

    var Node1 = {
      type: {
        target_id: 'products'
      },
      field_images: imgs
    };

    return imgs;

  }

})(jQuery);
