var qcStartButton = ['.qc-start'];
var selectedProduct = '';
var selectedProductSession = '';
var totalCount = 0;
var imagesRejected = 0;
var imagesApproved = 0;
var selectedProductStatus = '';
var qSession = '';
var isQCactive = 0;





//----------------------------------
// QC product click capture
//----------------------------------

$("body").on("click",'.qc-product-a',function(){
  //toggleQCContainer();
  var pid = $(this).attr('data-pid');
  var sid = $(this).attr('data-sid');
  var title = $(this).attr('data-title');
  var extra_shot = $('.requires-extra-shot-'+pid).val();

  changeProduct(sid, pid,title, extra_shot);


});



//----------------------------------
// Update the status of QC for the product
//----------------------------------

function UpdateStatus(approvalType) {



  totalCount = 0;
  selectedProductStatus = 'Pending';

  //Get total images in the product
  totalCount = $( ".bulkviewfilesqc" ).length;

  //Get individual image status
  imagesRejected = $(".bulkviewfilesqc[data-status='rejected']").length;
  imagesApproved = $(".bulkviewfilesqc[data-status='approved']").length;

  if(totalCount !=0 && imagesApproved == totalCount){
    selectedProductStatus = 'Approved';
  }

  if(totalCount ==0 || imagesRejected > 0){
    selectedProductStatus = 'Rejected';
  }

  switch(selectedProductStatus){
    case 'Approved':
    $('.qc-product-status').removeClass('text-amber-darker').removeClass('text-danger').addClass('text-success');
    break;
    case 'Rejected':
    $('.qc-product-status').removeClass('text-amber-darker').removeClass('text-success').addClass('text-danger');
    break;
    case 'Pending':
    $('.qc-product-status').removeClass('text-success').removeClass('text-danger').addClass('text-amber-darker');
    break;
  }

  $('.qc-product-status').html(selectedProductStatus);
  console.log(imagesRejected + " " + imagesApproved);
  console.log(selectedProductStatus);
}

//UpdateStatus();
//----------------------------------
// QC Operations disabled when start QC not clicked
//----------------------------------

function WorkSpace(show) {
  if(show == 0){
    $('#qc-work-area').addClass('hidden');
  } else {
    $('#qc-work-area').removeClass('hidden');
  }

}

//Load First Functions
//TODO: if a product is locked and screen is refreshed, load that product
//UpdateStatus();
WorkSpace(0);



//----------------------------------
// QC Operations start for the product
//----------------------------------

$(".qc-start").on("click",function(){
  swal({
    title: "Start QC?",
    text: "Are you sure you want to QC this product? This action locks the product for your user.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: 'btn-success',
    confirmButtonText: 'Start',
    cancelButtonText: "Cancel",
    closeOnConfirm: true,
    closeOnCancel: true
  },
  function(isConfirm){
    if (isConfirm) {
      startQC();
      var prod_id = $("#liveshoot-pid").val();

      $.ajax({
          url: '/qsession/prod_time',
          type: 'get',
          data: {'prod_id': prod_id},
          success: function( data, textStatus, jQxhr ){
              console.log( data );
          },
          error: function( jqXhr, textStatus, errorThrown ){
              console.log( errorThrown );
          }
      });
    }
  });

});

//----------------------------------
// QC Operations submit for the product
//----------------------------------

$(".qc-submit").on("click",function(){

  var setMesage;

  if(selectedProductStatus == 'Pending'){
    swal({
      title: "Product Still Pending",
      text: "This product is still pending for review. Please read the QC guidelines for assistance.",
      type: "error",
      showCancelButton: false,
      confirmButtonClass: 'btn-success',
      confirmButtonText: 'Ok',
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    })

  } else{

    if(selectedProductStatus == 'Approved'){
      setMessage = 'This product has been approved by QC';
    }else{
      setMessage = 'This product has been rejected by QC';
      if(totalCount ==0 ){ setMessage += ' due to no images'}
    }

    tinyMCE.get('0').setContent(setMessage);

    $('#qc-submit-modal').modal('show');
  }


});


//----------------------------------
// Start the QC operations process
//----------------------------------

function startQC(){

  isQCactive = 1;

  //hide startQC button and unhide all controls
  $.each( qcStartButton, function( index, value ){
    $(value).addClass('hidden');
  });

  //enable all image controls

  $( ".qcControls" ).each( function( index, element ){
    $(element).removeClass('hidden');
  });

  $('.lock-product-'+ selectedProduct).removeClass('hidden');
  $('#qc-product-'+ selectedProduct).addClass('new');


  var now = new Date();
  var next=AddMinutesToDate(now,5);
  $('.product-lock-timer').countdown({until: next });
}

//----------------------------------
// Check if current product has been submitted or not
//----------------------------------

function changeProduct(sid, pid ,title, extra_shot){


  if(selectedProduct == pid && isQCactive === 1){

    //Set the hidden item values
    var psession_id = $('#liveshoot-psession-id').val(sid);
    var product_id = $('#liveshoot-pid').val(pid);



    startQC();
    return;
  }


  if(selectedProduct != '' && selectedProduct != pid && isQCactive === 1){
    //There is a product currently being QC
    swal({
      title: "Discard Current Product",
      text: "You have no submitted your QC feedback for the current product, do you want to discard these changes?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Discard',
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm){
      if (isConfirm) {
        $('.lock-product-'+ selectedProduct).addClass('hidden');
        $('#qc-product-'+ selectedProduct).removeClass('new');

        $('.product-lock-timer').countdown('destroy').html('-- : -- : --');

        isQCactive = 0;
        selectedProduct = '';
        selectedProductSession = '';
        WorkSpace(0);
        changeProduct(sid, pid, title, extra_shot);


      }
    });
  } else {
    var psession_id = $('#liveshoot-psession-id').val(sid);
    var product_id = $('#liveshoot-pid').val(pid);
    WorkSpace(0);
    totalImages = 0;
    imagesApproved = 0;
    imagesRejected = 0;
    selectedProduct = pid;
    selectedProductSession = sid;



    resetQC();
    GetNewImages(sid, pid, title, extra_shot);
    GetProductInfo('qc');


  }



}

function AddMinutesToDate(date, minutes) {
     return new Date(date.getTime() + minutes*60000);
}


//----------------------------------
// Reset QC operations process
//----------------------------------


function resetQC(){

  $( ".qc-pi" ).each( function( index, element ){
    $(element).html('--');
  });

  //hide startQC button and unhide all controls
  $.each( qcStartButton, function( index, value ){
    $(value).removeClass('hidden');
  });

  //enable all image controls

  $( ".qcControls" ).each( function( index, element ){
    $(element).addClass('hidden');
  });

}



//----------------------------------
// To get images for the product
//----------------------------------


function GetNewImages(psession_id, product_id, title, extra_shot){

  //var d = $.Deferred();
  var r = $.get("/images?psid="+psession_id+"&pid="+product_id, function(data, status){
    //qc-selected-product-title
    $('.qc-selected-product-title').html(title);
    var a = data;
    document.getElementById('imagecontainer').innerHTML = '';
    if(a){
      if(a.length == 0 ){
        $('#imagecontainer').html('<div class="col-xs-12 alert bg-danger-darker">There are no images in this product.</div>')

      }

      for(var i in a){
        if ($('#qc-warpper-img-'+ i).length == 0) {
          appendImageContainer(a[i],i,'qc');
          totalImages++;

        }
      }
    }
  })
  .then(function() {
    $('.qc-total-images').html(totalImages);
    if(extra_shot == 1){
      $('.requires-extra-shot-notification').removeClass('hidden');
    }else{
      $('.requires-extra-shot-notification').addClass('hidden');
    }
    WorkSpace(1);
    UpdateStatus();
  })

}


//----------------------------------
// To append QC image to container
//----------------------------------

function appendImageQC(img,fid) {
  if ($('#qc-warpper-img-'+ img.id).length > 0) {
    return;
  }

  var container, inputs, index;

  // Get the container element
  container = document.getElementById('imagecontainer');

  // Find its child `input` elements
  //inputs = container.getElementsByTagName('input');
  inputs = container.getElementsByClassName("bulkviewfilesqc");
  var seq = inputs.length + 1;


  var ul = document.getElementById('imagecontainer');
  var li = document.createElement("div");

  var classes= "bulkviewfiles bulkviewfilesqc ui-sortable-handle swing paper paper-curve-vert";
  li.setAttribute("class", classes); // added line
  li.setAttribute('id','qc-warpper-img-' + img.id);
  li.setAttribute('data-id', img.id);
  li.setAttribute('data-status','pending');


  var block = '';

  block +=  '<div class="scancontainer"><div class="hovereffect">';
  block +=  '<div class="shadow animated-background" id="image-parent-'+img.id+'"> \
  <img class="scanpicture" data-imageid="'+ img.id +'" id="img-'+ img.id +'" onerror="imgError(this);"> \
  </div>';
  block +=  '</div>';

  block +=  '<div class="file-name">';
  block +=  '<div id="tag-seq-img-'+img.id+'" type="hidden"></div>'
  block += '<div class="row">';

  block += '<div class="col col-sm-12"> \
  <span id= "'+img.id+'"><a class=" col-sm-3" > \
  <span class="sequence-number">#<span id="seq-' + img.id +'">' + seq +'</span></span></a> \
  <a class="col-sm-3 text-center" href= "/image/view/'+img.id+'" target="_blank" ><i class="icon-magnifier image-ops"></i></a> \
  <a class="col-sm-3 text-center qc-approve-single" data-id= "'+img.id+'"><i class="icon-check qcControls hidden image-ops"></i></a> \
  <a class="col-sm-3 text-center qc-reject-single" data-id="'+ img.id +'"><i class="icon-cross qcControls hidden image-ops"></i></a></span></div>';

  block += '</div>';
  block += '</div>';
  block += '</div>';
  block += '<div class="studio-img-weight"><input type="hidden" value="'+img.id+'"></div>';
  block += '</div>';

  li.innerHTML = block;
  ul.appendChild(li);

  LoadImage("img-"+ img.id, "/" + img.thumbpath, "image-parent-" + img.id)

}



//----------------------------------
// This function will mark all images as selected for rejection
//----------------------------------

$(".qc-approve-all").on("click",function(){
  $( ".bulkviewfilesqc" ).each( function( index, element ){
    $(this).attr('data-status', 'approved');
    $(element).addClass('border-selected-approved').removeClass('border-selected-rejected');
  });

  UpdateStatus();
  CollectImageStatus();
});

//----------------------------------
// This function will mark all images as selected for rejection
//----------------------------------

$(".qc-reject-all").on("click",function(){
  $( ".bulkviewfilesqc" ).each( function( index, element ){
    $(this).attr('data-status', 'rejected');
    $(element).addClass('border-selected-rejected').removeClass('border-selected-approved');
  });
  UpdateStatus();
});

//----------------------------------
// This function will mark specific images as selected for approval
// Dynamic Elements
//----------------------------------
$('body').on("click", '.qc-approve-single', function(){
  var image_id = $(this).attr('data-id');
  UpdateImage(image_id, 'approved')
});

//----------------------------------
// This function will mark specific images as selected for rejection
// Dynamic Elements
//----------------------------------

$('body').on("click", '.qc-reject-single', function(){
  var image_id = $(this).attr('data-id');
  UpdateImage(image_id, 'rejected')
});


function UpdateImage(imgID, status){
  //check if the image alredy holds the status
  var current_status = $('#qc-warpper-img-'+imgID).closest('.bulkviewfilesqc').attr('data-status');

  if(status == current_status){
    //already holds the same status, remove the status and move it back to pending
    status = 'pending'
  }

  switch(status){
    case 'approved':
    $('#qc-warpper-img-'+imgID).addClass('border-selected-approved').removeClass('border-selected-rejected').attr('data-status', 'approved');
    break;
    case 'rejected':
    $('#qc-warpper-img-'+imgID).addClass('border-selected-rejected').removeClass('border-selected-approved').attr('data-status', 'rejected');
    break;
    case 'pending':
    $('#qc-warpper-img-'+imgID).removeClass('border-selected-rejected').removeClass('border-selected-approved').attr('data-status', 'pending');
    break;
  }

  UpdateStatus();

}

//----------------------------------
// To persist the data in database
// Controllers: /qc/operation/product
//----------------------------------

function flagProduct(product_id, psession_id, message, status){

  var csrfToken = $('[name=_token]').val();
  var qid = $('#qsession-id').val();
  //var message =  tinyMCE.activeEditor.getContent();
  var uid = document.getElementById('qc-uid').value;

  var msg_title = 'Product Approved';
  var msg_text = 'This product has been successfully approved.';
  var msg_type = 'success';

  if(status == 0){
    msg_title = 'Product Rejected';
    msg_text = 'This product has been rejected. The contributors will be notified about your feedback.';
    msg_type = 'success';
  }


  var images_status = CollectImageStatus()


  var data = {
    "product_id": product_id,
    "psession_id": psession_id,
    "qsession_id": qid,
    "status": status,
    "message": message,
    "operation_on": 'product',
    "path": "qc/view",
    "uid": uid,
    "images_status": images_status
  };


  if (product_id && psession_id) {
    $.ajax({
      url: '/qc/operation/product',
      method: 'post',
      headers: {
        'Content-Type': 'application/hal+json',
        'X-CSRF-Token': csrfToken
      },
      data: JSON.stringify(data),
      success: function (file) {
        console.log(file);
        swal({
          title: msg_title,
          text: msg_text,
          timer: 1500,
          type: msg_type,
          showConfirmButton: false
        });
        if(status == 1){
          $('#approve-product').modal('hide');
        }else{
          $('#reject-product').modal('hide');
        }
        //reject-product
        tinyMCE.activeEditor.setContent('');
        //document.getElementById('live-shoot-modal-flag-options').value = 0;
        //'qc-product-' + product_id
        document.getElementById('qc-product-' + product_id).remove();
        document.getElementById('imagecontainer').innerHTML = '';
        document.getElementById('qc-selected-product-title').innerHTML = '';
      },
      error: function () {
        swal({
          title: "Error",
          text: "There was an error, please try again.",
          type: "error",
          timer: 2500,
          showConfirmButton: false
        });
      }

    });
  }
}

//----------------------------------
//----------------------------------
//----------------------------------
// QC ACTIONS
// Contains all approve or rejects
//----------------------------------
//----------------------------------
//----------------------------------

$('body').on("click", "#qc-submit-final", function () {

  var approvalStatus;

  if(selectedProductStatus == 'Approved'){
    approvalStatus = 1;
    setMessage = 'This product has been approved by QC';
  }else{
    approvalStatus = 0;
    setMessage = 'This product has been rejected by QC';
  }

  var message =  tinyMCE.get('0').getContent();

  if(selectedProduct != '' && selectedProductSession != ''){
    console.log(selectedProduct, selectedProductSession, message, approvalStatus);
    flagProduct(selectedProduct, selectedProductSession, message, approvalStatus);
    isQCactive = 0;
    WorkSpace(0);
    $('#qc-submit-modal').modal('hide');



    var current_total = $('#qc-total-products').text();
    var new_total = current_total - 1 ;
    if(new_total < 0){ new_total = 0;}
    $('#qc-total-products').html(current_total - 1);
    $('.product-lock-timer').countdown('destroy').html('-- : -- : --');

  }else{
    //alert('No product selected!');
    return;
  }

});


//----------------------------------
// Update the status of QC for the product
//----------------------------------

function CollectImageStatus() {

  var data = [];
  var current_id = '';
  var current_status = '';


  $( ".bulkviewfilesqc" ).each( function( index, element ){
    current_id = $(this).attr('data-id');
    current_status = $(this).attr('data-status');
    data.push({
      'image_id' : current_id,
      'status' : current_status
    });
  });

  return data;

}


function renderProducts(){
  //load the products
  var qsession = $('#qsession-id').val();

  var $request = $.get("/qsessions/"+qsession+"/refresh"); // make request
  var $container = $('.qc-master-list');

  $('#status-qc').removeClass('hidden');

  $request.done(function(data) { // success
    console.log(data);
    $container.html(data.html);
  });
  $request.always(function() {
    $('#status-qc').addClass('hidden');
    var total_count = $('.qc-product-a').length;
    $('#qc-total-products').html(total_count);
  });


  //$('.initials-profile-qc').initial();
}
