
//----------------------------------
// Render the dashboard based on the selected dates
//----------------------------------

function renderDashboard(timeframe){


  var $request = $.get("/dashboard/"+timeframe); // make request
  var $container = $('#dashboard_metrics');

  //$('.loader-dashboard').removeClass('hidden');
  //$container.addClass('hidden');

  $request.done(function(data) { // success
    $container.html(data.html);
    // drawLineChart();
    // drawPie();
  });

  $request.always(function(){
   // $('.loader-dashboard').addClass('hidden');
    //$container.removeClass('hidden');

  })


}


$('body').on("change","#time_filter",function(){
  var timeframe = $('#time_filter').find(":selected").text();
  renderDashboard(timeframe);
});
