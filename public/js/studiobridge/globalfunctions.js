var current_identifier = '';
var userId = $('#current_user_id').val();
var current_sequence;
var temp_seq = "1,2,3,4,5,6,7,8"
const product_call = 10000;
const image_call = 10000;

function addMessage(notifyobject) {
  notifContent = '<div class="alert alert-dark media fade in bd-0" id="message-alert">\
  <div class="media-left">\
  <img data-src="'+notifyobject.data.avatar+'" class="dis-block img-circle"></div>\
  <div class="media-body width-100p"><h4 class="alert-title f-14">Product Rejected</h4>\
  <p class="f-12 alert-message pull-left">'+notifyobject.data.user.name+' rejected your product</p><p class="pull-right">\
  </p></div></div>';
  setTimeout(function() {
    if (!$('#quickview-sidebar').hasClass('open') && !$('.page-content').hasClass('page-builder') && !$('.morphsearch').hasClass('open')) generateNotifDashboard(notifContent);
  }, 3000);
}

//----------------------------------
// Notification Methods
//----------------------------------

function generateNotifDashboard(content) {
  var position = 'topRight';
  if ($('body').hasClass('rtl')) position = 'topLeft';
  var n = noty({
    text: content,
    type: 'success',
    layout: position,
    theme: 'made',
    animation: {
      open: 'animated bounceIn',
      close: 'animated bounceOut'
    },
    timeout: 4500,
    callback: {
      onShow: function() {
        $('#noty_topRight_layout_container, .noty_container_type_success').css('width', 350).css('bottom', 10);
      },
      onCloseClick: function() {
        setTimeout(function() {
          $('#quickview-sidebar').addClass('open');
        }, 500)
      }
    }
  });
}
function generate(position, container, content, confirm) {

  if(position == 'bottom') {
    openAnimation = 'animated fadeInUp';
    closeAnimation = 'animated fadeOutDown';
  }
  else if(position == 'top'){
    openAnimation = 'animated fadeIn';
    closeAnimation = 'animated fadeOut';
  }
  else{
    openAnimation = 'animated bounceIn';
    closeAnimation = 'animated bounceOut';
  }

  if(container == '') {

    var n = noty({
      text        : content,
      type        : type,
      dismissQueue: true,
      layout      : position,
      closeWith   : ['click'],
      theme       : 'made',
      maxVisible  : 10,
      animation   : {
        open  : openAnimation,
        close : closeAnimation,
        easing: 'swing',
        speed : 100
      },
      timeout: method,
      buttons     : confirm ? [
        {addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
          $noty.close();
          noty({dismissQueue: true, layout: 'topRight', theme : 'defaultTheme', text: 'You clicked "Ok" button', animation   : {
            open  : 'animated bounceIn', close : 'animated bounceOut'},type: 'success', timeout:3000});
            confirm = false;
          }
        },
        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
          $noty.close();
          noty({dismissQueue: true, layout: 'topRight', theme : 'defaultTheme',text: 'You clicked "Cancel" button', animation   : {
            open  : 'animated bounceIn', close : 'animated bounceOut'}, type: 'error', timeout:3000});
            confirm = false;
          }
        }
      ] : '',
      callback: {
        onShow: function() {
          leftNotfication = $('.sidebar').width();
          if($('body').hasClass('rtl')){
            if(position == 'top' || position == 'bottom') {
              $('#noty_top_layout_container').css('margin-right', leftNotfication).css('left', 0);
              $('#noty_bottom_layout_containe').css('margin-right', leftNotfication).css('left', 0);
            }
            if(position == 'topRight' || position == 'centerRight' || position == 'bottomRight') {
              $('#noty_centerRight_layout_container').css('right', leftNotfication + 20);
              $('#noty_topRight_layout_container').css('right', leftNotfication + 20);
              $('#noty_bottomRight_layout_container').css('right', leftNotfication + 20);
            }
          }
          else{
            if(position == 'top' || position == 'bottom') {
              $('#noty_top_layout_container').css('margin-left', leftNotfication).css('right', 0);
              $('#noty_bottom_layout_container').css('margin-left', leftNotfication).css('right', 0);
            }
            if(position == 'topLeft' || position == 'centerLeft' || position == 'bottomLeft') {
              $('#noty_centerLeft_layout_container').css('left', leftNotfication + 20);
              $('#noty_topLeft_layout_container').css('left', leftNotfication + 20);
              $('#noty_bottomLeft_layout_container').css('left', leftNotfication + 20);
            }
          }

        }
      }
    });

  }
  else  {
    var n = $(container).noty({
      text        : content,
      dismissQueue: true,
      layout      : position,
      closeWith   : ['click'],
      theme       : 'made',
      maxVisible  : 10,
      buttons     : confirm ? [
        {addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
          $noty.close();
          noty({dismissQueue: true, layout: 'topRight', theme : 'defaultTheme', text: 'You clicked "Ok" button', animation   : {
            open  : 'animated bounceIn', close : 'animated bounceOut'},type: 'success', timeout:3000});
          }
        },
        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
          $noty.close();
          noty({dismissQueue: true, layout: 'topRight', theme : 'defaultTheme',text: 'You clicked "Cancel" button', animation   : {
            open  : 'animated bounceIn', close : 'animated bounceOut'}, type: 'error', timeout:3000});
          }
        }
      ] : '',
      animation   : {
        open  : openAnimation,
        close : closeAnimation
      },
      timeout: method,
      callback: {
        onShow: function() {
          var sidebarWidth = $('.sidebar').width();
          var topbarHeight = $('.topbar').height();
          if(position == 'top' && style == 'topbar') {
            $('.noty_inline_layout_container').css('top', 0);
            if($('body').hasClass('rtl')) {
              $('.noty_inline_layout_container').css('right', 0);
            }
            else{
              $('.noty_inline_layout_container').css('left', 0);
            }

          }

        }
      }
    });

  }

}

//----------------------------------
// Equal height for containers
//----------------------------------

var equalheight = function(container){

  var currentTallest = 0,
  currentRowStart = 0,
  rowDivs = new Array(),
  topPosition = 0;
  $(container).each(function() {

    var el = jQuery(this);
    el.height('auto')
    topPosition = el.position().top;

    if (currentRowStart != topPosition) {
      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
      rowDivs.length = 0; // empty the array
      currentRowStart = topPosition;
      currentTallest = el.height();
      rowDivs.push(el);
    } else {
      rowDivs.push(el);
      currentTallest = (currentTallest < el.height()) ? (el.height()) : (currentTallest);
    }
    var givenHeight = $('.shot-today').height();
    for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
      rowDivs[currentDiv].height(givenHeight);
    }
  });
}

//Load the function after clicking resizable elements
$('body').on("click",".trigger-resize",function(){
  equalheight('.equalheight');
});

//Load the function after DOM
$(function () {
  equalheight('.equalheight');
});

//set up a function

function triggerEqualHeight(){
  equalheight('.equalheight');
}

//----------------------------------
// Helper function to replace undefined and emptytext
//----------------------------------
function replaceUD(text){
  if(text == undefined || text == ''){
    return '--';
  } else{
    return text;
  }
}
//----------------------------------
// Convert to time
//----------------------------------
function getTime(sec) {

  var hours = Math.floor(sec / 3600);
  var minutes = Math.floor((sec - (hours*3600)) / 60);
  var seconds = Math.floor(sec % 60);

  //how many seconds are left
  //leftover = leftover - (minutes * 60);
  return(hours + ':' + minutes + ':' + seconds);
}
//----------------------------------
// On clickng the delete button
//----------------------------------
$(".img-delete-btn").click(function () {

  var source = $(this).attr('data-source');
  processDelete(source);
});
//----------------------------------
// Logout confirmation
//----------------------------------
function logOutApplication(){
  swal({
    title: "Logout Confirmation",
    text: "Are you sure you want to log out?",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: 'btn-success',
    confirmButtonText: 'Logout',
    cancelButtonText: "Cancel",
    closeOnConfirm: false,
    closeOnCancel: true
  },
  function(isConfirm){
    if (isConfirm) {
      window.location.href = "/logout";
    } else {
    }
  });

};
//----------------------------------
// Generating a quote on live shoot page
//----------------------------------
function generateQuote(){
  var quoteArray = [
    '"Photography is the only language that can be understood anywhere in the world"',
    '"There are no rules for good photographs, there are only good photographs"',
    '"A good photograph is knowing where to stand"',
    '"You dont take a photograph, you make it"',
    '"In photography there is a reality so subtle that it becomes more real than reality"',
    '"Photography for me is not looking, it’s feeling. If you can’t feel what you’re looking at, then you’re never going to get others to feel anything when they look at your pictures"'
  ];
  var randomNumber = Math.floor(Math.random()*quoteArray.length);


  return quoteArray[randomNumber];

}

//generate a random quote
//$('#quote-liveshoot').html(generateQuote());

//----------------------------------
// To append recycle bin images to container
//----------------------------------
function appendImageRecycleBin(img,fid, source) {
  if ($('#warpper-img-'+ img.id).length > 0) {
    return;
  }

  var container, inputs, index;

  // Get the container element
  container = document.getElementById('imagecontainer');

  // Find its child `input` elements
  inputs = container.getElementsByClassName("form-checkbox");
  var seq = inputs.length + 1;
  var ul = document.getElementById('imagecontainer');
  var li = document.createElement("div");


  var classes= "bulkviewfiles ui-sortable-handle swing ";
  li.setAttribute("class", classes); // added line
  li.setAttribute('id',source+'-warpper-img-' + img.id);


  var block = '';
  block +=  '<div class="scancontainer"><div class="hovereffect">';
  block +=  '<div class="shadow animated-background" id="image-parent-'+img.id+'"> \
  <img class="scanpicture" data-imageid="'+ img.id +'" id="img-'+ img.id +'" onerror="imgError(this);"> \
  </div>';
  block +=  '<div class="overlay2"><input type="checkbox" class="form-checkbox" id="del-img-'+ img.id +'" hidden value="'+ img.id +'"> \
  <a class="info select-delete" data-id="'+ img.id +'" data-click="no" data-source="'+ source +'">Select image</a>\
  </div>';
  block +=  '</div>';

  block +=  '<div class="file-name">';
  block +=  '<div id="tag-seq-img-'+img.id+'" type="hidden"></div>';
  block +=  '<div class="row">';

  block += '<div class="col col-sm-12"> \
  <span id= "'+img.id+'"><a class=" col-sm-4" > \
  <span class="sequence-number"><span id="seq-' + img.id +'">' + seq +'</span></span></a> \
  <a class="col-sm-2 text-center" href= "/'+img.path+'" target="_blank" ><i class="icon-magnifier image-ops"></i></a> \
  <a class="col-sm-2 text-center select-delete" data-id="'+ img.id +'" data-click="no" data-source="'+ source +'"><i class="icon-check image-ops"></i></a> \
  </span></div>';

  block += '</div>';
  block += '</div>';
  block += '</div>';
  block += '<div class="studio-img-weight"><input type="hidden" value="'+img.id+'"></div>';
  block += '</div>';

  li.innerHTML = block;
  ul.appendChild(li);

  LoadImage("img-"+ img.id, "/" + img.thumbpath, "image-parent-" + img.id)
}
//----------------------------------
// To append recycle bin images to container
//----------------------------------
function appendImageContainer(img,fid,source) {


  if ($('#'+source+'-warpper-img-'+ img.id).length > 0) {
    return;
  }

  var container, inputs, index, qc, qcclass;

  // Get the container element
  container = document.getElementById('imagecontainer');

  // Find its child `input` elements
  inputs = container.getElementsByClassName("form-checkbox");
  var seq_num = inputs.length + 1;
  var seq = tempRename(seq_num);
  var ul = document.getElementById('imagecontainer');
  var li = document.createElement("div");

  var classes= "bulkviewfiles ui-sortable-handle ";
  if(source=='qc'){ classes+= ' bulkviewfilesqc' };
  li.setAttribute("class", classes); // added line
  li.setAttribute('id',source+'-warpper-img-' + img.id);
  li.setAttribute('data-id', img.id);
  li.setAttribute('data-status','pending');


  var block = '';

  //build image controls

  if(img.qcoperation){

    if(img.qcoperation.length > 0){
      var array = img.qcoperation;
      var lastEl = array[img.qcoperation.length-1];
      if(lastEl.status == 0){
        qc = 'Rejected';
        qcclass="bg-danger";
      }
      if(lastEl.status == 1){
        qc = 'Approved'
        qcclass="bg-success";
      }
      block += '<div class="ribbon"><span class="'+ qcclass +'">' + qc + '</span></div>'
    }

  }

  block +=  '<div class="scancontainer"><div class="hovereffect">';
  block +=  '<div class="shadow animated-background" id="image-parent-'+img.id+'"> \
  <img class="scanpicture" data-imageid="'+ img.id +'" id="img-'+ img.id +'" onerror="imgError(this);"> \
  </div>';
  block +=  '<div class="overlay2"><input type="checkbox" class="form-checkbox" id="del-img-'+ img.id +'" hidden value="'+ img.id +'"> \
  <a class="info select-delete" data-id="'+ img.id +'" data-click="no" data-source="'+ source +'">Select image</a>\
  </div>';
  block +=  '</div>';


  block +=  '<div class="file-name">';
  block +=  '<div id="tag-seq-img-'+img.id+'" type="hidden"></div>';
  block +=  '<div class="row">';

  if(source=='qc'){

    block += '<div class="col col-sm-12"> \
    <span id= "'+img.id+'"><a class=" col-sm-4" > \
    <span class="sequence-number">#<span id="seq-' + img.id +'">'+ seq +'</span></span></a> \
    <a class="col-sm-2 text-center" href="/'+img.thumbpath+'" data-lightbox="product-images"><i class="icon-magnifier image-ops"></i></a> \
    <a class="col-sm-2 text-center select-delete" data-id="'+ img.id +'" data-click="no" data-source="'+ source +'"><i class="icon-trash image-ops"></i></a> \
    <a class="col-sm-2 text-center qc-approve-single" data-id= "'+img.id+'"><i class="icon-check qcControls hidden image-ops"></i></a> \
    <a class="col-sm-2 text-center qc-reject-single" data-id="'+ img.id +'"><i class="icon-cross qcControls hidden image-ops"></i></a></span></div>';

  } else{

    block += '<div class="col col-sm-12"> \
    <span id= "'+img.id+'"> \
    <a class="col-sm-5"><span class="sequence-number"><span id="seq-' + img.id +'">' + seq +'</span></span></a> \
    <a class="col-sm-2 text-center" href="/'+img.thumbpath+'" data-lightbox="product-images"><i class="icon-magnifier image-ops"></i></a> \
    <a class="col-sm-2 text-center select-delete" data-id="'+ img.id +'" data-click="no" data-source="'+ source +'"><i class="icon-trash image-ops"></i></a> \
    <a class="col-sm-2 text-center liveshoot-img-pin" data-id="'+ img.id +'"><i class="icon-pin image-ops"></i></a> \
    </span></div>';
  }


  block += '</div>';
  block += '</div>';
  block += '</div>';
  block += '<div class="studio-img-weight"><input type="hidden" value="'+img.id+'"></div>';
  block += '</div>';

  li.innerHTML = block;

  ul.appendChild(li);
  LoadImage("img-"+ img.id, "/" + img.thumbpath, "image-parent-" + img.id)

}

function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
  rv[i+1] = arr[i];
  return rv;
}

function tempRename(seq) {


  if(current_sequence == null || current_sequence == ''){

    var renaming_scheme = toObject(temp_seq.split(","));
  } else {
    var renaming_scheme = toObject(current_sequence.split(","));
  }


  if(renaming_scheme[seq] == undefined){
    return seq;
  } else{
    return renaming_scheme[seq];
  }


}
//----------------------------------
// To process image delete from database
//----------------------------------
function processDelete(source){
  var container, inputs, index;
  var imgs = [];
  var imgsOriginal = [];
  var csrfToken = $('[name=_token]').val();
  var psession_id = $('#liveshoot-psession-id').val();
  var psession_uid = $('#liveshoot-psession-uid').val();
  var product_id = $('#liveshoot-pid').val();

  // Get the container element
  container = document.getElementById('imagecontainer');

  // Find its child `input` elements
  inputs = container.getElementsByClassName("form-checkbox");
  var unchecked = 0;
  for (index = 0; index < inputs.length; ++index) {
    // deal with inputs[index] element.
    //console.log(inputs[index].value);
    if((inputs[index].checked)){
      //imgs.push({"target_id": inputs[index].value});
      var fid =  inputs[index].value;
      if(fid){
        imgsOriginal.push(fid);
      }
    }else{
      ++unchecked;
    }
  }


  if(imgsOriginal.length){
    var data = {
      "product_id":product_id,
      "psession_id": psession_id,
      "psession_uid": psession_uid,
      "img": imgsOriginal
    };

    if(imgsOriginal){

      swal({
        title: "Move to trash?",
        text: "Are you sure you want to move the selected images to trash?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete!",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
          processImageDelete(data,source);
        }
      });

    }
  }else{
    //swal("No images", "No image/s selected!", "error");
    swal({
      title: "No images selected",
      text: "You have not selected any images to delete. Please select at least one image to move to recycle bin.",
      timer: 2000,
      type: "info",
      showConfirmButton: false
    });
  }

}

//----------------------------------
// Process the deleting of the image using API Call
//----------------------------------

function processImageDelete(data, source){
  var csrfToken = $('[name=_token]').val();
  var container, inputs, index;


  $.ajax({
    url: '/images/delete',
    method: 'POST',
    headers: {
      //'Content-Type': 'application/hal+json',
      'X-CSRF-Token': csrfToken
    },
    //data: JSON.stringify(data),
    data: data,
    success: function (node) {
      //alert('success!');
      //var obj = JSON.parse(node);

      deleteImageContainer(node, source);


      // Update image wrappers.
      container = document.getElementById('imagecontainer');
      var inputs2 = container.getElementsByClassName("form-checkbox");
      for (index2 = 0; index2 < inputs2.length; ++index2) {
        // deal with inputs[index] element.
        document.getElementById('seq-'+inputs2[index2].value).innerHTML = tempRename(index2 + 1);


      }

      swal({
        title: "Moved to trash",
        text: "Your images have been moved to the recycle bin.",
        timer: 1000,
        type: "success",
        showConfirmButton: false
      });
    },
    error: function(){
      alert('Failed! **');
    }

  });

}

//----------------------------------
// To delete image from container
//----------------------------------

function deleteImageContainer(unwanted, source){

  var del = [];
  unwanted.forEach(function(entry) {
    console.log("#"+source+"-warpper-img-"+entry);
    $("#"+source+"-warpper-img-"+entry).remove();

  });

}

//----------------------------------
// To process image restore from database
//----------------------------------

function processRestore(){
  var container, inputs, index;
  var imgs = [];
  var imgsOriginal = [];
  var csrfToken = $('[name=_token]').val();
  var psession_id = $('#liveshoot-psession-id').val();
  var psession_uid = $('#liveshoot-psession-uid').val();
  var product_id = $('#liveshoot-pid').val();

  // Get the container element
  container = document.getElementById('imagecontainer');

  // Find its child `input` elements
  inputs = container.getElementsByClassName("form-checkbox");
  var unchecked = 0;
  for (index = 0; index < inputs.length; ++index) {
    // deal with inputs[index] element.
    //console.log(inputs[index].value);
    if((inputs[index].checked)){
      //imgs.push({"target_id": inputs[index].value});
      var fid =  inputs[index].value;
      if(fid){
        imgsOriginal.push(fid);
      }
    }else{
      ++unchecked;
    }
  }


  if(imgsOriginal.length){
    var data = {
      "product_id":product_id,
      "psession_id": psession_id,
      "psession_uid": psession_uid,
      "img": imgsOriginal
    };

    if(imgsOriginal){

      swal({
        title: "Restore Selected Images",
        text: "Are you sure you want want to restore the selected images?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: 'btn-success',
        confirmButtonText: 'Restore',
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
          processImageRestore(data);
          window.location.href = '/psession/'+psession_id+'/product/add';
        }
      });

    }
  }else{
    //swal("No images", "No image/s selected!", "error");
    swal({
      title: "No images selected",
      text: "You have not selected any images to delete. Please select at least one image to move to recycle bin.",
      timer: 2000,
      type: "info",
      showConfirmButton: false
    });
  }

}

//----------------------------------
// Process the restoring of the image using API Call
//----------------------------------

function processImageRestore(data){
  var csrfToken = $('[name=_token]').val();
  var container, inputs, index;


  $.ajax({
    url: '/images/restore',
    method: 'POST',
    headers: {
      'X-CSRF-Token': csrfToken
    },
    data: data,
    success: function (node) {

      deleteImageContainer(node, 'recycle');


      container = document.getElementById('imagecontainer');
      var inputs2 = container.getElementsByClassName("form-checkbox");
      for (index2 = 0; index2 < inputs2.length; ++index2) {
        // deal with inputs[index] element.
        document.getElementById('seq-'+inputs2[index2].value).innerHTML = tempRename(index2 + 1);


      }

      swal({
        title: "Images Restored",
        text: "Your images have been restored to their respective products.",
        timer: 1000,
        type: "success",
        showConfirmButton: false
      });
    },
    error: function(err){
      console.log(err);
      swal({
        title: "Error",
        text: "There was an error trying to restore your images. Please try again or refresh the page",
        timer: 5000,
        type: "error",
        showConfirmButton: false
      });
    }

  });

}

//----------------------------------
// To process image import from reshoot
//----------------------------------

function processReshootImport(){
  var container, inputs, index;
  var imgs = [];
  var imgsOriginal = [];
  var csrfToken = $('[name=_token]').val();
  var psession_id = $('#liveshoot-psession-id').val();
  var psession_uid = $('#liveshoot-psession-uid').val();
  var product_id = $('#liveshoot-pid').val();

  // Get the container element
  container = document.getElementById('reshoot-product-model');

  // Find its child `input` elements
  inputs = container.getElementsByClassName("reshoot-image-checkbox");
  var unchecked = 0;
  for (index = 0; index < inputs.length; ++index) {
    // deal with inputs[index] element.
    //console.log(inputs[index].value);
    if((inputs[index].checked)){
      //imgs.push({"target_id": inputs[index].value});

      var fid =  inputs[index].value;
      var original_pid =  $('#'+inputs[index].id).attr('data-original-pid');
      var original_sid =  $('#'+inputs[index].id).attr('data-original-sid');

      if(fid){
        imgsOriginal.push({'image_id':fid,'original_pid':original_pid,'original_sid':original_sid});
      }
    }else{
      ++unchecked;
    }
  }


  if(imgsOriginal.length){
    var data = {
      "product_id":product_id,
      "psession_id": psession_id,
      "psession_uid": psession_uid,
      "img": imgsOriginal
    };

    if(imgsOriginal){

      swal({
        title: "Import images?",
        text: "Are you sure you want to import selected images in your current session?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Import",
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {

          processImageReshoot(data);

        }
      });

    }
  }else{

    swal({
      title: "No images selected",
      text: "You have not selected any images to import. Please select at least one image to import for reshoot.",
      timer: 2000,
      type: "info",
      showConfirmButton: false
    });
  }

}

//----------------------------------
// Process image import as reshoot
//----------------------------------

function processImageReshoot(data){
  var csrfToken = $('[name=_token]').val();
  var container, inputs, index;


  $.ajax({
    url: '/images/importreshoot',
    method: 'POST',
    headers: {
      'X-CSRF-Token': csrfToken
    },
    data: data,
    success: function (node) {

      $('#reshoot-product-model').modal('hide');
      swal({
        title: "Images Imported",
        text: "Your selected images have been imported in your current product shoot.",
        timer: 1000,
        type: "success",
        showConfirmButton: false
      });


    },
    error: function(err){

      swal({
        title: "Error",
        text: "There was an error trying to import your images. Please try again or refresh the page",
        timer: 2000,
        type: "error",
        showConfirmButton: false
      });
    }

  });

}

//----------------------------------
// To display product flags
//----------------------------------

jQuery.fetchOtherSessions = function fetchOtherSessions(identifier){

  var block = '';
  var current_session = $('#liveshoot-psession-id').val();


  $.get("/test/getProductSessions/"+identifier, function(data, status){

    var a = data;



    if(a.length > 0){
      block += '<ul class="nav nav-tabs nav-tabs-component nav-justified">';

      //first create tab elements
      for(var i in a){

        if(current_session != a[i].session.id){
          var liclass = '';
          if(i==1){liclass="active"};
          block += '<li class="'+liclass+'" style="display:table-cell !important;"><a href="#Session-'+a[i].session.id+'" data-toggle="tab">PS '+a[i].session.id+'</a></li>';
        }
      }
      block += '</ul>';
      block += '<div class="tab-content" class="m-b-10">';

      //now create tab content
      for(var i in a){
        var liclass = '';
        if(i==1){liclass="active"};

        block += '<div class="tab-pane '+liclass+'" id="Session-'+a[i].session.id+'">';
        block += '<ul class="menu-list m-t-10 m-b-20">';

        block += '<li><span><span class="text-blue-darker">Session Date </span><span class="pull-right text-info"><strong >'+a[i].session.created_at+'</strong></span></span></li>';
        block += '</ul>';

        if(a[i].images.length == 0){

          block += '<h3 class="text-center">No images found in this session</h3>';
        }

        for(var x in a[i].images){

          if(a[i].images[x].id != undefined){
            block += '<input type="checkbox" class="reshoot-image-checkbox" id="reshoot-image-'+a[i].images[x].id+'"  hidden="" value="'+a[i].images[x].id+'" data-original-sid="'+a[i].session.id+'" data-original-pid="'+a[i].product.id+'">'
            block += '<img class="reshoot-image-container" id="image-'+a[i].images[x].id+'" class="" src="/'+a[i].images[x].thumbpath+'" data-id="'+a[i].images[x].id+'" data-click="no" width="150px" height="150px">'
          }
        }
        block += '</div>';
      }
      block += '</div>';

      $("#reshoot-product-model-tabs").html(block)
    }

  });

}

//----------------------------------
// To Fetch images for the product
//----------------------------------

var current_identifier = '';

$(function() {
  var identifier = $('#liveshoot-psession-id').val();
  var pidentifier = $('#liveshoot-pid').val();

  //alert(identifier)
  if(identifier != ''){
    // setInterval(function() {
    //
    // }, 500);
    GetNewImages(identifier);
    GetProductInfo();
    // GetRecentProducts(identifier);
  }
});

function GetNewImages(identifier){


  if (!$("body").hasClass("isLiveShoot")) {
    return false;
  }


  var identifier = $('#liveshoot-psession-id').val();
  var pidentifier = $('#liveshoot-pid').val();

  var jqxhr =  $.get("/images?psid="+identifier, function(data, status){
    var a = data;
    if(a){

      for(var i in a){
        if ($('#warpper-img-'+ i).length == 0) {
          appendImageContainer(a[i],i,'liveshoot');
        }
      }
    }
  })
  .always(function() {
    //setTimeout(GetNewImages, image_call, identifier);
  });


}

//----------------------------------
// To display product information
//----------------------------------

function GetProductInfo(source){


  if (!$("body").hasClass("isLiveShoot")) {
    return false;
  }

  var identifier = $('#liveshoot-psession-id').val();
  var pidentifier = $('#liveshoot-pid').val();

  var ul = document.getElementById('flagged-messages-list');

  if(current_identifier != pidentifier){
    $('#flagged-messages-list').html("");
    current_identifier = pidentifier;
  }

  var jqxhrs = $.get("/product/info/"+identifier+"/"+pidentifier, function(data, status){

    var a = data[0];
    var b = data[1];

    if(a){
      if(a.images > 0){
        //$('#quote-liveshoot').removeClass('hidden');
        $('#quote-liveshoot').addClass('hidden');
      } else {
        //$('#quote-liveshoot').addClass('hidden');
        $('#quote-liveshoot').removeClass('hidden');
      }

      document.getElementById('live-imgs-total-count').innerHTML =  a.images;
      document.getElementById('live-imgs-deleted-count').innerHTML =  a.deleted_images;

      //check if the extra shot is requires
      if(a.requires_extra_shot != undefined && a.requires_extra_shot.length != 0 && a.requires_extra_shot == 1){

        var checked = $("#requires-extra-shot-chk").parent('[class*="icheckbox"]').hasClass("checked");

        if(!checked) {
          $('#requires-extra-shot-chk').attr('data-shownotification', 'no');
          $('#requires-extra-shot-chk').iCheck('check');
          $('.extra-shot-container').removeClass('hidden');
        }

      } else {
        var checked = $("#requires-extra-shot-chk").parent('[class*="icheckbox"]').hasClass("checked");

        if(checked) {
          $('#requires-extra-shot-chk').attr('data-shownotification', 'no');
          $('#requires-extra-shot-chk').iCheck('uncheck');
          $('.extra-shot-container').addClass('hidden');
        }

      }
      //check if the product is flagged
      if(a.comments != undefined && a.comments != 0){
        $('.flag-container').removeClass('hidden');
      }
      else {
        $('.flag-container').addClass('hidden');
      }

      if(a.qc_done != undefined && a.qc_done == 1){
        if(a.qc_state == 1){
          $('.product-qc-class').addClass('fa-check-circle text-success').removeClass('fa-times-circle c-red');
          $('.product-qc-state').html('Product Approved');
          $('.product-qc-status').html('Approved').removeClass('text-amber-dark').removeClass('c-red').addClass('text-success');
        } else{
          $('.product-qc-class').removeClass('fa-check-circle text-success').addClass('fa-times-circle c-red');
          $('.product-qc-state').html('Product Rejected');
          $('.product-qc-status').html('Rejected').removeClass('text-amber-dark').removeClass('text-success').addClass('text-danger');
        }
        $('.product-qc').removeClass('hidden');
        $('.reset-qc').removeClass('hidden');
      }
      else {
        $('.product-qc-status').html('Pending').addClass('text-amber-dark').removeClass('text-success').removeClass('c-red');
        $('.product-qc').addClass('hidden');
      }

    }

    if(b){
      $('.product-sequence-convention').removeClass('hidden')
      $('.product-sequence-convention').html(b.short_name)
      if(b.sequence != current_sequence){
        current_sequence = b.sequence
        $('#imagecontainer').html('');
      }
      current_sequence = b.sequence


    }

  })
  .error(function(error){
    if(source == 'qc'){
      setTimeout(function(){ GetProductInfo('qc'); }, product_call);
    } else{
      setTimeout(GetProductInfo, product_call);
    }
  })
  .done(function() {
    if(source == 'qc'){
      setTimeout(function(){ GetProductInfo('qc'); }, product_call);
    } else{
      setTimeout(GetProductInfo, product_call);
    }

  });


}

$('body').on("click",".liveshoot-refresh-images",function(){
  $('#imagecontainer').html('');
});


//----------------------------------
// Append flags messages to container
//----------------------------------

function appendFlagItems(current_flag){


  if ($('#flagged-message-'+ current_flag.comment_id).length == 0) {
    var li = document.createElement("li");
    li.setAttribute('id','flagged-message-' + current_flag.comment_id);
    var type = current_flag.subject;
    var type_class = '';
    switch (type) {
      case ('Note'):
      type_class = 'fa fa-2x fa-fw fa-sticky-note text-success'
      break;
      case ('Complete the look'):
      type_class = 'fa fa-2x fa-fw fa-shirtsinbulk text-info'
      break;
      case ('QC Product Rejected'):
      type_class = 'fa fa-2x fa-fw fa-times text-danger'
      break;
      case ('QC Product Approved'):
      type_class = 'fa fa-2x fa-fw fa-check text-success'
      break;
      default:
      type_class = 'fa fa-2x fa-fw fa-flag text-warning'

    }

    var block = '';

    block += '<div id="fire-message-'+current_flag.comment_id+'"><span class="chat-avatar"> \
    <i class="'+type_class+'"></i></span><span class="chat-u-info">'+ current_flag.subject+'<cite>'+ current_flag.name + ' in session ' + current_flag.psession_id + '</cite></span> \
    </div><input type="hidden" id="fire-message-'+current_flag.comment_id+'-body" value="'+current_flag.body+'"> \
    <input type="hidden" id="fire-message-'+current_flag.comment_id+'-user" value="'+current_flag.name+'"> \
    <input type="hidden" id="fire-message-'+current_flag.comment_id+'-created" value="'+current_flag.created_at+'"> \
    <input type="hidden" id="fire-message-'+current_flag.comment_id+'-useravatar" value="'+current_flag.avatar+'">' ;
    li.innerHTML = block;
  }
  return li;
}

//----------------------------------
// To load an image
//----------------------------------

function LoadImage(element, src, parent) {
  var img = new Image(),
  x = document.getElementById(element);
  parent = document.getElementById(parent);

  img.onload = function() {
    x.src = img.src;
    $(parent).removeClass('animated-background');
    $(x).addClass('animated fadeIn');
  };

  img.onerror = function() {
    x.src = "/assets/images/no-image.png";
    $(parent).removeClass('animated-background');
    $(x).addClass('animated fadeIn');
  };


  img.src = src;
}

//
// LIVESHOOT FUNCTIONS CORE

$('#edit-identifier').keypress(function (e) {
  var key = e.which;
  if(key == 13)
  {
    //LiveShootWorkspace(0);
    triggerIt(0, 0);
  }
});

$('body').on("click",".recent-product-click",function(){
  var pid = $(this).attr('data-pid');
  if(pid != $('#liveshoot-last-scanned_identifier').val()){
    swal({
      title: "Switch Product",
      text: "Are you sure you want to switch to this product?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Switch",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm){
      if (isConfirm) {
        $('#edit-identifier').val(pid);
        var e = jQuery.Event("keypress");
        e.keyCode = e.which = 13;
        $("#edit-identifier").trigger(e);
      }
    });
  }
});

function triggerIt(reshoot, onload){
  //Remove the normal class and append a search class
  $('#edit-identifier').prop('disabled', true);

  //Get the identifier of the product
  var identifier = $('#edit-identifier').val();
  var last_scanned_identifier = $('#liveshoot-last-scanned_identifier').val();

  //Check if identifier is empty
  if(jQuery.trim(identifier).length < 1 && !onload){
    //alert('No product scanned yet');
    swal({
      title: "No identifier entered",
      text: "Please enter an identifier to continue",
      timer: 1000,
      showConfirmButton: false
    });
    $('#edit-identifier').prop('disabled', false);
    $('#edit-identifier').removeProp("disabled");
    return;
  }

  //If the last scanned and current scanned item is the same, dont proceed
  if(last_scanned_identifier == identifier && !onload && !reshoot){

    // Double check that the product identifier is displaying same
    var identifier_displaying = document.getElementById('dd-identifier').innerHTML;
    if(identifier == identifier_displaying){
      swal({
        title: "Same Product Scanned",
        text: "You have scanned the current product identifier",
        timer: 1500,
        showConfirmButton: false,
        type:"warning"
      });
      $('#edit-identifier').prop('disabled', false);
      $('#edit-identifier').removeProp("disabled");
      $('#edit-identifier').val("");
      return;
    }
  }

  //Get the session id and user id
  var psession_id = $('#liveshoot-psession-id').val();
  var psession_uid = $('#liveshoot-psession-uid').val();
  var model_uid = $('#selected-model-id').val();

  var data = {"identifier":jQuery.trim(identifier), "psession_id": psession_id, "psession_uid": psession_uid, "onload": onload, "model": model_uid};
  if(reshoot){
    var data = {"identifier":jQuery.trim(identifier), "psession_id": psession_id, "psession_uid": psession_uid, "reshoot": true, "onload": onload, "model": model_uid};
  }

  var csrfToken = $('[name=_token]').val();

  //make the ajax call and check if product is found
  $.ajax({
    url: '/liveshoot',
    method: 'POST',
    headers: {
      //'Content-Type': 'application/hal+json',
      'X-CSRF-Token': csrfToken
    },
    //data: JSON.stringify(data),
    data: data,
    success: function (node) {

      if(node['reshoot']){

        swal({
          title: "Reshoot product",
          text: "This product already exists in the system, are you sure you want to reshoot this product?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Reshoot",
          cancelButtonText: "Cancel",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            triggerIt(1, 0);
            swal("Reshoot Product Added", "Product added to your session as reshoot", "success");
            return;
          } else {
            $('#edit-identifier').val("");
          }
        });
        $('#edit-identifier').prop('disabled', false);
        $('#edit-identifier').removeProp("disabled");
        return;
        //alert("It's a reshoot product!");
      }

      //display information about the current product
      var reshoot = displayProductDetails(node);
      var product = node['product'];
      product = product[0];
      if(product.type=='unmapped'){
        NotifContent = '<div class="alert alert-warning media fade in">\
        <h4 class="alert-title">Unmapped Product</h4>\
        <p>This product has been added to your session as an unmapped product</p>\
        </div>',
        autoClose = true;
        type = 'warning';
        method = 3000;
        position = 'top';
        container ='';
        style = 'box';
      } else{
        NotifContent = '<div class="alert alert-success media fade in">\
        <h4 class="alert-title">Mapped Product</h4>\
        <p>This product has been added to your session as a mapped product</p>\
        </div>',
        autoClose = true;
        type = 'success';
        method = 3000;
        position = 'top';
        container ='';
        style = 'box';
      }

      generate('topLeft', '', NotifContent);
      $('#edit-identifier').prop('disabled', false);

      //Clear the Workspace
      LiveShootWorkspace(0);

      //If product is a reshoot then, show that information
      if(reshoot == 'reshoot'){
        $('.reshoot-product').removeClass('hidden');
        $.fetchOtherSessions(product.id);
      }


    },
    error: function(err){
      alert('Failed!' + err);
    }

  });
}

function displayProductDetails(node){

  var product = node['product'];
  var counts = node['count'];
  product = product[0];
  if(node['product_state'] != 'Reshoot'){
    $('.reshoot-product').addClass('hidden');
  }
  if(node['product_state'] == 'Reshoot'){
    // $('.reshoot-product').removeClass('hidden');
    // $.fetchOtherSessions(product.id);
  }
  //getVideo(product.identifier);
  document.getElementById('dd-identifier').innerHTML = product.identifier;
  document.getElementById('dd-identifier2').innerHTML = product.identifier;
  document.getElementById('dd-color').innerHTML =  replaceUD(product.color_name);
  document.getElementById('dd-description').innerHTML =  replaceUD(product.description);
  document.getElementById('dd-concept').innerHTML =  replaceUD(product.brand_name);
  document.getElementById('dd-gender').innerHTML =  replaceUD(product.gender);
  document.getElementById('dd-baseproductid').innerHTML =  replaceUD(product.base_product_id);
  document.getElementById('dd-stylefamily').innerHTML =  replaceUD(product.style_family);
  document.getElementById('liveshoot-last-scanned_identifier').value =  replaceUD(product.identifier);
  document.getElementById('liveshoot-pid').value =  replaceUD(product.id);

  document.getElementById('session-total-products').innerHTML =  counts.total;
  document.getElementById('liveshoot-Unmapped').innerHTML =  counts.unmapped;
  document.getElementById('liveshoot-drop').innerHTML =  counts.drop;

  document.getElementById('live-product-state').innerHTML =  node['product_state'];
  document.getElementById('live-imgs-total-count').innerHTML =  node['imgs_count'].total;
  document.getElementById('live-imgs-deleted-count').innerHTML =  node['imgs_count'].deleted;

  document.getElementById('liveshoot-drop-product').innerHTML =  node['product_drop'].action_text;
  $('#liveshoot-drop-product').removeClass('btn-success').addClass(node['product_drop'].action_class);


  $('#liveshoot-drop-product').attr( 'data-drop',node['product_drop'].action);

  $('#icon-identifier-search').addClass( "fa-search" ).removeClass( "spinner fa-spin fa-spinner" );
  $('#edit-identifier').val("");
  $("#dd-identifier").click();

  LiveShootWorkspace(0);


  $(".builder-toggle").trigger('click').delay(3000).queue(function(n) {
    $(this).trigger('click'); n();
  });

  if(node['product_state'] == 'Reshoot'){
    return 'reshoot';
  } else {
    return 'new';
  }

}

function LiveShootWorkspace(clear){
  if(clear == 0){
    $('#imagecontainer').html('');
    $('.flag-container').addClass('hidden');
    $('.product-qc').addClass('hidden');
    $('.product-video-container').addClass('hidden');
    $('.reshoot-product').addClass('hidden');
    $('.reset-qc').addClass('hidden');
  }

}

function liveShootSessionInfo(){

  $('body').addClass('sidebar-collapsed')
  $('.page-liveshoot').addClass('open');
  $( ".topbar" ).css( "padding-right", "0px");

}

$('.builder-toggle').on('click', function(){
  if($('#builder').hasClass('open')) {
    $('.page-liveshoot').removeClass('open');
    $( ".topbar" ).css( "padding-right", "0px");
  }
  else {  $('.page-liveshoot').addClass('open');
  $( ".topbar" ).css( "padding-right", "239px");
}
});

//----------------------------------
// On clickng change model button
//----------------------------------
$(document).on("click",".reset-qc",function(){
  var psid = $('#liveshoot-psession-id').val();
  var pid = $('#liveshoot-pid').val();
  swal({
    title: "Resubmit Product",
    text: "Are you sure you want to resubmit product for QC",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#F44336",
    confirmButtonText: "Submit",
    cancelButtonText: "Cancel",
    closeOnConfirm: true,
    closeOnCancel: true
  },
  function(isConfirm){
    if (isConfirm) {
      //make the ajax call and check if product is found
      $.ajax({
        url: '/product/state/reset/' + psid + '/' + pid,
        method: 'GET',
        success: function (node) {
          swal({
            title: "Product Resubmitted",
            text: "Your product has been submitted for review to QC",
            timer: 1000,
            type: "info",
            showConfirmButton: false
          });
          $('.product-qc').addClass('hidden');
          $('.reset-qc').addClass('hidden');
        },
        error: function(err){
          alert('Failed!' + err);
        }
      });
    }
  });
});
$(document).on("click",".fab-label-visible",function(){
  var modelname = $(this).attr("data-fab-label")
  var modelimage = $(this).attr("data-fab-img")
  var modelid = $(this).attr("data-model-id")
  var modelgender = $(this).attr("data-model-gender")
  swal({
    title: "Switch Model",
    text: "Are you sure you want to switch model to " + modelname,
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#F44336",
    confirmButtonText: "Switch",
    cancelButtonText: "Cancel",
    closeOnConfirm: true,
    closeOnCancel: true
  },
  function(isConfirm){
    if (isConfirm) {
      $('#model-name').html(modelname);
      $('#model-image').attr('src', modelimage);
      $('#selected-model-id').val(modelid);
      $('#selected-model-gender').val(modelgender);
    }
  });
});
$(document).ready(function() {
  var sessionid = $('#liveshoot-pid').val()
  //if product is reshoot show reshoot optionss
  if($('#live-product-state').html() == 'Reshoot'){
    $('.reshoot-product').removeClass('hidden');
    $.fetchOtherSessions(sessionid);
  }
  //generate a random quote

});
$(document).on("click",".reshoot-image-container",function(){
  //alert('no');
  //TODO: create a select all function +2
  var id = $(this).attr("data-id")
  var clicked = $(this).attr("data-click")
  var container = document.getElementById( "warpper-img-"+id );
  if(clicked==='yes'){
    console.log(id);
    $(this).removeClass('border-selected-green');
    $(this).attr("data-click","no")
    $('#reshoot-image-'+id).prop('checked', false);
    selected--
  }else{
    $(this).addClass('border-selected-green');
    $(this).attr("data-click","yes")
    $('#reshoot-image-'+id).prop('checked', true);
    selected++
  }
});


$('.morphsearch-input').keyup(function(e) {
  $('.product-search').html();
  clearTimeout($.data(this, 'timer'));
  if (e.keyCode == 13)
  search(true);
  else
  $(this).data('timer', setTimeout(search, 500));
});
function search(force) {
  console.log('searching');
  var existingString = $(".morphsearch-input").val();
  if (!force && existingString.length < 3) return; //wasn't enter, not > 2 char
  $('.product-search').html();
  $.get('/search/' + existingString, function(data) {
    $('.product-search').html(data.html);
  });
}

//----------------------------------
// Update product Extra Shot
//----------------------------------
function addProductExtraShot(type, pid, sid, notify){
  console.log(notify);
  var csrfToken = $('[name=_token]').val();

  $.ajax({
    url: '/product/extrashot',
    method: 'POST',
    headers: {
      'X-CSRF-Token': csrfToken
    },
    data: {
      'type': type,
      'pid': pid,
      'sid': sid
    },
    success: function (node) {

      if(node == 'added' && notify == 'yes'){
        swal({
          title: "Extra Shot Added",
          text: "Your request to add extra shot has been completed.",
          timer: 1000,
          type: "success",
          showConfirmButton: false
        });
        $('.extra-shot-container').removeClass('hidden');
      } else if(node == 'removed' && notify == 'yes') {
        swal({
          title: "Extra Shot Removed",
          text: "Your request to remove extra shot has been completed.",
          timer: 1000,
          type: "success",
          showConfirmButton: false
        });
        $('#requires-extra-shot-chk').iCheck('enable');
        $('#requires-extra-shot-chk').iCheck('uncheck');
        $('.extra-shot-container').addClass('hidden');
      } else {
        swal({
          title: "Extra Shot Complete",
          text: "Your have marked extra shot as complete",
          timer: 1000,
          type: "success",
          showConfirmButton: false
        });
        $('#requires-extra-shot-chk').iCheck('uncheck');
        $('.extra-shot-container').addClass('hidden');


      }
    },
    error: function(err){
      console.log(err);
      swal({
        title: "Error",
        text: "There was an error trying to add your request.",
        timer: 2000,
        type: "error",
        showConfirmButton: false
      });
    }

  });
}

$(document).on("click",".show-product-flags",function(){
  var csrfToken = $('[name=_token]').val();

  var product_id = $(this).attr('data-product-id')
  var source_type = $(this).attr('data-source-type')


  if(source_type == 'asn'){
    var type_id = $(this).attr('data-asn-id')
  }else{
    var type_id = $(this).attr('data-psession-id')
  }



  var $request = $.ajax({
    url: '/helper/productflags',
    type:'post',
    headers: {
      'X-CSRF-Token': csrfToken
    },
    data:{product_id: product_id, type:source_type, type_id:type_id  },
  });
  $request.done(function(data) { // success
    if(data.status == 'success'){
      var container = $('.item-comment')
      container.html(data.html)

      $('#product-flags').modal('show');
    }
  });
});
