/**
 * Created by krishna on 28/12/16.
 */

(function ($) {


    //on clicking the import button for images
    //1. Check if any images were selected
    $(document).on("click", ".liveshoot-img-pin", function () {

        var fid = $(this).attr('data-id');
        var csrfToken = $('[name=_token]').val();
        var psession_id = $('#liveshoot-psession-id').val();
        var psession_uid = $('#liveshoot-psession-uid').val();
        var product_id = $('#liveshoot-pid').val();

        var data = {
            "product_id": product_id,
            "psession_id": psession_id,
            "psession_uid": psession_uid,
            "fid": fid
        };

        if (fid) {
            $.ajax({
                url: '/psession/image/pin',
                method: 'post',
                headers: {
                    'Content-Type': 'application/hal+json',
                    'X-CSRF-Token': csrfToken
                },
                data: JSON.stringify(data),
                success: function (file) {
                    console.log(file);

                    if (file.err) {
                        swal({
                            title: "Cannot pin this image",
                            text: "This image has already been pinned, you cannot pin this image again",
                            type: "error",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    } else {
                        swal({
                            title: "Success",
                            text: "This image has been pinned to current session. You can view it by clicking on Add Pinned button",
                            timer: 1000,
                            type: "success",
                            showConfirmButton: false
                        });

                        addImageToPinBlock(file);

                    }
                },
                error: function () {
                    swal({
                        title: "Pin Image Error",
                        text: "There was an error, please try again.",
                        type: "error",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }

            });
        }

    });


    function append_img(img,fid) {
        if ($('#warpper-img-'+ img.id).length > 0) {
            return;
        }
        var container, inputs, index;
        // Get the container element
        container = document.getElementById('imagecontainer');
        // Find its child `input` elements
        inputs = container.getElementsByClassName("form-checkbox");
        var seq = inputs.length + 1;

        var ul = document.getElementById('imagecontainer');
        var li = document.createElement("div");

        var textArray = [
            'paper-lift',
            'paper-curve-right',
            'paper-curve-vert',
            'paper-curve-below'
        ];
        var randomNumber = Math.floor(Math.random()*textArray.length);
        var classes= "bulkviewfiles ui-sortable-handle swing paper paper-curve-vert";
        li.setAttribute("class", classes); // added line
        li.setAttribute('id','warpper-img-' + img.id);

        var block = '';

        block +=  '<div class="scancontainer"><div class="hovereffect">';
        block +=  '<div class="shadow"><img src="/'+ img.thumbpath +'" class="scanpicture" data-imageid="'+ img.id +'" id=img-'+ img.id +'></div>';
        block += '<div class="overlay"><input type="checkbox" class="form-checkbox" id="del-img-'+ img.id +'" hidden value="'+ img.id +'"><a class="info select-delete" data-id="'+ img.id +'" data-click="no">Select image</a></div>';
        block +=  '</div>';

        block +=  '<div class="file-name">';
        block +=  '<div id="tag-seq-img-'+img.id+'" type="hidden"></div>';
        block += '<div class="row">';

                 block += '<div class="col col-sm-12"> \
                 <span id= "'+img.id+'"><a class=" col-sm-4" > \
                 <span class="sequence-number">#<span id="seq-' + img.id +'">' + seq +'</span></span></a> \
                 <a class="col-sm-4 text-center" href= "'+img.path+'" target="_blank" ><i class="icon-magnifier image-ops"></i></a> \
                 <a class="col-sm-4 text-center liveshoot-img-pin" data-id="'+ img.id +'"><i class="icon-pin image-ops"></i></a></span></div>';

        block += '</div>';
        block += '</div>';
        block += '</div>';
        block += '<div class="studio-img-weight"><input type="hidden" value="'+img.id+'"></div>';
        block += '</div>';

        li.innerHTML = block;
        ul.appendChild(li);

    }

})(jQuery);
