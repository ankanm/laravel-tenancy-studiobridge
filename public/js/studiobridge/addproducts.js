(function ($) {

    $(document).on('click', '.chat-user-list > li > div, .chat-back', function(e){
        e.preventDefault();
        if ($("#flagged-message-bar").hasClass("right-chat-toggle")) {
            $("#flagged-message-bar").removeClass("right-chat-toggle");
        } else {
            $("#flagged-message-bar").addClass("right-chat-toggle");
        }
        var message = $('#'+$(this).attr("id")+"-body").val();
        var user = $('#'+$(this).attr("id")+"-user").val();
        var avatar = $('#'+$(this).attr("id")+"-useravatar").val();
        var created = $('#'+$(this).attr("id")+"-created").val();

        $('.flag-message').html();
        $('.flag-message').html(message);
        $('.flag-user').html(user);
        $('.flag-created').html(created);
        $('.flag-user-avatar').attr('src', avatar);

    });




})(jQuery);
