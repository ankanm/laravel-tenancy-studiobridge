//----------------------------------
// All Functions specific to Organizations Section
//----------------------------------

/*
|--------------------------------------------------------------------------
| Add Location Function
|--------------------------------------------------------------------------
*/

  $(document).on("click", ".add-loc", function () {
      $("#country").val(0).trigger('change');
      $("#state").val(0).trigger('change');
      $("#city").val(0).trigger('change');
  })


  $(document).on("click", "#add-location-bt", function () {
    var short_name = $("#short_name").val();
    var address1 = $('#address1').val();
    var country = $('#country').val();
    var city = $('#city').val();

    if(short_name=="" || address1=="" || country=="" || city==""){
        alert('Missing required fields');
        return;
    } else {
        addLocation();
    }

    function addLocation(){

        var org_id = '0';
        var vendor_id = '0';
        var agency_id = '0';
        var csrfToken = $('[name=_token]').val();
        
        var address2 = $('#address2').val();
        var province = $('#state').val();
        var zipcode = $('#zipcode').val();

        if($("#org-id").val()!='') {
          org_id = $("#org-id").val();
        }

        if($("#vendor-id").val()!='') {
          vendor_id = $("#vendor-id").val();
        }

        if($("#agency-id").val()!='') {
          agency_id = $("#agency-id").val();
        }

        

        var data = {
            "short_name": short_name,
            "address1": address1,
            "address2": address2,
            "country": country,
            "province": province,
            "city": city,
            "zipcode": zipcode,
            "org_id": org_id,
            "vendor_id": vendor_id,
            "agency_id": agency_id
        };

            $.ajax({
                url: '/organization/addlocation',
                method: 'post',
                headers: {
                    'Content-Type': 'application/hal+json',
                    'X-CSRF-Token': csrfToken
                },
                data: JSON.stringify(data),
                success: function (file) {

                        swal({
                            title: 'Add Location',
                            text: 'Location added successfully!',
                            timer: 2500,
                            type: "success",
                            showConfirmButton: false
                        });
                    $('#add-loc').modal('hide');
                    $("#short_name").val('');
                    $('#address1').val('');
                    $('#address2').val('');
                    $('#zipcode').val('');
                    $("#country").val(0).trigger('change');
                    $("#state").val(0).trigger('change');
                    $("#city").val(0).trigger('change');
                    location.reload();
                                 
                },
                error: function () {
                    swal({
                        title: "Error",
                        text: "There was an error, please try again.",
                        type: "error",
                        timer: 2500,
                        showConfirmButton: false
                    });
                }

            });
    }
});

/*
|--------------------------------------------------------------------------
| Country DropDown Function
|--------------------------------------------------------------------------
*/

$(document).on("change","#country",function(){
    var country_id=$(this).val();
    $("#state").empty().append('<option value="0">Select a State</option>');
    $.ajax({
        url: '/helper/state-by-country/'+country_id,
        success:function(data){
            $("#state").prop('disabled', false);              
              $("#state").append(data);
              $("#state").select2("destroy");
              $("#state").select2({
                placeholder: "Select a State"
              });

        }
    })
})

/*
|--------------------------------------------------------------------------
| State DropDown Function
|--------------------------------------------------------------------------
*/

$(document).on("change","#state",function(){
    var state_id=$(this).val();
    $("#city").empty().append('<option value="0">Select a City</option>');
    $.ajax({
        url: '/helper/city-by-state/'+state_id,
        success:function(data){
              $("#city").prop('disabled', false);              
              $("#city").append(data);
              $("#city").select2("destroy");
              $("#city").select2({
                placeholder: "Select a City"
              });

        }
    })
})

/*
|--------------------------------------------------------------------------
| Set Location Primary Function
|--------------------------------------------------------------------------
*/

$(document).on("click",".primarybtn",function(){

  var csrfToken = $('[name=_token]').val();
  var loc_id = $(this).attr('location_id');

  var data = {
            "loc_id": loc_id,
            "primary": '1'
        };

  swal({
    title: "Location Primary",
    text: "Are you sure you want to set this location as Primary Location ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#F44336",
    confirmButtonText: "Set Primary",
    cancelButtonText: "Cancel",
    closeOnConfirm: true,
    closeOnCancel: true
  },
  function(isConfirm){
    if (isConfirm) {

        $.ajax({
                url: '/organization/location/primary',
                method: 'post',
                headers: {
                    'Content-Type': 'application/hal+json',
                    'X-CSRF-Token': csrfToken
                },
                data: JSON.stringify(data),
                success: function (file) {

                        swal({
                            title: 'Set Primary Location',
                            text: 'Location set to primary !',
                            timer: 2500,
                            type: "success",
                            showConfirmButton: false
                        });
                location.reload();
                                 
                },
                error: function () {
                    swal({
                        title: "Error",
                        text: "There was an error, please try again.",
                        type: "error",
                        timer: 2500,
                        showConfirmButton: false
                    });
                }

              });
           }
        });
    });




    