//----------------------------------
// All Functions specific to Ops Desk - Add Products
//----------------------------------
/*
  |--------------------------------------------------------------------------
  | Key Press event for add product
  |--------------------------------------------------------------------------
  */
  $('#add-identifier').keypress(function (e) {
    var key = e.which;
    if(key == 13)
    {
      checkProduct();
      //productInfo();
    }
  });

  function productInfo() {

     var identifier = $('#add-identifier').val();
     var asn_id = $('#asn_id').val();
     var csrfToken = $('[name=_token]').val();
     // alert(identifier);

     $.ajax({
      url: '/operations/asn/productinfo',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{identifier:identifier, asn_id:asn_id},
      success:function(data){

          if(data!="") {

            $("#pro-id").val(data.product.productid);
            $("#asnpro-id").val(data.product.asnproduct_id);
            $("#pro-identifier").html(data.product.identifier);
            $("#pro-parent").html(data.product.base_product_id);
            $("#pro-stylefamily").html(data.product.style_family);
            $("#pro-brand").html(data.product.brand_name);
            $("#pro-gender").html(data.product.gender);
            $("#pro-color").html(data.product.color_name);
            $("#pro-description").html(data.product.description);
            $(".flagbutton").prop('disabled', false);
            $(".dropbutton").prop('disabled', false);
        }

      }
    });

  }

  /*
  |--------------------------------------------------------------------------
  | Check Product Function
  |--------------------------------------------------------------------------
  */
  function checkProduct() {

    //Remove the normal class and append a search class
    $('#icon-identifier-search').removeClass( "fa-search" ).addClass( "spinner fa-spinner" );
    var csrfToken = $('[name=_token]').val();

    //Get the identifier of the product
    var identifier = $('#add-identifier').val();
    var asn_id = $('#asn_id').val();
    var box_id = $('#box_id').val();
    var prod_type="check";

    $.ajax({

      url: '/operations/asn/storeproduct',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{identifier:identifier, asn_id:asn_id, box_id:box_id,
      prod_type:prod_type},
      success:function(data){
        // console.log(data);

        if(data==1) {

          NotifContent = '<div class="alert alert-success media fade in">\
          <h4 class="alert-title">Product Added</h4>\
          <p>This product has been added to your asn as a new product</p>\
          </div>',
          autoClose = true;
          type = 'success';
          method = 3000;
          position = 'top';
          container ='';
          style = 'box';

          generate('bottomRight', '', NotifContent);
          $('#icon-identifier-search').addClass( "fa-search" ).removeClass( "spinner fa-spinner" );
          productInfo();
          $('#add-identifier').val("");
          refreshContainers(asn_id);
          refreshBoxes(asn_id);
          switchBox('0',box_id);
          return;
        }
        else if(data==2 || data==3){
          addProducts(data);

        }

      }
    });
  }

  /*
  |--------------------------------------------------------------------------
  | Add Product Function
  |--------------------------------------------------------------------------
  */
  function addProducts(msg) {

    //Remove the normal class and append a search class
    $('#icon-identifier-search').removeClass( "fa-search" ).addClass( "spinner fa-spinner" );

    //Get the identifier of the product
    var csrfToken = $('[name=_token]').val();
    var identifier = $('#add-identifier').val();
    var asn_id = $('#asn_id').val();
    var box_id = $('#box_id').val();
    var prod_type="add";

    if(msg ==2) {
      var text = "This Product already exists in the Products list. Do you still want to add this product to ASN?";
    }

    if(msg ==3) {
      var text = "This Product already exists in both Products list and ASN. Do you still want to add this product to ASN?";
    }

    swal({
      title: "Add New Product",
      text: text,
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: 'btn-success',
      confirmButtonText: 'Add Product',
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {

        $.ajax({
          url: '/operations/asn/storeproduct',
          headers: {
          'X-CSRF-Token': csrfToken
          },
          type:'post',
          data:{identifier:identifier, asn_id:asn_id, box_id:box_id, addtype: msg, prod_type:prod_type},
          success:function(data){

            if(data==1) {

              NotifContent = '<div class="alert alert-warning media fade in">\
              <h4 class="alert-title">Product Added</h4>\
              <p>This product has been added to your asn as an existing product</p>\
              </div>',
              autoClose = true;
              type = 'warning';
              method = 3000;
              position = 'top';
              container ='';
              style = 'box';

              generate('bottomRight', '', NotifContent);
              $('#icon-identifier-search').addClass( "fa-search" ).removeClass( "spinner fa-spinner" );
              productInfo();
              $('#add-identifier').val("");
              refreshContainers(asn_id);
              refreshBoxes(asn_id);
              switchBox('0',box_id);
              return;
            }
          }
        });

      } else {
         $('#icon-identifier-search').addClass( "fa-search" ).removeClass( "spinner fa-spinner" );
        //swal("Cancelled", "You have cancelled adding the product", "error");
       
      }
    });

  }

  /*
  |--------------------------------------------------------------------------
  | Refresh Containers Function
  |--------------------------------------------------------------------------
  */

  function refreshContainers(asn_id){

    var csrfToken = $('[name=_token]').val();
    var $request = $.ajax({
      url: '/operations/asn/refreshContainer',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{asn_id:asn_id},
    });
    var $container = $('.asnProducts');
    $request.done(function(data) { // success
     // $(".countproduct").html(data.countProducts+" Products");
     $("#session-total-products").html(data.countProducts);
      $container.html(data.html);
    });

  }

   /*
  |--------------------------------------------------------------------------
  | Add New Box Function
  |--------------------------------------------------------------------------
  */

  $("#add-asnbox").on('click',function(){
    var csrfToken = $('[name=_token]').val();
    var asn_id = $('#asn_id').val();
    var dimension_l = $('#dimension_l').val();
    var dimension_w = $('#dimension_w').val();
    var dimension_h = $('#dimension_h').val();
    var dimension_unit = $('#dimension_unit').val();
    var weight = $('#weight').val();
    var weight_unit = $('#weight_unit').val();

    swal({
      title: "Add New Box",
      text: "Are you sure you want to add this box ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: 'btn-success',
      confirmButtonText: 'Add Box',
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {

        $.ajax({
          url: '/operations/asn/addBox',
          headers: {
          'X-CSRF-Token': csrfToken
          },
          type:'post',
          data:{asn_id:asn_id, dimension_l:dimension_l,
          dimension_w:dimension_w, dimension_h:dimension_h,dimension_unit:dimension_unit, weight:weight,
          weight_unit:weight_unit },
          success:function(data){

            if(data==1) {

              NotifContent = '<div class="alert alert-success media fade in">\
              <h4 class="alert-title">Box Added</h4>\
              <p>A new box has been added</p>\
              </div>',
              autoClose = true;
              type = 'success';
              method = 3000;
              position = 'top';
              container ='';
              style = 'box';

              $('#add-box').modal('hide');
              generate('bottomRight', '', NotifContent);
              refreshBoxes(asn_id);
              switchBox();
            }
          }
        });

      } else {
        swal("Cancelled", "You have cancelled adding the box", "error");
      }
    });

  })

  /*
  |--------------------------------------------------------------------------
  | Refresh boxes Function
  |--------------------------------------------------------------------------
  */
  function refreshBoxes(asn_id){

    var csrfToken = $('[name=_token]').val();
    var $request = $.ajax({
      url: '/operations/asn/refreshBoxes',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{asn_id:asn_id},
    });
    var $container = $('.asnBoxes');
    $request.done(function(data) { // success

      //console.log(data.html);
      $("#session-total-products").html(data.products_count);
      $("#session-total-box").html(data.box_count);
      $container.html(data.html);

      if(data.open_boxes > 0){
        $('.hasActiveBox').removeClass('hidden');
        $('.noActiveBox').addClass('hidden');
      }

    });

  }

   /*
  |--------------------------------------------------------------------------
  | Close box Function
  |--------------------------------------------------------------------------
  */

  $("#close-box").on('click',function(){
    var asn_id = $('#asn_id').val();
    var box_id = $('#box_id').val();
    var csrfToken = $('[name=_token]').val();

    swal({
      title: "Close Box",
      text: "Are you sure you want to close this box ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: 'btn-success',
      confirmButtonText: 'Close Box',
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {

        $.ajax({
          url: '/operations/asn/closeBox',
          headers: {
          'X-CSRF-Token': csrfToken
          },
          type:'post',
          data:{asn_id:asn_id, box_id:box_id },
          success:function(data){

            if(data.products_count > 0) {

              var notify_text = '<div class="alert alert-success media fade in">\
              <h4 class="alert-title">Box Closed</h4>\
              <p>The box has been closed</p>\
              </div>'

            } else {

              var notify_text = '<div class="alert alert-success media fade in">\
              <h4 class="alert-title">Box Closed</h4>\
              <p>The box has been deleted</p>\
              </div>'
            }

              NotifContent = notify_text,
              autoClose = true;
              type = 'success';
              method = 3000;
              position = 'top';
              container ='';
              style = 'box';

              $('#add-box').modal('hide');
              generate('bottomRight', '', NotifContent);
              refreshBoxes(asn_id);
              switchBox();
            }

        });

      } else {
        swal("Cancelled", "You have cancelled closing the box", "error");
      }
    });

  })

  /*
  |--------------------------------------------------------------------------
  | Switch Box Function
  |--------------------------------------------------------------------------
  */

  function switchBox(status=null,boxid=null) {

    if(status!=="" && status==1) {
      return false;
    } else {

      var csrfToken = $('[name=_token]').val();
      var asn_id = $('#asn_id').val();
      var $request = $.ajax({
        url: '/operations/asn/refreshcurrentBox',
        headers: {
          'X-CSRF-Token': csrfToken
        },
        type:'post',
        data:{box_id:boxid, asn_id:asn_id},
      });
      $request.done(function(data) { // success

        if(data.status == 'success'){
          $('#box_id').val(data.box_id);
          $('#current-boxid').html(data.box_id);
          $('#quantity-count').html(data.quantity);
        } else{

          $('.hasActiveBox').addClass('hidden');
          $('.noActiveBox').removeClass('hidden');

        }
      });
    }

  }

  /*
  |--------------------------------------------------------------------------
  | Drop Function
  |--------------------------------------------------------------------------
  */
$('.dropbutton').click(function(){
      var product_id = $("#asnpro-id").val();

      var quantity = $("#list-"+product_id).attr("prod_quantity");
      if(quantity > 1) {
        var prod_quantity = '2';
      } else {
        var prod_quantity = '1';
      }
      dropDialog(product_id, prod_quantity);

})

$(document).on('click', ".dropProd", function (e) {

    var prod_id = $(this).attr('prod_id');
    var prod_quantity = $(this).attr('prod_quantity');
    dropDialog(prod_id, prod_quantity);

})

function dropDialog(prod_id,prod_quantity) {

      if(prod_quantity == 1) {

      var prod_type="1";
      var text = "Are you sure you want to drop this product ?";
      var button = [{
                label: 'Drop Product',
                cssClass: 'btn-danger',
                data: {
                    js: 'btn-confirm'
                },
                action: function(dialogItself){
                    dialogItself.close();
                    callajaxdrop(prod_id,1);
                }

            }, {
                label: 'Cancel',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }];

    } else if(prod_quantity > 1) {

      var prod_type="2";
      var text = "Are you sure you want to reduce quantity OR drop all quantities ?";
      var button = [{
                label: 'Drop All Quantities',
                cssClass: 'btn-danger',

                data: {
                    js: 'btn-confirm'
                },
                action: function(dialogItself){
                    dialogItself.close();
                    callajaxdrop(prod_id,1);
                }

            }, {
                label: 'Reduce Quantity',
                // no title as it is optional
                cssClass: 'btn-primary',
                data: {
                    js: 'btn-confirm'
                },
                action: function(dialogItself){
                  dialogItself.close();
                  callajaxdrop(prod_id,2);

                }
            }, {
                label: 'Cancel',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }];

    }

    BootstrapDialog.show({
            title: 'Drop Product',
            message: text,
            buttons: button
    });
}

function callajaxdrop(prod_id,prod_type) {

  var asn_id = $('#asn_id').val();
  var box_id = $('#box_id').val();
  var csrfToken = $('[name=_token]').val();

    $.ajax({
        url: '/operations/asn/dropproduct',
        headers: {
          'X-CSRF-Token': csrfToken
        },
        type:'post',
        data:{prod_id:prod_id,prod_type:prod_type,asn_id:asn_id},
        success:function(data){

        if(prod_type==1 || prod_type==3) {
          var notify_type = '<div class="alert alert-success media fade in">\
          <h4 class="alert-title">Drop Product</h4>\
          <p>This product has been dropped</p>\
          </div>';

        } else if(prod_type==2) {
          var notify_type = '<div class="alert alert-success media fade in">\
          <h4 class="alert-title">Drop Product</h4>\
          <p>This product quantity has been reduced</p>\
          </div>';
        }


          NotifContent = notify_type,
          autoClose = true;
          type = 'success';
          method = 3000;
          position = 'top';
          container ='';
          style = 'box';

          generate('bottomRight', '', NotifContent);
          $('#icon-identifier-search').addClass( "fa-search" ).removeClass( "spinner fa-spinner" );
          $("#session-total-dropped").html(data.count_dropped);
          $("#pro-id").val('');
          $("#asnpro-id").val('');
          $("#pro-identifier").html('');
          $("#pro-parent").html('');
          $("#pro-stylefamily").html('');
          $("#pro-brand").html('');
          $("#pro-gender").html('');
          $("#pro-color").html('');
          $("#pro-description").html('');
          $(".flagbutton").prop('disabled', true);
          $(".dropbutton").prop('disabled', true);
          refreshContainers(asn_id);
          refreshBoxes(asn_id);
          switchBox('0',box_id);

          return;
        }
      });
}

  //----------------------------------
      // To get product flags
      //----------------------------------


      $(document).on("click",".show-product-flags",function(){

        var product_id = $(this).attr('data-product-id');
        var asn_id = $(this).attr('data-asn-id');
        var csrfToken = $('[name=_token]').val();

        var $request = $.ajax({
          url: '/helper/productflags',
          type:'post',
          headers: {
            'X-CSRF-Token': csrfToken
          },
          data:{product_id: product_id, type:'asn', type_id:asn_id},
        });
        $request.done(function(data) { // success

          if(data.status == 'success'){
            var container = $('.asn-comment')
            container.html(data.html)

            $('#product-flags-asn').modal('show');
          }
        });
      });

  /*
  |--------------------------------------------------------------------------
  | Flag Product Function
  |--------------------------------------------------------------------------
  */
  $('.flagbutton').click(function(){

      var product_id = $("#pro-id").val();
      $('#flag-product-id').val(product_id);

  })

  $(document).on('click', ".flagproduct", function (e) {
      var product_id = $(this).attr('prod_id');
      $('#flag-product-id').val(product_id);

  })


  $(document).on("click", "#liveshoot-flag-product-bt", function () {

    var options = document.getElementById('live-shoot-modal-flag-options').value;
    var product_id = $('#flag-product-id').val();

    if(product_id && options.length > 1){
        flagProduct(product_id);
    }else{
        alert('Missing options');
        return;
    }

    function flagProduct(product_id){

        var csrfToken = $('[name=_token]').val();
        var asn_id = $('#asn_id').val();
       // var psession_uid = $('#liveshoot-psession-uid').val();
        var options = document.getElementById('live-shoot-modal-flag-options').value;
        var message =  tinyMCE.activeEditor.getContent();


        var data = {
            "product_id": product_id,
            "asn_id": asn_id,
            "path": "/operations/asn/"+asn_id+"/product/add",
            "subject": options,
            "type": 'flag',
            "body": message
        };


        if (product_id && asn_id) {
            $.ajax({
                url: '/operations/asn/flag-product',
                method: 'post',
                headers: {
                    'Content-Type': 'application/hal+json',
                    'X-CSRF-Token': csrfToken
                },
                data: JSON.stringify(data),

                success: function (file) {

                        swal({
                            title: 'Product flag',
                            text: 'Product flagged successfully!',
                            timer: 1000,
                            type: "success",
                            showConfirmButton: false
                        });
                    $('#flag-product').modal('hide');
                    tinyMCE.activeEditor.setContent('');
                    $("#live-shoot-modal-flag-options").val(0).trigger('change');
                    refreshContainers(asn_id);
                },
                error: function () {
                    swal({
                        title: "Error",
                        text: "There was an error, please try again.",
                        type: "error",
                        timer: 2500,
                        showConfirmButton: false
                    });
                }

            });
        }
    }
});
