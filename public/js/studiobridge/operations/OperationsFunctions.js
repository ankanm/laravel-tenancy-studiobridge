//----------------------------------
// All Functions specific to Ops Desk
//----------------------------------

//----------------------------------
// To get location address based on location id
//----------------------------------
function getLocationAddress(address_name, location_id){
  var csrfToken = $('[name=_token]').val();

 $.ajaxSetup({ async: false });
  $.ajax({
        url: '/operations/api/location/details',
        dataType: "json",
        async: false,
        headers: {
          'X-CSRF-Token': csrfToken
        },
        data:{ option:location_id },
          success:function(data){

          $.getJSON("/operations/api/country/details/",
          { option: data.country },
          function(countryinfo) {
            data.country_name = countryinfo.name
          });

          $.getJSON("/operations/api/state/details/",
            { option: data.province },
            function(stateinfo) {
              data.state_name = stateinfo.name
          });

          $.getJSON("/operations/api/city/details/",
            { option: data.city },
            function(cityinfo) {
               data.city_name = cityinfo.name
          });

          var model = $(address_name);
          model.empty();
          model.append("<p class='width-300 m-t-10'><strong>"+ data.short_name+"</strong></p>");
          model.append("<p class='width-300'>"+ data.address1+"</p>");
          model.append("<p class='width-300'>"+ data.address2+"</p>");
          model.append("<p class='width-300'>"+ data.city_name+ "," +data.state_name+ "," +data.country_name+"</p>");
          model.append("<p class='width-300'>"+ data.zipcode+"</p>");



          }

  });

}






//----------------------------------
// Load all addresses when DOM is ready
//----------------------------------
$(function() {
  $( "address" ).each(function( index ) {
    location_id = $(this).attr('data-location-id');
    if(typeof location_id !== 'undefined' && location_id != ''){
      getLocationAddress($(this), location_id)
    }
  });

});

//----------------------------------
// When clicking on Contents List Button
//----------------------------------
$(document).on("click",".content-list",function(){

  var box_id = $(this).attr('data-boxid');
  var asn_id = $('#asn_id').val();
  var contentlist = 1;
  var html = $('.box-content-' + box_id).html();
  var div = $('#shipping-content').closest('div').find('.modal-body').html(html);
  $('#shipping-content').modal('show');
  refreshContainers(asn_id,box_id,contentlist);


});

/*
|--------------------------------------------------------------------------
| Close ASN Function
|--------------------------------------------------------------------------
*/

$(document).on("click",".close-asn",function(){
  var csrfToken = $('[name=_token]').val();
  var asn_id = $("#asn_id").val();

  swal({
    title: "End ASN",
    text: "Are you sure you want to end this asn?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#F44336",
    confirmButtonText: "End ASN",
    cancelButtonText: "Cancel",
    closeOnConfirm: true,
    closeOnCancel: true
  },
  function(isConfirm){
    if (isConfirm) {

      $.ajax({
        url: '/operations/asn/closeASN',
        type:'post',
        headers: {
          'X-CSRF-Token': csrfToken
        },
        data:{
          asn_id:asn_id,
          status:'Packing Closed' },
          success:function(data){

            NotifContent = '<div class="alert alert-success media fade in">\
            <h4 class="alert-title">ASN Closed</h4>\
              <p>The asn has been closed</p>\
              </div>',
              autoClose = true;
              type = 'success';
              method = 3000;
              position = 'bottom';
              container ='';
              style = 'box';

              generate('topRight', '', NotifContent);
              location.reload();
            }
          });

        }
      });

    });

    /*
    |--------------------------------------------------------------------------
    | Close Checkin ASN Function
    |--------------------------------------------------------------------------
    */

    $(document).on("click",".close-checkin-asn",function(){
      var csrfToken = $('[name=_token]').val();
      var asn_id = $("#asn_id").val();

      swal({
        title: "Close ASN Checkin",
        text: "Are you sure you want to end close asn checkin ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Close ASN Checkin",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {

          $.ajax({
            url: '/operations/asn/closecheckinASN',
            type:'post',
            headers: {
              'X-CSRF-Token': csrfToken
            },
            data:{asn_id:asn_id, status:'Checkin Closed' },
            success:function(data){

              NotifContent = '<div class="alert alert-success media fade in">\
              <h4 class="alert-title">ASN Closed</h4>\
                <p>The asn has been closed</p>\
                </div>',
                autoClose = true;
                type = 'success';
                method = 3000;
                position = 'top';
                container ='';
                style = 'box';

                generate('topRight', '', NotifContent);
                location.reload();
              }
            });

          }
        });

      });

      /*
      |--------------------------------------------------------------------------
      | Refresh Containers Function
      |--------------------------------------------------------------------------
      */
      function refreshContainers(asn_id,box_id,contentlist){

        var csrfToken = $('[name=_token]').val();

        var $request = $.ajax({
          url: '/operations/asn/refreshContainer',
          type:'post',
          headers: {
            'X-CSRF-Token': csrfToken
          },
          data:{asn_id:asn_id, box_id:box_id, contentlist:contentlist},
        });
        var $container = $('.asnProducts');
        $request.done(function(data) { // success
          $container.html(data.html);
        });

      }


      $(document).on("click",".boxclass",function(){

        var asn_id = $('#asn_id').val();
        var box_id = $(this).attr('box_id');
        var contentlist = 0;
        refreshContainers(asn_id,box_id,contentlist);
      });


      //----------------------------------
      // To get product flags
      //----------------------------------


      $(document).on("click",".show-product-flags",function(){

        var product_id = $(this).attr('data-product-id')
        var asn_id = $(this).attr('data-asn-id')
        var csrfToken = $('[name=_token]').val();

        var $request = $.ajax({
          url: '/helper/productflags',
          type:'post',
          headers: {
            'X-CSRF-Token': csrfToken
          },
          data:{product_id: product_id, type:'asn', type_id:asn_id},
        });
        $request.done(function(data) { // success

          if(data.status == 'success'){
            var container = $('.asn-comment')
            container.html(data.html)

            $('#product-flags-asn').modal('show');
          }
        });
      });

  /*
    |--------------------------------------------------------------------------
    | Ship ASN Function
    |--------------------------------------------------------------------------
    */

    $(document).on("click",".ship-asn",function(){
      var csrfToken = $('[name=_token]').val();
      var asn_id = $("#asn_id").val();
      var carrier_id = $("#carrier_id").val();
      var shipping_method = $("#shipping_method").val();
      var estimated_ship_date = $("#estimated_ship_date").val();
      var tracking_number = $("#tracking_number").val();

      swal({
        title: "Ship ASN",
        text: "Are you sure you want to ship this ASN?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Ship ASN",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {

             $.ajax({
              url: "/operations/asn/checkin/shipasn",
              headers: {
              'X-CSRF-Token': csrfToken
              },
              type:'post',
              data:{asn_id:asn_id,status:'Shipped',carrier_id:carrier_id,
              shipping_method:shipping_method, estimated_ship_date:estimated_ship_date,
              tracking_number:tracking_number},
              success:function(data){

                NotifContent = '<div class="alert alert-success media fade in">\
                <h4 class="alert-title">ASN Shipped</h4>\
                  <p>This ASN has been shipped</p>\
                  </div>',
                  autoClose = true;
                  type = 'success';
                  method = 3000;
                  position = 'top';
                  container ='';
                  style = 'box';

                  generate('topRight', '', NotifContent);
                  location.reload();
              }
            });
          }
        });

      });

     /*
    |--------------------------------------------------------------------------
    | Export as PDF Function
    |--------------------------------------------------------------------------
    */

    $(document).on("click",".export-pdf",function(){

      var box_id = $(this).attr('exportboxid');

      var html = $('.box-content-' + box_id).html();
      var asn_id = $("#asn_name").val();
      var csrfToken = $('[name=_token]').val();

         $.ajax({
              url: "/operations/asn/checkin/exportpdf",
              headers: {
                'X-CSRF-Token': csrfToken
              },
              type:'post',
              data:{pdf_html:html, asn_id:asn_id, box_id:box_id},
              success:function(data){

                // if(data==1) {
                //   NotifContent = '<div class="alert alert-success media fade in">\
                //   <h4 class="alert-title">ASN Shipped</h4>\
                //     <p>The Export Ship PDF file has been created successfully</p>\
                //     </div>',
                //     autoClose = true;
                //     type = 'success';
                //     method = 3000;
                //     position = 'top';
                //     container ='';
                //     style = 'box';

                //     generate('topRight', '', NotifContent);
                //   }
              }
            });

    });


    /*
    |--------------------------------------------------------------------------
    | Print Function
    |--------------------------------------------------------------------------
    */

    $(document).on("click",".print-list",function(){

      var box_id = $(this).attr('printboxid');

      var divContents = $('.box-content-' + box_id).html();

      var printWindow = window.open();
      printWindow.document.write('<html><head><title>BOX CONTENT LIST</title>');
      printWindow.document.write('</head><body >');
      printWindow.document.write(divContents);
      printWindow.document.write('</body></html>');
      printWindow.document.close();
      printWindow.print();

    });

      /*
    |--------------------------------------------------------------------------
    | Send by Email Function
    |--------------------------------------------------------------------------
    */

     $(document).on("click",".email-list",function(){
          $('#shipping-content').modal('hide');
          $('#send-email').modal('show');
          var box_id = $(this).attr('mailboxid');
          $('#send-box-id').val(box_id);

      });

    $(document).on("click","#asn-send-mail-btn",function(){

      $(".mail_error").html('');
      var csrfToken = $('[name=_token]').val();
      var box_id =  $('#send-box-id').val();
      var html = $('.box-content-' + box_id).html();
      var asn_id = $("#asn_name").val();

      if($('#send-email-address').val()=="") {
        $(".mail_error").html("Please enter email addres !")
        return false;
      }

      var email = $('#send-email-address').val();


      if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)))
      {
        $(".mail_error").html('');
        $(".mail_error").html("You have entered an invalid email address !")
        $('#send-email-address').val('');
        $('#send-email-address').focus();
        return (false);
      }


         $.ajax({
              url: "/operations/asn/checkin/sendmail",
              headers: {
                'X-CSRF-Token': csrfToken
              },
              type:'post',
              data:{html:html, asn_id:asn_id, box_id:box_id, email:email},
              success:function(data){

                  NotifContent = '<div class="alert alert-success media fade in">\
                  <h4 class="alert-title">ASN Shipped</h4>\
                    <p>Email sent successfully</p>\
                    </div>',
                    autoClose = true;
                    type = 'success';
                    method = 3000;
                    position = 'top';
                    container ='';
                    style = 'box';

                    generate('topRight', '', NotifContent);

                    $('#send-email').modal('hide');
                    $('#shipping-content').modal('show');
              }
            });

    });




  /*
  |--------------------------------------------------------------------------
  | Drop Function
  |--------------------------------------------------------------------------
  */
  $(document).on('click', ".dropProd", function (e) {

    var prod_id = $(this).attr('prod_id');
    var prod_quantity = $(this).attr('prod_quantity');
    var box_id = $(this).attr('box_id');

    if(prod_quantity == 1) {

      var prod_type="1";
      var text = "Are you sure you want to drop this product ?";
      var button = [{
        label: 'Drop Product',
        cssClass: 'btn-danger',
        data: {
          js: 'btn-confirm'
        },
        action: function(dialogItself){
          dialogItself.close();
          callajaxdrop(box_id,prod_id,1);
        }

      }, {
        label: 'Cancel',
        action: function(dialogItself){
        dialogItself.close();
        }
      }];

    } else if(prod_quantity > 1) {

      var prod_type="2";
      var text = "Are you sure you want to reduce quantity OR drop all quantities ?";
      var button = [{
        label: 'Drop All Quantities',
        cssClass: 'btn-danger',

        data: {
          js: 'btn-confirm'
        },
        action: function(dialogItself){
          dialogItself.close();
          callajaxdrop(box_id,prod_id,1);
        }

      }, {
        label: 'Reduce Quantity',
        // no title as it is optional
        cssClass: 'btn-primary',
        data: {
          js: 'btn-confirm'
        },
        action: function(dialogItself){
        dialogItself.close();
        callajaxdrop(box_id,prod_id,2);

        }
      }, {
        label: 'Cancel',
        action: function(dialogItself){
          dialogItself.close();
        }
      }];

    }

    BootstrapDialog.show({
      title: 'Drop Product',
      message: text,
      buttons: button
    });

  })


  function callajaxdrop(box_id,prod_id,prod_type) {

    var asn_id = $('#asn_id').val();
    var csrfToken = $('[name=_token]').val();
    $.ajax({
      url: '/operations/asn/dropproduct',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{prod_id:prod_id,prod_type:prod_type,asn_id:asn_id},
      success:function(data){

        if(prod_type==1) {
          var notify_type = '<div class="alert alert-success media fade in">\
          <h4 class="alert-title">Drop Product</h4>\
          <p>This product has been dropped</p>\
          </div>';

        } else if(prod_type==2) {
          var notify_type = '<div class="alert alert-success media fade in">\
          <h4 class="alert-title">Drop Product</h4>\
          <p>This product quantity has been reduced</p>\
          </div>';
        }


        NotifContent = notify_type,
        autoClose = true;
        type = 'success';
        method = 3000;
        position = 'top';
        container ='';
        style = 'box';

        generate('topRight', '', NotifContent);
        $('#icon-identifier-search').addClass( "fa-search" ).removeClass( "spinner fa-spinner" );
        refreshContainers(asn_id,box_id);
        return;
      }
    });
  }



//----------------------------------
      // Add ASN Functions
//----------------------------------


  $('#sender_id').on('change', function (e) {

    $("#sender_location_details").html('');
    var vendor_name = $(this).find('option:selected').attr('data-vendor-contact');
   // var name = vendor_name.toUpperCase().replace(/[^A-Z]/g, '');

    var namestr = vendor_name.replace(/\b[a-z]/g, function ($0) {
        return $0.toUpperCase();
    });

    var matches = namestr.match(/\b(\w)/g);
    var name = matches.join('');

    var random_no = Math.floor(100 + Math.random() * 900);
    var date =$('#today_date').val();
    var asn_id = "ASN-"+name+"-"+date+random_no;

    $("#asn_number").html(asn_id);
    $("#asn_id").val(asn_id);

    var vendor_type = $(this).find('option:selected').attr('data-vendor-type');
    var location_url = '';
    if(vendor_type == "organization") {
      location_url = "/operations/api/dropdown/locations/org";
    } else {
      location_url = "/operations/api/dropdown/locations";
    }

    var old_sender_loc = $("#old-sender-location").val();

    $.getJSON(""+location_url+"",
    { option: $(this).val() },
    function(data) {
      var model = $('#sender_location_id');
      model.empty();
      model.append('<option value="0">Select a Location</option>');
      $.each(data, function(index, element) {

        model.append("<option value='"+element.id+"'>" + element.short_name + "</option>");
        if(old_sender_loc==element.id) {
          $("#sender_location_id").val(old_sender_loc).trigger('change');
        }

        if(element.is_primary == 1){
          model.val(element.id).trigger('change.select2');
          getLocationAddress('#sender_location_details', element.id)
        }

      });

      model.select2("destroy"); // destroy first next initialize again
      model.select2({
        placeholder: "Select a Location"
      });

    });
  });


  $('#receiver_id').on('change', function (e) {

    $("#receiver_location_details").html('');
    var vendor_type = $(this).find('option:selected').attr('data-vendor-type');
    var location_url = '';
    if(vendor_type == "organization") {
      location_url = "/operations/api/dropdown/locations/org";
    } else {
      location_url = "/operations/api/dropdown/locations";
    }

    var old_recv_loc = $("#old-receiver-location").val();

    $.getJSON(""+location_url+"",
    { option: $(this).val() },
    function(data) {
      var model = $('#receiver_location_id');
      model.empty();
      model.append('<option value="0">Select a Location</option>');

      $.each(data, function(index, element) {

        model.append("<option value='"+element.id+"'>" + element.short_name + "</option>");
        if(old_recv_loc==element.id) {
          $("#receiver_location_id").val(old_recv_loc).trigger('change');
        }

        if(element.is_primary == 1){
          model.val(element.id).trigger('change.select2');
          getLocationAddress('#receiver_location_details', element.id)
        }

      });

      model.select2("destroy"); // destroy first next initialize again
      model.select2({
        placeholder: "Select a Location"
      });
    });
  });


  $('#sender_location_id').change(function(){

      getLocationAddress('#sender_location_details', $(this).val())
  });

  $('#receiver_location_id').change(function(){
    getLocationAddress('#receiver_location_details', $(this).val())
  });

  /*
  |--------------------------------------------------------------------------
  | Checkin Products Functions
  |--------------------------------------------------------------------------
  */

/*
  |--------------------------------------------------------------------------
  | Checkin Products Functions
  |--------------------------------------------------------------------------
  */


  /*
  |--------------------------------------------------------------------------
  | Key Press event for scan product
  |--------------------------------------------------------------------------
  */
  $('#add-identifier').keypress(function (e) {
    var key = e.which;
    if(key == 13)
    {
      checkProduct();
    }
  });

  function productInfo() {
     var csrfToken = $('[name=_token]').val();
     var identifier = $('#add-identifier').val();
     var asn_id = $('#asn_id').val();

     $.ajax({
      url: "/operations/asn/productinfo",
      headers: {
        'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{identifier:identifier, asn_id:asn_id},
      success:function(data){

          if(data!="") {

            $("#pro-id").val(data.product.productid);
            $("#asnpro-id").val(data.product.asnproduct_id);
            $("#pro-identifier").html(data.product.identifier);
            $("#pro-parent").html(data.product.base_product_id);
            $("#pro-stylefamily").html(data.product.style_family);
            $("#pro-brand").html(data.product.brand_name);
            $("#pro-gender").html(data.product.gender);
            $("#pro-color").html(data.product.color_name);
            $("#pro-description").html(data.product.description);
            $(".flagbutton").prop('disabled', false);
            $(".dropbutton").prop('disabled', false);
        }

      }
    });

  }

  /*
  |--------------------------------------------------------------------------
  | Check Product Function
  |--------------------------------------------------------------------------
  */
  function checkProduct() {

    //Remove the normal class and append a search class
    $('#icon-identifier-search').removeClass( "fa-search" ).addClass( "spinner fa-spinner" );

    //Get the identifier of the product
    var identifier = $('#add-identifier').val();
    var asn_id = $('#asn_id').val();

    if($('#box_id').val()!='') {
      var box_id = $('#box_id').val();
    } else {
      var box_id = '';
    }
    var csrfToken = $('[name=_token]').val();



    $.ajax({

      url: '/operations/asn/checkinproducts',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{identifier:identifier, asn_id:asn_id, box_id:box_id},
      success:function(data){

        if(data.return_type=='box') {

            if(data.exist_boxid) {
                  NotifContent = '<div class="alert alert-warning media fade in">\
                  <h4 class="alert-title">Box Scanned</h4>\
                  <p>Please close BOX '+data.exist_boxid+' first to check in BOX '+data.new_box+'</p>\
                  </div>',
                  autoClose = true;
                  type = 'error';
                  method = 3000;
                  position = 'top';
                  container ='';
                  style = 'box';

                  generate('topRight', '', NotifContent);

            }

            if(data.msg=="no_box") {

               swal({
                  title: "Box Scanned",
                  text: "Box ID is not found for scanning. Do you want to add this box as an Exception?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: 'btn-danger',
                  confirmButtonText: 'Add Exception',
                  cancelButtonText: "Cancel",
                  closeOnConfirm: true,
                  closeOnCancel: true
                },
                function(isConfirm) {
                  if (isConfirm) {
                    $("#product-modal-flag-options").val(1).trigger('change');
                    var identifier = $('#add-identifier').val();
                    $('#add-exception-identifier').val(identifier);
                    $('#myModal').modal('show');

                  } else {
                    swal("Cancelled", "You have cancelled adding an exception", "error");
                    $('#add-identifier').val('');
                    $("#icon-identifier-search").removeClass( "fa fa-fw icon icon--fumi spinner fa-spinner" ).addClass( "fa fa-fw fa-search icon icon--fumi" );
                  }
                });
            }

            if(data.countProducts >=0) {
              var $container = $('.asnProductsList');
              $("#box_id").val(data.box_id);
              $("#scan-box-id").html(data.box_id);
              $("#current-boxid").html(data.box_id);
              $("#quantity-count").html(data.quantity);
              $("#scan-products").html(data.countProducts);
              $('#add-identifier').val("");
              $("#close-box").show();
              $container.html(data.html);
              refreshBoxesCheckin(asn_id);

              if(data.box_id > 0){
                $('.hasActiveBox').removeClass('hidden');
                $('.noActiveBox').addClass('hidden');
              }
            }

        } else {

                if(data.countProducts == 0) {

                    swal({
                      title: "Product Scanned",
                      text: "Product is not found for scanning. Do you want to add this product as an Exception?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Add Exception',
                      cancelButtonText: "Cancel",
                      closeOnConfirm: true,
                      closeOnCancel: true
                    },
                    function(isConfirm) {
                      if (isConfirm) {

                        $("#product-modal-flag-options").val(2).trigger('change');
                        var identifier = $('#add-identifier').val();
                        $('#add-exception-identifier').val(identifier);
                        $('#myModal').modal('show');

                      } else {
                        swal("Cancelled", "You have cancelled adding an exception", "error");
                        $('#add-identifier').val('');
                        $("#icon-identifier-search").removeClass( "fa fa-fw icon icon--fumi spinner fa-spinner" ).addClass( "fa fa-fw fa-search icon icon--fumi" );
                      }
                    });
                }
                else if(data.countProducts == 1) {

                  NotifContent = '<div class="alert alert-success media fade in">\
                  <h4 class="alert-title">Product Scanned</h4>\
                  <p>This product has been scanned and status updated to checkin</p>\
                  </div>',
                  autoClose = true;
                  type = 'success';
                  method = 3000;
                  position = 'top';
                  container ='';
                  style = 'box';

                  generate('topRight', '', NotifContent);
                  $('#icon-identifier-search').addClass( "fa-search" ).removeClass( "spinner fa-spinner" );
                  productInfo();
                  $('#add-identifier').val("");
                  refreshProducts(asn_id,box_id);
                  return;
                }
                else if(data.countProducts == 2) {

                    swal({
                      title: "Product Scanned",
                      text: "More quantity is received than is expected. Do you want to increase the product quantity as an Exception?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Add Exception',
                      cancelButtonText: "Cancel",
                      closeOnConfirm: true,
                      closeOnCancel: true
                    },
                    function(isConfirm) {
                      if (isConfirm) {

                        $("#product-modal-flag-options").val(3).trigger('change');
                        var identifier = $('#add-identifier').val();
                        $('#add-exception-identifier').val(identifier);
                        $('#myModal').modal('show');

                      } else {
                        swal("Cancelled", "You have cancelled adding an exception", "error");
                        $('#add-identifier').val('');
                        $("#icon-identifier-search").removeClass( "fa fa-fw icon icon--fumi spinner fa-spinner" ).addClass( "fa fa-fw fa-search icon icon--fumi" );
                      }
                    });
                }
            }
          }
    });
  }

    /*
  |--------------------------------------------------------------------------
  | Refresh Products Function
  |--------------------------------------------------------------------------
  */

  function refreshProducts(asn_id,box_id){
    var csrfToken = $('[name=_token]').val();
    var $request = $.ajax({
      url: "/operations/asn/refreshContainer",
      headers: {
        'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{asn_id:asn_id, box_id:box_id, type:'checkin'},
    });
    var $container = $('.asnProductsList');
    $request.done(function(data) { // success

      $("#session-total-scanned").html(data.scannedProducts);
      $("#scan-products").html(data.countProducts);
      $("#session-total-products").html(data.totalProducts);

      $container.html(data.html);
    });

  }

    /*
  |--------------------------------------------------------------------------
  | Close box Function
  |--------------------------------------------------------------------------
  */

  $("#close-box").on('click',function(){
    var asn_id = $('#asn_id').val();
    if($('#box_id').val()=="" || $('#box_id').val()=='0') {
      alert("Please enter a Box first");
      return;
    }
    var box_id = $('#box_id').val();
    var csrfToken = $('[name=_token]').val();

    swal({
      title: "Close Checkin",
      text: "Are you sure the you want to close checkin for this box?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: 'btn-success',
      confirmButtonText: 'Close Checkin',
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {

        $.ajax({
          url: "/operations/asn/checkin/checkoutBox",
          headers: {
            'X-CSRF-Token': csrfToken
          },
          type:'post',
          data:{asn_id:asn_id, box_id:box_id },
          success:function(data){

            if(data.box_status == 1) {

                var notify_text = '<div class="alert alert-success media fade in">\
                <h4 class="alert-title">Close Checkin </h4>\
                <p>This Box has been Closed for Checkin</p>\
                </div>'

            } else if(data.box_status == 2) {
              var notify_text = '<div class="alert alert-warning media fade in">\
              <h4 class="alert-title">Box Deleted </h4>\
              <p>This Box has been deleted for having no products</p>\
              </div>'

            }
            else {

              var notify_text = '<div class="alert alert-warning media fade in">\
              <h4 class="alert-title">Close Checkin</h4>\
              <p>No Box Found</p>\
              </div>'
            }

              NotifContent = notify_text,
              autoClose = true;
              type = 'success';
              method = 3000;
              position = 'top';
              container ='';
              style = 'box';

              $('#add-box').modal('hide');
              generate('topRight', '', NotifContent);
              $("#close-box").hide();
              $('tbody td').css('display','table-cell');
              $('.asnProductsList').html('');
              $("#box_id").val('');
              $("#scan-box-id").html('0');
              $("#current-boxid").html('0');
              $("#quantity-count").html('0');
              $("#scan-products").html('0');
              $('#add-identifier').val("");
              $('#session-total-box').html(data.total_box_count);
              refreshBoxesCheckin(asn_id,box_id);

            }

        });

      } else {
        swal("Cancelled", "You have cancelled closing the box", "error");
      }
    });

  })

   /*
  |--------------------------------------------------------------------------
  | Refresh boxes Function
  |--------------------------------------------------------------------------
  */
  function refreshBoxesCheckin(asn_id){
    var csrfToken = $('[name=_token]').val();
    var box_id = $("#box_id").val();
    var $request = $.ajax({
      url: "/operations/asn/checkin/refreshBoxes",
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{asn_id:asn_id, box_id:box_id},
    });
    var $container = $('.asnBoxes');
    $request.done(function(data) { // success
    $container.html(data.html);
    $("#quantity-count").html(data.quantity);
    });

  }

  /*
  |--------------------------------------------------------------------------
  | Add Product Exception
  |--------------------------------------------------------------------------
  */

  $(document).on("click", "#exception-btn", function () {

    $("#product-modal-flag-options").val(0).trigger('change');
    var identifier = $('#add-identifier').val();
    $('#add-exception-identifier').val(identifier);
  })

  $(document).on("click", "#liveshoot-add-exception-bt", function () {

    var identifier = $('#add-exception-identifier').val();
    var options = document.getElementById('product-modal-flag-options').value;
    if(options != 0 && identifier!=''){
        addProductException();
    }else{
        alert('Missing options');
        return;
    }

    function addProductException(){

        var csrfToken = $('[name=_token]').val();
        var asn_id = $('#asn_id').val();
        var box_id = $('#box_id').val();
        var identifier = $('#add-exception-identifier').val();

        var addtype = document.getElementById('product-modal-flag-options').value;

         var data = {
            "identifier": identifier,
            "asn_id": asn_id,
            "box_id": box_id,
            "addtype": addtype
        };


        if (asn_id) {
            $.ajax({
                url: '/operations/asn/checkin/addexception',
                method: 'post',
                headers: {
                    'Content-Type': 'application/hal+json',
                    'X-CSRF-Token': csrfToken
                },
                data: JSON.stringify(data),
                success: function (file) {

                   if(file.err) {
                      swal({
                        title: "Error",
                        text: file.err,
                        type: "error",
                        timer: 2500,
                        showConfirmButton: false
                        });
                   } else {

                      swal({
                            title: 'Exception added',
                            text: 'Exception added successfully!',
                            timer: 2500,
                            type: "success",
                            showConfirmButton: false
                        });
                        $('#myModal').modal('hide');
                        $('#add-identifier').val("");
                        $('#add-exception-identifier').val("");
                        $("#product-modal-flag-options").val(0).trigger('change');
                        $('#session-total-box').html(file.total_box_count);
                        refreshProducts(asn_id,box_id);
                        refreshBoxesCheckin(asn_id,box_id);

                   }

                }
            });
        }
    }
});
