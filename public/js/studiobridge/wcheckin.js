$(document).ready(function() {    

    $('#warehouse-identifier-in').keypress(function (e) {
        var key = e.which;
        if(key == 13)
        {          
          triggerItW(0, 0);
        }
    });

    function triggerItW(reshoot, onload){
      //Remove the normal class and append a search class
      $('#warehouse-identifier-in').prop('disabled', true);

      //Get the identifier of the product
      var identifier = $('#warehouse-identifier-in').val();
      var last_scanned_identifier = $('#liveshoot-last-scanned_identifier').val();

      //Check if identifier is empty
      if(jQuery.trim(identifier).length < 1 && !onload){
        //alert('No product scanned yet');
        swal({
          title: "No identifier entered",
          text: "Please enter an identifier to continue",
          timer: 1000,
          showConfirmButton: false
        });
        $('#warehouse-identifier-in').prop('disabled', false);
        $('#warehouse-identifier-in').removeProp("disabled");
        return;
      }      

      

      var data = {"identifier":jQuery.trim(identifier)};
      

      var csrfToken = $('[name=_token]').val();

      //make the ajax call and check if product is found
      $.ajax({
        url: '/warehouse/checkin/store',
        method: 'POST',
        headers: {
          //'Content-Type': 'application/hal+json',
          'X-CSRF-Token': csrfToken
        },
        //data: JSON.stringify(data),
        data: data,
        success: function (node) {

            if(node==''){
                NotifContent = '<div class="alert alert-warning media fade in">\
                <h4 class="alert-title">Product Not Found</h4>\
                <p>This product is not found in the system</p>\
                </div>',
                autoClose = true;
                type = 'warning';
                method = 3000;
                position = 'top';
                container ='';
                style = 'box';
            }
            else{

              //display information about the current product
              
              var product = node['product'];
              product = product[0];
              if(product.type=='unmapped'){
                NotifContent = '<div class="alert alert-warning media fade in">\
                <h4 class="alert-title">Unmapped Product</h4>\
                <p>This product has been added to your warehouse as an unmapped product</p>\
                </div>',
                autoClose = true;
                type = 'warning';
                method = 3000;
                position = 'top';
                container ='';
                style = 'box';
              } else{
                NotifContent = '<div class="alert alert-success media fade in">\
                <h4 class="alert-title">Mapped Product</h4>\
                <p>This product has been added to your warehouse as a mapped product</p>\
                </div>',
                autoClose = true;
                type = 'success';
                method = 3000;
                position = 'top';
                container ='';
                style = 'box';
              }
            }
          generate('topLeft', '', NotifContent);
          $('#warehouse-identifier-in').prop('disabled', false);

          //Clear the Workspace
          LiveShootWorkspace(0);

          setTimeout(function(){ window.location.reload(); }, 3000);
          

        },
        error: function(err){
          alert('Failed!' + err);
        }

      });
    }

    
    $( "ul.media-list" ).on( "click", "a.text-danger", function() {
        $prod_id = $( this ).data('id');
        swal({
          title: "Delete ",
          text: "Are you sure you want to delete this?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#F44336",
          confirmButtonText: "Delete",
          cancelButtonText: "Cancel",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            window.location.replace("/warehouse/destroyin/"+$prod_id);
          }
        });
    });

    $(".equalheight").equalHeights();
  });
