/**
 * Created by krishna on 29/12/16.
 */
$(document).on("click", ".session-view-drop-product", function () {
    var product_id = this.getAttribute('data-id');
    var status = this.getAttribute('data-drop');

    //alert(product_id+'---'+status); return;


    var confirm_title =  'Drop Product Confirmation';
    var confirm_text =  'Are you sure you want to drop this product from current session?';
    var confirm_button_text =  'Drop Product';

    if(status == 0){
        confirm_title =  'Restore Product Confirmation';
        confirm_text =  'Are you sure you want to restore this product in the current session?';
        confirm_button_text =  'Restore Product';
    }

    swal({
            title: confirm_title,
            text: confirm_text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#F44336",
            confirmButtonText: confirm_button_text,
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                dropProduct(status, product_id);
            }
        });



    function dropProduct(status, product_id){

        var csrfToken = $('[name=_token]').val();
        var psession_id = $('#psession_id').val();
        var psession_uid = $('#psession_uid').val();

        var status_text = 'This product has been sucessfully dropped from current session.';
        var status_title = "Product Dropped";


        var data = {
            "product_id": product_id,
            "psession_id": psession_id,
            "psession_uid": psession_uid,
            "status": status
        };

        if(status == 0){
            status_text = 'This product has been sucessfully restored into your current session.';
            status_title = 'Product Restored';
        }


        if (product_id && psession_id) {
            $.ajax({
                url: '/psession/product/drop',
                method: 'post',
                headers: {
                    'Content-Type': 'application/hal+json',
                    'X-CSRF-Token': csrfToken
                },
                data: JSON.stringify(data),
                success: function (file) {
                    console.log(file);

                    if (file.err) {
                        swal({
                            title: "Error Dropping Product",
                            text: file.err,
                            type: "error",
                            timer: 1000,
                            showConfirmButton: false
                        });
                    } else {
                        swal({
                            title: status_title,
                            text: status_text,
                            timer: 1000,
                            type: "success",
                            showConfirmButton: false
                        });
                        updateProductLeftBlock(file, status);
                    }
                },
                error: function () {
                    swal({
                        title: "Error",
                        text: "There was an error, please try again.",
                        type: "error",
                        timer: 2500,
                        showConfirmButton: false
                    });
                }

            });
        }
    }

    function updateProductLeftBlock(file, status){
        location.reload();
        //var psession_id = $('#psession_id').val();

        //window.location = "/psession"+psession_id;

        //        document.getElementById('session-total-products').innerHTML =  file.total;
        //        document.getElementById('liveshoot-Unmapped').innerHTML =  file.unmapped;
        //        document.getElementById('liveshoot-drop').innerHTML =  file.drop;
        //
        //        if(status == 1){
        //            document.getElementById('liveshoot-drop-product').innerHTML =  'Restore';
        //            $('#liveshoot-drop-product').addClass('btn-success').removeClass('btn-danger');
        //            $('#liveshoot-drop-product').attr( 'data-drop',0);
        //        }else if(status == 0){
        //            document.getElementById('liveshoot-drop-product').innerHTML =  'Drop';
        //            $('#liveshoot-drop-product').addClass('btn-danger').removeClass('btn-success');
        //            $('#liveshoot-drop-product').attr( 'data-drop',1);
        //        }
    }

});
