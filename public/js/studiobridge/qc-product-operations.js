// /**
// * Created by krishna on 28/12/16.
// */
//
//
//
//
//
// /*
// * Approve/Reject process a product to api
// */
// function flagProduct(product_id, psession_id, message, status){
//
//     var csrfToken = $('[name=_token]').val();
//     //        var message =  tinyMCE.activeEditor.getContent();
//     var uid = document.getElementById('qc-uid').value;
//
//     var msg_title = 'Product Approved';
//     var msg_text = 'Product approved successfully!';
//     var msg_type = 'success';
//
//     if(status == 0){
//         msg_title = 'Product Rejected';
//         msg_text = 'Product rejected successfully!';
//         msg_type = 'success';
//     }
//
//
//     var data = {
//         "product_id": product_id,
//         "psession_id": psession_id,
//         "status": status,
//         "message": message,
//         "operation_on": 'product',
//         "path": "view-qc",
//         "uid": uid
//     };
//
//
//     if (product_id && psession_id) {
//         $.ajax({
//             url: '/qc/operation/product',
//             method: 'post',
//             headers: {
//                 'Content-Type': 'application/hal+json',
//                 'X-CSRF-Token': csrfToken
//             },
//             data: JSON.stringify(data),
//             success: function (file) {
//                 console.log(file);
//                 swal({
//                     title: msg_title,
//                     text: msg_text,
//                     timer: 1500,
//                     type: msg_type,
//                     showConfirmButton: false
//                 });
//                 if(status == 1){
//                     $('#approve-product').modal('hide');
//                 }else{
//                     $('#reject-product').modal('hide');
//                 }
//                 //reject-product
//                 tinyMCE.activeEditor.setContent('');
//                 //document.getElementById('live-shoot-modal-flag-options').value = 0;
//                 //'qc-product-' + product_id
//                 document.getElementById('qc-product-' + product_id).remove();
//                 document.getElementById('imagecontainer').innerHTML = '';
//                 document.getElementById('qc-selected-product-title').innerHTML = '';
//             },
//             error: function () {
//                 swal({
//                     title: "Error",
//                     text: "There was an error, please try again.",
//                     type: "error",
//                     timer: 2500,
//                     showConfirmButton: false
//                 });
//             }
//
//         });
//     }
// }
//
// /*
// * Approve/Reject process a image to api
// */
// function flagImage(product_id, psession_id, image_id, status){
//
//     var csrfToken = $('[name=_token]').val();
//     //        var message =  tinyMCE.activeEditor.getContent();
//     var uid = document.getElementById('qc-uid').value;
//
//     var msg_title = 'Image Approved';
//     var msg_text = 'Image approved successfully!';
//     var msg_type = 'success';
//
//     if(status == 0){
//         msg_title = 'Image Rejected';
//         msg_text = 'Image rejected successfully!';
//         msg_type = 'success';
//     }
//
//
//     var data = {
//         "product_id": product_id,
//         "psession_id": psession_id,
//         "image_id": image_id,
//         "status": status,
//         "operation_on": 'image',
//         "path": "view-qc",
//         "uid": uid
//     };
//
//
//     if (product_id && psession_id) {
//         $.ajax({
//             url: '/qc/operation/product/img',
//             method: 'post',
//             headers: {
//                 'Content-Type': 'application/hal+json',
//                 'X-CSRF-Token': csrfToken
//             },
//             data: JSON.stringify(data),
//             success: function (file) {
//                 console.log(file);
//                 swal({
//                     title: msg_title,
//                     text: msg_text,
//                     timer: 500,
//                     type: msg_type,
//                     showConfirmButton: false
//                 });
//                 //$('#approve-product').modal('hide');
//                 //tinyMCE.activeEditor.setContent('');
//                 //document.getElementById('live-shoot-modal-flag-options').value = 0;
//             },
//             error: function () {
//                 swal({
//                     title: "Error",
//                     text: "There was an error, please try again.",
//                     type: "error",
//                     timer: 2500,
//                     showConfirmButton: false
//                 });
//             }
//
//         });
//     }
// }
