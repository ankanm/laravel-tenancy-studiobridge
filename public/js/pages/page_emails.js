$(function() {
	'use strict';

	tinymce.init({
	  selector: '.basic-editor',
	  height: 200,
	  menubar: false,
	  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
	});

});
