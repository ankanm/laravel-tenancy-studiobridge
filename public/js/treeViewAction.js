$(window).load(function(){
    setTimeout(function(){
        var org_id = $("#org_id").val();

        if (typeof org_id !== "undefined") {

            $('#treeview-checkbox-demo').treeview({
                debug: true,
                data: $("#org_module_values").val().split(',')
            });

        } else {
            $('#treeview-checkbox-demo').treeview({
                debug: true
            });
        }
    }, 2000);
});