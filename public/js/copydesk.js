
var filtered_products = 0;
var total_products = 0;

$(function() {
  refreshProductCounts()
});

$('body').addClass('sidebar-collapsed')

$('.filter-products').keyup(function(){
  var valThis = $(this).val().toLowerCase();
  filterProducts(valThis)
});

function filterProducts(val) {
  var filtered = 0;
  if(val == ""){
    $('.qc-master-list > li').show();
    filtered_products = total_products
    $('.filtered-products').html(filtered_products)
  } else {
    $('.qc-master-list > li').each(function(){
      var text = $(this).text().toLowerCase();
      if(text.indexOf(val) >= 0) {
        $(this).show()
        filtered++;
      } else {
        $(this).hide();
      }
    });
    $('.filtered-products').html(filtered)
  };

  $('.total-products').html(total_products)

}

function refreshProductCounts(){
  total_products =  $('.pending-copy-product').length;
  $('.total-products').html(total_products)
  $('.filtered-products').html(total_products)

}

function checkforUpdates(){

  var numItems = $('.pending-copy-product').length;
  $('.total-products').html(total_products)
  var csession = $('#csession-id').val();

  var $request = $.get("/copywriting/"+csession+"/refresh/count"); // make request

  $request.done(function(data) { // success

    if(data > numItems){
      console.log(data - numItems)
      $('.btn-toolbar-refresh').addClass('bg-blue');
      $('.refresh-product-count').html('There are ' + (data - numItems) + ' new products')
      $('.btn-toolbar-refresh').addClass('animated slideInDown').removeClass('hidden');
    }
  });
}

function clearRefreshVar(){
  $('.btn-toolbar-refresh').removeClass('bg-blue');
  $('.refresh-product-count').html()
  $('.btn-toolbar-refresh').addClass('hidden')
}

//setInterval(checkforUpdates, 5000);

$(".copy-refresh-btn").on("click",function(){
  $('.filter-products').val()
  filterProducts();
  refreshProducts();
  clearRefreshVar();
});

/* Check All Products */
$(document).on("click", ".check-all-products", function(){
  $(this).removeClass('check-all-products').addClass('uncheck-all-products').html('Uncheck All');
  $('.todo-list li').removeClass('bounceInDown').addClass("done");
  $('input').iCheck('check');
});

/* Uncheck All Products */
$(document).on("click", ".uncheck-all-products", function(){
  $(this).removeClass('uncheck-all-products').addClass('check-all-products').html('Check All');
  $('.todo-list li').removeClass("done");
  $('input').iCheck('uncheck');
});

/* Add product */
//copywriting-product-check
$(document).on("click", ".addtosession", function(){
  var csrfToken = $('[name=_token]').val();
  var csession = $('#csession-id').val();
  var checked_products = [];

  $("input:checkbox[name=check-product-id]:checked").each(function(){
    checked_products.push($(this).val());
  });

  if(checked_products.length == 0){
    swal({
      title: "No Products Selected",
      text: "You have not selected any products, please select atleast one product to add.",
      type: "warning"
    });
  } else{

    $.ajax({
      url: '/copywriting/session/products/store',
      method: 'POST',
      headers: {
        'X-CSRF-Token': csrfToken
      },
      data: {
        "products": checked_products,
        "csession": csession,

      },
      success: function (node) {

        swal({
          title: "Products added to Session",
          text: "Your selected products have been added to this copywriting session",
          timer: 1000,
          type: "success",
          showConfirmButton: false
        });

        $('.copyProducts').html(node);
      },
      error: function(){
        alert('Failed! **');
      }

    });

  }
});

/* Get product */
//copywriting-product-check
$(document).on("click", ".pending-copy-product", function(){
  var csrfToken = $('[name=_token]').val();
  var csession = $('#csession-id').val();
  var product_id = $(this).attr('id');

  $.ajax({
    url: '/copywriting/session/products/write/product',
    method: 'POST',
    headers: {
      'X-CSRF-Token': csrfToken
    },
    data: {
      "product": product_id,
      "csession": csession,
      "session_type" : "copy",
    },
    success: function (node) {


      $('.productCopy').html(node);
      renderMCE();

      var $slider = $('.slickSlider')
      .on('init', function(slick) {
        console.log('fired!');
        $('.slickSlider').fadeIn(3000);
      })
      .slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
      });

    },
    error: function(){
      alert('Failed! **');
    }

  });


});

// Percentage sum check
function addPercentage() {

  // initialize the sum (total price) to zero
  var sum = 0;
  $('.percentage').each(function() {
    if($(this).val()=="") {
      alert("Percentage field cannot be blank")
      return false;
    }
    sum += Number($(this).val());
  });

  if(sum >100) {
    alert("Total material composition cannot exceed 100%")
    return false;
  } else {
    return true;
  }
}

// Onchange Percentage check
$(document).on("keyup",".percentage",function(){

  var count = $("#count-material").val();

  var pcheck = addPercentage();
  if(pcheck==false) {

    $("#percentage-"+count).val("");
    $("#percentage-"+count).focus();

  }

});

// Add material div
$(document).on("click", ".addmaterialbtn", function(){

  var count_material = $("#count-material").val();

  if($("#percentage-"+count_material).val()=="") {
    alert("Percentage field cannot be blank !!")
    $("#percentage-"+count_material).focus();
    return false;
  }

  var count = parseInt(count_material) + 1;

  $("#material-add").append('<span id="materialdiv-'+count+'" class="materialinfo row"><div class="col-sm-2 p-0"><input type="text" class="form-control percentage col-sm-2" id="percentage-'+count+'" size="1" placeholder="%"></div><div class="col-sm-9 p-0 p-l-10 p-r-10"><select name="material" id="material-'+count+'" class="form-control select2 material" data-placeholder="Select Material"><option value = "cotton">Cotton</option><option value = "polyster">Polyster</option><option value = "silk">Silk</option></select></div><input type="button" btnid="reduce-'+count+'" class="reducematerialbtn btn btn-default col-sm-1" value="X"></span>');

  $('#material-'+count).select2({
    minimumResultsForSearch: Infinity,
    allowClear: true
  });

  $("#count-material").val(count);

});

// Remove material div
$(document).on("click", ".reducematerialbtn", function(){
  var btnid = $(this).attr("btnid");
  var btnArr=btnid.split("-");
  var count = btnArr[1];
  $("#materialdiv-"+count).remove();
});

/* Save Draft */
//copywriting-product-check
$(document).on("click", ".save-product-draft", function(){
  $(".titleerror").html("");
  $(".descerror").html("");
  var csrfToken = $('[name=_token]').val();
  var csession = $('#csession-id').val();
  var product_id = $(this).attr('id');
  var product_title = $('.product_title').val();
  var product_description = $('.product_copy').val();
  var product_description = tinyMCE.get('description').getContent();

  if(product_title=="") {
    $(".titleerror").html("Please enter Product Title");
    $(".product_title").focus();
    return false;
  }

  if(product_description=="") {
    $(".descerror").html("Please enter Product Description");
    $(".product_copy").focus();
    return false;
  }

  var additional_details = tinyMCE.get('design_details').getContent();
  var careinstructions = $('.careinstructions').val();
  var mcount = parseInt($("#count-material").val());
  var materialarr = new Array();

  // Ounass Mod
  var key_details = tinyMCE.get('key_details').getContent();
  var ingredients = tinyMCE.get('ingredients').getContent();
  var size_and_fit = tinyMCE.get('size_and_fit').getContent();
  var dimensions = tinyMCE.get('dimensions').getContent();
  var frame_type = $('.frame_type').val();
  var lens_type = $('.lens_type').val();
  var hundredml_restriction = $('.hundredml_restriction').val();
  var hair_concern = $('.hair_concern').val();
  var heel_height = $('.heel_height').val();
  var heel_style = $('.heel_style').val();
  var metal_stone = $('.metal_stone').val();
  var skin_concern = $('.skin_concern').val();

  var material_data = $('.materialinfo');

  for (i = 1; i <= mcount; i++) {

    var percentage = $("#percentage-"+i+"").val();
    var material = $("#material-"+i+"").val();

    if (typeof(percentage) != "undefined" && typeof(material) != "undefined") {
      var material_info = percentage+"-"+material;
    }
    materialarr.push(material_info);
  }

  materialarr = $.unique(materialarr);

  if($(this).attr("btntype")=="save") {
    var copy_state = '0';
  }

  if($(this).attr("btntype")=="submit") {
    var copy_state = '1';
  }

  $.ajax({
    url: '/copywriting/session/products/write/store',
    method: 'POST',
    headers: {
      'X-CSRF-Token': csrfToken
    },
    data: {
      "product_id": product_id,
      "csession_id": csession,
      "product_title": product_title,
      "product_description":product_description,
      "additional_details":additional_details,
      "care_instructions":careinstructions,
      "material_composition":materialarr,
      "copy_state":copy_state,
      "key_details":key_details,
      "ingredients":ingredients,
      "size_and_fit":size_and_fit,
      "dimensions":dimensions,
      "frame_type":frame_type,
      "lens_type":lens_type,
      "hundredml_restriction":hundredml_restriction,
      "hair_concern":hair_concern,
      "heel_height":heel_height,
      "metal_stone":metal_stone,
      "heel_style":heel_style,
      "skin_concern":skin_concern,

    },
    success: function (node) {

      swal({
        title: "Draft Copy Saved",
        text: "A draft copy has been saved for this product",
        timer: 1000,
        type: "success",
        showConfirmButton: false
      });

      $('.productCopy').html(node);
      renderMCE();

      jQuery(function(){
        $('.select2').select2({
          minimumResultsForSearch: Infinity,
          allowClear: true
        });
      })

      if(copy_state=='1') {

        NotifContent = '<div class="alert alert-success media fade in">\
        <h4 class="alert-title">Product Submitted</h4>\
        <p>Product has been submitted</p>\
        </div>',
        autoClose = true;
        type = 'success';
        method = 3000;
        position = 'bottom';
        container ='';
        style = 'box';

        generate('topRight', '', NotifContent);

        refreshProducts();
        $('.productCopy').html('');



      }


    },
    error: function(){
      alert('Failed! **');
    }

  });


});

function refreshProducts(){
  //load the products
  var csession = $('#csession-id').val()

  var $request = $.get("/copywriting/session/"+csession+"/review/refresh"); // make request
  var $container = $('.qc-master-list');

  $('#status-qc').removeClass('hidden');

  $request.done(function(data) { // success
    $container.html(data.html);
    $('#session-total-products').html(data.total_products);
    refreshProductCounts();
    $('.qc-master-list li:first').click();
  });
}

function renderMCE(){
  tinymce.remove();
  tinymce.init({
    selector: '.basic-editor',
    height: 125,
    menubar: false,
    toolbar: 'undo | bullist',
    valid_elements: 'p,ul,li',
    plugins: "paste",
    paste_as_text: true,
    content_css: [
      '//www.tinymce.com/css/codepen.min.css'
    ]
  });

}

// $('.page-content').addClass('fixed');
