//******************************** CHAT MENU SIDEBAR ******************************//
function quickviewSidebar() {

    function toggleqQuickview(){
        $('#quickview-toggle').on('click', function(e){
            e.preventDefault();
            if($('#quickview-sidebar').hasClass('open')) $('#builder').removeClass('open');
            else $('#quickview-sidebar').addClass('open');
        });
    }

    $('.chat-back').on('click', function(){
        $('.chat-conversation').removeClass('current');
        $('.chat-body').addClass('current');
    });

    $('.chat-list').on('click', 'li', function(e){

        e.preventDefault();

        var url, request, tag, data;
        var chat_user_id  = $(this).find('.user-name').attr('data-id');

        url = __baseUrl + '/message/' + chat_user_id;

        request = $.ajax({
            method: "get",
            url: url,
        });

        request.done(function (response) {
            if (response.status == 'success') {
                $( "input[name='_id']").val(response.user.id)
                $('.chat-conversation .user-name').html(response.user.name);
                $('.chat-conversation .user-txt').html('hello');
                $('.chat-conversation .user-status').html('hello');
                $('.chat-conversation .user-img img').attr("src",response.user.avatar);
                $('.chat-conversation .conversation-body .conversation-img img').attr("src",response.user.avatar);
                $('.chat-conversation .conversation-body').html(response.html);


                $('.chat-body').removeClass('current');
                $('.chat-conversation').addClass('current');
                ResetsideBar();
            }
        });





    });

    /* Open / Close right sidebar */
    $('#quickview-toggle').on('click', function(){
        $('#chat-notification').hide();
        setTimeout(function () {
            $('.mm-panel .badge-danger').each(function () {
                $(this).removeClass('hide').addClass('animated bounceIn');
            });
        }, 1000);
    });

    /* Remove current message when opening */
    $('.have-message').on('click', function () {
        $(this).removeClass('have-message');
        $(this).find('.badge-danger').fadeOut();
    });

    /* Send messages */
    $('.send-message').keypress(function (e) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if (e.keyCode == 13) {

            e.preventDefault();
            var url, request, tag, data;
            tag = $('#talkSendMessage');
            url = __baseUrl + '/ajax/message/send';
            data = tag.serialize();

            immediate_message = tag.serializeArray();
            user_avatar = $('.userimage').attr('src');
            random_string = Math.floor((Math.random() * 129923882303782) + 1);

            if(immediate_message[0].value !== null || immediate_message[0].value !== '')
            {
                html_contruct = '<li class="img faded-message message-'+random_string+'" id="12">\
                <span>\
                    <div class="chat-detail chat-right">\
                        <img src="'+user_avatar+'" width="32px">\
                            <div class="chat-detail">\
                                <div class="chat-bubble">'+immediate_message[0].value+'</div>\
                                </div>\
                            </div>\
                        </span>\
                    </li>';

                    $('.conversation-body ul').append(html_contruct).fadeIn()

                }

                request = $.ajax({
                    method: "post",
                    url: url,
                    data: data + '&message-id=' + random_string
                });
                
                request.done(function (response) {
                    if (response.status == 'success') {
                        $('#message-data').val('');
                        $('.message-'+response.messageId).removeClass('faded-message');
                        quickviewHeight();
                        customScroll();
                        $('.chat-container').animate({scrollTop: $('.chat-container').prop("scrollHeight")}, 200);
                    }
                });

            }
        });


        content.addEventListener('click', function(ev) {
            chatSidebar = document.getElementById('quickview-sidebar');
            var target = ev.target;
            if( target !== chatSidebar ) {
                if($('#quickview-sidebar').hasClass('open')){
                    $('#quickview-sidebar').addClass('closing');
                    $('#quickview-sidebar').removeClass('open');
                    setTimeout(function(){
                        $('#quickview-sidebar').removeClass('closing');
                    },400);
                }
            }
        });

        document.querySelector('.sidebar').addEventListener('click', function(ev) {
            chatSidebar = document.getElementById('quickview-sidebar');
            var target = ev.target;
            if( target !== chatSidebar ) {
                if($('#quickview-sidebar').hasClass('open')){
                    $('#quickview-sidebar').addClass('closing');
                    $('#quickview-sidebar').removeClass('open');
                    setTimeout(function(){
                        $('#quickview-sidebar').removeClass('closing');
                    },400);
                }
            }
        });

        if($('.settings-chart .progress-bar').length) {
            $('.settings-tab').on('click', function () {
                setTimeout(function () {
                    $('.settings-chart .setting1').progressbar();
                    if($('#setting-chart').length){
                        window.myRadar = new Chart(document.getElementById("setting-chart").getContext("2d")).Radar(radarChartData, {
                            responsive: true,
                            tooltipCornerRadius:0,
                            animationSteps: 60,
                        });
                    }
                }, 200);
                setTimeout(function () {
                    $('.settings-chart .setting2').progressbar();
                }, 400);
            });
        };

        /* Radar Chart */
        var radarChartData = {
            labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
            datasets: [
                {
                    label: "My Second dataset",
                    backgroundColor: "rgba(151,187,205,0.2)",
                    borderColor: "rgba(151,187,205,1)",
                    pointBackgroundColor: "rgba(151,187,205,1)",
                    pointBorderColor: "#fff",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "rgba(151,187,205,1)",
                    data: [38,48,40,89,96,27,90]
                }
            ]
        };

        toggleqQuickview();
    }

    function quickviewHeight(){
        $('.chat-conversation').height('');
        chatConversationHeight = $('.chat-conversation').height();
        windowHeight= $(window).height();
        if(chatConversationHeight < windowHeight) {
            $('.chat-conversation').height($(window).height() - 50);
        }
    }

    /****  Initiation of Main Functions  ****/
    $(document).ready(function () {

        quickviewSidebar();
        quickviewHeight();
        ResetsideBar();

    });

    /****  On Resize Functions  ****/
    $(window).resize(function () {
        noteTextarea();
        quickviewHeight();
        ResetsideBar();
    });

    function ResetsideBar(){

        var sidebarHeightChat = $('.sidebar-inner').height();
        $(".chat-conversation").css('height', sidebarHeightChat );

        //$(".quickview").css('height', sidebarHeightChat)
        $(".chat-container").slimscroll({
            size: '4px',
            color: 'rgba(0,0,0,.6)',
            distance: '0px',
            railVisible: false,
            railColor: 'rgba(255,255,255,.1)',
            railOpacity: .3,
            wheelStep: 20,
            borderRadius: '3px',
            railBorderRadius: '0px',
            allowPageScroll: false,
            opacity: 0,
            width:'100%',
            start: 'bottom',
        }).mouseover(function() {
            $(this).next('.slimScrollBar').css('opacity', 0.4);
        });

        $( ".quickview" ).scrollTop( 0 );
        $('.chat-container').animate({scrollTop: $('.chat-container').prop("scrollHeight")}, 500);
        //$(window).trigger('resize');

    }
