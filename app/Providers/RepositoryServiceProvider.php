<?php

namespace App\Providers;

use App\Repositories\Agency\AgencyRepository;
use App\Repositories\Agency\AgencyRepositoryInterface;
//use App\Repositories\Organization\OrganizationUserRepositoryInterface;
//use App\Repositories\Organization\OrganizationModuleRepositoryInterface;
use App\Repositories\Copy\CopyRepository;
use App\Repositories\Copy\CopyRepositoryInterface;
use App\Repositories\CSC\CSCRepository;
use App\Repositories\CSC\CSCRepositoryInterface;
use App\Repositories\Locations\LocationsRepository;
use App\Repositories\Locations\LocationsRepositoryInterface;
use App\Repositories\Module\ModuleRepository;
use App\Repositories\Module\ModuleRepositoryInterface;
use App\Repositories\Organization\OrganizationRepository;
use App\Repositories\Organization\OrganizationRepositoryInterface;
use App\Repositories\Ounass\OunassRepository;
use App\Repositories\Ounass\OunassRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {

        $this->app->bind(
            CopyRepositoryInterface::class,
            CopyRepository::class
        );

        $this->app->bind(
            AgencyRepositoryInterface::class,
            AgencyRepository::class
        );

        /*----------  Ounass Repository  ----------*/

        $this->app->bind(
            OunassRepositoryInterface::class,
            OunassRepository::class
        );

        /*----------  Organization Repository  ----------*/

        $this->app->bind(
            OrganizationRepositoryInterface::class,
            OrganizationRepository::class
        );

        /*----------  Module Repository  ----------*/

        $this->app->bind(
            ModuleRepositoryInterface::class,
            ModuleRepository::class
        );

        /*----------  Country-State-City Repository  ----------*/

        $this->app->bind(
            CSCRepositoryInterface::class,
            CSCRepository::class
        );

        /*----------  Organization Location Repository  ----------*/

        $this->app->bind(
            LocationsRepositoryInterface::class,
            LocationsRepository::class
        );

        /*----------  User Repository  ----------*/

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
    }

}
