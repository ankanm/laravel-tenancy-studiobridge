<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TenantServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $hostname = app(CurrentHostname::class);
        if ($hostname) {
            /** @var Connection $connection */
            $connection = app(Connection::class);

            Config::set('database.default',$connection->tenantName());
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
