<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Contracts\Foundation\Application;
use DB ;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //protected $app;

    // public function __construct(Application $app){
    //     $this->app = $app;
    // }

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    // public function index()
    // {
    //     return md5(sprintf(
    //         '%s.%d',$this->app['config']->get('app.key'),1
    //     ));
    //     echo DB::connection()->getConfig("password"); die ;
    // }

}
