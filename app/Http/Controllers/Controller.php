<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Modules\Organization\Models\Organization;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $organization_id;
    
    /**
     * get global org id
     *
     */
    
    public function getOrganizationId() {
    	$user = Auth::User();
	    $org = new Organization();
	    $organization = $org->getOrganization($user->id);
	    return $organization_id = $organization[0]->id;
    }

    /**
     *
     * vendor and agency signup mail
     *
     */
    
    public function user_signup_email($data_email) {

        Mail::send('emails.vendors.signup', $data_email, function($message) use ($data_email)
        {
            $message->from(config('studiobridge.system_emails.organization_created.from'));
            $message->to($data_email['email'],$data_email["username"]);
            $message->subject(config('studiobridge.system_emails.organization_created.subject_'.
            $data_email["type"].''),$data_email);
        });
        
    }
}
