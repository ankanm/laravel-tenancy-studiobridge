<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Modules\Psession\Models\Psession;
use App\Modules\Product\Models\Product;
use App\Modules\Comment\Models\Comment;
use App\Modules\ProductState\Models\ProductState;
use App\Modules\Image\Models\Image;
use App\Modules\Copywriting\Models\ProductCopywritingsession;
use App\Modules\Ounass\Models\Channel;

use Cache;

class DashboardController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct(Product $product)
  {
    $this->middleware('auth');
    $this->product = $product;
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    return view('general.dashboard');
  }

  public function generalDashboard()
  {
    //Collect the Data to be displayed on the dashboard

    //Check who is accessing the dashboard, if admin, show all session Activities

    $period = 'This Week';
    switch ($period) {
      case 'This Week':
      $fromDate = \Carbon\Carbon::now()->startOfWeek()->toDateString(); // or ->format(..)
      $tillDate = \Carbon\Carbon::now()->toDateString();
      break;
      case 'Last Week':
      $fromDate = \Carbon\Carbon::now()->startOfWeek()->subDays(7)->toDateString(); // or ->format(..)
      $tillDate = \Carbon\Carbon::now()->startOfWeek()->subDays(1)->toDateString(); // or ->format(..)
      break;
      default:
      $fromDate = \Carbon\Carbon::now()->startOfWeek()->toDateString(); // or ->format(..)
      $tillDate = \Carbon\Carbon::now()->toDateString();
    }

    $user = Auth::user();

    $cache_duration = '1';

    //Get the current week
    // $fromDate = \Carbon\Carbon::now()->startOfWeek()->toDateString(); // or ->format(..)
    // $tillDate = \Carbon\Carbon::now()->toDateString();

    $total_shot     = 0;
    $total_dropped  = 0;
    $total_reshoots = 0;
    $total_approved = 0;
    $total_rejected = 0;

    // $total_shot = Cache::remember('total_shot', $cache_duration, function() use($fromDate, $tillDate) {
    //   return ProductState::whereBetween(
    //     \DB::raw('date(created_at)'), [$fromDate, $tillDate])
    //     ->count();
    //   });
    //
    //   $total_approved = Cache::remember('total_approved', $cache_duration, function() use($fromDate, $tillDate) {
    //
    //     return ProductState::whereBetween(
    //       \DB::raw('date(created_at)'), [$fromDate, $tillDate])
    //       ->where('qc_done', '1')
    //       ->where('qc_state', '1')
    //       ->count();
    //     });
    //
    //     $total_rejected = Cache::remember('total_rejected', $cache_duration, function() use($fromDate, $tillDate) {
    //
    //       return ProductState::whereBetween(
    //         \DB::raw('date(created_at)'), [$fromDate, $tillDate])
    //         ->where('qc_done', '1')
    //         ->where('qc_state', '0')
    //         ->count();
    //       });
    $ounass_count = Channel::where('id_channel', '1')->first()->products()->count();
    $nass_count = Channel::where('id_channel', '2')->first()->products()->count();
    $map_count = Channel::where('id_channel', '3')->first()->products()->count();
    $nisnass_count = Channel::where('id_channel', '4')->first()->products()->count();


    $return_data = [
      //Session Info
      'from' => $fromDate,
      'to' => $tillDate,

      'total_shot' => $total_shot,
      'total_dropped' => $total_dropped,
      'total_approved' => $total_approved,
      'total_rejected' => $total_rejected,

      'ounass_count' => $ounass_count,
      'nass_count' => $nass_count,
      'map_count' => $map_count,
      'nisnass_count' => $nisnass_count,
    ];

    dd($return_data);

    return view('general.dashboard', $return_data);

  }

  public function renderDashboard($period)
  {

    //Collect the Data to be displayed on the dashboard

    //Check who is accessing the dashboard, if admin, show all session Activities

    $period = 'This Week';
    switch ($period) {
      case 'This Week':
      $fromDate = \Carbon\Carbon::now()->startOfWeek()->toDateString(); // or ->format(..)
      $tillDate = \Carbon\Carbon::now()->toDateString();
      break;
      case 'Last Week':
      $fromDate = \Carbon\Carbon::now()->startOfWeek()->subDays(7)->toDateString(); // or ->format(..)
      $tillDate = \Carbon\Carbon::now()->startOfWeek()->subDays(1)->toDateString(); // or ->format(..)
      break;
      default:
      $fromDate = \Carbon\Carbon::now()->startOfWeek()->toDateString(); // or ->format(..)
      $tillDate = \Carbon\Carbon::now()->toDateString();
    }

    $user = Auth::user();

    $cache_duration = '30';

    $graph_shot = [];
    $graph_approved = [];
    $graph_copy = [];

    $day_array = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

    foreach ($day_array as $day) {
      $shot_of_day = ProductState::whereBetween(\DB::raw('date(created_at)'), [$fromDate, $tillDate])
      ->where(\DB::raw('DAYNAME(created_at)'), [$day])
      ->count();
      $approved_of_day =  ProductState::whereBetween(\DB::raw('date(created_at)'), [$fromDate, $tillDate])
      ->where(\DB::raw('DAYNAME(created_at)'), [$day])
      ->where('qc_done', '1')
      ->where('qc_state', '1')
      ->count();
      $copy_of_day =  ProductCopywritingsession::whereBetween(\DB::raw('date(created_at)'), [$fromDate, $tillDate])
      ->where(\DB::raw('DAYNAME(created_at)'), [$day])
      ->where('qc_state', '1')
      ->groupBy('product_id')
      ->count();

      array_push($graph_shot, $shot_of_day);
      array_push($graph_approved, $approved_of_day);
      array_push($graph_copy, $copy_of_day);
    }




    $return_data = [
      //Session Info
      'from' => $fromDate,
      'to' => $tillDate,

      // 'recent_comments' => $recent_comments,
      // 'recent_activity' => $recent_activity,
      'graph_shot' => $graph_shot,
      'graph_approved' => $graph_approved,
      'graph_copy' => $graph_copy
    ];
    //return response()->json(compact('html'));

    $html = view('partials.dashboardmetrics', $return_data)->render();

    return response()->json(compact('html'));
  }
}
