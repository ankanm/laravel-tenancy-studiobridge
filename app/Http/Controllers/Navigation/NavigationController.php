<?php

namespace App\Http\Controllers\Navigation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Menu;
use App\User;
use App\Models\Role;
use App\Models\Navigation\NavigationMenu;
use App\Models\Module;
use App\Models\Organization\Organization;
use App\Models\Navigation\NavigationRole;
use App\Models\Organization\OrganizationUser;
use App\Models\Organization\OrganizationModule;
use App\Models\Navigation\NavigationCache;

class NavigationController extends Controller
{
    public function MakeNav(){



        $user=Auth::user();

        NavigationCache::where('user_id', $user->id)->delete();

        $user_role = $user->roles;
        $role_id = $user_role[0]->id;

        $organization = OrganizationUser::where('user_id',$user->id)
        ->select('organization_id')->first();

        $replace_array = ['{org_id}' => $organization['organization_id']];

        if(!empty($organization) || $user->hasRole('Super Admin')) {


            $navigation_ids = NavigationRole::where('role_id',$role_id)->pluck('navigation_id')->toArray();

            if($user->hasRole('Super Admin')) {
                $modules = Module::orderBy('weight')->pluck('id')->toArray();

            } else {
                $modules = OrganizationModule::where('organization_id', $organization['organization_id'])
                ->join('module', 'module.id', '=', 'module_organization.module_id')
                ->orderBy('module.weight')
                ->pluck('module_id')->toArray();
            }


            foreach ($modules as $module) {

                $navigations = NavigationMenu::where('module_id',$module)->where('enabled','1')
                ->get();

                foreach ($navigations as $navigation) {

                    if($user->hasRole('Super Admin') || (is_array($navigation_ids) && in_array($navigation['id'], $navigation_ids))) {

                        $itemArr = explode(" ",$navigation['item_name']);
                        $item_part1 = lcfirst($itemArr[0]);
                        $item_part2 = "";
                        if(!empty($itemArr[1])) {
                            $item_part2 = ucfirst($itemArr[1]);
                        }

                        $item_name = $item_part1."".$item_part2;

                        $item_url = strtr($navigation['active_url'],$replace_array);

                        //ADD TO NAVCACHE

                        $create_nav_cache_parent = new NavigationCache;

                        $create_nav_cache_parent->user_id = $user->id;
                        $create_nav_cache_parent->item_name = $navigation['item_name'];
                        $create_nav_cache_parent->item_icon = $navigation['item_icon'];
                        $create_nav_cache_parent->is_parent = $navigation['is_parent'];
                        $create_nav_cache_parent->child_of = $navigation['child_of'];
                        $create_nav_cache_parent->active_url = $item_url;

                        $create_nav_cache_parent->save();
                        $parent_id = $create_nav_cache_parent->id;

                        // $menu->add($navigation['item_name'])->active(''.$item_url.'')
                        // ->prepend(' <i class="'.$navigation['item_icon'].'"></i><span class="">')
                        // ->append('</span>');
                        $navigation_items = NavigationMenu::where('child_of',$navigation['id'])->get();

                        if(!empty($navigation_items)) {

                            foreach ($navigation_items as $item) {

                                $item_url = strtr($item['active_url'],$replace_array);

                                // $menu->item($item_name)->add($item['item_name'], $item_url)
                                // ->active($navigation['active_url']);

                                //ADD TO NAVCACHE

                                $create_nav_cache = new NavigationCache;

                                $create_nav_cache->user_id = $user->id;
                                $create_nav_cache->item_name = $item['item_name'];
                                $create_nav_cache->is_parent = $item['is_parent'];
                                $create_nav_cache->child_of = $parent_id;
                                $create_nav_cache->active_url = $item_url;

                                $create_nav_cache->save();
                            }

                        }
                    }
                }
            }

            // Organizations Menu for Super Admin
            // if($user->hasRole('Super Admin')) {
            //     // Organizations Menu
            //     $menu->add('Organizations')->active('organizations/*')
            //     ->prepend(' <i class="icon-tag"></i><span class="">')
            //     ->append('</span>');
            //     $menu->item('organizations')->add('Add organization', 'organization/add')->active('organizations/my');
            //     $menu->item('organizations')->add('All Organizations', 'organizations')->active('organizations/all');
            // }

        }





    }
}
