<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Models\License;
use App\Models\Organization\Organization;
use App\Models\PermissionRole ;

class RouteByRole
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */

    public function handle($request, Closure $next)
    {

        // current actual route path
        //echo  $currentPath= Route::getFacadeRoot()->current()->uri(); die;

        $admin = 'Super Admin';
        if ($request->user()) {
            $user_roles = $request->user()->roles;
        }

        $user = $request->user(); 

        $currentPath= Route::getFacadeRoot()->current()->uri();
        $PermissionRole = new PermissionRole(); 

        //If User is not logged in, redirect to login.
        //Anything past this point, user must be logged in
        if(!isset($user)){
            return redirect('/login');
        }


        //For these roles, return forbidden error
        if (($user->hasRole('Make Up Artist') || $user->hasRole('Model'))) {
            return abort(403);
        }

        $user_license = License::where('user_id',$user['id'])->where('is_active','1')->count();

        $organization = new Organization();
        $orginfo = $organization->getOrganization($user['id']); 

        //User Login conditions
        if($user['active']!='0' || !empty($user['activation_token']) ||
            (!$user->hasRole('Super Admin') && !$user->hasRole('Organization Admin') && $user_license==0) ||
            (!$user->hasRole('Super Admin') && $orginfo[0]->status=='1'))
        {
            return redirect('/logout')->with('message','Login Failed. This User is not Active');

        }

        if (isset($user_roles) == true) {
            if (count($user_roles) == 1) {
                // Bypass for admin user.
                if ($user_roles[0]->name == $admin) {
                    return $next($request);
                } else {
                    $permission_found = $PermissionRole->where(['path' => $currentPath, 'role' => $user_roles[0]->id])->count();
                    if ($permission_found) {
                        return $next($request);
                    }
                }
            } else {
                //$role_names = [];
                foreach ($user_roles as $role) {
                    //$role_names = $role->name;

                    if ($role->name == $admin) {
                        return $next($request);
                    }

                    $permission_found = $PermissionRole->where(['path' => $currentPath, 'role' => $role->id])->count();
                    if ($permission_found) {
                        return $next($request);
                    }
                }
            }
        } else {

            return abort(403);
        }

        return abort(403);
    }
}
