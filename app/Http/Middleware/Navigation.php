<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Menu;
use App\User;
use App\Models\Role;
use App\Models\Navigation\NavigationMenu;
use App\Models\Module;
use App\Models\Organization\Organization;
use App\Models\Navigation\NavigationRole;
use App\Models\Organization\OrganizationUser;
use App\Models\Organization\OrganizationModule;
use App\Models\Navigation\NavigationCache;

class Navigation
{
  /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @return mixed
  */
  public function handle($request, Closure $next)
  {

    $user=Auth::user();

    if($user){

      if(config('studiobridge.settings.cache_navigation') == 'NO') {
        $make_nav = new \App\Http\Controllers\Navigation\NavigationController;
        $make_nav->MakeNav();
      }

      Menu::make('myNavBar', function ($menu) use($user) {

        //Check if Nav Cache is empty
        $navigation_counter = NavigationCache::where('user_id', $user->id )->count();

        if($navigation_counter > 0){

          $menu_items = NavigationCache::where('is_parent', 1)->where('user_id', $user->id)->get();

          foreach($menu_items as $menu_item){
            $itemArr = explode(" ",$menu_item->item_name);
            $item_part1 = lcfirst($itemArr[0]);
            $item_part2 = "";
            if(!empty($itemArr[1])) {
              $item_part2 = ucfirst($itemArr[1]);
            }

            $item_name = $item_part1."".$item_part2;
            $menu->add($menu_item->item_name)->active(''.$menu_item->active_url.'')
            ->prepend(' <i class="'.$menu_item->item_icon.'"></i><span class="">')
            ->append('</span>');

            $menu_children = $menu_items = NavigationCache::where('child_of', $menu_item->id)->where('user_id', $user->id)->get();
            if($menu_children && count($menu_children) > 0){
              foreach($menu_children as $menu_child) {
                $menu->item($item_name)->add($menu_child->item_name, $menu_child->active_url)
                ->active($menu_child->active_url);
              }
            }
          }

        }

      });

    }
    return $next($request);
  }
}
