<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrganizationStoreRequest extends FormRequest
{

	private $requestID ;

	/**
	 *
	 * Assign the values of Request id for checking validation
	 *
	 */
	public function requestedID($id)
    {
        $requestID = $id ;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if(!empty($requestID)) {

            return [
                'name' => 'required',
                'adminname' => 'required',
                'email' => 'required|email|unique:users,email,'.$request->user_id,
                'confirm_email' => 'required|same:email'                        
            ];

        } else {

            return [
                'type' => 'required',
                'name' => 'required',
                'adminname' => 'required',
                'licenses' => 'required|integer|min:1',
                'email' => 'required|email|unique:users,email',
                'confirm_email' => 'required|same:email',
                'module_values' => 'required'
            ];
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is required.'
        ];
    }

}