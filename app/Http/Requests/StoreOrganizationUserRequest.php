<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrganizationUserRequest extends FormRequest
{
    private $requestUserID ;

    /**
     *
     * Assign the values of Request id for checking validation
     *
     */
    public function requestedUserID($user_id)
    {
        return $requestUserID = $user_id ;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(!empty($requestUserID)) {

            return [
                'fullname'      => 'required',
                'email'         => 'required|email|unique:users,email,' . $request->user_id,
                'confirm_email' => 'required|same:email',
            ];

        } else {

            return [
                'fullname'      => 'required',
                'email'         => 'required|email|unique:users,email',
                'confirm_email' => 'required|same:email',
                'primary_role'  => 'required',
            ];
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.'
        ];
    }

}
