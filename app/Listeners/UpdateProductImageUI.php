<?php

namespace App\Listeners;

use App\Events\ProductImage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateProductImageUI
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductImage  $event
     * @return void
     */
    public function handle(ProductImage $event)
    {
        //
    }
}
