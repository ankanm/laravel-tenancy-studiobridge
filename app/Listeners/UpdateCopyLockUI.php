<?php

namespace App\Listeners;

use App\Events\CopyLock;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateCopyLockUI
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductImage  $event
     * @return void
     */
    public function handle(CopyLock $event)
    {
        //
    }
}
