<?php

namespace App\Listeners;

use App\Listeners\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Events\Login;
use App\Models\Navigation\NavigationCache;
use Illuminate\Support\Facades\DB;

class GenerateNavigation
{
     /**
     * Create the event listener.
     *
     * @return void
     */
     public function __construct()
     {
     }

     /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
     public function handle(Login $event)
     {
          // DB::statement("SET foreign_key_checks=0");
          // NavigationCache::truncate();
          // DB::statement("SET foreign_key_checks=1");
          $make_nav = new \App\Http\Controllers\Navigation\NavigationController;
          $make_nav->MakeNav();
     }
}
