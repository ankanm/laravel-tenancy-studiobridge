<?php

namespace App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, UsesTenantConnection;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // public function getAvatarAttribute()
    // {
    //  return new LetterAvatar($this->name, 'square');
    // }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }

    public function organization()
    {
        return $this->belongsToMany('App\Models\Organization\Organization');
    }

    public function routeNotificationForSlack()
    {

        // return 'https://hooks.slack.com/services/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';
        return 'https://hooks.slack.com/services/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';
    }

    public function hasRole($rolename)
    {
        $user = Auth::user();

        foreach ($this->roles()->get() as $role) {

            $type = DB::table('role_user')->where('user_id', $user['id'])->where('role_id', $role->id)
                ->select('is_primary')->get();
            if ($role->name == $rolename && $role->name == 'Super Admin') {
                return true;
            } elseif ($role->name == $rolename && $type[0]->is_primary == '1') {
                return true;
            }
        }

        return false;
    }

    public function hasRole_1($rolename)
    {

        $user = User::where('id', $userid)->roles();

        if ($user) {
            $user_roles = $user->roles();
            return json_encode($user);
        }

    }
}
