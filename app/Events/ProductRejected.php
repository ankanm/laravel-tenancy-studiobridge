<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProductRejected implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $audience;
    public $user;
    public $message;
    public $avatar;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($audience, $user, $message, $avatar)
    {
        $this->audience = $audience;
        $this->user = $user;
        $this->message = $message;
        $this->avatar = $avatar;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('qc_channel');
    }

}
