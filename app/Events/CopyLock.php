<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CopyLock implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $product_id;
    public $user_id;
    public $lock;
    public $complete;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($product_id, $user_id, $lock, $complete)
    {
        $this->product_id = $product_id;
        $this->user_id = $user_id;
        $this->lock = $lock;
        $this->complete = $complete;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('copy_channel');
    }

}
