<?php

namespace App\Modules\ProductState\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class ProductState extends Model
{
	use UsesTenantConnection;

    protected $fillable = ['psession_id','product_id', 'qc_state', 'qc_done', 'model_uid' , 'state'];


    public function products()
    {
   //1 Session can have many products
        return $this->belongsTo('App\Modules\Product\Models\Product', 'product_id');
    }

    public function qsessions()
    {
   //1 Session can have many products
        return $this->belongsTo('App\Modules\Qsession\Models\Qsession', 'qsession_id');
    }

    public function psessions()
    {
   //1 Session can have many products
        return $this->belongsTo('App\Modules\Psession\Models\Psession', 'psession_id');
    }


    /*
     * @param qc_state
     *  0 : rejected, 1 : approved
     * @param qc_done
     *  0 : false, 1 : true
     */
    public function updateQc($psession_id, $product_id, $qc_state = 0, $qc_done = 1, $qid)
    {
        $productState = $this->where(['psession_id' => $psession_id,'product_id' => $product_id])
        ->orderBy('id', 'DESC')->first();

        if ($productState) {

            $productState->qc_state = $qc_state;
            $productState->qc_done = $qc_done;
            $productState->qsession_id = $qid;
            $productState->save();
            return $productState->toArray();
        } else {
            $new = $this->create(
                [
                'psession_id' => $psession_id,
                'product_id' => $product_id,
                'qc_state' => $qc_state,
                'qc_done' => $qc_done
                ]
            );
            return $new->toArray();
        }
    }

    /*
     * @param qc_state
     *  0 : rejected, 1 : approved
     */
    public function updateQcState($psession_id, $product_id, $qc_state = 0)
    {
        $productState = $this->where(
            [
            'psession_id' => $psession_id,
            'product_id' => $product_id,
            ]
        )->first();
        if ($productState) {
            $productState->qc_state = $qc_state;
            $productState->save();
            return $productState->toArray();
        } else {
            $new = $this->create(
                [
                'psession_id' => $psession_id,
                'product_id' => $product_id,
                'qc_state' => $qc_state
                ]
            );
            return $new->toArray();
        }
    }

    /*
     * @param qc_done
     *  0 : false, 1 : true
     */
    public function updateQcDone($psession_id, $product_id, $qc_done = 1)
    {
        $productState = $this->where(['psession_id' => $psession_id,'product_id' => $product_id])
        ->first();
        if ($productState) {
            $productState->qc_done = $qc_done;
            $productState->save();
            return $productState->toArray();
        } else {
            $new = $this->create(
                [
                'psession_id' => $psession_id,
                'product_id' => $product_id,
                'qc_done' => $qc_done
                ]
            );
            return $new->toArray();
        }
    }

    /*
     * @param model_uid
     *  model user id value
     */
    public function updateModel($psession_id, $product_id, $model_uid)
    {
        $productState = $this->where(
            [
            'psession_id' => $psession_id,
            'product_id' => $product_id,
            ]
        )->first();
        if ($productState) {
            $data = $productState->toArray();

            $productState->model_uid = $model_uid;
            $productState->save();
            return $productState->toArray();
        } else {
            $new = $this->create(
                [
                'psession_id' => $psession_id,
                'product_id' => $product_id,
                'model_uid' => $model_uid
                ]
            );
            return $new->toArray();
        }
    }

    /*
     * Helper function, to update state & model in product states.
     */
    public function updateState($psession_id, $product_id, $state, $model_uid = 0)
    {
        $productState = $this->where(['psession_id' => $psession_id,'product_id' => $product_id])->first();

        //Check the sequence rule that should apply



        if ($productState) {

            $productState->state = $state;
            $productState->model_uid = $model_uid;
            $productState->save();
            return $productState->toArray();

        } else {
            $new = $this->create(
                [
                'psession_id' => $psession_id,
                'product_id' => $product_id,
                'state' => $state,
                'model_uid' => $model_uid
                ]
            );
            return $new->toArray();
        }
    }

    public function getSequenceRule($psession_id, $product_id){

    }

    public function resetQC($psession_id, $product_id)
    {

            $productstate = ProductState::where(['psession_id' => $psession_id,'product_id' => $product_id])->first();
            if ($productstate) {
                $productstate->qc_done = 0;
                $productstate->save();
                return $productstate->toArray();
            } else {
                $new = new ProductState();
                $new = $this->create(
                    [
                    'psession_id' => $psession_id,
                    'product_id' => $product_id,
                    'qc_done' => 0
                    ]
                );
                return $new->toArray();
            }
    }

    public function getProductStatesFromSession($psession_id, $product_id)
    {
        return $this->where(['psession_id' => $psession_id,'product_id' => $product_id])
        ->orderBy('id', 'desc')
        ->first();
    }

    public function getProductStatesInAllSessions($product_id)
    {
        return $this->where(['product_id' => $product_id])
        ->all();
    }
}
