<?php

Route::group(['module' => 'ProductState', 'middleware' => ['api'], 'namespace' => 'App\Modules\ProductState\Controllers'], function() {

    Route::resource('productState', 'ProductStateController');

});
