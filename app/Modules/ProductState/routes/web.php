<?php

Route::group(['module' => 'ProductState', 'middleware' => ['web'], 'namespace' => 'App\Modules\ProductState\Controllers'], function() {

    Route::resource('productState', 'ProductStateController');

});
