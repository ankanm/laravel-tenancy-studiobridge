<?php

namespace App\Modules\User\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class User extends Authenticatable
{
    use Notifiable, HasApiTokens, UsesTenantConnection;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','active','activation_token','token_start','token_end',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function organization()
 	{
  		return $this->belongsToMany('App\Models\Organization\Organization');
 	}

 	public function routeNotificationForSlack()
 	{

  		return 'https://hooks.slack.com/services/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';
 	}
}
