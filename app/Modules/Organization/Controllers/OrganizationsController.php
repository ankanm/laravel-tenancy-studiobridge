<?php

namespace Modules\Organization\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrganizationStoreRequest;
use App\Http\Requests\StoreOrganizationUserRequest;
use App\Models\License;
use App\Models\Role;
use App\Repositories\CSC\CSCRepositoryInterface;
use App\Repositories\Module\ModuleRepositoryInterface;
use App\Repositories\Organization\OrganizationRepositoryInterface;
use App\Traits\Custom\CustomResponseTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session ;
use App\Models\SequenceConvention;
use Yajra\Datatables\Datatables;
use Redirect;
use Validator;
use App\Models\Category\SimpleCategory ;
use Modules\Copywriting\Models\CopyTemplates;
use App\Models\NamingConvention;
use App\Models\Ounass\Channel;
use App\Models\Ounass\Ounass;

class OrganizationsController extends Controller
{
    use CustomResponseTrait;

    private $organizationRepo;
    private $moduleRepo;
    private $moduleCSC;

    public function __construct(
        OrganizationRepositoryInterface $organizationRepository,
        ModuleRepositoryInterface $modulesRepository,
        CSCRepositoryInterface $CSCRepository
    ) {
        $this->organizationRepo = $organizationRepository;
        $this->moduleRepo       = $modulesRepository;
        $this->moduleCSC        = $CSCRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $organizations = $this->organizationRepo->all();
        return view('Organization::test.ViewOrganizations')->with('organizations', $organizations);
    }

    /**
     * Add Organization
     *
     * @return \Illuminate\Http\Response
     */
    public function AddOrganization()
    {
        // todo tmp info.
        $user         = Auth::User();
        $menu_modules = $this->moduleRepo->findBy(['parent_id' => '0']);
        $all_modules  = $this->moduleRepo->pluck('name', 'id');
        $countryList  = $this->moduleCSC->getCoutries();
        $heading      = "Add";

        return view('Organization::AddOrganization')->with('user', $user)->with('modules', $menu_modules)
            ->with('all_modules', $all_modules)->with('heading', $heading)->with('countryList', $countryList);
    }

    /**
     * Edit Organization
     *
     * @return \Illuminate\Http\Response
     */
    public function EditOrganization($id)
    {
        $module_id_arr = [];

        $organization = $this->organizationRepo->find($id);

        $organization_admin = $this->organizationRepo->getOrganizationAdmin($id);

        $module_id_arr = $this->organizationRepo->getOrganizationModulePlucked($id);

        $organization_locations = $this->organizationRepo->getOrganizationLocations($id);

        $menu_modules = $this->moduleRepo->findBy(['parent_id' => '0']);
        $all_modules  = $this->moduleRepo->pluck('name', 'id');

        $module_id = implode(",", $module_id_arr);

        $heading = "Edit";

        $countryList = $this->moduleCSC->getCoutries();
        $stateList   = $this->moduleCSC->getStates();
        $cityList    = $this->moduleCSC->getCities();

        return view('Organization::AddOrganization')->with('organization', $organization)
            ->with('organizationadmin', $organization_admin)->with('modules', $menu_modules)
            ->with('all_modules', $all_modules)->with('org_module_id', $module_id)
            ->with('org_location', $organization_locations)
            ->with('heading', $heading)
            ->with('countryList', $countryList)
            ->with('stateList', $stateList)
            ->with('cityList', $cityList);
    }

    /**
     * Edit Organization User
     *
     * @return \Illuminate\Http\Response
     */
    public function EditOrganizationUser($id)
    {
        $user              = Auth::User();
        $organization_user = User::find($id);
        $organization      = $this->organizationRepo->getOrganization($user->id);

        $roles            = new Role();
        $primary_roles    = $roles->getPrimaryRoles();
        $additional_roles = $roles->getAdditionalRoles();
        $user_role        = $roles->getUserRole($id);
        $user_license     = License::where('user_id', $id)->where('is_active', '1')->first();

        return view('Organization::EditOrganizationUser')->with('organization', $organization)
            ->with('organization_user', $organization_user)->with('primary_roles', $primary_roles)
            ->with('additional_roles', $additional_roles)->with('user_role', $user_role)
            ->with('user_license', $user_license);
    }

    /**
     * Return sequences for Datatable
     *
     * @return \Illuminate\Http\Response
     */

    public function getSequence(){

        $sequences = SequenceConvention::select(['id_sequence_convention', 'short_name', 'applies_when', 'sequence']);

        return Datatables::of($sequences)
            ->addColumn('action', function ($sequences) {
                    return '<a id="edit_rule_'.$sequences->id_sequence_convention.'" class="btn btn-xs btn-primary myModal" data-toggle="modal" data-id="'.$sequences->id_sequence_convention.'" data-target="#edit_rule"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                })
            ->make();

    }

    /**
     * Return view for organization settings
     *
     * @return \Illuminate\Http\Response
     */
    public function getSettings($id){

        $organization = $this->organizationRepo->find($id);

        if(empty($organization)) {
            Session::flash('error', "Organization does not exist");
            return redirect('/dashboard');
        }

        //Getting column names from Ounass table
        $columns = \Schema::getColumnListing('ounass');
        $columns_not_include = array('Item','Item_Parent','Barcode','Mother_Reference','Description',
        'Item_Description','updated_at','created_at');
        $headers = array_diff($columns, $columns_not_include);
        //Getting Naming conventions
        $namingConventions = NamingConvention::all();

        //Getting Channels
        $channels = Channel::select('channel_code','channel_name')->get();

        //Getting from genders and categories from Ounass table
        $genders = Ounass::select('gender')->groupby('gender')->get();

        //$categories = Ounass::select('category')->groupby('category')->get();
        $categories = DB::table('ounass')
                        ->leftjoin('simple_category', function($join) {
                        $join->on('ounass.Class', '=', 'simple_category.class');
                        $join->on('ounass.SubClass', '=', 'simple_category.subclass');
                        })
                        ->select('simple_category.category')
                        ->groupby('simple_category.category')
                        ->get();

        $template_all = CopyTemplates::get();

        //for simple category
        $classes = SimpleCategory::select('class')->groupby('class')->get();
        $subclasses = SimpleCategory::select('subclass')->groupby('subclass')->get();
        $simple_cats = SimpleCategory::select('category')->groupby('category')->get();

        return view('Organization::settings.organization')->with(['organization' => $organization,'namingConventions' => $namingConventions, 'channels' => $channels, 'genders' => $genders, 'categories' => $categories, 'headers' => $headers, 'template_all' => $template_all, 'classes' => $classes, 'subclasses' => $subclasses, 'simple_cats' => $simple_cats]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Save Organization
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrganizationStoreRequest $request)
    {
        dd($request->all()) ;
        $request->requestedID($request->id);
        $data    = $this->responseBindOrganizationData($request->all());
        $dataloc = $this->responseBindLocation($request->all());

        // Edit an Organization
        if (!empty($request->id)) {

            //$organization= Organization::where('id', $request->id)->update($data);
            $organization = $this->organizationRepo->update($data, $request->id);

            $user = $this->organizationRepo->getOrganizationAdmin($request->id);

            $data_user['name']  = $request->adminname;
            $data_user['email'] = $request->email;

            User::where('id', $user[0]->user_id)->update($data_user);

            if (!empty($request->module_values)) {

                $this->organizationRepo->organizationModuleDelete(['organization_id' => $request->id]);
                // OrganizationModule::where('organization_id', $request->id)->delete();

                $modules = explode(',', $request->module_values);
                foreach ($modules as $module) {

                    $data['module_id']       = $module;
                    $data['organization_id'] = $request->id;

                    $this->organizationRepo->organizationModuleCreate($data);
                    //OrganizationModule::create($data);
                }

            }

            // Create location for an organization based on the location
            $dataloc['organization_id'] = $request->id;

            //$orgloc = Locations::select('id')->where('organization_id',$request->id)->get();
            $orgloc = $this->locationRepo->all(['id'], ['organization_id' => $request->id]);

            if (count($orgloc) > 0) {

                $this->locationRepo->update($dataloc, $orgloc[0]->id);

            } else {

                $this->locationRepo->create($dataloc);
            }

            Session::flash('success', "Organization Updated Successfully");

        } else {

            /**
             * Create an Organization
             */
            $organization = $this->organizationRepo->create($data);

            /**
             * Create an Organization User
             */
            $data_user['name']             = $request->input('adminname');
            $data_user['email']            = $request->input('email');
            $pwd                           = str_random(10);
            $data_user['password']         = bcrypt($pwd);
            $data_user['active']           = '1'; // User Inactive
            $activation_token              = hash_hmac('sha256', str_random(40), config('app.key'));
            $data_user['activation_token'] = $activation_token;
            $data_user['token_start']      = date("Y-m-d H:i:s");
            $data_user['token_end']        = date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s") .
                config('studiobridge.system_emails.organization_created.token_valid_upto')));

            $user = User::create($data_user);

            // Create relation between Role and Organization User
            $data_orguser['organization_id'] = $organization->id;
            $data_orguser['user_id']         = $user->id;
            $data_orguser['is_admin']        = '1';
            $this->organizationRepo->organizationUserCreate($data_orguser);
            //OrganizationUser::create($data_orguser);

            // Create relation between Organization User and Role
            DB::table('role_user')->insert([
                'user_id'    => $user->id,
                'role_id'    => '2',
                'is_primary' => '1',
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            /**
             * Create license for an organization based on no of license
             */
            for ($i = 1; $i <= $request->licenses; $i++) {

                DB::table('licenses')->insert([
                    'key'             => hash_hmac('sha256', str_random(40), config('app.key')),
                    'start'           => date("Y-m-d"),
                    'end'             => date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " + 365 day")),
                    'user_id'         => '',
                    'organization_id' => $organization->id,
                    'is_active'       => '0',
                    'created_at'      => date("Y-m-d H:i:s"),
                ]);
            }

            /**
             * Create location for an organization based on the location
             */
            $dataloc['organization_id'] = $organization->id;
            $this->locationRepo->create($dataloc);
            //Locations::create($dataloc);

            /**
             * Insert Main Modules for Organization
             */

            if (!empty($request->module_values)) {
                $modules = explode(',', $request->module_values);
                foreach ($modules as $module) {

                    $data['module_id']       = $module;
                    $data['organization_id'] = $organization->id;

                    $this->organizationRepo->organizationModuleCreate($data);
                    //OrganizationModule::create($data);
                }
            }

            // Get User Role Name
            $user_roles = $user->roles;
            $user_role  = $user_roles[0]->name;

            // Account Creation Email To Organization Admin
            $data_email = ['email' => $user->email, 'name'                    => $user->name,
                'organization_name'    => $organization->name, 'activation_token' => $activation_token,
                'user_role'            => $user_role, 'type'                      => 'admin'];

            Session::flash('success', "Organization Created Successfully");

            User::where('id', $user->id)->update(['activation_mail_sent' => '1']);

            $this->user_account_email($data_email);

        }

        return redirect('/organizations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organization_users = [];
        $organization       = $this->organizationRepo->find($id);

        if (empty($organization)) {
            Session::flash('error', "Organization does not exist");
            return redirect('/dashboard');
        }

        $user_list          = $this->organizationRepo->getOrganizationUsers($id);
        $organization_admin = $this->organizationRepo->getOrganizationAdmin($id);
        $modules            = $this->organizationRepo->getModules($id);
        $locations          = $this->organizationRepo->getOrganizationLocations($id);
        $vendors            = $this->organizationRepo->getOrganizationVendors($id);
        $countryList        = $this->moduleCSC->getCoutries();

        return view('Organization::ViewOrganization')->with('organization', $organization)
            ->with('userlist', $user_list)->with('organization_admin', $organization_admin)
            ->with('modules', $modules)->with('locations', $locations)->with('countryList', $countryList)
            ->with('vendors', $vendors);
    }

    /**
     * Add Organization User
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function AddOrganizationUser()
    {
        $user         = Auth::User();
        $organization = $this->organizationRepo->getOrganization($user->id);

        //Role List
        $roles            = new Role();
        $primary_roles    = $roles->getPrimaryRoles();
        $additional_roles = $roles->getAdditionalRoles();

        return view('Organization::AddOrganizationUser')->with('organization', $organization)
            ->with('primary_roles', $primary_roles)->with('additional_roles', $additional_roles);
    }

    /**
     * Save Organization User
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function StoreOrganizationUser(StoreOrganizationUserRequest $request)
    {
        $user_role = "" ;

        if (!empty($request->user_id)) {

            $validationRules = [
                'fullname'      => 'required',
                'email'         => 'required|email|unique:users,email,' . $request->user_id,
                'confirm_email' => 'required|same:email',

            ];
        } else {
            $validationRules = [
                'fullname'      => 'required',
                'email'         => 'required|email|unique:users,email',
                'confirm_email' => 'required|same:email',
                'primary_role'  => 'required',
            ];
        }

        $validatorObj = \Validator::make($request->all(), $validationRules);

        if ($validatorObj->fails()) {
            return \Redirect::back()->withErrors($validatorObj)->withInput();
        }

        $organization_id    = $this->getOrganizationId();
        $data_user['name']  = $request->input('fullname');
        $data_user['email'] = $request->input('email');

        // Update an Organization User
        if (!empty($request->user_id)) {

            User::where('id', $request->user_id)->update($data_user);

            // Update Additional Roles for User
            if (!empty($request->additional_role)) {

                DB::table('role_user')->where('user_id', $request->user_id)->where('is_primary', '0')->delete();

                foreach ($request->additional_role as $role) {

                    DB::table('role_user')->insert([
                        'user_id'    => $request->user_id,
                        'role_id'    => $role,
                        'is_primary' => '0',
                        'created_at' => date("Y-m-d H:i:s"),
                    ]);
                }
            }

            Session::flash('success', "Organization User Updated Successfully");

        } else {

            // License Based Role
            $role    = Role::where('id', $request->primary_role)->get();
            $license = License::where('organization_id', $request->input('organization_id'))
                ->where('is_active', '0')->where('end', '>', date("Y-m-d"))->first();

            //Create an Organization User
            $pwd                           = str_random(10);
            $data_user['password']         = bcrypt($pwd);
            $data_user['active']           = '1'; // User Inactive
            $activation_token              = hash_hmac('sha256', str_random(40), config('app.key'));
            $data_user['activation_token'] = $activation_token;
            $data_user['token_start']      = date("Y-m-d H:i:s");
            $data_user['token_end']        = date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s") .
                config('studiobridge.system_emails.organization_created.token_valid_upto')));

            $user = User::create($data_user);

            // Create relation between Role and Organization User
            $data_orguser['organization_id'] = $request->input('organization_id');
            $data_orguser['user_id']         = $user->id;
            $data_orguser['is_admin']        = '0';
            $this->organizationRepo->organizationUserCreate($data_orguser);

            // Create relation between Organization User and Role

            // Insert Primary Role for User
            DB::table('role_user')->insert([
                'user_id'    => $user->id,
                'role_id'    => $request->primary_role,
                'is_primary' => '1',
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            // Insert Additional Roles for User
            if (!empty($request->additional_role)) {
                foreach ($request->additional_role as $role) {
                    DB::table('role_user')->insert([
                        'user_id'    => $user->id,
                        'role_id'    => $role,
                        'is_primary' => '0',
                        'created_at' => date("Y-m-d H:i:s"),
                    ]);
                }
            }

            // Get User Role Name
            $user_roles = $user->roles;
            if (!empty($user_roles[0])) {
                $user_role  = $user_roles[0]->name;
            }

            // Account Creation Email To Organization User
            $data_email = [
                'name'              => $user->name,
                'email'             => $user->email,
                'organization_name' => $request->input('organization_name'),
                'activation_token'  => $activation_token, 'user_role' => $user_role,
                'type'              => 'user',
            ];

            if (count($license) > 0) {

                User::where('id', $user->id)->update(['activation_mail_sent' => '1']);

                License::where('id', $license['id'])->update(['is_active' => '1', 'user_id' => $user->id]);

                $this->user_account_email($data_email);
            }

            Session::flash('success', "Organization User Created Successfully");

        }

        return redirect('/organization/' . $organization_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
