@extends('layouts.body')


@section('main-content')
  <link rel="stylesheet" href="{{ URL::asset('css/jquery.dataTables.min.css') }}">
  <!--Page Container-->
  <div class="app-content-body">
    <!--BEGIN PAGE HEADER-->
    <div class="header">
      <h2>Organization <strong>Rules</strong></h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          <li><a href="#">Home</a>
          </li>
          <li><a href="#">Organizations</a>
          </li>
          <li class="active">{{ $organization->name }}</li>
        </ol>
      </div>
    </div>
    <!--END PAGE HEADER-->

    <div class="row">
      <div class="col-md-9">
        <ul class="nav nav-tabs " style="z-index:1;">
          <li class="active"><a href="#rules" data-toggle="tab"> Sequence</a></li>
          <li><a href="#naming" data-toggle="tab"> Naming</a></li>
          <li><a href="#copyheader" data-toggle="tab"> Copy Header</a></li>
          <li><a href="#simplecat" data-toggle="tab"> Simple Category </a></li>
        </ul>
        <div class="tab-content">

          <div class="tab-pane active" id="rules">
            <div class="row m-b-10">
              <div class="col-sm-2">
                <h3 class="m-t-5">Image <strong>Naming</strong></h3>
              </div>

              <div class="col-sm-2 pull-right">
                <div class="pull-right">
                  <a class="ripple btn btn-default bg-blue" id="add-rule" data-toggle="modal" data-target="#add_rule">Add New Rule </a>
                </div>
              </div>

            </div>

            <div class="panel pagination2 table-responsive">
              <table class="table datatable datatable-column-search-inputs dt-bootstrap no-footer" id="sequences">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Short Name</th>
                    <th>When Applied</th>
                    <th>Sequence</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>


          </div>

          <div class="tab-pane" id="naming">
            <div class="row m-b-10">
              <div class="col-sm-4">
                <h3 class="m-t-5">Naming <strong>Convention</strong></h3>
              </div>

              <div class="col-sm-2 pull-right">
                <div class="pull-right">
                  <a class="ripple btn btn-default bg-blue" id="add-convention" data-toggle="modal" data-target="#add_convention">Add New Convention </a>
                </div>
              </div>

            </div>
            <div class="panel pagination2 table-responsive">
              <table class="table datatable datatable-column-search-inputs dt-bootstrap no-footer"  id="naming_convention">

                <thead>
                  <tr>
                    <th>#</th>
                    <th>Short Name</th>
                    <th>Parameter 1</th>
                    <th>Parameter 2</th>
                    <th>Parameter 3</th>
                    <th>Parameter 4</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
              <div class="text-center">
                <strong>

                </strong>
              </div>
            </div>



        </div>



        <div class="tab-pane" id="copyheader">
          <div class="row m-b-10">
            <div class="col-sm-4">
              <h3 class="m-t-5">Copy <strong>Header</strong></h3>
            </div>

            <div class="col-sm-2 pull-right">
              <div class="pull-right hide">
                <a class="ripple btn btn-default bg-blue" id="add-copyheader" data-toggle="modal" data-target="#add_copyheader">Add New Copy Header </a>
              </div>
            </div>

          </div>
          <div class="panel pagination2 table-responsive">
            <table class="table datatable datatable-column-search-inputs dt-bootstrap no-footer" id="tablech">

                <thead>
                  <tr>
                    <th>Weight</th>
                    <th>Header Name</th>
                    <th>Header Type</th>
                    <th>Default Value</th>
                    <th>Required</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody id="chcontents">

                </tbody>
              </table>
              <div class="text-center">
                <strong>

                </strong>
              </div>
            </div>



        </div>
        <div class="tab-pane" id="simplecat">
          <div class="row m-b-10">
            <div class="col-sm-4">
              <h3 class="m-t-5">Simple <strong>Category</strong></h3>
            </div>

            <div class="col-sm-2 pull-right">
              <div class="pull-right">
                <a class="ripple btn btn-default bg-blue" id="add-simplecat" data-toggle="modal" data-target="#add_simplecat">Add New Simple Category </a>
              </div>
            </div>

          </div>
          <div class="panel pagination2 table-responsive">
            <table class="table datatable datatable-column-search-inputs dt-bootstrap no-footer" id="tablesimple" >

                <thead>
                  <tr>
                    <th>Class</th>
                    <th>Sub-Class</th>
                    <th>Category Name</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody id="simplecontents">

                </tbody>
              </table>
              <div class="text-center">
                <strong>

                </strong>
              </div>
            </div>
        </div>

      </div>
    </div>

    <div class="col-md-3" style="margin-top:51px;">
      <div class="panel panel-flat">
        <div class="panel-header">
          <h3>Organization Information</h3>
        </div>
        <div class="panel-body p-t-0">
          <ul class="menu-list m-t-10">
            <li><span>Name <span class="pull-right text-blue"><strong>{{ $organization->name }}</strong></span></span></li>
            <li><span>Address <span class="pull-right text-blue"><strong >{{ $organization->address }}</strong></span></span></li>
            <li><span>Contact <span class="pull-right text-blue"><strong >{{ $organization->email }}</strong></span></span></li>

          </ul>
        </div>
      </div>
      <div class="panel panel-flat">
        <div class="panel-header">
          <h3>License Information</h3>
        </div>
        <div class="panel-body p-t-0">
          <ul class="menu-list m-t-10">
            <li><span>Type <span class="pull-right text-blue"><strong>Beta</strong></span></span></li>
            <li><span>Licenses <span class="pull-right text-blue"><strong >{{ $organization->licenses }}</strong></span></span></li>
            <li><span>Expires In <span class="pull-right text-blue"><strong >6 Months, 12 Days</strong></span></span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  </div>
  @include('partials.modals.addRulesModal')
  @include('partials.modals.editRulesModal')
  @include('partials.modals.addConventionModal')
  @include('partials.modals.editConventionModal')

  @include('partials.modals.addCopyHeaderModal')
  @include('partials.modals.editCopyHeaderModal')

  @include('partials.modals.addSimpleCatModal')
  @include('partials.modals.editSimpleCatModal')
@stop

@section('page-scripts')
  <script src="{{ URL::asset('js/tables/datatables/datatables.min.js') }}"></script>
  <script src="{{ URL::asset('js/pages/datatable_basic.js') }}"></script>

  <script type="text/javascript">
  $(document).ready(function(){

    //removing subclasses related to selected class
    $('#add_sc_class').on('change', function() {
    if(this.value != 0){
      var $request = $.get("/organization/class/"+this.value); // make request

      $request.done(function(data) { // success
        console.log(data);

        $.each(data, function(i, item) {
          $('#add_sc_subclass option[value="'+item.subclass+'"]').remove();
        });
        $('#add_sc_subclass').removeAttr("disabled");

      });
    }else{
      $('#add_sc_subclass').attr("disabled", true);
      $('#add_sc_subclass option[value="0"]').attr('selected','selected');
      $('#add_sc_subclass').change();
    }

  });
    //Submit Add rule form
    $("#add_rule_bt").click(function(){

      var short_name = $("#short_name").val();
      var channel = $('#channel').val();
      var shoot_type = $('#shoot_type').val();
      var gender = $('#gender').val();
      var category = $('#category').val();
      var sequence = $('#sequence').val();
      var convention_id = $('#convention_id').val();

      if(short_name=="" || sequence=="" || convention_id==0 || channel==0 || shoot_type==0 || gender==0 || category==0){
        alert('Please fill all required fields');
        return;
      } else {

        var data = {
          "short_name": short_name,
          "channel": channel,
          "shoot_type": shoot_type,
          "gender": gender,
          "category": category,
          "sequence": sequence,
          "convention_id": convention_id
        };

        $.ajax({

          url: '/organization/addrule',
          method: 'post',
          headers: {
            'Content-Type': 'application/hal+json',
            'X-CSRF-Token': $('[name=_token]').val()
          },
          data: JSON.stringify(data),
          success: function (data) {
            //console.log(data);
            if(data){

              $('#add_rule').modal('hide');
              $('#add_rule_form').trigger('reset');
              swal({
                title: 'Add Rule',
                text: 'Rule added successfully!',
                timer: 4500,
                type: "success",
                showConfirmButton: false
              });
              location.reload();

            }
          }

        });

      }

    });

    //Submit Edit form
    $("#edit_rule_bt").click(function(){

      var short_name = $("#edit_short_name").val();
      var channel = $('#edit_channel').val();
      var shoot_type = $('#edit_shoot_type').val();
      var gender = $('#edit_gender').val();
      var category = $('#edit_category').val();
      var sequence = $('#edit_sequence').val();
      var convention_id = $('#edit_convention_id').val();

      if(short_name=="" || sequence=="" || convention_id==0 || channel==0 || shoot_type==0 || gender==0 || category==0){
        alert('Please fill all required fields');
        return;
      } else {

        var data = {
          "short_name": short_name,
          "channel": channel,
          "shoot_type": shoot_type,
          "gender": gender,
          "category": category,
          "sequence": sequence,
          "convention_id": convention_id,
          "id_sequence_convention": $('[name=id_sequence_convention]').val()
        };

        $.ajax({

          url: '/organization/updaterule',
          method: 'post',
          headers: {
            'Content-Type': 'application/hal+json',
            'X-CSRF-Token': $('[name=_token]').val()
          },
          data: JSON.stringify(data),
          success: function (data) {
            //console.log(data);
            if(data){

              $('#edit_rule').modal('hide');
              $('#edit_rule_form').trigger('reset');
              swal({
                title: 'Update Rule',
                text: 'Rule updated successfully!',
                timer: 4500,
                type: "success",
                showConfirmButton: false
              });
              location.reload();

            }
          }

        });

      }

    });
    //Populate Edit Form
    $("#sequences").on('click', '.myModal', function() {

      var dataId = $(this).data('id');
      $.ajax({
        url: '/organization/rule/'+dataId,
        method: 'get',
        success:function(data){

          $('#edit_short_name').val(data.short_name);
          $('#edit_sequence').val(data.sequence);
          $('[name=id_sequence_convention]').val(data.id_sequence_convention);

          $('#edit_channel option').each(function(){
            if($(this).val().toLowerCase()==data.channel){
              $(this).attr('selected','selected').change();
            }
          });

          $('#edit_shoot_type option').each(function(){
            if($(this).val().toLowerCase()==data.shoot_type){
              $(this).attr('selected','selected').change();
            }
          });

          $('#edit_gender option').each(function(){
            if($(this).val().toLowerCase()==data.gender){
              $(this).attr('selected','selected').change();
            }
          });

          $('#edit_category option').each(function(){

            if($(this).val().toLowerCase()==data.category){
              $(this).attr('selected','selected').change();

            }
          });

          $('#edit_convention_id option').each(function(){
            if($(this).val()==data.id_naming_convention){
              $(this).attr('selected','selected').change();
            }
          });


        }

      });

    });

  });

  //building datatable for Simple Category
  $(function() {
    $('#tablesimple').DataTable({
      processing: true,
      serverSide: true,
      ajax: '/organization/simplecategory',
      columns: [
        { data: 'class', name: 'class' },
        { data: 'subclass', name: 'subclass' },
        { data: 'category', name: 'category' },
        {data: 'action', name: 'action', orderable: false, searchable: false}
      ]
    });
  });

  //building datatable for sequence
  $(function() {
    $('#sequences').DataTable({
      processing: true,
      serverSide: true,
      ajax: '/organization/sequence',
      columns: [
        { data: 'id_sequence_convention', name: 'id_sequence_convention' },
        { data: 'short_name', name: 'short_name' },
        { data: 'applies_when', name: 'applies_when' },
        { data: 'sequence', name: 'sequence' },
        {data: 'action', name: 'action', orderable: false, searchable: false}
      ]
    });
  });

  //building datatable for naming
  $(function() {
    $('#naming_convention').DataTable({
      processing: true,
      serverSide: true,
      ajax: '/organization/naming',
      columns: [
        { data: 'id_naming_convention', name: 'id_naming_convention' },
        { data: 'short_name', name: 'short_name' },
        { data: 'parameter_1', name: 'parameter_1' },
        { data: 'parameter_2', name: 'parameter_2' },
        { data: 'parameter_3', name: 'parameter_3' },
        { data: 'parameter_4', name: 'parameter_4' },
        {data: 'action', name: 'action', orderable: false, searchable: false}
      ]
    });
  });

  //Loading Naming Convention Edit Form
  $("#naming_convention").on('click', '.myNaming', function() {

    var dataId = $(this).data('id');
    $.ajax({
      url: '/organization/naming/'+dataId,
      method: 'get',
      success:function(data){

        $('#edit_short_name_con').val(data.short_name);
        $('[name=id_naming_convention]').val(data.id_naming_convention);
        $('#edit_parameter_1 option').each(function(){
          if($(this).val().toLowerCase()==data.parameter_1){
            $(this).attr('selected','selected').change();
            $("#edit_parameter_2 option[value='"+$(this).val()+"']").remove();
            $("#edit_parameter_3 option[value='"+$(this).val()+"']").remove();
            $("#edit_parameter_4 option[value='"+$(this).val()+"']").remove();
          }
        });
        $('#edit_parameter_2 option').each(function(){
          if($(this).val().toLowerCase()==data.parameter_2){
            $(this).attr('selected','selected').change();
            $("#edit_parameter_3 option[value='"+$(this).val()+"']").remove();
            $("#edit_parameter_4 option[value='"+$(this).val()+"']").remove();
          }
        });
        $('#edit_parameter_3 option').each(function(){
          if($(this).val().toLowerCase()==data.parameter_3){
            $(this).attr('selected','selected').change();
            $("#edit_parameter_4 option[value='"+$(this).val()+"']").remove();
          }
        });
        $('#edit_parameter_4 option').each(function(){
          if($(this).val().toLowerCase()==data.parameter_4){
            $(this).attr('selected','selected').change();
          }
        });

      },

      error:function(){
        swal({
          title: "Error",
          text: "There is no data.",
          type: "error",
          showCancelButton: false,
          confirmButtonClass: 'btn-success',
          confirmButtonText: 'Ok',
          cancelButtonText: "Cancel",
          closeOnConfirm: true,
          closeOnCancel: true
        });
      }

    });

  });

  //Setting Options for Parameter 2 fo redit naming form
  $('#edit_parameter_1').on('change', function() {
    if(this.value != 0){

      var options = $("#edit_parameter_1 > option:not(:first)").clone();
      $('#edit_parameter_2 option[value!="0"]').remove();
      $('#edit_parameter_2').append(options);
      $('#edit_parameter_2 option:selected').removeAttr('selected');
      $("#edit_parameter_2 option[value='"+this.value+"']").remove();
      $('#edit_parameter_2').removeAttr("disabled");
      $('#edit_parameter_2 option[value="0"]').attr('selected','selected');
      $('#edit_parameter_2').change();
    }else{

      $('#edit_parameter_2 option[value!="0"]').remove();
      $('#edit_parameter_2').change();
      $('#edit_parameter_2').attr("disabled", true);

      $('#edit_parameter_3 option[value!="0"]').remove();
      $('#edit_parameter_3').change();
      $('#edit_parameter_3').attr("disabled", true);

      $('#edit_parameter_4 option[value!="0"]').remove();
      $('#edit_parameter_4').change();
      $('#edit_parameter_4').attr("disabled", true);

    }

  });

  //Setting options for Parameter 3 for edit naming form
  $('#edit_parameter_2').on('change', function() {
    if(this.value != 0 && $('#edit_parameter_1').val() != 0){
      var options = $("#edit_parameter_2 > option:not(:first)").clone();
      $('#edit_parameter_3 option[value!="0"]').remove();
      $('#edit_parameter_3').append(options);
      $('#edit_parameter_3 option:selected').removeAttr('selected');
      $("#edit_parameter_3 option[value='"+this.value+"']").remove();
      $('#edit_parameter_3').removeAttr("disabled");
      $('#edit_parameter_3').change();
    }else{

      $('#edit_parameter_3 option[value!="0"]').remove();
      $('#edit_parameter_3').change();
      $('#edit_parameter_3').attr("disabled", true);

      $('#edit_parameter_4 option[value!="0"]').remove();
      $('#edit_parameter_4').change();
      $('#edit_parameter_4').attr("disabled", true);

    }

  });

  //Setting options for Parameter 4 for edit form
  $('#edit_parameter_3').on('change', function() {
    if(this.value != 0 && $('#edit_parameter_1').val() != 0 && $('#edit_parameter_2').val() != 0){
      var options = $("#edit_parameter_3 > option:not(:first)").clone();
      $('#edit_parameter_4').append(options);
      $('#edit_parameter_4 option:selected').removeAttr('selected');
      $("#edit_parameter_4 option[value='"+this.value+"']").remove();
      $('#edit_parameter_4').removeAttr("disabled");
      $('#edit_parameter_4').change();
    }else{

      $('#edit_parameter_4 option[value!="0"]').remove();
      $('#edit_parameter_4').change();
      $('#edit_parameter_4').attr("disabled", true);

    }

  });

  //Loading options for Parameter 2
  $('#parameter_1').on('change', function() {
    if(this.value != 0){

      var options = $("#parameter_1 > option:not(:first)").clone();
      $('#parameter_2 option[value!="0"]').remove();
      $('#parameter_2').append(options);
      $("#parameter_2 option[value='"+this.value+"']").remove();
      $('#parameter_2').removeAttr("disabled");
      $('#parameter_2').change();

    }else{

      $('#parameter_2 option[value!="0"]').remove();
      $('#parameter_2').change();
      $('#parameter_2').attr("disabled", true);

      $('#parameter_3 option[value!="0"]').remove();
      $('#parameter_3').change();
      $('#parameter_3').attr("disabled", true);

      $('#parameter_4 option[value!="0"]').remove();
      $('#parameter_4').change();
      $('#parameter_4').attr("disabled", true);

    }

  });

  //Loading options for Parameter 3
  $('#parameter_2').on('change', function() {
    if(this.value != 0 && $('#parameter_1').val() != 0){
      var options = $("#parameter_2 > option:not(:first)").clone();
      $('#parameter_3 option[value!="0"]').remove();
      $('#parameter_3').append(options);
      $("#parameter_3 option[value='"+this.value+"']").remove();
      $('#parameter_3').removeAttr("disabled");
      $('#parameter_3').change();

    }else{

      $('#parameter_3 option[value!="0"]').remove();
      $('#parameter_3').change();
      $('#parameter_3').attr("disabled", true);

      $('#parameter_4 option[value!="0"]').remove();
      $('#parameter_4').change();
      $('#parameter_4').attr("disabled", true);

    }

  });

  //Loading options for Parameter 4
  $('#parameter_3').on('change', function() {
    if(this.value != 0 && $('#parameter_1').val() != 0 && $('#parameter_2').val() != 0){
      var options = $("#parameter_3 > option:not(:first)").clone();
      $('#parameter_4 option[value!="0"]').remove();
      $('#parameter_4').append(options);
      $("#parameter_4 option[value='"+this.value+"']").remove();
      $('#parameter_4').removeAttr("disabled");
      $('#parameter_4').change();
    }else{

      $('#parameter_4 option[value!="0"]').remove();
      $('#parameter_4').change();
      $('#parameter_4').attr("disabled", true);

    }

  });
  //Saving Add Naming form
  $("#add_convention_bt").click(function(){

    var short_name = $("#short_name_con").val();
    var parameter_1 = $('#parameter_1').val();
    var parameter_2 = $('#parameter_2').val();
    var parameter_3 = $('#parameter_3').val();
    var parameter_4 = $('#parameter_4').val();

    if(short_name=="" || parameter_1==0 || parameter_2==0 || parameter_3==0 || parameter_4==0){
      alert('Please fill all required fields');
      return;
    } else {

      var data = {
        "short_name": short_name,
        "parameter_1": parameter_1,
        "parameter_2": parameter_2,
        "parameter_3": parameter_3,
        "parameter_4": parameter_4
      };

      $.ajax({

        url: '/organization/addnaming',
        method: 'post',
        headers: {
          'Content-Type': 'application/hal+json',
          'X-CSRF-Token': $('[name=_token]').val()
        },
        data: JSON.stringify(data),
        success: function (data) {
          //console.log(data);
          if(data){

            $('#add_convention').modal('hide');
            $('#add_naming_form').trigger('reset');
            swal({
              title: 'Add Naming',
              text: 'Naming added successfully!',
              timer: 4500,
              type: "success",
              showConfirmButton: false
            });
            location.reload();

          }
        }

      });

    }

  });
  //Saving Edit form
  $("#edit_convention_bt").click(function(){

    var short_name = $("#edit_short_name_con").val();
    var parameter_1 = $('#edit_parameter_1').val();
    var parameter_2 = $('#edit_parameter_2').val();
    var parameter_3 = $('#edit_parameter_3').val();
    var parameter_4 = $('#edit_parameter_4').val();

    if(short_name=="" || parameter_1==0 || parameter_2==0 || parameter_3==0 || parameter_4==0){
      alert('Please fill all required fields');
      return;
    } else {

      var data = {
        "id_naming_convention":$('[name=id_naming_convention]').val(),
        "short_name": short_name,
        "parameter_1": parameter_1,
        "parameter_2": parameter_2,
        "parameter_3": parameter_3,
        "parameter_4": parameter_4
      };

      $.ajax({

        url: '/organization/updatenaming',
        method: 'post',
        headers: {
          'Content-Type': 'application/hal+json',
          'X-CSRF-Token': $('[name=_token]').val()
        },
        data: JSON.stringify(data),
        success: function (data) {
          //console.log(data);
          if(data){

            $('#edit_naming').modal('hide');
            $('#edit_naming_form').trigger('reset');
            swal({
              title: 'Update Naming',
              text: 'Naming updated successfully!',
              timer: 4500,
              type: "success",
              showConfirmButton: false
            });
            location.reload();

          }
        }

      });

    }

  });
  </script>
  <!-- for copy header //-->
  <script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
  <script>
  $(function() {

    var copyheaderdatatable = $('#tablech').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('copywriting.headers.data') !!}',
      columns: [
        { data: 'weight', name: 'weight'},
        { data: 'header_name', name: 'header_name' },
        { data: 'header_type', name: 'header_type' },
        { data: 'default_value', name: 'default_value' },
        { data: 'required', name: 'required'},
        { data: 'actions', name: 'actions', orderable: false, searchable: false }
      ]
    });




    $("#tablech").on('click', '.delmodal', function() {
      var delurl = ($(this).data('href'));
      swal({
        title: "Delete ",
        text: "Are you sure you want to delete this?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Delete",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
          $.get( delurl, function( data ) {
            swal({
              title: "Copy Header Deleted",
              text: "A copy header has been deleted.",
              timer: 1000,
              type: "success",
              showConfirmButton: false
            });

            setTimeout(function(){
              copyheaderdatatable.ajax.reload();
            }, 3000);
          });
          //window.location.replace(delurl);
        }
      });

    });

    $("#add_ch_bt").click(function(){
      var header_name = $("#header_name").val();
      var header_type = $('#header_type').val();
      var channels = $('#channels').val();
      if(header_name=="" || header_type==""){
        alert('Please fill all required fields');
        return false;
      }
      else {
        var frmdata = $( "#add_ch_form" ).serializeArray();
        $.ajax({
          url: '/copywriting/header/store',
          type: 'POST',
          data: frmdata,
          success: function (node) {
            if(node.id!=""){
              swal({
                title: "Copy Header Saved",
                text: "A copy header has been saved.",
                timer: 1000,
                type: "success",
                showConfirmButton: false
              });

              setTimeout(function(){
                //window.location.replace('/copywriting/header/list');
                $('#add_copyheader').modal('hide');
                copyheaderdatatable.ajax.reload();

              }, 3000);
            }
            else{
              swal({
                title: "Error",
                text: "There are some error occured!!",
                timer: 1000,
                type: "warning",
                showConfirmButton: false
              });
            }

          },
          error: function(){
            alert('Failed! **');
          }

        });
      }
    });

    $("#tablech").on('click', '.editmodal', function() {
      var editurl = ($(this).data('href'));
      $.ajax({
        url: editurl,
        type: 'GET',
        dataType: 'json',
        success: function (node) {

          if(node.id=="0"){
            swal({
              title: "Error",
              text: "There are some error occured!!",
              timer: 2000,
              type: "warning",
              showConfirmButton: false
            });
          }
          else{
            $("#edit_id").val(node.id);
            $('#header_name_edit').val(node.header_name);
            $('#header_type_edit option[value='+node.header_type+']').attr('selected','selected');
            $('#header_type_edit').select2('destroy').select2();

            $('#default_value_edit').val(node.default_value);
            if(node.required==1){
              $('#required_edit1').trigger('click');
              $('#required_edit1').iCheck('check');
            }
            else{
              $('#required_edit0').trigger('click');
              $('#required_edit0').iCheck('check');
            }
            $("#edit_ch_form input[name='required'][value='"+node.required+"']").attr('checked', 'checked');

            $('#channels_edit').val(node.channels);
            $('#weight_edit').val(node.weight);
          }

        },
        error: function(){
          alert('Failed! **');
        }

      });
    });

    $("#edit_ch_bt").click(function(){
      var header_name = $("#header_name_edit").val();
      var header_type = $('#header_type_edit').val();
      var channels = $('#channels_edit').val();
      if(header_name=="" || header_type=="" ){
        alert('Please fill all required fields');
        return false;
      }
      else {
        var frmdata = $( "#edit_ch_form" ).serializeArray();
        $.ajax({
          url: '/copywriting/header/store',
          type: 'POST',
          data: frmdata,
          success: function (node) {
            if(node.id!=""){
              swal({
                title: "Copy Header Saved",
                text: "A copy header has been saved.",
                timer: 1000,
                type: "success",
                showConfirmButton: false
              });

              setTimeout(function(){
                //window.location.replace('/copywriting/header/list');
                $('#edit_copyheader').modal('hide');
                copyheaderdatatable.ajax.reload();
              }, 3000);
            }
            else
            {
              swal({
                title: "Error",
                text: "There are some error occured!!",
                timer: 1000,
                type: "warning",
                showConfirmButton: false
              });
            }

          },
          error: function(){
            alert('Failed! **');
          }

        });
      }
    });

    $( "#chcontents" ).sortable({
      items: "tr",
      cursor: 'move',
      opacity: 0.6,
      update: function() {
        sendOrderToServer();
      }
    });

    function sendOrderToServer() {

      var order = [];
      $('#chcontents tr').each(function(index,element) {
        var newid = $(this).attr('id');

        order.push({
          id: newid.replace("row_", ""),
          position: index+1
        });
      });

      $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{ url('copywriting/header/orderupdate') }}",
        data: {
          order:order,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response.status == "success") {
            console.log(response);
          } else {
            console.log(response);
          }
        }
      });

    }

    //simple category start
    $("#add_sc_bt").click(function(){
      var class_val = $("#add_sc_class").val();
      var subclass = $("#add_sc_subclass").val();
      var category = $("#add_sc_category").val();

      if(class_val==0 || subclass==0 || category==0 ){
        alert('Please fill all required fields');
        return false;
      }
      else {
        var frmdata = $( "#add_sc_form" ).serializeArray();
        $.ajax({
          url: '/simple_category/store',
          type: 'POST',
          data: frmdata,
          success: function (node) {
            console.log(node);
            if(node.id!=""){
              swal({
                title: "Simple Category Saved",
                text: "A simple category has been saved.",
                timer: 1000,
                type: "success",
                showConfirmButton: false
              });

              setTimeout(function(){
                window.location.reload();


              }, 3000);
            }
            else{
              swal({
                title: "Error",
                text: "There are some error occured!!",
                timer: 1000,
                type: "warning",
                showConfirmButton: false
              });
            }

          },
          error: function(){
            alert('Failed! **');
          }

        });
      }
    });


    $("#simplecontents").on('click', '.simpleModal', function() {
      var dataId = $(this).data('id');
      
      $.ajax({
        url: "/simple_category/edit/"+dataId,
        type: 'GET',
        dataType: 'json',
        success: function (node) {
          console.log(node);
          if(node.id=="0"){
            swal({
              title: "Error",
              text: "There are some error occured!!",
              timer: 2000,
              type: "warning",
              showConfirmButton: false
            });
          }
          else{
            $("#edit_sc_id").val(node.id);
            $('#edit_sc_class option[value="'+node.class+'"]').attr('selected','selected').change();
            $('#edit_sc_subclass option[value="'+node.subclass+'"]').attr('selected','selected').change();
            $('#edit_sc_category option[value="'+node.category+'"]').attr('selected','selected').change();

          }

        },
        error: function(){
          alert('Failed! **');
        }

      });
    });


    $("#edit_sc_bt").click(function(){
      var class_val = $("#edit_sc_class :selected").val();
      var subclass = $("#edit_sc_subclass :selected").val();
      var category = $("#edit_sc_category :selected").val();
      
      if(class_val==0 || subclass==0 || category==0 ){
        alert('Please fill all required fields');
        return false;
      }
      else {
        var frmdata = $( "#edit_sc_form" ).serializeArray();
        $.ajax({
          url: '/simple_category/store',
          type: 'POST',
          data: frmdata,
          success: function (node) {
            if(node.id!=""){
              swal({
                title: "Simple Category Saved",
                text: "A simple category has been saved.",
                timer: 1000,
                type: "success",
                showConfirmButton: false
              });

              setTimeout(function(){
                window.location.reload();
              }, 3000);
            }
            else
            {
              swal({
                title: "Error",
                text: "There are some error occured!!",
                timer: 1000,
                type: "warning",
                showConfirmButton: false
              });
            }

          },
          error: function(){
            alert('Failed! **');
          }

        });
      }
    });

  });

</script>

@stop
