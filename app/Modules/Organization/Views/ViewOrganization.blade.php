@extends('layouts.body')


@section('main-content')
  <!--Page Container-->
  <div class="app-content-body">
    <!--BEGIN PAGE HEADER-->
    <div class="header">
      <h2>Manage <strong>Organization</strong></h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          <li><a href="#">Home</a>
          </li>
          <li><a href="#">Organizations</a>
          </li>
          <li class="active">{{ $organization->name }}</li>
        </ol>
      </div>
    </div>
    <!--END PAGE HEADER-->

    <div class="row">
      <div class="col-md-9">
        <ul class="nav nav-tabs " style="z-index:1;">
          <li class="active"><a href="#users" data-toggle="tab"> Users </a></li>
          <li><a href="#modules" data-toggle="tab"> Modules</a> </li>
          <li><a href="#rules" data-toggle="tab"> Rules</a> </li>
          {{-- <li><a href="#api" data-toggle="tab"> API </a></li> --}}
          <li><a href="#locations" data-toggle="tab"> Locations</a> </li>
          <li><a href="#vendors" data-toggle="tab"> Vendors</a> </li>
          <li><a href="#reports" class="trigger-resize" data-toggle="tab"> </span></a></li>
        </ul>
        <div class="tab-content">
          <input type="hidden" id="org-id" value="{{$organization->id}}">
          {{ csrf_field() }}
          <div class="tab-pane active" id="users">
            @if(Session::has('success'))
              <div class="alert alert-success">
                <strong>Success!</strong> {{ Session::get('success') }}
              </div>
            @endif
            <span class="error"></span>
            <div class="row m-b-10">
              <div class="col-sm-2">
                <select  class="form-control m-b-10" data-placeholder="Select a filter" style="width: 150px !important;" onChange="window.location.href=this.value">
                  <option value="/psession/list/all" {{ Request::is('/psession/list/all') ? ' selected ' : null }}>All Users</option>
                  <option value = "/psession/list/open" {{ Request::is('/psession/list/open') ? ' selected' : null }}>Active</option>
                  <option value = "/psession/list/closed" {{ Request::is('/psession/list/closed') ? ' selected' : null }}>Deactivated</option>
                </select>
              </div>

              <div class="col-sm-2 pull-right">
                <div class="pull-right">
                  <a class="ripple btn btn-default bg-blue" href="/organization-users/add">Add New User </a>
                </div>
              </div>

            </div>

            <div class="panel">
              <table class="table datatable user-list" id="datatable">
                <thead>
                  <tr>
                    <th>Avatar</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Primary Role</th>
                    <th>License</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($userlist as $user)
                    <tr>
                      <td><img alt="" src="{{$user->avatar}}" class="img-rounded img-sm">
                        <td>{{ ucwords($user->name) }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role_name }}</td>
                        <td>
                          @if(!($user->role_name=="Organization Admin" || $user->role_name=="Super Admin"))
                            <a class="ripple  edit" id="license-{{$user->id}}"
                              active="{{$user->active}}}">
                              @if($user->role_name=="Organization Admin") <span class="label label-success">No License</span>
                              @elseif($user->is_active==1) <span class="label label-success">Active
                              </span>
                            @else <span class="label label-danger">Inactive</span>
                            @endif
                          @endif
                        </a>
                      </td>
                      <td>
                        @if(!($user->role_name=="Organization Admin" || $user->role_name=="Super Admin"))
                          <a class="ripple edit b_status"  title="status"
                          user_role="{{$user->role_name}}" user_id="{{$user->id}}"
                          active="{{$user->active}}}">
                          @if($user->active==0) <span id="statuschk" class="label label-success active">Active</span>
                          @else <span class="label label-danger inactive">Inactive</span>
                          @endif
                        @endif
                      </a>
                    </td>
                    <td>
                      @if(!($user->role_name=="Super Admin"))
                        <a href="/organization-users/edit/{{$user->id}}">EDIT</a>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>

            </table>
          </div>
        </div>
        <div class="tab-pane " id="modules">
          <div class="row m-b-10">
            <div class="col-sm-3">
              <h3 class="m-t-5">Modules <strong>Active</strong></h3>
            </div>

          </div>
          <div class="">
            @foreach($modules as $index => $module)
              @if($module->parent_id == 0)
                <div class="panel panel-body">
                  <ul class="media-list">
                    <li class="media">
                      <div class="media-left">
                        <i class="icon-puzzle m-t-5" style="font-size: 30px !important;"></i>
                      </div>

                      <div class="media-body">
                        <h5 class="media-heading text-navy text-bold"> {{$module->name}}</h5>
                        {{$module->description}}
                      </div>
                      <div class="media-right">
                        <a class="ripple btn btn-default disabled" href="">Active </a>
                      </div>
                    </li>
                  </ul>
                </div>
              @endif
            @endforeach
          </div>
          <div class="row m-b-10">
            <div class="col-sm-3">
            </div>

          </div>

        </div>
        <div class="tab-pane " id="rules">
          <div class="row m-b-10">
            <div class="col-sm-2">
              <h3 class="m-t-5">Image <strong>Naming</strong></h3>
            </div>

            <div class="col-sm-2 pull-right">
              <div class="pull-right">
                {{-- <a class="ripple btn btn-default bg-blue" href="/psession/add">Add New Rule </a> --}}
              </div>
            </div>

          </div>
          <div class="panel">


            <div class="table-responsive">
              <table class="table table-hover user-list" id="datatable">
                <thead>
                  <tr>
                    <th>Rule</th>
                    <th>Image Sequence</th>
                    <th>Image Name</th>
                    <th>Status</th>

                  </tr>
                </thead>
                <tbody>
                  @foreach(config('client.naming_rules') as $key => $naming_array)

                    @foreach($naming_array as $key => $value)
                      @if($key == 'applies_when')
                        @continue
                      @endif
                      <tr>
                        <td>{{(isset($rule) ? $rule : '')}}</td>
                        <td>{{$key}}</td>
                        <td>{{$value}}</td>
                        <td><span class="label label-success">Active</span></td>

                      </tr>
                    @endforeach
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>


        </div>
        <div class="tab-pane panel" id="api">
          <div class="panel panel-flat">
            <div class="">
              <form class="form-horizontal" method="post" action="" name="formaddSession" id="formaddSession">

                <h6 class="form-wizard-title">
                  Product API
                  <small class="display-block">To retrieve product information</small>
                </h6>

                <div class="form-group pb-10">
                  <label class="control-label col-lg-2">URL</label>
                  <div class="col-lg-4">
                    <input type="text" name="notes" class="form-control" value="http://sampleapiaddress.com/product/lookup">
                  </div>

                </div>

                <div class="form-group pb-10">
                  <label class="control-label col-lg-2">User Name</label>
                  <div class="col-lg-4">
                    <input type="text" name="notes" class="form-control" value="ababar">
                  </div>
                  <label class="control-label col-lg-2">Password</label>
                  <div class="col-lg-4">
                    <input type="password" name="notes" class="form-control" value="ashar">
                  </div>
                </div>

              </form>
            </div>

          </div>
        </div>

        <div class="tab-pane " id="locations">
          <div class="row m-b-10">
            <div class="col-sm-3">
              <h3 class="m-t-5">Locations</h3>
            </div>

            <div class="col-sm-2 pull-right">
              <div class="pull-right">
                <a class="ripple btn btn-default bg-blue add-loc" data-toggle="modal" data-target="#add-loc">Add New Location</a>
              </div>
            </div>

          </div>
          <div class="">
            @foreach($locations as $index => $location)
              @php
              $country = @App\Models\Country::where('id', $location->country)->first()->name;
              $province = @App\Models\State::where('id', $location->province)->first()->name;
              $city = @App\Models\City::where('id', $location->city)->first()->name;
              @endphp
                <div class="panel panel-body">
                  <ul class="media-list">
                    <li class="media">
                      <div class="media-left">
                        <i class="fa fa-map-marker m-t-5 {{ ($location->is_primary == 1 ? 'text-info' : '' ) }}" style="font-size: 30px !important;"></i>
                      </div>

                      <div class="media-body">
                        <h5 class="media-heading text-navy text-bold"> {{$location->short_name}}</h5>
                        <p class='width-300'>{{$location->address1}}</p>
                        @if(!empty($location->address2))
                          <p class='width-300'>{{$location->address2}}</p>
                        @endif
                        <p class='width-300'>{{$city}}, {{ $province }},
                          {{ $country }}</p>
                          @if(!empty($location->zipcode))
                            <p class='width-300'>
                              {{ $location->zipcode }}
                            @endif
                          </p>
                        </div>
                        @if($location->is_primary == 0)
                          <div class="media-right">
                            <a class="ripple btn btn-danger primarybtn" location_id="{{ $location->loc_id }}">Set Primary </a>
                          </div>
                        @endif
                      </li>
                    </ul>
                </div>
            @endforeach
            </div>
            <div class="row m-b-10">
              <div class="col-sm-3">
              </div>
            </div>
          </div>

          <div class="tab-pane " id="vendors">
            <div class="row m-b-10">
              <div class="col-sm-3">
                <h3 class="m-t-5">Vendors</h3>
              </div>
            </div>
            <div class="">
              @foreach($vendors as $index => $vendor)
                <div class="panel panel-body">
                  <ul class="media-list">
                    <li class="media">
                      <div class="media-left">
                        <i class="fa fa-truck m-t-5" style="font-size: 30px !important;"></i>
                      </div>

                      <div class="media-body">
                        <h5 class="media-heading text-navy text-bold"> {{$vendor->vendor_name}}</h5>
                        <p class='width-300'>Contact: {{$vendor->contact}}</p>

                      </div>
                    </li>
                  </ul>
                </div>
              @endforeach
            </div>
            <div class="row m-b-10">
              <div class="col-sm-3">
              </div>

            </div>

          </div>

        </div>
      </div>

      <div class="col-md-3" style="margin-top:51px;">
        <div class="panel panel-flat">
          <div class="panel-header">
            <h3>User Stats</h3>
          </div>
          <div class="panel-body p-t-0">
            <div class="row">
              <div class="col-sm-4 text-center">
                <h6 class="semi-bold">Total</h6>
                <h1 class="m-xs">{{count($userlist)}}</h1>
              </div>
              <div class="col-sm-4 text-center text-success-light">
                <h6 class="semi-bold">Premium</h6>
                <h1 class="m-xs">0</h1>
              </div>
              <div class="col-sm-4 text-center text-slate">
                <h6 class="semi-bold">Regular</h6>
                <h1 class="m-xs">0</h1>
              </div>
            </div>

          </div>
        </div>
        <div class="panel panel-flat">
          <div class="panel-header">
            <h3>Organization Information</h3>
          </div>
          <div class="panel-body p-t-0">
            <ul class="menu-list m-t-10">
              <li><span>Name <span class="pull-right text-blue"><strong>{{ $organization->name }}</strong></span></span></li>
              <li><span>Admin <span class="pull-right text-blue"><strong >--</strong></span></span></li>
              <li><span>Contact <span class="pull-right text-blue"><strong >--</strong></span></span></li>

            </ul>
          </div>
        </div>
        <div class="panel panel-flat">
          <div class="panel-header">
            <h3>License Information</h3>
          </div>
          <div class="panel-body p-t-0">
            <ul class="menu-list m-t-10">
              <li><span>Type <span class="pull-right text-blue"><strong>Beta</strong></span></span></li>
              <li><span>Licenses <span class="pull-right text-blue"><strong >3</strong></span></span></li>
              <li><span>Expires In <span class="pull-right text-blue"><strong >6 Months, 12 Days</strong></span></span></li>

            </ul>
          </div>
        </div>
      </div>

    </div>


    <!--/Page Container-->
    @include('partials.modals.addLocationModal')
  @stop

  @section('page-scripts')

    <!-- Page scripts -->
    <script src="{{ URL::asset('js/forms/jquery.validate.js') }}"></script>
    <script src="{{ URL::asset('js/forms/switch.min.js') }}"></script>
    <script src="{{ URL::asset('js/pages/form_validations.js') }}"></script>
    <script src="{{ URL::asset('js/globaljs.js') }}"></script>
    <script src="{{ URL::asset('js/classie.js') }}"></script>
    <script src="{{ URL::asset('js/studiobridge/OrganizationFunctions.js') }}"></script>
    <script src="{{ URL::asset('js/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('js/pages/datatable_basic.js') }}"></script>

    <!-- /page scripts -->

    <script>
    $(document).on("click", ".add-loc", function () {
      $("#country").val(0).trigger('change');
      $("#state").val(0).trigger('change');
      $("#city").val(0).trigger('change');
    })

    $(".b_status").on('click',function(){

      if($(this).attr('user_role')=="Organization Admin") {
        $(".error").html("<font color='red'>Status cannot be changed for Organization Admin</font>");
        return;
      }
      var t=$(this);
      var id=$(this).attr('user_id');
      var status=($(this).find('.active').length)?0:1;

      $.ajax({

        url: '{{url("/organization-users/change-status")}}',
        type:'post',
        data:{_token:'{{csrf_token()}}', id: id, status: status},
        success:function(data){

          if(data==2) {
            $(".error").html("<font color='red'>User Status cannot be Activated. No License has been allocated to this user.</font>");
            return;
          }

          if(data==1){
            if(status==0){
              $(t).html('<span class="label label-danger inactive">Inactive</span>');
              $(".error").html("<font color='red'>User Status has been Inactivated.</font>");
            }else{
              $(t).html('<span class="label label-success active">Active</span>');
              $(".error").html("<font color='green'>User Status has been Activated.</font>");
            }
          }
        }
      });
    })


    </script>
  @stop
