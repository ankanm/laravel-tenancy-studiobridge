@extends('layouts.body')

@section('main-content')
  <link rel="stylesheet" href="{{ URL::asset('css/snippets.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/htimeline.css') }}">
  <!--Page Container-->
  <div class="app-content-body">
    <!--Page Header-->
    <div class="header">
      <h2>Add <strong>User</strong></h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          <li><a href="#">Home</a>
          </li>
          <li><a href="#">Settings</a>
          </li>
          <li class="active">Add User</li>
        </ol>
      </div>
    </div>

    <div class=" ">
      <div class="">
        <div class="">
          <div class="panel panel-flat">
            <div class="panel-body p-0">
              <form class="form-horizontal" method="post" action="/organization-users/store" name="formaddOrganization" id="formaddOrganization">
              <input type="hidden" id="form_submit" value="">
              <input type="hidden" name="organization_id" value="{{$organization[0]->id}}">
                {{ csrf_field() }}
                <h6 class="form-wizard-title">
                  <span>Organization Type</span>
                </h6>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Organization Type <span class="text-danger">*</span></label>
                  <div class="col-lg-10">
                    <label class="radio-inline">
                      <input type="radio" name="type" class="styled"
                      value="studio" @php if($organization[0]->type=='studio') echo "checked='checked'"; @endphp
                       disabled="disabled">
                      Studio
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="type" class="styled" value="retouching_agency"
                      @php if($organization[0]->type=='retouching_agency') echo "checked='checked'"; @endphp disabled="disabled">
                      Retouching Agency
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="type" class="styled" value="modelling_agency"
                      @php if($organization[0]->type=='modelling_agency') echo "checked='checked'";@endphp disabled="disabled">
                      Modelling Agency
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Organization Name <span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                    <input type="text" class="form-control" name="organization_name" placeholder="Organization Name" value="{{ $organization[0]->name }}" readonly="readonly">
                  </div>
                </div>

                <h6 class="form-wizard-title">
                  <span>Licenses</span>


                </h6>
                <div class="form-group pb-10">
                  <label class="control-label col-lg-2">Licenses <span class="text-danger">*</span></label>
                  <div class="col-lg-2">
                    <input type class="form-control" name="licenses"
                    value="{{ $organization[0]->licenses }}" readonly="readonly">
                  </div>
                </div>


                <h6 class="form-wizard-title">
                  <span>Organization User</span>

                  <small class="display-block">Which user are you creating?</small>
                </h6>

                <div class="form-group">
                  <label class="control-label col-lg-2">Full Name <span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                    <input type class="form-control"  name="fullname" placeholder="Full Name"
                    value="{{ old('fullname') }}">
                     <div class="text-danger">{{ $errors->first('fullname') }}</div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Email Address <span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                    <input type class="form-control" name="email" placeholder="Email"
                    value="{{ old('email') }}">
                    <div class="text-danger">{{ $errors->first('email') }}</div>
                  </div>

                  <label class="control-label col-lg-2">Confirm Email Address <span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                    <input type class="form-control" name="confirm_email" placeholder="Confirm Email"
                    value="{{ old('confirm_email') }}">
                    <div class="text-danger">{{ $errors->first('confirm_email') }}</div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Primary Role <span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                   <select class="form-control role" name="primary_role">
                  <option value="">Select Primary Role</option>
                  @foreach($primary_roles as $role)
                    <option value="{{ $role->id }}" @if($role->name=='Organization Admin'){{'disabled'}}
                     @endif @php if(old('primary_role') && old('primary_role')==$role->id) echo "selected='selected'"; @endphp>{{ $role->name }}
                    </option>
                  @endforeach
                </select>
                <div class="text-danger">{{ $errors->first('primary_role') }}</div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Additional Role</label>
                  <div class="col-lg-4">
                      <select class="form-control multiple" name="additional_role[]" id="role_id" multiple="multiple">
                        @foreach($additional_roles as $role)
                          <option value="{{ $role->id }}"
                          {{ (collect(old('additional_role'))->contains($role->id)) ? 'selected':'' }}>
                          {{ $role->name }}
                          </option>
                        @endforeach
                      </select>
                  </div>
                </div>


                <div class="form-wizard-actions">
                  <input class="ripple btn btn-default ui-wizard-content ui-formwizard-button" type="reset">
                  <a class="ripple btn btn-info addbtn">Add Organization User </a>
                </div>
              </form>
            </div>

          </div>

        </div>

      </div>

    </div>
    <!--/Page Container-->

  @stop

  @section('page-scripts')

    <!-- Page scripts -->
    <script src="{{ URL::asset('js/forms/jquery.validate.js') }}"></script>
    <script src="{{ URL::asset('js/forms/switch.min.js') }}"></script>
    <script src="{{ URL::asset('js/pages/form_validations.js') }}"></script>

    <script src="{{ URL::asset('js/bootstrap-multiselect.js') }}"></script>
    <link href="{{ URL::asset('css/bootstrap-multiselect.css') }}" rel="stylesheet"
    type="text/css" />




  <!-- /page scripts -->

  <script type="text/javascript">

    $("input[type='reset']").click(function(){
      $("#form_submit").val("");
    });

    $(".addbtn").click(function(){

      if($("#form_submit").val() == "1") {
        return false;
      }

      swal({
        title: "Add New User",
        text: "Are you sure you want to add this user?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: 'btn-success',
        confirmButtonText: 'Add Organization User',
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {

          $(".addbtn").attr("disabled",true);
          $(".addbtn").css({cursor:"default"});
          $("input[type='reset']").attr("disabled",true);
          $("#form_submit").val(1);

          $( "#formaddOrganization" ).submit();
        } else {
          swal("Cancelled", "You have cancelled adding the user", "error");
        }
      });

    });

</script>
  @stop
