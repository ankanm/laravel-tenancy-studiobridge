@extends('layouts.body')

@section('main-content')
  <link rel="stylesheet" href="{{ URL::asset('css/snippets.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/htimeline.css') }}">
  <!--Page Container-->
    <!--/Page Header-->
    <div class="app-content-body">
      <!--Page Header-->
      <div class="header">
        <h2>{{$heading}} <strong>Organization</strong></h2>
        <div class="breadcrumb-wrapper">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Organizations</a></li>
            <li class="active">{{$heading}}  Organization</li>
          </ol>
        </div>
      </div>
      <!--/Page Header-->

    <div class="">

      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="panel panel-flat">
            <div class="panel-body p-0">
              <form class="form-horizontal" method="post" action="/organization" name="formaddOrganization" id="formaddOrganization">
                <input type="hidden" id="form_submit" value="">
                @if(!empty($organization->id))
                  <input type="hidden" id="org_id" name="id" value="{{$organization->id}}">
                  <input type="hidden" name="user_id" value="{{$organizationadmin[0]->user_id}}">
                  <input type="hidden" id="org_module_values" name="org_module_values" value="{{$org_module_id}}">
                @endif

                {{ csrf_field() }}
                <h6 class="form-wizard-title">
                  <span>Organization Type</span>
                  <small class="display-block">What type of organization are you creating</small>
                </h6>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Organization Type <span class="text-danger">*</span></label>
                  <div class="col-lg-10">
                    <label class="radio-inline">
                      <input type="radio" name="type" value="studio" @php if(isset($organization->type)
                      &&
                      $organization->type=='studio') echo "checked='checked'"; @endphp
                      @php if(old('type') && old('type')=='studio') echo "checked='checked'"; @endphp >
                      Studio
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="type" value="retouching_agency"
                      @php if(isset($organization->type) && $organization->type=='retouching_agency') echo "checked='checked'"; @endphp
                      @php if(old('type') && old('type')=='retouching_agency') echo "checked='checked'"; @endphp>
                      Retouching Agency
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="type" value="modelling_agency"
                      @php if(isset($organization->type) && $organization->type=='modelling_agency') echo "checked='checked'";@endphp
                       @php if(old('type') && old('type')=='modelling_agency') echo "checked='checked'"; @endphp>
                      Modelling Agency
                    </label>
                    <div class="text-danger">{{ $errors->first('type') }}</div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Organization Name <span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                    <input type="text" class="form-control"  name="name" placeholder="Organization Name"
                    value="{{ isset($organization->name)?$organization->name:old('name') }}">
                    <div class="text-danger">{{ $errors->first('name') }}</div>
                  </div>

                </div>

                <h6 class="form-wizard-title">
                  <span>Licenses</span>
                  <small class="display-block">How many licences associated with this organization</small>
                </h6>
                <div class="form-group pb-10">
                  <label class="control-label col-lg-2">Licenses <span class="text-danger">*</span></label>
                  <div class="col-lg-2">
                    <input type="number" class="form-control" name="licenses" placeholder="0"
                    value="{{ isset($organization->licenses)?$organization->licenses:old('licenses') }}"
                    @php if(isset($organization->licenses)) echo "readonly='readonly'"; @endphp>
                    <div class="text-danger">{{ $errors->first('licenses') }}</div>
                  </div>

                </div>


                <h6 class="form-wizard-title">
                  <span>Organization Admin</span>
                  <small class="display-block">Who is the main point of contact for this organization</small>
                </h6>


                <div class="form-group">
                  <label class="control-label col-lg-2">Admin Name <span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                    <input type class="form-control"  name="adminname" placeholder="Admin Name"
                    value="{{ isset($organizationadmin[0]->name)?$organizationadmin[0]->name:old('adminname') }}">
                     <div class="text-danger">{{ $errors->first('adminname') }}</div>
                  </div>

                </div>
              <div class="form-group">
                  <label class="control-label col-lg-2">Email Address <span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                    <input type class="form-control" name="email" placeholder="Admin Email"
                    value="{{ isset($organizationadmin[0]->email)?$organizationadmin[0]->email:old('email') }}">
                    <div class="text-danger">{{ $errors->first('email') }}</div>
                  </div>

                  <label class="control-label col-lg-2">Confirm Email Address <span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                    <input type class="form-control" name="confirm_email" placeholder="Confirm Admin Email"
                     value="{{ isset($organizationadmin[0]->email)?$organizationadmin[0]->email:old('confirm_email') }}">
                     <div class="text-danger">{{ $errors->first('confirm_email') }}</div>
                  </div>
                </div>

                 <h6 class="form-wizard-title">
                  <span>Organization Modules</span>
                  <small class="display-block">Organization Modules</small>
                </h6>
                  <div class="form-group">
                  <label class="control-label col-lg-2">Select Modules<span class="text-danger">*</span></label>
                  <div class="col-lg-4">
                        <div id="treeview-checkbox-demo">
                        <ul>
                            @if(!empty($organization->id))
                                  @foreach($modules as $module)
                                   <li data-value="{{ $module->id }}">
                                    {{ $module->name }}
                                    @if(count($module->childs))
                                     <ul>
                                      @foreach($module->childs as $child)
                                       <li data-value="{{ $child->id }}">{{ $child->name }}</li>
                                      @endforeach
                                     </ul>
                                    @endif
                                   </li>
                                  @endforeach
                            @else
                                @foreach($modules as $module)
                                  <li data-value="{{ $module->id }}">
                                      {{ $module->name }}
                                      @if(count($module->childs))
                                          <ul>
                                          @foreach($module->childs as $child)
                                            <li data-value="{{ $child->id }}">{{ $child->name }}</li>
                                          @endforeach
                                          </ul>
                                      @endif
                                  </li>
                                @endforeach
                            @endif
                        </ul>
                        </div>
                        <input type="hidden" id="module_values" name="module_values">
                        <div class="text-danger">{{ $errors->first('module_values') }}</div>
                  </div>
                </div>

                  <h6 class="form-wizard-title">
                  <span>Organization Locations</span>
                  <small class="display-block">Where is this organization located</small>
                </h6>
                 <div class="form-group pb-10">
                  <label class="control-label col-lg-2">Short name</label>
                  <div class="col-lg-4">
                    <input type class="form-control" name="short_name" placeholder="Short Name"
                    value="{{ isset($org_location[0]->short_name)?
                    $org_location[0]->short_name:old('short_name') }}">
                  </div>
                </div>

                 <div class="form-group pb-10">
                  <label class="control-label col-lg-2">Address 1</label>
                  <div class="col-lg-4">
                  <textarea name="address1" rows="3" cols="5" class="form-control" placeholder="Address 1">{{ isset($org_location[0]->address1)?$org_location[0]->address1:old('address1') }}
                  </textarea>
                  </div>
                </div>
                <div class="form-group pb-10">
                  <label class="control-label col-lg-2">Address 2</label>
                  <div class="col-lg-4">
                    <textarea name="address2" rows="3" cols="5" class="form-control"
                    placeholder="Address 2">
                    {{ isset($org_location[0]->address2)?$org_location[0]->address2:old('address2') }}
                    </textarea>
                  </div>
                </div>

                <div class="form-group pb-10">
                  <label class="control-label col-lg-2">Country</label>
                  <div class="col-lg-4">
                     <select id="country" class="form-control" name="country">
                      <option value="">Select Country</option>
                      @foreach($countryList as $country)
                        <option value="{{ $country->id }}" @if(old('country')==$country->id) selected="selected" @endif @if(isset($org_location[0]->country) &&
                        $org_location[0]->country==$country->id) selected="selected" @endif>{{ $country->name }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>

                 <div class="form-group pb-10">
                  <label class="control-label col-lg-2">Province</label>
                  <div class="col-lg-4">
                  @if(isset($org_location[0]->province))
                  <select id="state" name="province" class="form-control">
                      @foreach($stateList as $state)
                        <option value="{{ $state->id }}" @if(isset($org_location[0]->province) &&
                        $org_location[0]->province==$state->id) selected="selected" @endif >
                        {{ $state->name }}
                      @endforeach
                  </select>
                  @else
                  <select id="state" name="province" class="form-control" disabled="disabled">
                    <option value="">Select Province</option>
                  </select>
                  @endif
                  </div>
                </div>

                <div class="form-group pb-10">
                  <label class="control-label col-lg-2">City</label>
                  <div class="col-lg-4">
                  @if(isset($org_location[0]->city))
                  <select id="city" name="city" class="form-control">
                      @foreach($cityList as $city)
                        <option value="{{ $city->id }}"
                        @if(isset($org_location[0]->city) &&
                        $org_location[0]->city==$city->id) selected="selected" @endif>
                        {{ $city->name }}
                      @endforeach
                  </select>
                  @else
                  <select id="city" name="city" class="form-control" disabled="disabled">
                    <option value="">Select City</option>
                  </select>
                  @endif
                  </div>
                </div>

                 <div class="form-group pb-10">
                  <label class="control-label col-lg-2">ZipCode</label>
                  <div class="col-lg-4">
                    <input type class="form-control" name="zipcode" placeholder="ZipCode"
                    value="{{ isset($org_location[0]->zipcode)?
                    $org_location[0]->zipcode:old('zipcode') }}">
                  </div>
                </div>


                <h6 class="form-wizard-title">
                  <span>Organization Address</span>
                  <small class="display-block">Where is this organization located</small>
                </h6>
                <div class="form-group pb-10">
                  <label class="control-label col-lg-2">Address</label>
                  <div class="col-lg-4">
                    <textarea name="address" rows="3" cols="5" class="form-control" placeholder="Organization address">{{ isset($organization->address)?$organization->address:old('address') }}
                    </textarea>
                  </div>
                </div>

                <div class="form-wizard-actions">
                  <input class="btn btn-default ui-wizard-content ui-formwizard-button" type="reset">
                    @if(!empty($organization->id))
                       <a class="btn btn-info editbtn">Edit Organization </a>
                    @else
                      <a class="btn btn-info addbtn">Add Organization </a>
                    @endif
                </div>
              </form>
            </div>

          </div>

        </div>

      </div>

    </div>
    <!--/Page Container-->

  @stop

  @section('page-scripts')

    <!-- Page scripts -->


   <script src="{{ URL::asset('js/forms/jquery.validate.js') }}"></script>
   <script src="{{ URL::asset('js/forms/switch.min.js') }}"></script>
   <script src="{{ URL::asset('js/pages/form_validations.js') }}"></script>

   <!-- Tree View Module -->
  <script src="{{ URL::asset('js/logger.js') }}"></script>
  <script src="{{ URL::asset('js/treeview.js') }}"></script>
  <script src="{{ URL::asset('js/treeViewAction.js') }}"></script>
   <!-- Tree View Module -->

    <!-- /page scripts -->

    <script type="text/javascript">

    $("input[type='reset']").click(function(){
      $("#form_submit").val("");
    });

      var org_id = $("#org_id").val();
    if (typeof org_id !== "undefined") {

      $(".editbtn").click(function(){
          swal({
                  title: "Edit Organization",
                  text: "Are you sure you want to edit this organization?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: 'btn-success',
                  confirmButtonText: 'Edit Organization',
                  cancelButtonText: "Cancel",
                  closeOnConfirm: true,
                  closeOnCancel: true
              },
              function(isConfirm) {
                  if (isConfirm) {
                      $('#module_values').val(
                          $('#treeview-checkbox-demo').treeview('selectedValues')
                      );

                      $("#formaddOrganization").submit();
                  } else {
                      swal("Cancelled", "You have cancelled editing the organization", "error");
                  }
              });

      });
    } else {

      $(".addbtn").click(function(){

          if($("#form_submit").val() == "1") {
            return false;
          }

          swal({
                  title: "Add New Organization",
                  text: "Are you sure you want to add this organization?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: 'btn-success',
                  confirmButtonText: 'Add Organization',
                  cancelButtonText: "Cancel",
                  closeOnConfirm: true,
                  closeOnCancel: true
              },
              function(isConfirm) {
                  if (isConfirm) {

                      $(".addbtn").attr("disabled",true);
                      $("input[type='reset']").attr("disabled",true);
                      $(".addbtn").css({cursor:"default"});
                      $('#module_values').val(
                          $('#treeview-checkbox-demo').treeview('selectedValues')
                      );
                      $("#form_submit").val(1);
                      $("#formaddOrganization").submit();

                  } else {
                      swal("Cancelled", "You have cancelled adding the organization", "error");
                  }
              });

      });
    }

    $("#country").change(function(){
        var country_id=$(this).val();
        $("#state").empty().append('<option value="0">Select a State</option>');
        $.ajax({
            url: "{{url('helper/state-by-country')}}/"+country_id,
            success:function(data){
              $("#state").prop('disabled', false);
              $("#state").append(data);
              $("#state").select2("destroy");
              $("#state").select2({
                placeholder: "Select a State"
              });

            }
        })
    })


    $(document).on('change', '#state', function (e) {
        var state_id=$(this).val();
        $("#city").empty().append('<option value="0">Select a City</option>');
        $.ajax({
            url: "{{url('helper/city-by-state')}}/"+state_id,
            success:function(data){
              $("#city").prop('disabled', false);
              $("#city").append(data);
              $("#city").select2("destroy");
              $("#city").select2({
                placeholder: "Select a City"
              });

            }
        })
    })


    </script>
  @stop
