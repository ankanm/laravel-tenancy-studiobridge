@extends('layouts.body')

@section('main-content')
	<!--Page Container-->
	<div class="app-content-body">
		<!--BEGIN PAGE HEADER-->
		<div class="header">
			<h2>View <strong>Organizations</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="#">Home</a>
					</li>
					<li><a href="#">Organizations</a>
					</li>
					<li class="active">View Organizations</li>
				</ol>
			</div>
		</div>
		<!--END PAGE HEADER-->

		<div class="">

			@if(Session::has('success'))
				<div class="alert alert-success">
					<strong>Success!</strong> {{ Session::get('success') }}
				</div>
			@endif
			<div class="row m-b-10">
				<div class="col-sm-2">
					<select  class="form-control m-b-10" data-placeholder="Select a filter" style="width: 150px !important;">
						<option value = "This Week" selected>All Organizations</option>
						<option value = "Last Week">Active</option>
						<option value = "Custom">Deactivated</option>
					</select>
				</div>

				<div class="col-sm-2 pull-right">
					<div class="pull-right">
						<a class="ripple btn btn-default bg-blue" href="/organization/add">
							Add New Organization 
						</a>
					</div>
				</div>

			</div>
			<!-- Individual column searching (text fields) -->
			<div class="panel panel-flat">
				<table class="table datatable datatable-column-search-inputs">
					<thead>
						<tr>
							<th>Organization Name</th>
							<th>Email</th>
							<th>Type</th>
							<th>Create Date</th>
							<th>Status</th>
							<th class="text-center">Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach($organizations as $organization)
							@php

							$createdate = Carbon\Carbon::parse($organization->created_at)->format('d M, Y h:i a');
							@endphp
							<tr>
								<td>{{ $organization->name }}</td>
								<td>{{ $organization->email }}</td>
								<td>{{ ucfirst($organization->type) }}</td>
								<td>{{ $createdate }}</td>
								<td>

									<a class="ripple btn btn-app edit b_status"  title="status" organization_id="{{$organization->id}}">
										@if($organization->status==0) <span id="statuschk" class="label label-success active">Active</span>
										@else <span class="label label-danger inactive">Inactive</span>
										@endif
									</a>
								</td>
								<td>
									<ul class="icons-list">
										<li><a href="/organization/{{$organization->id}}"><i class="icon-eye2"></i></a></li>
										<li class="dropdown">
											<a href="user_list.htm#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="/organization/edit/{{$organization->id}}"><i class="icon-pencil6"></i>Edit</a></li>
											</ul>
										</li>
									</ul>
									<input type="hidden" name="_token" value="{{csrf_token()}}">
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<!-- /Individual column searching (text fields) -->

		</div>


	</div>
	<!--/Page Container-->
@stop

@section('page-scripts')

	<!-- Page scripts -->
	<script src="{{ URL::asset('js/pnotify.min.js') }}"></script>
	<script src="{{ URL::asset('js/pages/extension_pnotify.js') }}"></script>

	<script src="{{ URL::asset('js/tables/datatables/datatables.min.js') }}"></script>
	<script src="{{ URL::asset('js/pages/datatable_basic.js') }}"></script>
	<!-- /page scripts -->

@stop
