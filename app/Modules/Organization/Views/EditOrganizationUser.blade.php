@extends('layouts.body')

@section('main-content')
  <!--Page Container-->
  <div class="app-content-body">


    <div class="header">
      <h2>Edit <strong>User</strong></h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          <li><a href="#">Home</a>
          </li>
          <li><a href="#">Organizations</a>
          </li>
          <li class="active">Edit User</li>
        </ol>
      </div>
    </div>
    <!--/Page Header-->

    <div class="">
      @if ($errors->any())
        <div class="alert alert-danger"><strong>Oops!</strong> {{ implode('', $errors->all(':message')) }}</div>
      @endif
      <span class="error"></span>

      <div class="">
        <div class="">
          <div class="panel panel-flat">
            <div class="panel-body p-0">
              <form class="form-horizontal" method="post" action="/organization-users/store" name="formaddOrganization" id="formaddOrganization">
                <input type="hidden" name="user_id" id="user_id" value="{{$organization_user->id}}">
                <input type="hidden" name="organization_id" value="{{$organization[0]->id}}">
                {{ csrf_field() }}
                <h6 class="form-wizard-title">
                  <span>Organization Type</span>

                </h6>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Organization Type <span class="text-danger">*</span></label>
                  <div class="col-lg-10">
                    <label class="radio-inline">
                      <input type="radio" name="type" class="styled"
                      value="studio" @php if($organization[0]->type=='studio') echo "checked='checked'"; @endphp
                      disabled="disabled">
                      Studio
                      </label>
                      <label class="radio-inline">
                      <input type="radio" name="type" class="styled" value="retouching_agency"
                      @php if($organization[0]->type=='retouching_agency') echo "checked='checked'"; @endphp disabled="disabled">
                      Retouching Agency
                      </label>
                      <label class="radio-inline">
                      <input type="radio" name="type" class="styled" value="modelling_agency"
                      @php if($organization[0]->type=='modelling_agency') echo "checked='checked'";@endphp disabled="disabled">
                      Modelling Agency
                      </label>
                      </div>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-lg-2">Organization Name <span class="text-danger">*</span></label>
                      <div class="col-lg-4">
                      <input type="text" class="form-control" name="organization_name" placeholder="Organization Name" value="{{ $organization[0]->name }}" disabled="disabled">
                      </div>
                      </div>

                      <h6 class="form-wizard-title">
                      <span>Licenses</span>
                      </h6>
                      <div class="form-group pb-10">
                      <label class="control-label col-lg-2">Licenses <span class="text-danger">*</span></label>
                      <div class="col-lg-2">
                      <input type class="form-control" name="licenses"
                      value="{{ $organization[0]->licenses }}" readonly="readonly">
                      </div>
                      </div>


                      <h6 class="form-wizard-title">
                      <span>Organization User</span>

                      <small class="display-block">Which user are you editing</small>
                      </h6>

                      <div class="form-group">
                      <label class="control-label col-lg-2">Full Name <span class="text-danger">*</span></label>
                      <div class="col-lg-4">
                      <input type class="form-control"  name="fullname" placeholder="Full Name"
                      value="{{ isset($organization_user->name)?$organization_user->name:old('fullname') }}">
                      <div class="text-danger">{{ $errors->first('fullname') }}</div>
                      </div>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-lg-2">Email Address <span class="text-danger">*</span></label>
                      <div class="col-lg-4">
                      <input type class="form-control" name="email" placeholder="Email"
                      value="{{ isset($organization_user->email)?$organization_user->email:old('email') }}">
                      <div class="text-danger">{{ $errors->first('email') }}</div>
                      </div>

                      <label class="control-label col-lg-2">Confirm Email Address <span class="text-danger">*</span></label>
                      <div class="col-lg-4">
                      <input type class="form-control" name="confirm_email" placeholder="Confirm Email"
                      value="{{ isset($organization_user->email)?$organization_user->email:old('email') }}">
                      <div class="text-danger">{{ $errors->first('confirm_email') }}</div>
                      </div>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-lg-2">Primary Role <span class="text-danger">*</span></label>
                      <div class="col-lg-4">
                      <select class="form-control role" name="primary_role" disabled="true">
                      <option value="">Select Primary Role</option>
                      @foreach($primary_roles as $role)
                      <option value="{{ $role->id }}"
                      @if(is_array($user_role) &&
                      in_array($role->id, $user_role)){{'selected'}}
                      @endif
                      @php if(old('primary_role') && old('primary_role')==$role->id) echo "selected='selected'"; @endphp
                      >{{ $role->name }}
                      </option>
                      @endforeach
                      </select>
                      </div>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-lg-2">Additional Roles</label>
                      <div class="col-lg-4">
                      <select class="form-control select 2" name="additional_role[]" id="role_id"
                      multiple="multiple">
                      @foreach($additional_roles as $role)
                      <option value="{{ $role->id }}"
                      @if(is_array($user_role) &&
                      in_array($role->id, $user_role)){{'selected'}}
                      @endif
                      {{ (collect(old('additional_role'))->contains($role->id)) ? 'selected':'' }}
                      >{{ $role->name }}
                      </option>
                      @endforeach
                      </select>
                      </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-lg-2">User License Key</label>
                      <div class="col-lg-4">
                      <input type class="form-control" id="license" name="license"
                      value="{{ isset($user_license->key)?$user_license->key:'' }}" disabled="disabled">
                      </div>
                      @php
                      $license_text = '';
                      $license_exist = @App\Models\License::where('organization_id',$organization[0]->id)
                      ->where('is_active','0')->where('end','>',date("Y-m-d"))->first();
                      @endphp

                      @if(!empty($user_license))
                      @php $license_text = "Revoke License";
                      $disabled = '';
                      $attribute="revoke";
                      @endphp
                      @else
                      @if(!empty($license_exist))
                      @php $license_text = "Attach License";
                      $disabled = '';
                      $attribute="attach";
                      @endphp
                      @else
                      @php
                      $license_text = "No License Available";
                      $disabled = 'disabled="disabled"';
                      $attribute="nolicense";
                      @endphp
                      @endif
                      @endif
                      <div class="col-lg-4">
                      <a class="ripple btn btn-primary license" type="{{$attribute}}" {{$disabled}}>{{ $license_text }}</a>
                      </div>

                      </div>


                      <div class="form-wizard-actions">
                      <a class="ripple btn btn-default" href="/organization/{{$organization[0]->id}}">Cancel</a>
                      <a class="ripple btn btn-info " onclick="editOrganization()">Update User</a>
                      </div>
                      </form>
                      </div>

                      </div>

                      </div>

                      </div>
                      </div>
                      <!--/Page Container-->

                      @stop

                      @section('page-scripts')

                      <!-- Page scripts -->
                      <script src="{{ URL::asset('js/forms/jquery.validate.js') }}"></script>
                      <script src="{{ URL::asset('js/forms/switch.min.js') }}"></script>
                      <script src="{{ URL::asset('js/pages/form_validations.js') }}"></script>
                      <script src="{{ URL::asset('js/lightbox2-master/src/js/lightbox.js') }}">
                      </script>

                      <!-- /page scripts -->

                      <script type="text/javascript">

                      $(".license").on('click',function(){

                        var licensetype = $(this).attr("type");

                        if(licensetype=="nolicense") {
                          return false;
                        }

                        if(licensetype=="revoke") {
                          var text = "Revoke";
                        }

                        if(licensetype=="attach") {
                          var text = "Attach";
                        }

                        user_id = $("#user_id").val();

                        swal({
                          title: ""+text+" User License",
                          text: "Are you sure you want to "+text+" the license of this user?",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonClass: 'btn-success',
                          confirmButtonText: ''+text+'',
                          cancelButtonText: "Cancel",
                          closeOnConfirm: true,
                          closeOnCancel: true
                        },
                        function(isConfirm){
                          if (isConfirm) {
                            $.ajax({
                              url: '{{url("/organization-users/revoke-license")}}',
                              type:'post',
                              data:{_token:'{{csrf_token()}}', user_id: user_id,
                              licensetype:licensetype},
                              success:function(data){

                                if(licensetype=="attach") {
                                  var msg = "License has been assigned to this user"
                                  $("#license").val(data.key);
                                }

                                if(licensetype=="revoke") {
                                  var msg = "This User's license will be revoked and reassigned</font>";
                                  $("#license").val("");
                                }

                                NotifContent = '<div class="alert alert-success media fade in">\
                                <h4 class="alert-title">License</h4>\
                                <p>'+msg+'</p>\
                                </div>',
                                autoClose = true;
                                type = 'success';
                                method = 3000;
                                position = 'bottom';
                                container ='';
                                style = 'box';

                                generate('topRight', '', NotifContent);
                                setTimeout(function(){
                                  location.reload();
                                }, 2000);

                              }
                            });
                          } else {
                            swal("Cancelled", "You have cancelled to "+text+" the user license", "error");
                          }
                        });


                      })

                      function editOrganization(){
                        swal({
                          title: "Edit New User",
                          text: "Are you sure you want to edit this user?",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonClass: 'btn-success',
                          confirmButtonText: 'Edit Organization User',
                          cancelButtonText: "Cancel",
                          closeOnConfirm: false,
                          closeOnCancel: true
                        },
                        function(isConfirm){
                          if (isConfirm) {
                            $( "#formaddOrganization" ).submit();
                          } else {
                            swal("Cancelled", "You have cancelled editing the user", "error");
                          }
                        });

                      };
                      </script>
                      @stop
