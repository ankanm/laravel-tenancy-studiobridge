<?php

Route::group(['module' => 'Organization', 'middleware' => ['web', 'auth'], 'namespace' => '\Modules\Organization\Controllers'], function () {
	  
  	/* Routes for Organization */

	Route::get('organization/add','OrganizationsController@AddOrganization');
	Route::get('organization/edit/{id}', 'OrganizationsController@EditOrganization');
	Route::get('organization-users/add', 'OrganizationsController@AddOrganizationUser');
	Route::get('organization/sequence', 'OrganizationsController@getSequence');

	/**
	 * Settings for Organization Rules
	 *
	 */	
	Route::get('organization/{id}/settings', 'OrganizationsController@getSettings'); 
    Route::resource('organization','OrganizationsController');
	Route::get('organization-users/edit/{id}', 'OrganizationsController@EditOrganizationUser');
	Route::post('organization-users/store', 'OrganizationsController@StoreOrganizationUser');
	
	//Route::post('organization/store', 'OrganizationsController@store');
});