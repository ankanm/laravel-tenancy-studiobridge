<?php

namespace App\Modules\Organization\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class OrganizationModule extends Model
{
	use SoftDeletes, UsesTenantConnection;

	protected $table = 'module_organization';

	protected $fillable = ['organization_id','module_id','deleted_at'];

	protected $dates = ['deleted_at'];
}
