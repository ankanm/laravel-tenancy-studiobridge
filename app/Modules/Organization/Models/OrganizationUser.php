<?php

namespace App\Modules\Organization\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class OrganizationUser extends Model
{
	use UsesTenantConnection;
	protected $table = 'organization_user';

	protected $fillable = ['organization_id','user_id', 'is_admin'];
}
