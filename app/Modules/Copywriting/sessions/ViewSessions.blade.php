@extends('layouts.body')

@section('main-content')
	<!--Page Container-->
	<div class="app-content-body">

		<div class="header">
			<h2>Copywriting <strong>Sessions</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="#">Home</a>
					</li>
					<li><a href="#">Copywriting Desk</a>
					</li>
					<li class="active">My Sessions</li>
				</ol>
			</div>
		</div>
		<!--/Page Header-->

		<div class="">

			<!-- Individual column searching (text fields) -->
			<div class="row m-b-10">
				<div class="col-sm-2">
					<select  class="form-control m-b-10" data-placeholder="Select a filter" style="width: 150px !important;" onChange="window.location.href=this.value">
						<option value="/copywriting/session/list/all" {{ Request::is('copywriting/session/list/all') || Request::is('copywriting/list') ? ' selected ' : null }}>All Sessions</option>
						<option value = "/copywriting/session/list/open" {{ Request::is('copywriting/session/list/open') ? ' selected' : null }}>Open</option>
						<option value = "/copywriting/session/list/closed" {{ Request::is('copywriting/session/list/closed') ? ' selected' : null }}>Closed</option>
					</select>
				</div>

				<div class="col-sm-2 pull-right">
					@if(empty($open_session))
					<div class="pull-right">
						<a class="btn btn-default bg-blue" href="{{route('copywriting.session.add')}}">Add New Session</a>
					</div>
					@endif
				</div>

			</div>
			@if(count($csessions) > 0)
				<div class="panel pagination2 table-responsive">
					<table class="table datatable datatable-column-search-inputs dt-bootstrap no-footer">
						<thead>
							<tr>
								<th>Session ID</th>
								<th>Session Date</th>
								<th>Type</th>
								<th>Status</th>
								<th>Channel</th>
								<th>Contributors</th>
								<th>Products</th>
								<th class="text-center"></th>
							</tr>
						</thead>
						<tbody>

							@foreach ($csessions as $csession)
								@php
								$csessionclass = '';
								$subtext = 'No status available';
								if($csession->status == 'open') {$csessionclass = 'text-success'; $subtext = 'Session Open';}
								if($csession->status == 'closed') {$csessionclass = 'text-info'; $subtext = 'Session Closed';}
								if($csession->status == 'cancelled') {$csessionclass = 'text-danger'; $subtext = 'Session was cancelled';}

								$csession_date  = Carbon\Carbon::parse($csession->created_at)->format('d M, Y');
								$count_class = ($csession->products > 0 ? 'text-success' : 'text-danger');
								@endphp

								<tr>
									<td>{{$csession->id}}</td>
									<td>{{$csession_date}}</td>
									<td> <strong>{{ucfirst($csession->type)}}</strong> </td>
									<td>
										<h6 class="no-margin">
											<span class="text-bold {{$csessionclass}}"  href="">{{ucfirst($csession->status)}}</span>
											<small class="display-block text-muted">{{ucfirst($csession->method)}}</small>
										</h6>
									</td>
									<td>
										@php
										$channels = explode(",", $csession->channels)
										@endphp
										@if(isset($channels))
											@foreach ($channels as $key => $channel)
												<img src="/assets/images/channels/{{strtolower($channel)}}.svg" height="16px" alt=""><br>
											@endforeach
										@endif
									</td>
									<td>
										@if($csession->user_avatar != '')
											<img src="{{$csession->user_avatar}}" class=" img-responsive img-circle img-lg" alt="">
										@endif
									</td>
									<td>
										<span><h3 class="m-xs m-0 fs-25 fw-300 ">{{$csession->products}}</h3></span>
									</td>
									<td class="text-center">
										<ul class="icons-list">
											<li><a href="{!! route('copywriting.session.view', ['id' => $csession->id]) !!}" ><span class="fs-12 text-blue">VIEW</span></a></li>
										</ul>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="text-center">
					{{-- {{ $csessions->links() }} --}}
				</div>
			@else
				@include('templates.emptystate', ['empty_state_text' => 'You have no Session. To add a new Session, please click a button on the top right corner'])
			@endif
		</div>


	</div>
	<!--/Page Container-->
@stop

@section('page-scripts')

	<!-- Page scripts -->
	<script src="{{ URL::asset('js/pnotify.min.js') }}"></script>
	<script src="{{ URL::asset('js/pages/extension_pnotify.js') }}"></script>

	<script src="{{ URL::asset('js/tables/datatables/datatables.min.js') }}"></script>
	<script src="{{ URL::asset('js/pages/datatable_basic.js') }}"></script>
	<!-- /page scripts -->

@stop
