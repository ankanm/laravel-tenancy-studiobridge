@extends('layouts.body')

@section('main-content')
	{{ csrf_field() }}
	<input type="hidden" id="csession_id" value="{{$csession->id}}">
	<input type="hidden" id="csession_uid" value="{{ $csession->user_id }}">
	<input type="hidden" id="csession_method" value="{{ $csession->method }}">
	<input type="hidden" id="csession_minprod" value="{{ $csession->min_product }}">
	<input type="hidden" id="write_prod_count" value="{{ $write_products_count }}">
	<link rel="stylesheet" href="{{ URL::asset('css/snippets.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/htimeline.css') }}">
	<!--Page Container-->
	<div class="app-content-body">
		@if(Session::has('error'))
			<div class="alert alert-danger">
				<strong>Error!</strong> {{ Session::get('error') }}
			</div>
		@endif
		@php
		$button_text  = '';
		$batch_button_text  = '';
		$batchurl = "" ;
		$sessionclass = '';
		$sessiontext = '';

		//Perform some css class operations
		if($csession->status=='open') {$sessionclass = 'text-success'; $sessiontext = 'Session open for adding products'; $sessionstaus = 'Import';}
		if($csession->status=='copywriting') {$sessionclass = 'text-info'; $sessiontext = 'Copywriting in progress'; $sessionstaus = 'Closed';}
		if($csession->status=='cancelled') {$sessionclass = 'text-danger'; $sessiontext = 'Session cancelled by user'; $sessionstaus = 'Cancelled';}
		if($csession->status=='copy pending') {$sessionclass = 'text-info'; $sessiontext = 'Session ready for copywriting'; $sessionstaus = 'Closed';}
		if($csession->status=='review pending') {$sessionclass = 'text-info'; $sessiontext = 'Session submitted for review'; $sessionstaus = 'Review Pending';}

		$session_date = Carbon\Carbon::parse($csession->created_at)->format('d M, Y h:m a');
		@endphp
		<div class="row session-header">
			<div class="col-md-12 m-b-10 p-0">
				<div class="">
					<div class="panel-body p-5">
						<div class="text-center col-sm-1">
							{{-- <h1 class="c-dark">{{$psession}}</h1> --}}
							<i class="icon-pencil p-t-10" style="font-size: 55px"></i>
						</div>
						<div class="clearfix col-xs-12 col-sm-11 border-bottom">
							<p class="c-blue f-16 m-t-10 m-b-5 bold">Session Number {{ $csession->id }}</p>
							<span class="">
								<p class="c-gray inline-block"><i class="fa fa-calendar p-r-10"></i> created {{$session_date}}</p>
								<p class="pull-right inline-block"><i class="fa fa-circle text-success fs-12"></i> {{ucfirst($csession->status)}}</p>
							</span>
						</div>
						<div class="clearfix col-xs-12 col-sm-11 col-sm-offset-1">
							<div class="row">
								<div class="col-md-2">
									<div class="m-b-0 m-t-10">
										<div class="media" >
											<div class="media-body">
												<span class="fs-12 text-muted text-uppercase">Class </span>
												<div class="fs-15 bold">Copywriting</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="m-b-0 m-t-10">
										<div class="media" >
											<div class="media-body">
												<span class="fs-12 text-muted text-uppercase">Method </span>
												<div class="fs-15 bold">{{ ucfirst($csession->method) }}</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="m-b-0 m-t-10">
										<div class="media" >
											<div class="media-body">
												<span class="fs-12 text-muted text-uppercase">Type </span>
												<div class="fs-15 bold ">{{ ucfirst($csession->type) }}</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="pull-right m-t-10">
										<div class="clearfix pull-right">
											@if(strtolower($csession->type) == 'writing')
												@if($csession->status=="closed" || $csession->status=="cancelled")
												@else

													@if($write_products_count==0)
														@if($csession->method=="batch")
															@if((isset($total_batch_products)) && ($total_batch_products == 0))
																@php
																	$batch_button_text = 'Claim products';
																	$batchurl = '/copywriting/session/products/batch/';
																	$submit_text = '<a status="cancelled" class="btn btn-default close-session">Cancel Session</a>';
																@endphp
															@else
																@php
																	$batchurl = '/copywriting/session/products/write-batch/';
																	$batch_button_text = 'Start Writing';
																	$submit_text = '<a status="closed" class="btn btn-default close-session">Closed Session</a>';
																@endphp
															@endif
														@else
															@php $button_text = 'Start Writing'; $submit_text = '<a status="cancelled" class="btn btn-default close-session">Cancel Session</a>';@endphp
														@endif
													@elseif($write_products_count>=1)
														@if($csession->method=="batch")
															@if((isset($total_batch_products)) && ($total_batch_products == 0))
																@php
																	$batch_button_text = 'Claim products';
																	$batchurl = '/copywriting/session/products/batch/';
																	$submit_text = '<a status="closed" class="btn btn-default close-session">Submit Session</a>';
																@endphp
															@else
																@php
																	$batchurl = '/copywriting/session/products/write-batch/';
																	$batch_button_text = 'Continue Writing';
																	$submit_text = '<a status="closed" class="btn btn-default close-session">Submit Session</a>';
																@endphp
															@endif
														@else
															@php
															$button_text = 'Continue Writing';
															$submit_text = '<a status="closed" class="btn btn-default close-session">Submit Session</a>';
															@endphp
														@endif

													@endif

														@if(isset($submit_text)){!! $submit_text !!}@endif

														@if($csession->method=="batch")
															<a href="{{ $batchurl }}{{$csession->id}}" class="btn btn-default bg-blue"> {{ $batch_button_text}} </a>
														@else
															<a href="/copywriting/session/products/write/{{$csession->id}}" class="btn btn-default bg-blue">{{$button_text}}</a>
														@endif

												@endif
											@else
												@php
												$qc_count = \Modules\Copywriting\Models\ProductCopywritingsession::where('copywritingsession_id', $csession->id)->where('qc_state', 1)->count();
												@endphp
												@if($csession->status=="closed" || $csession->status=="cancelled")
												@else

													@if($qc_count == 0)
														@php $button_text = 'Start Review';  @endphp
														@if($csession->method=="batch")

															@if((isset($total_batch_products)) && ($total_batch_products == 0))
																@php
																	$batch_button_text = 'Claim products';
																	$batchurl = '/copywriting/session/products/batch-review/';
																	$submit_text = '<a status="closed" class="btn btn-default close-session">Submit Session</a>';
																@endphp
															@else
																@php
																	$batchurl = '/copywriting/session/products/review/';
																	$batch_button_text = 'Continue Review';
																	$submit_text = '<a status="closed" class="btn btn-default close-session">Submit Session</a>';
																@endphp
															@endif

															<a href="{{ $batchurl }}{{$csession->id}}" class="btn btn-default bg-blue"> {{ $batch_button_text}} </a>
															<a status="cancelled" class="btn btn-default close-session">Cancel Session</a>
														@else
															<a href="/copywriting/session/products/review/{{$csession->id}}" class="btn btn-default bg-blue">{{$button_text}}</a>
															<a status="cancelled" class="btn btn-default close-session">Cancel Session</a>
														@endif

													@else
														@php $button_text = 'Continue Review'; @endphp
														<a status="closed" class="btn btn-default close-session">Submit Session</a>

														@if($csession->method=="batch")
															@if((isset($total_batch_products)) && ($total_batch_products == 0))
																@php
																	$batch_button_text = 'Claim products';
																	$batchurl = '/copywriting/session/products/batch-review/';
																	$submit_text = '<a status="closed" class="btn btn-default close-session">Submit Session</a>';
																@endphp
															@else
																@php
																	$batchurl = '/copywriting/session/products/review/';
																	$batch_button_text = 'Continue Review';
																	$submit_text = '<a status="closed" class="btn btn-default close-session">Submit Session</a>';
																@endphp
															@endif
															<a href="{{ $batchurl }}{{$csession->id}}" class="btn btn-default bg-blue"> {{ $batch_button_text}} </a>
														@else
															<a href="/copywriting/session/products/review/{{$csession->id}}" class="btn btn-default bg-blue">{{$button_text}}</a>
														@endif

													@endif
												@endif
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-9 col-sm-9">
				<ul class="nav nav-tabs" style="z-index:1;">
					<li class="active"><a href="#products" data-toggle="tab"> Products</a></li>
					<li><a href="#export" data-toggle="tab"> Export</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane active " id="products">
						@if(count($products_list) == 0)
							<div id="quote-liveshoot" class="text-center text-black" style="min-height: 100%; padding-top: 150px; font-size: 25px; line-height: 50px; max-width: 400px; margin: auto;">
								<i class="fa fa-archive fa-4x text-grey-lighter"></i>
								<br>
								<h2 class="text-bold">You have no copywriting products</h2>
								<h5>To add products, please click add products on the top right corner.</h5>
							</div>
						@else
							@foreach ($products_list as $product)

								@php
								if ($product->Item == null || $product->Item_Parent == ''){
									continue;
								}

								$latest_revision = Modules\Copywriting\Models\CopyRevisions::where('product_id', $product->product_id)->orderBy('id','DESC')->first();
								$copy_header = '';
								$latest_ch_val = '';
								$copy_templates = '';
								$copy_template_name = '';

								if(!empty($product->Channel) && !empty($product->category)){
									$copy_templates = Modules\Copywriting\Models\CopyTemplates::where('channel', $product->Channel)->where('simple_category', $product->category)->first();
									if(!empty($copy_templates)){
										$copy_header = Modules\Copywriting\Models\CopyHeader::where(['is_visible' => 1, 'copy_template_id' => $copy_templates->id])->orderBy('weight', 'ASC')->get();
										$copy_template_name = $copy_templates->template_name;
									}
									if(!empty($latest_revision)){
										$latest_ch_val = Modules\Copywriting\Models\CopyHeaderValue::where('copy_revision_id', $latest_revision->id)->orderBy('id','ASC')->get();
										$copywriter = App\User::where('id', $latest_revision->user_id)->first()->avatar;
									}
								}

								$total_required = 0;
								$total_required_val = 0;

								$total_external = 0;
								$total_external_val = 0;

								if(!empty($copy_header) && !empty($latest_ch_val)){
									foreach($copy_header as $header){
										if($header->is_required==1){
											$total_required++;
											foreach($latest_ch_val as $value){
												if($value->header_id == $header->id){
													if(strlen($value->value)>=1){
														$total_required_val++;
													}
												}
											}
										} else{
											$total_external++;
											foreach($latest_ch_val as $value){
												if($value->header_id == $header->id){
													if(strlen($value->value)>=1){
														$total_external_val++;
													}
												}
											}
										}
									}
								}
								@endphp

								<div class="panel panel-body m-b-10 col-sm-11">
									<ul class="media-list">
										<li class="media">
											<div class="media-body is-table-row">
												<div class="col-sm-1">
													@if(isset($product->thumbpath))
														<img data-src="{{$product->thumbpath}}" style="width:48px!important;height:48px!important;">
													@else
														<img data-src="/assets/images/noimage_2.gif" style="width:48px!important;height:48px!important;">
													@endif
												</div>
												<div class="col-sm-3">
													@if(strtolower($product->Brand) == 'gap')
														<h5 class="media-heading text-navy text-bold"> {{$product->Item_Parent}}_{{$product->VPN}}_{{$product->GAP_Color_Code}}</h5>
													@else
														<h5 class="media-heading text-navy text-bold"> {{$product->Item_Parent}}_{{$product->ATG_Color_Code}}</h5>
													@endif
													{{$product->Item}}
												</div>
												@php
												$required_class = ($total_required_val == $total_required ? 'text-success bold' : '');
												$extra_class = ($total_external_val == $total_external ? 'text-success bold' : '');
												if($total_external_val == 0){$extra_class = '';}
												@endphp
												<div class="widget-infobox p-t-5 no-border col-sm-5 p-l-0 p-r-0">
													<div class="infobox product-qc" style="min-width:120px !important;">
														<div class="left">
															<div class="m-xs m-0 fs-36 fw-300 p-t-10 p-r-10  {{$required_class}}">{!! $total_required_val !!}/{!! $total_required !!}</div>
														</div>
														<div class="right">
															<div>
																<span class=" pull-left product-qc-state">Required</span>
															</div>
															<div class="txt ">Headers</div>
														</div>
													</div>

													<div class="infobox product-qc" style="min-width:120px !important;">
														<div class="left">
															<div class="m-xs m-0 fs-36 fw-300 p-t-10 p-r-10  {{$extra_class}}">{!! $total_external_val !!}/{!! $total_external !!}</div>
														</div>
														<div class="right">
															<div>
																<span class=" pull-left product-qc-state">Extra</span>
															</div>
															<div class="txt ">Headers</div>
														</div>
													</div>
												</div>
												<div class="col-sm-1 p-t-5">
													@if(isset($copywriter) && $copywriter != '')
														<img data-src="{{ $copywriter}}" class="img-sm img-circle" alt="">
													@else
														<div class="p-t-10">No Writer</div>
													@endif
												</div>
												<div class="col-sm-1 p-t-5">
													<span class="bold fs-14">{{ucfirst($product->Channel)}}</span> <br> {{$copy_template_name}}
												</div>
												<div class="col-sm-1 p-t-20 pull-right">
													<a  class="fs-12 text-blue" href="/product/X/{{$product->product_id}}">VIEW</a>
												</div>
											</div>
										</li>
									</ul>
								</div>

								<div class="col-sm-1 p-t-20 text-center">
									@if($product->qc_state == '1' || (!empty($product->copy_api_status2) && !empty($product->copy_api_details2)))
										@if ($product->copy_api_status2 =='success')
										<i class="fa fa-2x fa-flag-checkered text-success" title="Copy Sent"></i>
										@else
											<i class="fa fa-2x fa-exclamation-triangle fa-amber-darker" title="{{$product->copy_api_detail}}"></i>
										@endif
									@elseif($product->qc_state!=1 && strtolower($csession->type)=='review')
										<i class="fa fa-2x fa-pencil text-grey" data-toggle="tooltip" title="Review Pending"></i>
									@elseif($product->copy_state==1 && strtolower($csession->type)!='review')
										<i class="fa fa-2x fa-check text-grey" data-toggle="tooltip" title="Copy Done"></i>
									@else
										<i class="fa fa-2x fa-pencil text-grey" data-toggle="tooltip" title="Copy Pending"></i>
									@endif
								</div>
							@endforeach
						@endif

					</div>
					<div class="tab-pane" id="export">
						<!--ASN Label -->
						<h3 class="m-t-5">Export <strong>Copy</strong></h3>
						<div class="panel panel-body">
							<ul class="media-list">
								@php
								$channels = explode(",", $csession->channels);
								$templates = explode(",", $csession->templates)
								@endphp
								@if(isset($templates))
									@foreach ($channels as $key => $channel)
										<li class="media">
											<div class="media-left">
												<i class="fa fa-file-excel-o m-t-5" style="font-size: 30px !important;"></i>
											</div>
											<div class="media-body">
												<h5 class="media-heading text-navy text-bold"> {{$channel}}</h5>
												Export all {{$channel}} product's copy in excel format
											</div>
											<div class="media-right">
												<a class="btn btn-default bg-blue" target="_blank" href="/copywriting/session/export/{{$csession->id}}/{{$channel}}">Export </a>
											</div>
										</li>
									@endforeach
								@endif

							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--Right bar-->
			<div class="col-md-3 col-sm-3 p-t-50">
				<div class="panel panel-flat">
					<div class="panel-header">
						<h3>Products</h3>
					</div>
					<div class="panel-body p-t-0">
						<div class="row">
							<div class="col-sm-6 text-center">
								<h6 class="semi-bold">Total</h6>
								<h1 class="m-xs">{{ count($products_list) }}</h1>
							</div>
							<div class="col-sm-6 text-center text-success">
								<h6 class="semi-bold">Approved</h6>
								<h1 class="m-xs">--</h1>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-flat">
					<div class="panel-header">
						<h3>Contributors</h3>
					</div>
					<div class="panel-body p-t-0">
						<ul class="media-list m-b-10">
							@if ($csession->user_id != '')
								@php
								dd($csession->user_id) ;

								$user = App\User::where('id', $csession->user_id)->first();
								@endphp
								<li class="media">
									<a href="#" class="media-left"><img data-src="{{ $user->avatar }}" class="img-sm img-circle" alt=""></a>
									<div class="media-body">
										<a href="#" class="media-heading">{{$user->name}}</a>
										<span class="text-size-mini text-muted display-block">Copy Writer</span>
									</div>
								</li>
							@endif
						</ul>
					</div>
				</div>
			</div>
			<!--End Right bar-->
		</div>
		<!--/Page Container-->
	</div>

	<div id="product-flags-asn" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Product Flags </h4>
				</div>
				<div class="modal-body asn-comment" >
				</div>
			</div>
		</div>
	</div>
	@stop

	@section('page-scripts')

		<!-- Page scripts -->


		<script src="{{ URL::asset('js/studiobridge/session-view-page-operations.js') }}"></script>
		<script src="{{ URL::asset('js/charts/chartjs/chart.js') }}"></script>
		<script src="{{ URL::asset('js/extensions/countdown/jquery.plugin.js') }}"></script>
		<script src="{{ URL::asset('js/extensions/countdown/jquery.countdown.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/10.4.1/lazyload.min.js"></script>
		<!-- /page scripts -->
		<script type="text/javascript">
		$(function () {
			new LazyLoad();
		});

		$(document).on("click",".close-session",function(){

			var csrfToken = $('[name=_token]').val();
			var csession_id = $("#csession_id").val();
			var status = $(this).attr('status');
			var csession_method = $("#csession_method").val();
			var csession_minprod = parseInt($("#csession_minprod").val());
			var write_prod_count = parseInt($("#write_prod_count").val());

			if(status=='copy-pending') {
				var sesstitle = 'End Session';
				var sesstext = 'Are you sure you want to end this session?';
			}

			if(status=='closed') {
				var sesstitle = 'End Session';
				var sesstext = 'Are you sure you want to end this session?';
			}

			if(status=='cancelled') {
				var sesstitle = 'Cancel Session';
				var sesstext = 'Are you sure you want to end this session?';
			}

			if(status=='review-pending') {
				var sesstitle = 'Submit Session';

				if(csession_method=="continuous" && csession_minprod>0 &&
				csession_minprod>write_prod_count){

					var sesstext = 'The minimum products for this session has been set to '+csession_minprod+'. Do you want to submit this session for review ?';
				} else {
					var sesstext = 'Are you sure you want to submit this session for review?';
				}
			}


			swal({
				title: sesstitle,
				text: sesstext,
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#F44336",
				confirmButtonText: sesstitle,
				cancelButtonText: "Cancel",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(isConfirm){

				if (isConfirm) {
					$.ajax({
						url: '/copywriting/session/submit',
						type:'post',
						headers: {
							'X-CSRF-Token': csrfToken
						},
						data:{ csession_id:csession_id, status:status },
						success:function(data){

							NotifContent = '<div class="alert alert-success media fade in">\
							<h4 class="alert-title">Session Submitted</h4>\
							<p>Session Closed</p>\
							</div>',
							autoClose = true;
							type = 'success';
							method = 3000;
							position = 'bottom';
							container ='';
							style = 'box';

							generate('topRight', '', NotifContent);
							location.reload();
						}
					});
				}
			});

		});

		</script>
		<script>
		$(function () {

			$('[data-toggle="tooltip"]').tooltip();

			// panel-stat-chart, visitors-chart
			var widgetMapHeight = $('.widget-map').height();
			var pstatHeadHeight = $('.panel-stat-chart').parent().find('.panel-header').height() + 12;
			var pstatBodyHeight = $('.panel-stat-chart').parent().find('.panel-body').height() + 15;
			var pstatheight = widgetMapHeight - pstatHeadHeight - pstatBodyHeight + 30;
			$('.panel-stat-chart').css('height', 150);
			var clockHeight = $('.jquery-clock ').height();
			var widgetProgressHeight = $('.widget-progress-bar').height();

			var nextYear = moment("{{$csession->session_deadline}}");

			$('.total-time').countdown(nextYear.toDate())
			.on('update.countdown', function(event) {
				var $this = $(this);
				$this.html(event.strftime('<div>%D<span class="fs-12">Days</span> %H<span class="fs-12">Hours</span> %M<span class="fs-12">Minutes</span>%S<span class="fs-12">Seconds</span></div>'));

			});

			@if($csession->status == 'copy pending' || $csession->status == 'open')
			$('.session-deadline').removeClass('hidden')
			@endif


		});

		//----------------------------------
		// To get product flags
		//----------------------------------


		$(document).on("click",".show-product-flags",function(){

			var product_id = $(this).attr('data-product-id');
			var csession_id = $('#csession_id').val();
			var csrfToken = $('[name=_token]').val();

			var $request = $.ajax({
				url: '/helper/productflags',
				type:'post',
				headers: {
					'X-CSRF-Token': csrfToken
				},
				data:{product_id: product_id, type:'csession', type_id:csession_id},
			});
			$request.done(function(data) { // success

				if(data.status == 'success'){
					var container = $('.asn-comment')
					container.html(data.html)

					$('#product-flags-asn').modal('show');
				}
			});
		});


		$(document).on("click",".export-copy",function(){

			var csession_id = $('#csession_id').val();
			var csrfToken = $('[name=_token]').val();

			var $request = $.ajax({
				url: '/copywriting/session/export',
				type:'post',
				headers: {
					'X-CSRF-Token': csrfToken
				},
				data:{copywritingsession_id: csession_id},
			});
			$request.error(function() {
				console.log(error)

			});
		});
		</script>
	@stop
