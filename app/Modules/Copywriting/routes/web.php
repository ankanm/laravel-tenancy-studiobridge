<?php

/*
|--------------------------------------------------------------------------
| All routes for Copywriting Desk
|--------------------------------------------------------------------------
*/

Route::group(['module' => 'Copywriting', 'prefix' => 'copywriting', 'middleware' => ['web','roleroute'], 'namespace' => '\Modules\Copywriting\Controllers'], function() {

    //Route::resource('copywriting', 'CopywritingController');

    /* Routes for View Sessions */
  Route::get('/session/list/{status?}', 'CopywritingController@ViewAllSessions')->name('copywriting.session.all');
  /* Routes for Add Sessions */
  Route::get('/session/add', 'CopywritingController@AddSession')->name('copywriting.session.add');
  Route::post('/session/add', 'CopywritingController@StoreSession');
  /* Routes for View a Session */
  Route::get('/session/view/{id}', 'CopywritingController@ViewSession')->name('copywriting.session.view');
  /* Routes for Add Products */
  Route::get('/session/products/add/{id}', 'CopywritingController@AddProducts')->name('copywriting.session.products.add');
  /* Routes for Add Products : Batch */
  Route::get('/session/products/batch/{id}', 'CopywritingController@CopywritingAddBatchProduct');
  /* Routes for Storing Products */
  Route::post('/session/products/store', 'CopywritingController@StoreProducts');
  /* Routes for Dropping Products */
  Route::post('/session/dropproduct', 'CopywritingController@dropProducts');
  /* Export Session */
  Route::get('/session/export/{id}/{channel}', 'CopywritingController@ExportCopy');

  Route::get('/session/products/write/{id}', 'CopywritingController@WriteCopy');

  /* Write Copy */
  Route::get('/session/products/write-batch/{id}', 'CopywritingController@WriteBatchCopy');

  /* Routes for Getting Product */
  Route::post('/session/products/write/product', 'CopywritingController@GetProduct');
  /* Routes for Storing Copy */
  Route::post('/session/products/write/store', 'CopywritingController@StoreDraft');

  Route::post('/session/agencyusers', 'CopywritingController@GetCopywritingAgencyUsers');

  Route::post('/session/refreshProducts', 'CopywritingController@refreshProducts');
  Route::post('/session/refreshBatchProducts', 'CopywritingController@refreshBatchedProducts');
  /**
  * refreshReviewBatchProducts for listing after review batch product adding.
  */

  Route::post('/session/refreshReviewBatchProducts', 'CopywritingController@refreshBatchedReviewProducts');
  /* Routes for Clossing Session */
  Route::post('/session/submit', 'CopywritingController@statusUpdate');

  /* Routes for Filter Session Products*/
  Route::post('/session/filter', 'CopywritingController@filterProducts');

  // Start Review
  Route::get('/session/products/review/{id}', 'CopywritingController@StartReview');
  // Start Batch Review
  Route::get('/session/products/batch-review/{id}', 'CopywritingController@StartReviewBatchCopy');
  /* Routes for Getting Review Product */
  Route::post('/session/products/review/product', 'CopywritingController@GetReviewProduct');
  /* Routes for Storing Reviewed Product */
  Route::post('/session/products/review/store', 'CopywritingController@SubmitReview');
  /* Refresh Products*/
  Route::get('session/{id}/refresh', 'CopywritingController@RenderProducts');

  Route::post('/session/review/product/state', 'CopywritingController@ReviewStatusUpdate');

  Route::get('session/{id}/review/refresh', 'CopywritingController@RenderCopyListProducts');

  Route::get('session/{id}/review/render', 'CopywritingController@RenderReviewProducts');

  //refresh pending products
  Route::get('session/{id}/pending/refresh', 'CopywritingController@PendingCopyListProducts');

  Route::get('/session/actions/submit/{id}', 'CopywritingController@EndSession');

  Route::get('/{id}/refresh/count', 'CopywritingController@PendingItemsCount'); //Return live shoot page for adding a new product

  Route::post('/memory', 'CopyMemoryController@loadFromMemory')->name('copywriting.memory');

  /* Routes for Copy Headers */
  Route::get('/header/list', 'CopywritingController@ViewAllHeaders')->name('copywriting.headers.viewlist');
  Route::get('/header/data', 'CopywritingController@CopyHeaderData')->name('copywriting.headers.data');
  Route::get('/header/edit/{id}', 'CopywritingController@CopyHeaderEdit')->name('copywriting.headers.edit');
  Route::get('/header/delete/{id}', 'CopywritingController@CopyHeaderDel')->name('copywriting.headers.delete');
  Route::post('/header/store', 'CopywritingController@CopyHeaderStore');
  Route::post('/header/orderupdate', 'CopywritingController@updateOrderCopyHeader');
  Route::get('/templates', 'CopywritingController@ViewAllHeadersChannels')->name('copywriting.headers.viewlist');

  Route::get('/templates/edit/{id}', 'CopywritingController@CopyTemplateGet');
  Route::get('/templates/{copy_templates_id}', 'CopywritingController@ViewAllHeaders')->name('copywriting.headers.viewlist');
  Route::get('/header/data/{copy_templates_id}', 'CopywritingController@CopyHeaderData')->name('copywriting.headers.cdata');
  Route::post('/template/store', 'CopywritingController@CopyTemplateStore');

  /* Product locking for specific user */
  Route::post('/session/products/lock', 'CopywritingController@lockProductSpecificUser');

  Route::get('/GetParentCopyProducts/{id}', 'CopywritingController@GetParentCopyProducts');
  Route::get('/getReviewProducts/continuous/{id}/{channel}', 'CopywritingController@getContinuousReviewProducts');

});
