<?php

Route::group(['module' => 'Copywriting', 'middleware' => ['api'], 'namespace' => 'App\Modules\Copywriting\Controllers'], function() {

    Route::resource('copywriting', 'CopywritingController');

});
