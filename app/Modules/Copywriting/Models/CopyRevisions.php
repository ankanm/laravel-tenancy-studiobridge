<?php

namespace Modules\Copywriting\Models;

use Illuminate\Database\Eloquent\Model;

class CopyRevisions extends Model
{
 public function products()
 {
     return $this->hasOne('App\Models\Product', 'product_id');
 }
 public function copywritingsessions()
 {
     return $this->belongsTo('Modules\Copywriting\Models\Copywritingsession', 'copywritingsession_id');
 }
}
