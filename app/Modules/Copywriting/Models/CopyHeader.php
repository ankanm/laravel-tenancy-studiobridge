<?php

namespace Modules\Copywriting\Models;
use Illuminate\Database\Eloquent\Model;

class CopyHeader extends Model
{
	protected $fillable = [
        'id', 'header_name', 'header_type', 'default_value', 'required', 'copy_templates_id', 'channels', 'weight'
    ];

    public function copyheadervalue()
 	{
     	return $this->belongsTo('Modules\Copywriting\Models\CopyHeaderValue', 'header_id');
 	}

 	public function copytemplates()
 	{
     	return $this->belongsTo('Modules\Copywriting\Models\CopyTemplates', 'copy_templates_id');
 	}
}
