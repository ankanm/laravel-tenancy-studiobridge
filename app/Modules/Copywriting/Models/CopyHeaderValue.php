<?php

namespace Modules\Copywriting\Models;
use Illuminate\Database\Eloquent\Model;

class CopyHeaderValue extends Model
{
	protected $fillable = ['product_id','header_id','copy_revision_id','value'];
	public $timestamps = false;
    public function copyheader()
 	{
     	$this->hasOne('Modules\Copywriting\Models\CopyHeader', 'id');
 	}
}
