<?php

namespace Modules\Copywriting\Models;

use Illuminate\Database\Eloquent\Model;

class Copywritingsession extends Model
{
	protected $fillable = [
		'status',
		'type',
		'method',
		'user_id',
		'agency_id',
		'agency_uid',
		'session_type',
		'session_deadline',
		'hard_deadline',
		'notes',
		'min_product'];
	protected $guarded = [];

}
