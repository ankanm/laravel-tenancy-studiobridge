<?php

namespace Modules\Copywriting\Models;
use Illuminate\Database\Eloquent\Model;

class CopyTemplates extends Model
{
	protected $fillable = [
        'id', 'template_name'
    ];

    public function copyheaders()
 	{
     	return $this->hasOne('Modules\Copywriting\Models\CopyHeaders', 'id');
 	}
}
