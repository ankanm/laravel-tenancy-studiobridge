<?php

namespace Modules\Copywriting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductCopywritingsession extends Model
{
 	protected $guarded = [];
	protected $table = 'product_copywritingsessions';
	protected $fillable = ['product_id','copywritingsession_id','user_id','state', 'copy_state', 'batch', 'qc_state'];

 	public function products()
 	{
	//1 Session can have many products
     	return $this->belongsTo('App\Models\Product', 'product_id');
 	}

 	public function revisions()
 	{
	//1 Session can have many products
     	return $this->hasMany('Modules\Copywriting\Models\CopyRevisions', 'copywritingsession_id');
 	}

}
