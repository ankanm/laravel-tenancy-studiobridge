<form class="form-horizontal p-t-10" id="copywriting_scan_import" onsubmit="return false;">
  {{ csrf_field() }}
  <div class="form-group col-sm-12">
    <select id="copy-channel" class="form-control" name="channel">
      <option value="0">Select Channel</option>
      @php
      $channels = App\Models\Ounass\Channel::all();
      @endphp
      @if($channels->count() > 0)
        @foreach($channels as $channel)
          <option value="{{$channel->channel_code}}">{{$channel->channel_name}}</option>
        @endforeach
      @endif
    </select>
  </div>
  <div class="form-group col-sm-12">
    <input type="" class="form-control ui-wizard-content filter-textbox" id="copy-identifier" name="short_name" placeholder="Scan Identifier">
    <span class="text-xs filtered_products"></span>
  </div>
  <input type="hidden" id="copy-csession-id" name="csession_id" value="{{$csession->id}}">
  <input type="hidden" id="copy-csession-uid" name="csession_uid" value="{{Auth::id()}}">
  <input type="hidden" id="selected-model-id" name="model_id" value="">
</form>
<br style="clear: both;">
