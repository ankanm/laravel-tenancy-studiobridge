@extends('layouts.body')


@section('main-content')

  <link rel="stylesheet" href="{{ URL::asset('js/lightbox2-master/src/css/lightbox.css') }}">

  <div class="app-content-body app-content-full">

    {{ csrf_field() }}
    <input type="hidden" id="csession-id" name="csession_id" value="{{$csession->id}}">
    <input type="hidden" name="uid" id="qc-uid" value="{{ Auth::user()->id }}">
    <!-- hbox layout -->
    <div class="hbox hbox-auto-xs bg-light ">
      <!-- column -->
      <div class="col w lter b-r secondary-sidebar" >
        <div class="vbox">
          <div class="wrapper b-b">
            <div class="font-thin h4 m-t-0 m-b-0">Review <strong>Copy</strong> </div>
            <div class="text-size-small text-info bold"><a href="/copywriting/session/view/{{$csession->id}}">go to session page</a></div>
          </div>
          <div class="row-row">
             @if($csession->method!="batch") <br> @endif
            <div class="form-group col-sm-12 @if($csession->method=="batch") hide @endif ">
              <select id="review-channel" class="form-control" name="channel">
                <option value="0">Select Channel</option>
                @php
                $channels = App\Models\Ounass\Channel::all();
                @endphp
                @if($channels->count() > 0)
                  @foreach($channels as $channel)
                    <option value="{{$channel->channel_code}}">{{$channel->channel_name}}</option>
                  @endforeach
                @endif
              </select>
            </div>

            <div class="nav-tabs-alt">
              <ul class="nav nav-tabs nav-justified m-b-0">
                <li class="active">
                  <a data-target="#tab-1" role="tab" data-toggle="tab">Queue (<span class="queue-tab-count">{{-- $total_products --}}</span>) </a>
                </li>
                <li class="">
                  <a data-target="#tab-2" role="tab" data-toggle="tab">Complete (<span class="complete-tab-count">{{-- $total_products_completed --}}</span>) </a>
                </li>
              </ul>
            </div>

            <div class="cell scrollable hover">
              <div class="cell-inner">
                <div class="tab-content">

                  <div class="tab-pane active" id="tab-1">
                    <ul class="media-list media-list-linked media-list-bordered media-email new review-master-list">
                      <!-- @include('copywriting.partials.ReviewProducts') -->
                    </ul>
                  </div>

                  <div class="tab-pane" id="tab-2">
                    <ul class="media-list media-list-linked media-list-bordered media-email review-complete-list">
                    </ul>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- /column -->
      <div class="col w-4xl b-r">
        <div class="vbox">
          <div class="row-row">
            <div class="cell">
              <div class="cell-inner">
                <div class="panel-heading">
                  <h4>Product Details</h4>
                </div>
                @include('copywriting.partials.CopyProductDetails')
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- column -->
      <div class="col">
        <div class="vbox">
          <div class="row-row">
            <div class="cell">
              <div class="cell-inner">
                <div class="panel-heading">
                  <h4>Product Copy</h4>
                </div>
                @include('copywriting.partials.CopyProduct')
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!--/Page Container-->
    <div class="copy-memory-container">
      @include('partials.modals.addCopyMemory')
    </div>

  </div>

  <!--/Page Container-->
  <input type="hidden" name="copyActiveProduct" id="copyActiveProduct" value="0">
  <input type="hidden" name="isLocked" id="isLocked" value="0">
  <input type="hidden" name="issubmit" id="issubmit" value="0">
  <input type="hidden" name="csession_method" id="csession_method" value="{{ $csession->method }}">
@stop


@section('page-scripts')

  <!-- Page scripts -->
  <script src="{{ URL::asset('assets/editors/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ URL::asset('js/pages/page_emails.js') }}"></script>
  <script src="{{ URL::asset('js/studiobridge/addproducts.js') }}"></script>
  <script src="{{ URL::asset('js/extensions/countdown/jquery.plugin.js') }}"></script>
  <script src="{{ URL::asset('js/extensions/countdown/jquery.countdown.js') }}"></script>
  <script src="{{ URL::asset('js/studiobridge/qc-product-operations.js') }}"></script>
  <script src="{{ URL::asset('js/studiobridge/liveshoot-resequence.js') }}"></script>

  <script src="{{ URL::asset('js/lightbox2-master/src/js/lightbox.js') }}"></script>
  <!-- /page scripts -->

  <script>

  /**
   *
   * Constant | Global variables
   *
   */

  // set global Variables for Lock functionality.
  var selectedProduct = '';
  var totalCount = 0;
  var isLocked = $("#isLocked").val();
  var copyActiveProduct = $("#copyActiveProduct").val();
  var total_count_review_default = 0 ;

  //----------------------------------
  // When delete image is clicked
  //----------------------------------

  $('body').addClass('sidebar-collapsed')

  function checkforUpdates(){
    var numItems = $('.qc-product-a').length;

    if (numItems < 20) {
      //check if total products are less than 20 as well. in that case do nothing
      //renderProducts();
    }
  }

  setInterval(checkforUpdates, 5000);

  $(".qc-refresh-btn").on("click",function(){
    refreshProducts();
  });

  setTimeout(function() {
    $('a[href="#email"]').click();
  }, 1000);

  function refreshProducts(){
    //load the products
    //$("#issubmit").val(1);
    var qsession = $('#qsession-id').val();

    var $request = $.get("/copywriting/session/"+qsession+"/refresh"); // make request
    var $container = $('.review-master-list');

    $('#status-qc').removeClass('hidden');

    $request.done(function(data) { // success
      $container.html(data.html);
      $('.review-master-list li:first').click();
      setTimeout(function(){
        $("#edit_copy_write_product").removeClass('hide');
      }, 2000);

    });
    $request.always(function() {
      $('#status-qc').addClass('hidden');
      var total_count = $('.qc-product-a').length;
      $('#qc-total-products').html(total_count);
    });

    //$('.initials-profile-qc').initial();
  }


  /**
   *
   * Page laoding and ajax load after any action  
   *
   */
  function loadingProductsWithLoadPage(){

    var csession = $('#csession-id').val() ;

    var $request = $.get("/copywriting/session/"+csession+"/review/render"); // make request
    var $container = $('.review-master-list');
    var $complete_container = $('.review-complete-list');

    $('.queue-tab-count').html(total_count_review_default);
    $('.complete-tab-count').html(total_count_review_default);

    $('#status-qc').removeClass('hidden');

    $request.done(function(data) { // success

      //console.log(data) ;

      $container.html(data.html);
      $complete_container.html(data.html_complete);
      $('.queue-tab-count').html(data.total_products);
      $('.complete-tab-count').html(data.total_products_completed);

      $('#session-total-products').html(data.total_products);

      $('.review-master-list li:first').click();
      setTimeout(function(){
        $("#edit_copy_write_product").removeClass('hide');
      }, 2000);

    });
  }

  loadingProductsWithLoadPage() ;


  /**
   *
   * Check if current product has been submitted or not
   *
   */
  function changeProduct(pid) {
      if (selectedProduct == pid) {
          return;
      }
      if (selectedProduct != '' && selectedProduct != pid && $("#isLocked").val() == 1) {
          console.log('selectedProduct' + selectedProduct);
          //There is a product currently being QC
          $("#copyActiveProduct").val(1);
          swal({
              title: "Discard Current Product",
              text: "You could not switch your current product, do you want to discard these changes ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Discard',
              cancelButtonText: "Cancel",
              closeOnConfirm: true,
              closeOnCancel: true
          }, function(isConfirm) {
              if (isConfirm) {
                  $('.lock-product-' + selectedProduct).addClass('hidden');
                  $("#copyActiveProduct").val('0');
                  var csession = $('#csession-id').val();
                  // update Lock by calling ajax
                  $.ajax({
                      url: '/copywriting/session/products/lock',
                      method: 'POST',
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      },
                      data: {
                          "product": selectedProduct,
                          "csession": csession,
                          "type": 'discard',
                      },
                      success: function(node) {
                          //console.log(node) ;
                          $("#isLocked").val(0);
                          if (node.html == 'discard') {
                              $('.review-master-list li').each(function() {
                                  if ($(this).attr('id') == selectedProduct) {
                                      $(this).children('div').removeClass('copywritinglock');
                                  }
                              });
                          }
                          selectedProduct = '';
                          $("#" + pid).click();
                      },
                      error: function() {
                          alert('Failed! **');
                      }
                  });
              } else {
                  copyActiveProduct = $("#copyActiveProduct").val();
                  return copyActiveProduct;
              }
          });
      } else {
          selectedProduct = pid;
          $("#copyActiveProduct").val(0);
          copyActiveProduct = $("#copyActiveProduct").val();
          return copyActiveProduct;
      }
      copyActiveProduct = $("#copyActiveProduct").val();
      return copyActiveProduct;
  }

  /* Get product */
  //copywriting-product-check
  $(document).on("click", ".pending-copy-product", function(){
    var csrfToken = $('[name=_token]').val();
    var csession = $('#csession-id').val();
    var product_id = $(this).attr('id');
    var completion = $(this).attr('data-completion');

    var issubmit = $("#issubmit").val();
    if(issubmit == 0){
      var next_prod = changeProduct(product_id);
    }
    else{
      $("#issubmit").val('0');
    }
    if(next_prod==0){

      var editable = false ;
      if ($(this).children('div').hasClass('copywritinglock')) {
        var user_id = $(this).children('div .copywritinglock').data('user_id');
        var lock_user_id = $(this).children('div .copywritinglock').data('lock-user');
        if (parseInt(user_id) == parseInt(lock_user_id)) {
          $("#isLocked").val(1);
          editable = true ;
        }else{
          swal({
              title: "Products is locked.",
              text: "Your selected products have been locked by another user.",
              timer: 3000,
              type: "warning",
              showConfirmButton: false
            });
          return false;
        }
      }

      $('.media').removeClass('new')
      $(this).addClass('new')
      $('.no-product-container').addClass('hidden');
      $('.product-details-container').removeClass('hidden');
      $('.product-template-container').removeClass('hidden');

      open_copy_product(csrfToken, product_id, csession, editable, completion);
      
      // $.ajax({
      //   url: '/copywriting/session/products/write/product',
      //   method: 'POST',
      //   headers: {
      //     'X-CSRF-Token': csrfToken
      //   },
      //   data: {
      //     "product": product_id,
      //     "csession": csession,
      //     "session_type" : "review",
      //     "completion": completion,
      //   },
      //   success: function (node) {

      //     $('.no-product-container').addClass('hidden');
      //     $('.product-details-container').removeClass('hidden');
      //     $('.product-template-container').removeClass('hidden');
      //     $('.productCopy').html(node['copyProduct']);
      //     $('.productCopyDetails').html(node['copyProductDetails']);

      //     setTimeout(function(){
      //       $("#edit_copy_write_product").removeClass('hide');
      //     }, 2000);
      //         //renderMCE();
      //     if (!editable) {
      //       $('.required_value').each(function() {
      //         var datatype = $(this).attr('data-type') ;
      //         if (datatype == 'textarea') {
      //           //tinyMCE.get($(this).attr('id')).getBody().setAttribute('contenteditable', false);
      //           $(this).attr('disabled','disabled');
      //         }else{
      //           $(this).attr('disabled','disabled');
      //         }
      //       });
      //     }else{
      //       renderMCE();
      //     }

      //     var $slider = $('.slickSlider')
      //     .on('init', function(slick) {
      //       console.log('fired!');
      //       $('.slickSlider').fadeIn(3000);
      //     })
      //     .slick({
      //       dots: true,
      //       infinite: true,
      //       speed: 300,
      //       slidesToShow: 1,
      //     });

      //   },
      //   error: function(){
      //     alert('Failed! **');
      //   }

      // });
    }

  });

  $(document).on("click", ".statusbtn", function(){
    var csrfToken = $('[name=_token]').val();
    var csession = $('#csession-id').val();
    var btntype = $(this).attr('btntype');
    var product_id = $('#revision-prod-id').val();

    if(typeof product_id === 'undefined') {
      NotifContent = '<div class="alert alert-danger media fade in">\
      <h4 class="alert-title">Select Product</h4>\
      <p>Please select a product first !!</p>\
      </div>',
      autoClose = true;
      type = 'danger';
      method = 3000;
      position = 'bottom';
      container ='';
      style = 'box';

      generate('topRight', '', NotifContent);

      return false;
    }

    if(btntype=="approve") {
      var txt = 'Approve';
      var confirmtxt = 'Approved';
    }

    if(btntype=="reject") {
      var txt = 'Reject';
      var confirmtxt = 'Rejected';
    }

    swal({
      title: ""+txt+" Product",
      text: "Are you sure you want to "+txt+" this product?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: 'btn-success',
      confirmButtonText: ''+txt+' Product',
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
          url: '/copywriting/session/review/product/state',
          method: 'POST',
          headers: {
            'X-CSRF-Token': csrfToken
          },
          data: {
            "product": product_id,
            "csession": csession,
            "btntype":btntype,
            "session_type" : "review",
          },
          success: function (node) {

            NotifContent = '<div class="alert alert-success media fade in">\
            <h4 class="alert-title">Product '+confirmtxt+'</h4>\
            <p>Product has been '+confirmtxt+'</p>\
            </div>',
            autoClose = true;
            type = 'success';
            method = 3000;
            position = 'bottom';
            container ='';
            style = 'box';

            generate('topRight', '', NotifContent);

            refreshProducts();
            $('.no-product-container').addClass('hidden');
            $('.product-details-container').removeClass('hidden');
            $('.product-template-container').removeClass('hidden');
            $('.productCopy').html('');

          },
          error: function(){
            alert('Failed! **');
          }

        });
      } else {
        swal("Cancelled", "You have cancelled to "+txt+" the product", "error");
      }
    });

  });

  /* Save Draft */
    
  //copywriting-product-check
  $(document).on("click", ".save-product-draft", function() {
    var product_id = $('#revision-prod-id').val();
    var frmdata = $("#copydeksfrm").serializeArray();
    var csession_method = $('#csession_method').val();
    $(".basic-editor").each(function() {
        var id_editor = $(this).attr('id');
        frmdata.push({
            name: id_editor,
            value: tinyMCE.get(id_editor).getContent()
        });
    });
    frmdata.push({
        name: 'csession_id',
        value: $('#csession-id').val()
    });
    if ($(this).attr("btntype") == "save") {
        var copy_state = '1';
        var qc_state = '0';
    }
    if ($(this).attr("btntype") == "approve") {
        var copy_state = '1';
        var qc_state = '1';
    }
    var validate_copy = true;
    if ($(this).attr("btntype") == "submit" || $(this).attr("btntype") == "approve") {
        if ($('.required_value').length > 0) {
            $(".required_value").each(function() {
                var id_editor = $(this).attr('id');
                if (($(this).data('type') == "text" || $(this).data('type') == "dropdown") && $(this).val() == "") {
                    alert('Please fill all required fields');
                    validate_copy = false;
                    return false;
                } else if (($(this).data('type') == "textarea")) {
                    var txt_val = tinyMCE.get(id_editor).getContent({ format: 'text' });
                    var min_words = $(this).attr('data-min-words')
                    if (txt_val == "") {
                        alert('Please fill all required fields');
                        validate_copy = false;
                        return false;
                    }
                    var regex = /\s+/gi;
                    var wordCount = txt_val.trim().replace(regex, ' ').split(' ').length;
                    if (min_words != null || min_words != 0) {
                        var regex = /\s+/gi;
                        var txt_val2 = txt_val.toString();                        
                        var words = txt_val2.split(regex);
                        var wordCount = words.length;
                        
                        if (wordCount >= min_words) {} else {
                            alert('Please fill in the required minimum of ' + min_words + ' words');
                            validate_copy = false;
                            return false;
                        }
                    }
                }
            });
        }
    }
    frmdata.push({
        name: 'copy_state',
        value: copy_state
    });
    frmdata.push({
        name: 'qc_state',
        value: qc_state
    });
    frmdata.push({
        name: 'state',
        value: qc_state
    });
    if (validate_copy == true) {
        $("#issubmit").val('1');
        var csession = $('#csession-id').val();
        var product_id = $('#revision-prod-id').val();
        $.ajax({
            url: '/copywriting/session/products/write/store',
            method: 'POST',
            data: frmdata,
            success: function(node) {

              $.ajax({
                url: '/copywriting/session/products/lock',
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "product": product_id,
                    "csession": csession,
                    "type": 'discard',
                },
                success: function(node) {
                    console.log(node) ;
                    $("#isLocked").val(0);
                    if (node.html == 'discard') {
                        $('.review-master-list li').each(function() {
                            if ($(this).attr('id') == product_id) {
                                $(this).children('div').removeClass('copywritinglock');
                                $('.lock-product-' + product_id).addClass('hidden');
                            }
                        });
                    }
                },
                error: function() {
                    alert('Failed! **');
                }
              });

                
                swal({
                    title: "Product Submitted",
                    text: "Product approved and submitted to Magento",
                    timer: 1000,
                    type: "success",
                    showConfirmButton: false
                });


                if (copy_state == '1') {
                    NotifContent = '<div class="alert alert-success media fade in">\
                    <h4 class="alert-title">Product Submitted</h4>\
                    <p>Product has been submitted</p>\
                    </div>',

                    autoClose = true;
                    type = 'success';
                    method = 1000;
                    position = 'topRight';
                    container = '';
                    style = 'box';
                    generate('topRight', '', NotifContent);
                    //refreshProducts();
                    //$('.productCopy').html('');
                    $('.no-product-container').addClass('hidden');
                    $('.product-details-container').removeClass('hidden');
                    $('.product-template-container').removeClass('hidden');
                }

                $('.review-master-list li:first').click();

                if (csession_method != 'batch') {
                  refreshReviewProducts();
                }else{
                  loadingProductsWithLoadPage() ;
                }

                $('.copy-review-product-' + product_id).removeClass('hidden');
                //$('.lock-product-' + product_id).addClass('hidden');
                //location.reload();
            },
            error: function() {
                alert('Failed! **');
            }
        });
    }
  });

  function refreshProducts(){
    //load the products
    //$("#issubmit").val(1);
    var csession = $('#csession-id').val()

    var $request = $.get("/copywriting/session/"+csession+"/review/refresh"); // make request
    var $container = $('.review-master-list');

    $('#status-qc').removeClass('hidden');

    $request.done(function(data) { // success
      $container.html(data.html);
      $('#session-total-products').html(data.total_products);
    });
  }

  function renderMCE(){
    tinymce.remove();
    tinymce.init({
      selector: '.basic-editor',
      height: 100,
      menubar: false,
      toolbar: 'undo redo | bold italic | bullist numlist',
      content_css: [
        '//www.tinymce.com/css/codepen.min.css'
      ]
    });

  }

    $(document).on("click","#edit_copy_write_product" ,function(){
      //$("#copy_edit").show();
    $('.required_value').each(function() {
      var datatype = $(this).attr('data-type') ;
      if (datatype == 'textarea') {
        //tinyMCE.get($(this).attr('id')).getBody().setAttribute('contenteditable', true);
        $(this).attr('disabled', false);
      }else{
        $(this).attr('disabled', false);
      }
    });
    $('.ripple').attr('disabled',false); // button disabled
    renderMCE();
    var that = $(this) ;
    var csrfToken = $('[name=_token]').val();
    var csession = $('#csession-id').val();
    var product_id = $(this).attr('data-product');
    console.log(csrfToken + product_id + csession) ;

    $.ajax({
      url: '/copywriting/session/products/lock',
      method: 'POST',
      headers: {
        'X-CSRF-Token': csrfToken
      },
      data: {
        "product": product_id,
        "csession": csession,
        "type": 'lock-edit',
        "reviewtype": 1,
      },
      success: function (node) {
        console.log(node) ;
        $("#isLocked").val(1);
        that.parent().parent().hide();
        $('#discard_copy_write_product').parent().parent().show();
        $('.review-master-list li').each(function() {
          var product_id_li = $(this).attr('id') ;
          if (product_id_li == product_id) {
            $(this).append(node.html) ;
          }
        });

        open_copy_product(csrfToken, product_id, csession, true);

        var $slider = $('.slickSlider')
        .on('init', function(slick) {
          console.log('fired!');
          $('.slickSlider').fadeIn(3000);
        })
        .slick({
          dots: true,
          infinite: true,
          speed: 300,
          slidesToShow: 1,
        });

      },
      error: function(){
        alert('Failed! **');
      }

    });
  });


  $(document).on("click","#discard_copy_write_product" ,function(){
      //$("#copy_edit").show();
    $('.required_value').each(function() {
      var datatype = $(this).attr('data-type') ;
      if (datatype == 'textarea') {
        //tinyMCE.get($(this).attr('id')).getBody().setAttribute('contenteditable', false);
        $(this).attr('disabled','disabled');
      }else{
        $(this).attr('disabled','disabled');
      }
    });
    $('.ripple').attr('disabled','disabled'); // button disabled
    var that = $(this) ;

    var csrfToken = $('[name=_token]').val();
    var csession = $('#csession-id').val();
    var product_id = $(this).attr('data-product');
    console.log(csrfToken + product_id + csession) ;

    $.ajax({
      url: '/copywriting/session/products/lock',
      method: 'POST',
      headers: {
        'X-CSRF-Token': csrfToken
      },
      data: {
        "product": product_id,
        "csession": csession,
        "type": 'discard',
      },
      success: function (node) {
        console.log(node);
        $("#isLocked").val(0);
        tinymce.remove();
        that.parent().parent().hide();
        $('#edit_copy_write_product').parent().parent().show();
        if (node.html == 'discard') {
          $('.review-master-list li').each(function() {
            if ($(this).attr('id') == product_id) {
              $(this).children('div').remove('.copywritinglock');
              $('.lock-product-' + product_id).addClass('hidden');
            }
          });
        }else{
          $('.review-master-list li').each(function() {
            var product_id_li = $(this).attr('id') ;
            if (product_id_li == product_id) {
              $(this).append(node.html) ;
            }
          });
        }

        open_copy_product(csrfToken, product_id, csession, 1);

        var $slider = $('.slickSlider')
        .on('init', function(slick) {
          console.log('fired!');
          $('.slickSlider').fadeIn(3000);
        })
        .slick({
          dots: true,
          infinite: true,
          speed: 300,
          slidesToShow: 1,
        });

      },
      error: function(){
        alert('Failed! **');
      }

    });
  });

  function open_copy_product(csrfToken, product_id, csession, editable){
    $.ajax({
      url: '/copywriting/session/products/write/product',
      method: 'POST',
      headers: {
        'X-CSRF-Token': csrfToken
      },
      data: {
        "product": product_id,
        "csession": csession,
        "session_type" : "review",
      },
      success: function (node) {
        $('.no-product-container').addClass('hidden');
        $('.product-details-container').removeClass('hidden');
        $('.product-template-container').removeClass('hidden');
        $('.productCopy').html(node['copyProduct']);
        $('.productCopyDetails').html(node['copyProductDetails']);

        renderMCE(1);
        /*
        var text_to_insert = '<p>Please contact the customer care team via live chat or email support@bnkr-store.com for personalised sizing recommendations.</p>'
          text_to_insert += '<p>We have listed a definition of the most commonly used product measurement terms. To help you choose the perfect size, we recommend that you compare the chart when measuring yourself in order for getting the perfect fit.</p>'
        tinyMCE.get(String('size_and_fit')).execCommand( 'mceInsertContent', false, text_to_insert );
        */
        var $slider = $('.slickSlider')
        .on('init', function(slick) {
          console.log('fired!');
          $('.slickSlider').fadeIn(3000);
        })
        .slick({
          dots: true,
          infinite: true,
          speed: 300,
          slidesToShow: 1,
        });

      },
      error: function(){
        alert('Failed! **');
      }

    });
  }

  //
  // $('.page-content').addClass('fixed');

$('#review-channel').on("change", function(e) {
  refreshReviewProducts();

});

function refreshReviewProducts(){
  var channel = "" ;
  
  channel = $('#review-channel').val();
  
  if (channel == '0'){
    channel = 'ounass';
  }

  var csession = $('#csession-id').val()
  if(channel!=0){
    var $request = $.get("/copywriting/getReviewProducts/continuous/"+csession+"/"+channel); // make request
    var $container = $('.review-master-list');
    var $container_complete = $('.review-complete-list');

    $('.queue-tab-count').html(total_count_review_default);
    $('.complete-tab-count').html(total_count_review_default);

    $container.html('');
    $request.done(function(data) { // success
      if(data.html=="" && data.html_complete==""){
        $container.html('<li><h3 align="center">No record found!!</h3></li>');
        $container_complete.html('<li><h3 align="center">No record found!!</h3></li>');
      }
      else if(data.html=="" && data.html_complete!=""){
        $container.html('<li><h3 align="center">No record found!!</h3></li>');
        $container_complete.html(data.html_complete);
        // Counting
        $('.queue-tab-count').html(data.total_products);
        $('.complete-tab-count').html(data.total_products_completed);

        $('.review-master-list li:first').click();
         setTimeout(function(){
          $("#edit_copy_write_product").removeClass('hide');
        }, 2000);

      }
      else if(data.html!="" && data.html_complete==""){
        $container.html(data.html);
        $container_complete.html('<li><h3 align="center">No record found!!</h3></li>');
        // Counting
        $('.queue-tab-count').html(data.total_products);
        $('.complete-tab-count').html(data.total_products_completed);

        $('.review-master-list li:first').click();
         setTimeout(function(){
          $("#edit_copy_write_product").removeClass('hide');
        }, 2000);
         
      }
      else{
        $container.html(data.html);
        $container_complete.html(data.html_complete);
        // Counting
        $('.queue-tab-count').html(data.total_products);
        $('.complete-tab-count').html(data.total_products_completed);

        $('.review-master-list li:first').click();
         setTimeout(function(){
          $("#edit_copy_write_product").removeClass('hide');
        }, 2000);

      }

    });
  }


}
  </script>


@stop
