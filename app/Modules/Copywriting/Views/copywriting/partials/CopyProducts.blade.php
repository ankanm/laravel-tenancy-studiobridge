<div class="copyProducts">
  <div class="panel pagination2 table-responsive">
    <table id="copyProductsTable" class="table datatable" width="100%">
      <thead>
        <tr>
          <th>Identifier</th>
          <th>State</th>
          <th>Copy Status</th>
          <th></th>
          <th>Flags</th>
        </tr>
      </thead>
      <tbody>

        @if(!empty($product_list) && count($product_list) > 0)
          @foreach($product_list as $product)

          @php
          // $session = new Modules\Copywriting\Models\Copywritingsession();
          // $comments = $session->getCommentsCount($product->copywritingsession_id,$product->products['id']);
          @endphp


            <tr data-id="{{ $product->id }}">
              <td class="text-bold">{{ $product->products['identifier'] }}</td>
              <td>
                <span class="semi-bold ">{{ ucfirst($product->state) }}</span>
              </td>
               @php
                  $copy_state = ($product->copy_state == '0'? 'Copy Pending': 'Copy Done');
                @endphp
              <td class="text-bold text-amber-darker">{{ $copy_state }}</td>
              <td>
                {{-- @if($comments[0]->commentcount > 0)
                  <i class="fa fa-flag text-amber open-flag-dialog show-product-flags"
                  data-product-id="{{$product->products['id']}}"></i>
                @endif --}}
              </td>
              <td>
                <a class="flagproduct" prod_id="{{ $product->products['id'] }}" data-popup="tooltip" data-placement="left" data-original-title="Flag Product" data-toggle="modal" data-target="#flag-product">
                      <button type="button" class="btn btn-xs btn-warning btn-rounded" data-toggle="modal" data-target="#new-email"> Flag</button>
                </a>
                <a class="btn btn-xs btn-danger btn-rounded dropProd"  data-original-title="Drop Product" prod_id="{{ $product->id }}">Drop
                </a>
              </td>
            </tr>
          @endforeach
        @endif

      </tbody>
    </table>
  </div>
</div>
