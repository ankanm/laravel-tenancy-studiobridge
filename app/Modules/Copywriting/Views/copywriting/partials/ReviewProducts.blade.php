
@foreach ($reviewproductlist as $product)

  @php
  $revisions = Modules\Copywriting\Models\CopyRevisions::where('product_id', $product->id)
  ->where('copywritingsession_id',$product->cid)
  ->orderBy('id', 'desc')
  ->first();
  @endphp

  <li class="media pending-copy-product" id="{{$product->id}}" data-completion="{{ (isset($completion) && !empty($completion)) ? $completion : 0 }}">
    <a href="#" class="media media-link qc-product-a" data-pid="{{ $product->id }}" data-title="{{$product->identifier}}">
      <div class="media-left">
      </div>
      <div class="media-body">
        <h6 class="media-heading m-0"><span class="text-success bold">{{ $product->base_product_id }}_{{ $product->color_family }}</span>
          <p class="text-small p-0" style="height: 15px;">{{ $product->identifier }}</p>
          <p class="text-small p-0" style="height: 15px;">{{ $product->channel }}</p>
          <p class="text-small p-0" style="height: 15px;">{{ $product->brand_name }}</p>
          <p class="text-small p-0" style="height: 15px;">{{ $product->simple_category }}</p>
        </h6>

        <span class="media-annotation">{{ Carbon\Carbon::parse($product->created_at)->diffForHumans() }}</span> <br>
        <span class="media-annotation">{{ $product->copywriter }}</span>
        <h6 class="no-margin m-t-5 no-padding-bottom current-product-sid hidden">
          Copy Session {{$product->cid }}
        </h6>

      </div>
    </a>
    <!-- <div class="media-actions pull-right"> -->
      @if($product->lock == 1)
        <div class=" pull-left copywritinglock" data-user_id="{{auth()->user()->id}}" data-lock-user="{{$product->uid}}">
          <i class="fa fa-lock lock-product-{{ $product->product_id }}"></i>
        </div>
      @endif
      @if(($product->qc_state != null || $product->qc_state != '') && ($product->qc_state == 1))
        <i class="fa fa-check text-success copy-review-product-{{ $product->id }}"></i>
      @else
        <i class="fa fa-check text-success hidden copy-review-product-{{ $product->id }}"></i>
      @endif
    <!-- </div> -->
  </li>

@endforeach
