<style media="screen">
.form-group .select2-container {
	width: 100% !important;
}
</style>
<div class="productCopy" id="">
	@if(isset($product))
		<div class="tabbable">

			{{-- @if(isset($session_type) && $session_type=='copy') --}}
			<ul class="nav nav-tabs ">
				<li class="active"><a href="#draft" data-toggle="tab" aria-expanded="false">Draft</a></li>
				<li class="hide"><a href="#preview" data-toggle="tab" aria-expanded="false">Preview</a></li>
				{{-- <li><a href="#information" data-toggle="tab" aria-expanded="true">Information</a></li> --}}
				<li><a href="#sessions" data-toggle="tab" aria-expanded="true">Revisions</a></li>

				{{-- @if($completion != 1)
					@if(empty($copylock))
						<div class="col-sm-2 pull-right">
							<div class="pull-right">
								<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn btn-default bg-blue hide" id="edit_copy_write_product">Edit this Product</a>
							</div>
						</div>
						<div class="col-sm-2 pull-right" style="display: none;">
							<div class="pull-right">
								<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn btn-default bg-blue" id="discard_copy_write_product">Discard</a>
							</div>
						</div>
					@else
						<div class="col-sm-2 pull-right">
							<div class="pull-right">
								<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn btn-default bg-blue" id="discard_copy_write_product">Discard</a>
							</div>
						</div>
						<div class="col-sm-2 pull-right" style="display: none;">
							<div class="pull-right">
								<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn btn-default bg-blue hide" id="edit_copy_write_product">Edit this Product</a>
							</div>
						</div>
					@endif
				@endif --}}
			</ul>
			{{-- @endif --}}

			<div class="tab-content">

				<div class="tab-pane active" id="draft">
					@if($completion != 1)
						@if(($copylockuser != '' || $copylockuser != null) && ($copylockuser != auth()->user()->id))
							<div class="h2 col-sm-12 bold text-amber-darker text-center">
								This product is locked by {{App\User::where('id', $copylockuser)->first()->name}}
							</div>

						@elseif(($copylockuser != '' || $copylockuser != null) && ($copylockuser == auth()->user()->id))
							<div class="col-sm-6 pull-right text-right">
								<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn btn-default bg-blue" id="edit_copy_write_product">Edit this Product</a>
								@if($session_type != 'review' && !empty($auto_prod_data[0]->id))
									<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn btn-default bg-blue" style="display: none;" id="auto_populate_product">Auto Populate</a>
								@endif
								@if($session_type != 'review')
									<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn bg-danger" id="drop_copy_write_product">Drop</a>
								@endif
							</div>
							<br>
							<div class="h2 col-sm-12 bold text-info text-center">
								This product is locked by you for editing
							</div>
						@endif
					@endif
					<div class="col-sm-12 m-b-10" id="copy_edit" >
						<form class="form-horizontal" id="copydeksfrm">
							{{ csrf_field() }}

							@php
							$val = array();
							if(!empty($latest_ch_val)){
								foreach($latest_ch_val as $row){
									$val[$row->header_id] = $row->value;
								}
							}
							@endphp

							<input type="hidden" id="revision-prod-id" name="product_id" value="{{$product_id->id}}">

							@if(!empty($copy_header))
								<h2 class="text-dark p-5">Current Template: <strong>{{$template->template_name}}</strong></h2>
								@if(!empty($val))
									@foreach($copy_header as $ch)
										@continue($ch->is_visible==0)
										<div class="form-group">
											<label class="control-label col-sm-2">{{ ucwords(str_replace("_", " ", $ch->header_name)) }} @if($ch->is_required==1) <span class="text-danger">*</span> @endif </label>
												<div class="col-sm-9">

													@if($ch->header_type=="text")

														@if(!empty($val[$ch->id]))
															{{$val[$ch->id]}}
															<input type="hidden" name="copy_{{$ch->id}}" value="{{$val[$ch->id]}}">
														@endif


													@elseif($ch->header_type=="textarea")

														@if(!empty($val[$ch->id]))
															{!! html_entity_decode($val[$ch->id]) !!}
															<input type="hidden" name="copy_{{$ch->id}}" value="{{$val[$ch->id]}}">
														@endif

													@elseif($ch->header_type=="dropdown")

														@if(!empty($val[$ch->id]))
															{{$val[$ch->id]}}
															<input type="hidden" name="copy_{{$ch->id}}" value="{{$val[$ch->id]}}">
														@endif

													@endif
												</div>
											</div>
										@endforeach
									@endif
								@else
									<div class="text-center"><h2>No template found, please add a template for this product first</h2></div>
								@endif
								@if(!empty($copy_header))
									@if($completion != 1)
										@if(isset($session_type) && $session_type=='review')
											<div class="pull-right">
												<a class="btn bg-green save-product-draft" id="{{$product_id->id}}" btntype="approve">Approve and Submit</a>
											</div>
										@endif
									@endif
								@endif
							</form>
						</div>
					</div>
					<div class="tab-pane" id="sessions">
						<div class="panel">
							<table class="table datatable user-list" id="datatable">
								<thead>
									<tr>
										<th>Revision ID</th>
										<th>Copywriter</th>
										<th>Title</th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody>
									@if(isset($revisions))
										@foreach($revisions as $revision)
											@php
											$user = App\User::where('id', $revision->user_id)->first();
											@endphp
											<tr>
												<td>{{ $revision->id }}</td>
												<td>{{ $user->name }}</td>
												<td>{{ $revision->product_title }}</td>
												<td>{{ strip_tags($revision->product_description) }}</td>
											</tr>
										@endforeach
									@endif
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>

		@endif
	</div>
