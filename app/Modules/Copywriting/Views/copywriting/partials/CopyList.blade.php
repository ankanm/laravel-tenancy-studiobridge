@foreach ($result as $sid => $product)
@php
 $val = '';
 $latest_session = 0;
  if(count($product->states)>1){$latest_state = count($product->states) -1; } else { $latest_state = 0;}
@endphp

  @if(!empty($product_ids))
    @if(in_array($product->id,$product_ids))
      @php $val = 0 ; @endphp
    @else
      @php $val = 1 ;  @endphp
    @endif
  @else
    @php $val = 1 ;  @endphp
  @endif

  @if($val==1)
    <li class="media" id="copywriting-product-{{$product->id}}">

        <a href="#" class="media-link " data-pid="{{$product->id}}" data-title="{{product_identifier($product->base_product_id, $product->identifier, 0)}}">
         <div class="media-left ">
          <span class=" copywriting-product span-check">
            <input name="check-product-id" value="{{$product->id}}" type="checkbox" data-checkbox="icheckbox_square-blue" >
           <label for="copywriting-product-check-{{$product->id}}" class=""></label>
          </span>
         </div>

         <div class="media-body">
          <h6 class="media-heading">{!! product_identifier($product->base_product_id, $product->identifier) !!}

          </h6>
          <span class="media-annotation">
          {{ Carbon\Carbon::parse($product->states[$latest_state]->created_at)->diffForHumans() }}
          </span>
          {{-- <h6 class="no-margin m-t-5 no-padding-bottom current-product-sid">Session {{ $product->states[$latest_state]->psession_id }}</h6> --}}
         </div>
        </a>

        <div class="media-actions pull-right">
         <i class="fa fa-lock hidden lock-product-{{$product->id}}"></i>
        </div>
    </li>
  @endif
@endforeach
