@php
$loaded_array = array();
@endphp
@if(!empty($result))
  @foreach ($result as $product)

    @if(!empty($loaded_array))
      @if(in_array($product->id,$loaded_array))
        @php $val = 0 ; @endphp
      @else
        @php $val = 1 ;  @endphp
      @endif
    @else
      @php $val = 1 ;  @endphp
    @endif

    @php
    if(strtolower($product->Brand) == 'gap'){
      $title = $product->Item_Parent."_".$product->VPN."_".$product->GAP_Color_Code;
    } else{
      $title = $product->Item_Parent."_".$product->ATG_Color_Code;
    }
    @endphp

    @if($val==1)
      <li class="media pending-copy-product product-list-{{$product->product_id}}" id="{{$product->product_id}}" data-title="{{$product->Item}}" data-base_id="{{$product->Item_Parent}}" data-search="{{$title}}" data-completion="{{ (isset($completion) && !empty($completion)) ? $completion : 0 }}">
        @php
        array_push($loaded_array,$product->product_id)
        @endphp
        <a href="#" class="media media-link copy-product-a lock-event-{{$product->product_id}}" data-pid="{{$product->product_id}}"  data-title="{{$title}}">
          @if($method == 'batch')
            <div class="media-left ">
              <span class=" copywriting-product span-check">
                <input name="check-product-id" value="{{$product->product_id}}" type="checkbox" data-checkbox="icheckbox_square-blue" >
                <label for="copywriting-product-check-{{$product->product_id}}" class=""></label>
              </span>
            </div>
          @endif
          <div class="media-body">
            <h6 class="media-heading m-0"><span class="text-success bold">{{$product->Item_Description}}</span>
              <p class="text-small p-0" style="height: 15px;">Image: {{$title}}</p>
              <p class="text-small p-0" style="height: 15px;">Item: {{$product->Item}}</p>
              <p class="text-small p-0" style="height: 15px;">Brand :{{$product->Brand}}</p>
              <p class="text-small p-0" style="height: 15px;">{{$product->Channel}}</p>
            </h6>
            <span class="media-annotation">{{ Carbon\Carbon::parse($product->created_at)->diffForHumans() }}</span>
          </div>

          <div class="media-right product-lock-container-{{$product->product_id}}">
            @if(!empty($product->copy_state) && ($product->copy_state == 1))
                <i class="fa fa-check copy-approved-product-{{$product->id}}"></i>
            @elseif(!empty($product->has_draft))
                <i class="fa fa-pencil has-draft-product-{{$product->id}}"></i>
            @endif

            @if ($product->lock == 1)
              <div class="copywritinglock" data-user_id="{{auth()->user()->id}}" data-lock-user="{{$product->lock_user_id}}">
                <i class="fa fa-lock lock-product-{{ $product->product_id }}"></i>
              </div>
            @endif
          </div>

        </a>
      </li>
    @endif
  @endforeach

@endif
