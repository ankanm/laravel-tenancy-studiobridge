<style media="screen">
.form-group .select2-container {
	width: 100% !important;
}
</style>
<div class="productCopy" id="">
	@if(isset($product))
		<div class="tabbable">

			<div class="col-sm-12 pull-right">
				<div class="pull-right">

				</div>
			</div>

			{{-- @if(isset($session_type) && $session_type=='copy') --}}
			<ul class="nav nav-tabs col-sm-12">
				<li class="active col-sm-2"><a href="#draft" data-toggle="tab" aria-expanded="false">Draft</a></li>
				<li class="col-sm-2 hide"><a href="#preview" data-toggle="tab" aria-expanded="false">Preview</a></li>
				<li class="hide"><a href="#information" data-toggle="tab" aria-expanded="true">Information</a></li>
				<li class="col-sm-2"><a href="#sessions" data-toggle="tab" aria-expanded="true">Revisions</a></li>
			</ul>
			{{-- @endif --}}

			<div class="tab-content">
				<div class="tab-pane active" id="draft">

					@if(($copylockuser != '' || $copylockuser != null) && ($copylockuser != auth()->user()->id))
						<div class="h2 col-sm-12 bold text-amber-darker text-center">
							This product is locked by {{App\User::where('id', $copylockuser)->first()->name}}
						</div>

					@elseif(($copylockuser != '' || $copylockuser != null) && ($copylockuser == auth()->user()->id))
						<div class="col-sm-12 pull-right text-right">

							<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn btn-default bg-blue @if($session_type == 'copy') hide @endif" id="edit_copy_write_product">Edit this Product</a>
							
							@if($session_type != 'review' && !empty($auto_prod_data[0]->id))
								<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn btn-default bg-blue" id="auto_populate_product">Auto Populate</a>
							@endif
							@if($session_type != 'review')
								<a href="javascript:void(0);" data-product="{{$product_id->id}}" class=" btn bg-danger" id="drop_copy_write_product">Drop</a>
							@endif
						</div>
						<br>
						<div class="h2 col-sm-12 bold text-info text-center">
							This product is locked by you for editing
						</div>
					@endif

					<div class="col-sm-12 m-b-10" id="copy_edit" >
						<form class="form-horizontal" id="copydeksfrm">
							{{ csrf_field() }}
							@php
							$val = array();
							if(!empty($latest_ch_val)){
								foreach($latest_ch_val as $row){
									$val[$row->header_id] = $row->value;
								}
							}
							@endphp

							<input type="hidden" id="revision-prod-id" name="product_id" value="{{$product_id->id}}">

							@if(!empty($copy_header))
								{{-- <h2 class="text-dark p-5">Current Template: <strong>{{$template->template_name}}</strong></h2> --}}

								@foreach($copy_header as $ch)
									@continue($ch->is_visible==0)
									<div class="form-group">
										<label class="control-label col-sm-2">{{ ucwords(str_replace("_", " ", $ch->header_name)) }} @if($ch->is_required==1) <span class="text-danger">*</span> @endif </label>
											<div class="col-sm-9">

												@if($ch->header_type=="text")

													<input type="text" data-type="text" class="form-control input-lg {{$ch->header_name}} @if($ch->is_required==1) required_value @endif" name="copy_{{$ch->id}}" value="@if(!empty($val[$ch->id])) {{$val[$ch->id]}} @endif" placeholder="Enter a product {{ ucwords(str_replace("-", " ", $ch->header_name)) }}" >

													@elseif($ch->header_type=="textarea")

														<textarea data-type="textarea" data-min-words="{{$ch->minimum_words}}" class="basic-editor {{$ch->header_name}} @if($ch->is_required==1) required_value @endif form-control" name="copy_{{$ch->id}}" id="copy_{{$ch->id}}">
															@if(!empty($val[$ch->id])) {{$val[$ch->id]}} @endif
															</textarea>

														@elseif($ch->header_type=="dropdown")

															<select data-type="dropdown" class="form-control input-lg {{$ch->header_name}} @if($ch->is_required==1) required_value @endif" name="copy_{{$ch->id}}" id="copy_{{$ch->id}}">
																<option value="">Select</option>
																@php
																$select_val = explode(',', $ch->default_value);
																@endphp
																@foreach($select_val as $row =>$value)
																	<option value="{{ltrim($value)}}" {{ ( (!empty($val[$ch->id]) && ltrim($value) == ltrim($val[$ch->id])) ? 'selected' : '' )}}>{{$value}}</option>
																@endforeach
															</select>

														@endif
													</div>
												</div>
											@endforeach
										@else
											<div class="text-center"><h2>No template found, please add a template for this product first</h2></div>
										@endif
										@if(!empty($copy_header))
											@if(isset($session_type) && $session_type=='copy')
												<div class="pull-right">
													<a class="btn bg-blue save-product-draft" id="{{$product_id->id}}" btntype="save">Save Draft</a>
													<a class="btn bg-blue save-product-draft" id="{{$product_id->id}}" btntype="submit">Save and Submit</a>
												</div>
											@endif
											@if(isset($session_type) && $session_type=='review')
												<div class="pull-right">
													<a class="btn bg-green save-product-draft" id="{{$product_id->id}}" btntype="approve">Approve and Submit</a>
												</div>
											@endif
										@endif
									</form>
								</div>
							</div>
							<div class="tab-pane" id="sessions">
								<div class="panel">
									<table class="table datatable user-list" id="datatable">
										<thead>
											<tr>
												<th>Revision ID</th>
												<th>Copywriter</th>
												<th>Title</th>
												<th>Description</th>
											</tr>
										</thead>
										<tbody>
											@if(isset($revisions))
												@foreach($revisions as $revision)
													@php
													$user = App\User::where('id', $revision->user_id)->first();
													@endphp
													<tr>
														<td>{{ $revision->id }}</td>
														<td>{{ $user->name }}</td>
														<td>{{ $revision->product_title }}</td>
														<td>{{ strip_tags($revision->product_description) }}</td>
													</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>

				@endif
			</div>
