@php
$loaded_array = array();
@endphp

@foreach ($result as $product)

  @if(!empty($loaded_array))
    @if(in_array($product->id,$loaded_array))
      @php $val = 0 ; @endphp
    @else
      @php $val = 1 ;  @endphp
    @endif
  @else
    @php $val = 1 ;  @endphp
  @endif

  @if(strtolower($product->Brand) == 'gap')
    $title = $product->Item_Parent."_".$product->VPN."_".$product->GAP_Color_Code;
  @else
    $title = $product->Item_Parent."_".$product->ATG_Color_Code;
  @endif

  @if($val==1)
    <li class="media pending-copy-product product-list-{{$product->product_id}}" id="{{$product->product_id}}" data-title="{{$product->Item}}" data-base_id="{{$product->Item_Parent}}" data-search="{{$title}}">
      @php
      array_push($loaded_array,$product->product_id)
      @endphp
      <a href="#" class="media-link copy-product-a" data-pid="{{$product->product_id}}"  data-title="{{$title}}">

        <div class="media-left ">
        </div>
        <div class="media-body">
          <h6 class="media-heading m-0"><span class="text-success bold">{{$product->Item_Description}}</span>
            <p class="text-small p-0" style="height: 15px;">Image: {{$title}}</p>
              <p class="text-small p-0" style="height: 15px;">Item: {{$product->Item}}</p>
              <p class="text-small p-0" style="height: 15px;">Brand :{{$product->Brand}}</p>
              <p class="text-small p-0" style="height: 15px;">{{$product->Channel}}</p>


          </h6>
          <span class="media-annotation">{{ Carbon\Carbon::parse($product->created_at)->diffForHumans() }}
          </span>
        </div>
      </a>
      @if(isset($product->copy_state) && ($product->copy_state != null || $product->copy_state != '') && ($product->copy_state == 1))
        <div class="media-actions pull-right">
          <i class="fa fa-check copy-approved-product-{{$product->id}}"></i>
        </div>
      @elseif(isset($product->has_draft) && ($product->has_draft != null || $product->has_draft != ''))
        <div class="media-actions pull-right">
          <i class="fa fa-pencil has-draft-product-{{$product->id}}"></i>
        </div>
      @endif

      @if ($product->lock == 1)
      <div class="pull-right copywritinglock" data-user_id="{{auth()->user()->id}}" data-lock-user="{{$product->user_id}}">
        <i class="fa fa-lock lock-product-{{ $product->product_id }}"></i>
      </div>
      @endif
    </li>
  @endif
@endforeach
