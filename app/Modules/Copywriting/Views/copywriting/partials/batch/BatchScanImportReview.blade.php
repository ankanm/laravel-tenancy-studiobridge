<form class="form-horizontal p-t-10" id="copywriting_scan_import" onsubmit="return false;">
  {{ csrf_field() }}
  <!-- Not Selecting any individual -->
  <div class="form-group col-sm-12">
    <span class="text-xs filtered_products"></span>
  </div>

  <input type="hidden" id="copy-csession-id" name="csession_id" value="{{$csession->id}}">
  <input type="hidden" id="copy-csession-uid" name="csession_uid" value="{{Auth::id()}}">
  <input type="hidden" id="selected-model-id" name="model_id" value="">
</form>
<br style="clear: both;">
