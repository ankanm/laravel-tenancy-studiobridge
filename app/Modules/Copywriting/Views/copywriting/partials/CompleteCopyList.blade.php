@php
$loaded_array = array();
@endphp

@foreach ($result as $product)

  @if(!empty($loaded_array))
    @if(in_array($product->id,$loaded_array))
      @php $val = 0 ; @endphp
    @else
      @php $val = 1 ;  @endphp
    @endif
  @else
    @php $val = 1 ;  @endphp
  @endif


  @if($val==1)
    <li class="media pending-copy-product" id="{{$product->id}}" data-title="{{$product->identifier}}" data-search="{{ product_identifier($product->base_product_id, $product->identifier, 0) }}">
      @php
      array_push($loaded_array,$product->id)
      @endphp
      <a href="#" class="media-link " data-pid="{{$product->cid}}"
        data-title="{{ product_identifier_copy($product->base_product_id, $product->identifier, 0) }}">
        <div class="media-left ">
        </div>
        <div class="media-body">
          <h6 class="media-heading m-0"><span class="text-success bold">{!! $product->base_product_id !!}_{!! $product->color_family !!}</span>
            @if($product->base_product_id != '' || $product->base_product_id != null)
              <p class="text-small p-0" style="height: 15px;">{{$product->identifier}}</p>
              <p class="text-small p-0" style="height: 15px;">{{$product->channel}}</p>
              @if(isset($product->brand_name))
                <p class="text-small p-0" style="height: 15px;">{{$product->brand_name}}</p>
              @endif
              @if(isset($product->department))
                <p class="text-small p-0" style="height: 15px;">{{$product->department}}</p>
              @endif
            @else
              <p class="text-small p-0" style="height: 15px;" >Unmapped</p>
            @endif


          </h6>
          <span class="media-annotation">{{ Carbon\Carbon::parse($product->created_at)->diffForHumans() }}
          </span>
        </div>
      </a>
      @if(isset($product->copy_state) && ($product->copy_state != null || $product->copy_state != '') && ($product->copy_state == 1))
        <div class="media-actions pull-right">
          <i class="fa fa-check copy-approved-product-{{$product->id}}"></i>
        </div>
      @elseif(isset($product->has_draft) && ($product->has_draft != null || $product->has_draft != ''))
        <div class="media-actions pull-right">
          <i class="fa fa-pencil has-draft-product-{{$product->id}}"></i>
        </div>
      @endif
    </li>
  @endif
@endforeach
