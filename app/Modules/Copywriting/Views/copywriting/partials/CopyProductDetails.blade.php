

<div class="productCopyDetails p-20">

  @if(isset($product))
    @php
    $image_path = '';
    $images =  DB::table('image_transfers')
    ->leftJoin('images', function($join)
    {
      $join->on('images.id', '=', 'image_transfers.image_id');
    })
    ->select('images.thumbpath')
    ->where('image_transfers.product_id',$product_id->id)
    ->whereNull('images.deleted_at')
    ->orderby('image_transfers.weight')
    ->get();

    $latest_model = DB::table('product_states')
    ->leftJoin('psessions', 'psessions.id', '=','product_states.psession_id')
    ->select('product_states.model_uid as model')
    ->where('psessions.shoot_type', '=', 'model')
    ->where('product_states.product_id', '=', $product_id->id)
    ->orderby('product_states.id', 'DESC')
    ->first();

    if($latest_model){
      $model =  DB::table('users')
      ->leftJoin('user_profiles', 'users.id', '=', 'user_profiles.user_id')
      ->select('users.name', 'user_profiles.gender', 'user_profiles.model_stats', 'users.avatar')
      ->where('users.id', $latest_model->model)
      ->first();
    }
    @endphp

    <div class="text-center p-b-10">
      <img src="/assets/images/channels/{{strtolower($product->Channel)}}.svg" height="16px" alt="">
    </div>

    <div class="slickSlider">
      @if($images)
        @foreach($images as $image)
          @if(!isset($image->thumbpath))
            @continue
          @endif
          <div><a href="/{{ $image->thumbpath }}" data-lightbox="product-images"><img class="img-responsive copy-image" src="/{{ $image->thumbpath }}" alt=""></a></div>
        @endforeach
      @else
        <img src="https://www.jainsusa.com/images/store/landscape/not-available.jpg" width="100%" height="300px" alt="">
      @endif
    </div>
    <div class="media-body product-information">
      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h4">Product</div>
        @if(strtolower($product->Brand)=='gap')
          <div class="col-sm-6 text-right bold h4">{{$product->Item_Parent}}_{{ $product->VPN }}_{{ $product->GAP_Color_Code }}</div>
        @else
          <div class="col-sm-6 text-right bold h4">{{$product->Item_Parent}}_{{ $product->ATG_Color_Code }}</div>
        @endif
      </div>

      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h5">Item</div>
        <div class="col-sm-6 text-right bold h5">{{$product->Item}}</div>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h5">VPN</div>
        <div class="col-sm-6 text-right bold h5">{{$product->VPN}}</div>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h5">Brand</div>
        <div class="col-sm-6 text-right bold h5">{{$product->Brand}}</div>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h5">Color</div>
        <div class="col-sm-6 text-right bold h5">{{$product->ATG_Color_Desc}}</div>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h5">Description</div>
        <div class="col-sm-6 text-right bold h5">{{$product->Item_Description}}</div>
      </div>
    </div>

    @if(isset($model) && $model != '')
      <hr>
      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h4">Model</div>
        <div class="col-sm-6 text-right bold h5"><img src="{{$model->avatar}}" class=" img-responsive img-circle img-sm" alt=""></div>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h4">Name</div>
        <div class="col-sm-6 text-right bold h5">{{$model->name}}</div>
      </div>

      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h5">Gender</div>
        <div class="col-sm-6 text-right bold h5">{{ucwords($model->gender)}}</div>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h5">Model is wearing</div>
        <div class="col-sm-6 text-right bold h5">{{$product->Size}}</div>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left text-navy text-bold h5">Stats</div>
        <div class="col-sm-6 text-right bold h5">{{$model->model_stats}}</div>
      </div>
    @endif
  @endif
</div>
