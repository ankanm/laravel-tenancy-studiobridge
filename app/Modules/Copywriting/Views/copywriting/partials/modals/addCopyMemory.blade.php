<div id="add-from-memory" class="modal fade modal-slideright">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h2 class="p-l-10">Add from <strong>Memory</strong></h2>
        <form class="form-horizontal">
          <h6 class="form-wizard-title">
            <span>Description Memory</span>
            <small class="display-block">Select from list </small>
          </h6>

          <div class="form-group p-10">
            <input type="text" class="form-control input-md filter-memory-items" placeholder="Search from items">
          </div>
          @if(isset($memory_items))
            <div class="p-5 text-center">

              <div class="clearfix m-t-10">
                <div class="pull-left">
                  <button type="button" class="ripple btn btn-sm btn-default check-all-products">Check All</button>
                </div>
                <div class="pull-right">
                  <button type="button" class="ripple btn btn-sm bg-blue addcopytocontainer" data-target-container={{@$target_container}}>Add to Container</button>
                </div>
              </div>
            </div>

            <ul class="media-list media-list-linked media-list-bordered media-email memory-master-list filtered-memory-list">
              @foreach($memory_items as $memory)
                <li class="media">
                  <a href="#" class="media-link " >
                    <div class="media-left ">
                      <span class=" span-check">
                        <input name="check-memory-item" value="{!! $memory->memory_value !!}" type="checkbox" data-checkbox="icheckbox_square-blue" >
                        <label for="" class=""></label>
                      </span>
                    </div>
                    <div class="media-body">
                      <h6 class="media-heading m-0 ">{!! $memory->memory_value !!}
                      </h6>
                    </div>
                  </a>
                </li>
              @endforeach
            </ul>
          @endif
        </form>

      </div>
    </div>
  </div>
