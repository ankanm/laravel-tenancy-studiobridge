@extends('layouts.body')


@section('main-content')

  <!--Page Container-->
  <div class="app-content-body">
    <!--BEGIN PAGE HEADER-->
    @php
    if(!empty($template_data->created_at)){
        $tem_date = $template_data->created_at;
    }
    else{
        $tem_date = date('Y-m-d H:i:s');
    }
    //Check if date was today or yesterday
    $today = Carbon\Carbon::today()->format('Y-m-d');
    $yesterday = Carbon\Carbon::yesterday()->format('Y-m-d');
    $date = Carbon\Carbon::parse($tem_date)->format('Y-m-d');
    //
    if ($date == $today) {
      $create_date  = 'Today, at ' . Carbon\Carbon::parse($tem_date)->format('h:i a');
    } elseif($date == $yesterday){
      $create_date  = 'Yesterday, at ' . Carbon\Carbon::parse($tem_date)->format('h:i a');
    } else{
      $create_date = $date = Carbon\Carbon::parse($tem_date)->format('d M, Y h:i a');
    }

    if(!empty($template_data->channel)){
        $tem_channel = $template_data->channel;
    }
    else{
        $tem_channel = 'N/A';
    }

    if(!empty($template_data->channel)){
        $tem_simple_category = $template_data->simple_category;
    }
    else{
        $tem_simple_category = 'N/A';
    }

    @endphp
    <div class="row session-header">
      <div class="col-md-12 m-b-10 p-0">
        <div class="">
          <div class="panel-body p-5">
            <div class="text-center col-sm-1">
              <i class="fa fa-file-text-o p-t-10" style="font-size: 55px"></i>
            </div>
            <div class="clearfix col-xs-12 col-sm-11 border-bottom">
              <p class="c-blue f-16 m-t-10 m-b-5 bold">Copy Template</p>
              <span class="">
                <p class="c-gray inline-block"><i class="fa fa-calendar p-r-10"></i> Created {{$create_date}}</p>
                <p class="pull-right inline-block"><i class="fa fa-circle text-success fs-12"></i> Approved</p>
              </span>
            </div>
            <div class="clearfix col-xs-12 col-sm-11 col-sm-offset-1">
              <div class="row">
                <div class="col-md-2">
                  <div class="m-b-0 m-t-10">
                    <div class="media" >
                      <div class="media-body">
                        <span class="fs-12 text-muted text-uppercase">Channel </span>
                        <div class="fs-15 bold">{{ $tem_channel }}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="m-b-0 m-t-10">
                    <div class="media" >
                      <div class="media-body">
                        <span class="fs-12 text-muted text-uppercase">Category</span>
                        <div class="fs-15 bold">{{ $tem_simple_category }}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="pull-right m-t-10">
                    <div class="clearfix pull-right">
                      <a class="btn btn-default bg-blue" id="add-copyheader" data-toggle="modal" data-target="#add_copyheader">Add Copy Header </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--END PAGE HEADER-->
    <div class="row">
      <div class="col-md-9">
        <ul class="nav nav-tabs " style="z-index:1;">
          <li class="active"><a href="#rules" data-toggle="tab"> Copy Header</a></li>
          @if(!empty($copy_templates_id))
            <li><a href="#preview" data-toggle="tab"> Preview</a></li>
          @endif
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="rules">
            <div class="panel">
              <div class="table-responsive">
                <table class="table table-hover user-list" id="tablech">
                  <thead>
                    <tr>
                      <th>Header Name</th>
                      <th>Header Type</th>
                      <th>Required</th>
                      <th>Visible</th>
                      <th>Min Words</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  @php
                  $total_required = 0;
                  $total_extra = 0;
                  $total_data = count($copy_header_data);
                  @endphp
                  <tbody id="chcontents">
                    @if(!empty($copy_header_data))
                      @foreach($copy_header_data as $row)
                        <tr data-id="{{$row->id}}">
                          <td>{{$row->header_name}}</td>
                          <td>{{ucwords($row->header_type)}}</td>
                          <td>@if($row->is_required == 1) @php $total_required++; @endphp <span class="fa fa-check text-success"></span> @endif</td>
                          <td>@if($row->is_visible == 1) <span class="fa fa-check text-success"></span> @endif</td>
                          <td>@if($row->minimum_words) {{$row->minimum_words}} @endif</td>
                          <td>
                            <a href="javascript:void(0);" data-href="{{route('copywriting.headers.edit', $row->id)}}"  class=" btn btn-xs btn-primary editmodal" data-toggle="modal" data-target="#edit_copyheader"><i class="glyphicon glyphicon-edit"></i> Edit</a>

                            <a href="javascript:void(0);" data-href="{{route('copywriting.headers.delete', $row->id)}}" class=" btn btn-xs btn-danger delmodal"> <i class="glyphicon glyphicon-trash"></i> Delete</a>
                          </td>
                        </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          @if(!empty($copy_templates_id))
            <div class="tab-pane" id="preview">
              <div class="panel">
                <style media="screen">
                .form-group .select2-container {
                  width: 100% !important;
                }
                </style>
                <form class="form-horizontal">
                  <br>
                  @if(!empty($copy_header_data))
                    @foreach($copy_header_data as $ch)
                      <div class="form-group">
                        <label class="control-label col-sm-2">{{ ucwords(str_replace("_", " ", $ch->header_name)) }}</label>
                        <div class="col-sm-9">
                          @if($ch->header_type=="text")
                            <input type="text" class="form-control input-lg {{$ch->header_name}}" name="copy_{{$ch->id}}" value="" placeholder="Enter a product {{ ucwords(str_replace("-", " ", $ch->header_name)) }}">
                          @elseif($ch->header_type=="textarea")
                            <textarea class="basic-editor {{$ch->header_name}}" name="copy_{{$ch->id}}" id="copy_{{$ch->id}}" ></textarea>
                          @elseif($ch->header_type=="dropdown")
                            <select class="form-control input-lg {{$ch->header_name}}" name="copy_{{$ch->id}}">
                              <option value="">Select</option>
                              @php
                              $select_val = explode(',', $ch->default_value);
                              @endphp
                              @foreach($select_val as $row =>$val)
                                <option value="{{$val}}">{{$val}}</option>
                              @endforeach
                            </select>

                          @endif
                        </div>
                      </div>
                    @endforeach
                  @endif
                </form>
              </div>
            </div>
          @endif
        </div>
      </div>
      <div class="col-md-3 col-sm-3 p-t-50">
        <div class="panel panel-flat">
          <div class="panel-header">
            <h3>Headers</h3>
          </div>
          <div class="panel-body p-t-0">
            <div class="row">
              <div class="col-sm-4 text-center">
                <h6 class="semi-bold">Total</h6>
                <h1 id="total_product" class="m-xs">{{$total_data}}</h1>
              </div>
              <div class="col-sm-4 text-center">
                <h6 class="semi-bold">Required</h6>
                <h1 class="m-xs">{{$total_required}}</h1>
              </div>
              <div class="col-sm-4 text-center">
                <h6 class="semi-bold">Additional</h6>
                <h1 class="m-xs">{{ ($total_data - $total_required) }} </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('partials.modals.addCopyHeaderModal')
  @include('partials.modals.editCopyHeaderModal')
@stop
@section('page-scripts')

  <script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
  <script src="{{ URL::asset('assets/editors/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ URL::asset('js/pages/page_emails.js') }}"></script>
  <script>

  $(function() {

    $("#tablech").on('click', '.delmodal', function() {
      var delurl = ($(this).data('href'));
      swal({
        title: "Delete ",
        text: "Are you sure you want to delete this?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Delete",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
          $.get( delurl, function( data ) {
            swal({
              title: "Copy Header Deleted",
              text: "A copy header has been deleted.",
              timer: 1000,
              type: "success",
              showConfirmButton: false
            });

            setTimeout(function(){
              window.location.reload();
              //copyheaderdatatable.ajax.reload();
            }, 3000);
          });

        }
      });

    });

    $("#add_ch_bt").click(function(){
      var header_name = $("#header_name").val();
      var header_type = $('#header_type').val();
      var channels = $('#channels').val();
      if(header_name=="" || header_type==""){
        alert('Please fill all required fields');
        return false;
      } else {
        var frmdata = $( "#add_ch_form" ).serializeArray();
        $.ajax({
          url: '/copywriting/header/store',
          type: 'POST',
          data: frmdata,
          success: function (node) {
            if(node.id!=""){
              swal({
                title: "Copy Header Saved",
                text: "A copy header has been saved.",
                timer: 1000,
                type: "success",
                showConfirmButton: false
              });

              setTimeout(function(){
                window.location.reload();
                //$('#add_copyheader').modal('hide');
                //copyheaderdatatable.ajax.reload();
              }, 3000);
            }
            else{
              swal({
                title: "Error",
                text: "There are some error occured!!",
                timer: 1000,
                type: "warning",
                showConfirmButton: false
              });
            }

          },
          error: function(){
            alert('Failed! **');
          }

        });
      }
    });

    $("#tablech").on('click', '.editmodal', function() {
      var editurl = ($(this).data('href'));
      $.ajax({
        url: editurl,
        type: 'GET',
        dataType: 'json',
        success: function (node) {

          if(node.id=="0"){
            swal({
              title: "Error",
              text: "There are some error occured!!",
              timer: 2000,
              type: "warning",
              showConfirmButton: false
            });
          }
          else{
            $("#edit_id").val(node.id);
            $('#header_name_edit').val(node.header_name);
            $('#header_type_edit option[value='+node.header_type+']').attr('selected','selected');
            $('#header_type_edit').select2('destroy').select2();

            $('#default_value_edit').val(node.default_value);
            if(node.is_required==1){
              $('#required_edit1').trigger('click');
              $('#required_edit1').iCheck('check');
            }
            else{
              $('#required_edit0').trigger('click');
              $('#required_edit0').iCheck('check');
            }
            $("#edit_ch_form input[name='is_required'][value='"+node.is_required+"']").attr('checked', 'checked');

            if(node.is_visible==1){
              $('#visible_edit1').trigger('click');
              $('#visible_edit1').iCheck('check');
            }
            else{
              $('#visible_edit0').trigger('click');
              $('#visible_edit0').iCheck('check');
            }
            $("#edit_ch_form input[name='is_visible'][value='"+node.is_visible+"']").attr('checked', 'checked');

            $('#channels_edit').val(node.channels);
            $('#weight_edit').val(node.weight);
            $('#copy_templates_id_edit option[value='+node.copy_templates_id+']').attr('selected','selected');
            $('#copy_templates_id_edit').select2('destroy').select2();
          }

        },
        error: function(){
          alert('Failed! **');
        }

      });
    });

    $("#edit_ch_bt").click(function(){
      var header_name = $("#header_name_edit").val();
      var header_type = $('#header_type_edit').val();
      var channels = $('#channels_edit').val();
      if(header_name=="" || header_type==""){
        alert('Please fill all required fields');
        return false;
      } else {
        var frmdata = $( "#edit_ch_form" ).serializeArray();
        $.ajax({
          url: '/copywriting/header/store',
          type: 'POST',
          data: frmdata,
          success: function (node) {
            if(node.id!=""){
              swal({
                title: "Copy Header Saved",
                text: "A copy header has been saved.",
                timer: 1000,
                type: "success",
                showConfirmButton: false
              });

              setTimeout(function(){
                window.location.reload();
                //$('#edit_copyheader').modal('hide');
                //copyheaderdatatable.ajax.reload();
              }, 3000);
            }
            else{
              swal({
                title: "Error",
                text: "There are some error occured!!",
                timer: 1000,
                type: "warning",
                showConfirmButton: false
              });
            }

          },
          error: function(){
            alert('Failed! **');
          }

        });
      }
    });

    $( "#chcontents" ).sortable({
      items: "tr",
      cursor: 'move',
      opacity: 0.6,
      update: function() {
        sendOrderToServer();
      }
    });

    function sendOrderToServer() {

      var order = [];
      $('#chcontents tr').each(function(index,element) {
        var newid = $(this).data('id');

        order.push({
          id: newid,
          position: index+1
        });
      });

      $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{ url('copywriting/header/orderupdate') }}",
        data: {
          order:order,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response.status == "success") {
            console.log(response);
          } else {
            console.log(response);
          }
        }
      });

    }
  });

</script>


@stop
