@extends('layouts.body')

@section('main-content')
	<!--Actual Page-->
	<!--Page Container-->
	<div class="app-content-body">

		<div class="header">
			<h2>Copy <strong>Template</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="#">Home</a>
					</li>
					<li><a href="#">Photography Desk</a>
					</li>
					<li class="active">My Sessions</li>
				</ol>
			</div>
		</div>
		<!--/Page Header-->

		<div class="">
			<!-- Individual column searching (text fields) -->
			<div class="row m-b-10">
				<div class="col-sm-2">

				</div>

				<div class="col-sm-2 pull-right">

						<div class="pull-right">
							<a class="ripple btn btn-default bg-blue" href="#"  id="add-copytemplate" data-toggle="modal" data-target="#add_copytemplate">Add New Template</a>
						</div>
				</div>

			</div>
			@if(count($header_data) > 0)
				<div class="panel pagination2 table-responsive">
					<table class="table datatable datatable-column-search-inputs dt-bootstrap no-footer" id="tablech">
						<thead>
							<tr>
								<th>Template</th>
								<th>Channel</th>
								<th>Category</th>
								<th>Headers</th>
								<th>Required Headers</th>
								<th>Extra Headers</th>
								<th>Created</th>
								<th class="text-center"></th>
							</tr>
						</thead>
						<tbody>

							@foreach ($header_data as $data)
								@php

								//Check if date was today or yesterday
								$today = Carbon\Carbon::today()->format('Y-m-d');
								$yesterday = Carbon\Carbon::yesterday()->format('Y-m-d');
								$date = Carbon\Carbon::parse($data->created_at)->format('Y-m-d');
								//
								if ($date == $today) {
									$create_date  = 'Today, at ' . Carbon\Carbon::parse($data->created_at)->format('h:i a');
								} elseif($date == $yesterday){
									$create_date  = 'Yesterday, at ' . Carbon\Carbon::parse($data->created_at)->format('h:i a');
								} else{
									$create_date = $date = Carbon\Carbon::parse($data->created_at)->format('d M, Y h:i a');
								}

								@endphp

								<tr>


									<td>
										<h6 class="no-margin">
											<span class="text-bold">{{ ucwords($data->template_name) }}</span>

										</h6>
									</td>
									<td><h6 class="no-margin"><span class="text-bold">{{$data->channel}}</h6></span></td>
									<td><h6 class="no-margin"><span class="text-bold">{{$data->simple_category}}</h6></span></td>
									<td><h3 class="m-xs m-0 fs-25 fw-300">{{$data->total_row}}</h3></td>
									<td><h3 class="m-xs m-0 fs-25 fw-300">{{ ucwords($data->required_headers) }}</h3></td>
									<td><h3 class="m-xs m-0 fs-25 fw-300">{{ ucwords($data->extra_headers) }}</h3></td>
									<td>
										<span>{{$create_date}}</span>
									</td>

									<td class="text-center">
										<ul class="icons-list">
											<li> <a href="javascript:void(0);" data-href="{{url('/copywriting/templates/edit')}}/{{$data->copy_template_id}}"  class=" btn btn-xs btn-primary editmodal" data-toggle="modal" data-target="#edit_copytemplate"><i class="glyphicon glyphicon-edit"></i> Edit</a></li>
											<li><a href="/copywriting/templates/{{$data->copy_template_id}}" class="btn btn-xs btn-primary " > <i class="glyphicon glyphicon-eye-open"></i> VIEW</a></li>
										</ul>
									</td>
								</tr>

							@endforeach


						</tbody>
					</table>
				</div>

				<div class="text-center">

				</div>

				<!-- /Individual column searching (text fields) -->
			@else
				@include('templates.emptystate', ['empty_state_text' => 'There are no copy header.'])
			@endif
		</div>


	</div>
	<!--/Page Container-->
@include('partials.modals.addCopyTemplateModal')
@include('partials.modals.editCopyTemplateModal')
@stop

@section('page-scripts')

	<!-- Page scripts -->
	<script src="{{ URL::asset('js/pnotify.min.js') }}"></script>
	<script src="{{ URL::asset('js/pages/extension_pnotify.js') }}"></script>


	<!-- /page scripts -->
	<script type="text/javascript">
	$(function() {
		$("#add_ct_bt").click(function(){
        	var template_name = $("#template_name").val();
        	var category_name = $("#category").val();
        	var channel_name = $("#channel").val();
	        if(template_name=="" || category_name==""){
            	alert('Please fill all required fields');
            	return false;
        	} else {
        		var frmdata = $( "#add_ct_form" ).serializeArray();
        		$.ajax({
			    url: '/copywriting/template/store',
			    type: 'POST',
			    data: frmdata,
			    success: function (node) {
			    	if(node.id!=""){
				      swal({
				        title: "Copy Template Saved",
				        text: "A copy template has been saved.",
				        timer: 1000,
				        type: "success",
				        showConfirmButton: false
				      });

				      setTimeout(function(){
				      		window.location.replace('/copywriting/templates');
				      	}, 3000);
				  	}
				  	else if(node.error!=""){
				  		swal({
					        title: "Error",
					        text:  node.error,
					        timer: 3000,
					        type: "warning",
					        showConfirmButton: false
					    });
				  	}
				  	else{
				  		swal({
					        title: "Error",
					        text: "There are some error occured!!",
					        timer: 1000,
					        type: "warning",
					        showConfirmButton: false
					    });
				  	}

			    },
			    error: function(){
			      alert('Failed! **');
			    }

			  });
        	}
        });

         $("#tablech").on('click', '.editmodal', function() {
        	var editurl = ($(this).data('href'));
        	$.ajax({
			    url: editurl,
			    type: 'GET',
			    dataType: 'json',
			    success: function (node) {

			    	if(node.id=="0"){
			    		swal({
					        title: "Error",
					        text: "There are some error occured!!",
					        timer: 2000,
					        type: "warning",
					        showConfirmButton: false
					    });
			    	}
			    	else{
			    		$("#edit_id").val(node.id);
			    		$('#edit_template_name').val(node.template_name);
			    		//$('#edit_category').val(node.category);
			    		$('#edit_category option[value="'+node.category+'"]').attr('selected','selected');
			    		$('#edit_category').select2('destroy').select2();

			    		$('#edit_channel option[value="'+node.channel+'"]').attr('selected','selected');
			    		$('#edit_channel').select2('destroy').select2();

			    	}

			    },
			    error: function(){
			      alert('Failed! **');
			    }

			});
        });

        $("#edit_ct_bt").click(function(){
        	var template_name = $("#edit_template_name").val();
	        var category = $('#edit_category').val();
	        if(template_name=="" || category==""){
            	alert('Please fill all required fields');
            	return false;
        	} else {
        		var frmdata = $( "#edit_ct_form" ).serializeArray();
        		$.ajax({
			    url: '/copywriting/template/store',
			    type: 'POST',
			    data: frmdata,
			    success: function (node) {
			    	if(node.id!=""){
				      swal({
				        title: "Copy Template Saved",
				        text: "A copy template has been saved.",
				        timer: 1000,
				        type: "success",
				        showConfirmButton: false
				      });

				      setTimeout(function(){
				      		window.location.reload();
				      		//$('#edit_copyheader').modal('hide');
                			//copyheaderdatatable.ajax.reload();
				      }, 3000);
				  	}
				  	else{
				  		swal({
					        title: "Error",
					        text: "There are some error occured!!",
					        timer: 1000,
					        type: "warning",
					        showConfirmButton: false
					    });
				  	}

			    },
			    error: function(){
			      alert('Failed! **');
			    }

			  });
        	}
        });

    });
	</script>
@stop
