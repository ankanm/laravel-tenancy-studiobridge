@extends('layouts.body')

@section('main-content')
@php
  $users = [];
@endphp
  <link rel="stylesheet" href="{{ URL::asset('css/snippets.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/htimeline.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">

  <!--Page Container-->
  <div class="app-content-body">
    <!--Page Header-->
    <div class="header">
      <h2>Add <strong>Copywriting Session</strong></h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          <li><a href="#">Home</a>
          </li>
          <li><a href="#">Copywriting Desk</a>
          </li>
          <li class="active">Add Session</li>
        </ol>
      </div>
    </div>
    <!--/Page Header-->


    <div class="panel panel-flat">
      <div class="panel-body p-0">
        <form class="form-horizontal form-validate" method="post" action="/copywriting/session/add" name="formaddSession" id="formaddSession" novalidate="novalidate">
        <input type="hidden" id="form_submit" value="">
          {{ csrf_field() }}
          <h6 class="form-wizard-title">
            <span>Session Type</span>
            <small class="display-block">What type of session are you creating</small>
          </h6>

          <div class="form-group ">
          <label class="control-label col-lg-2">Session Mode <span class="text-danger">*</span></label>
            <div class="col-lg-4">
              <div class="icheck-inline">
                <label><input type="radio" name="method" data-radio="iradio_square-blue" value="continuous" checked> Continuous</label>
                <label><input type="radio" name="method" data-radio="iradio_square-blue" value="batch">Batch</label>
                 <div class="text-danger">{{ $errors->first('method') }}</div>
              </div>
            </div>

          <label class="control-label col-lg-2">Priority Type <span class="text-danger">*</span></label>
            <div class="col-lg-4">
              <div class="icheck-inline">
                <label><input type="radio" name="session_type" data-radio="iradio_square-blue" value="normal" checked>Normal</label>
                <label><input type="radio" name="session_type" data-radio="iradio_square-blue" value="urgent" class="model-option"> Urgent</label>
                 <div class="text-danger">{{ $errors->first('session_type') }}</div>
              </div>
            </div>

            <span class="model-view">
              <label class="control-label col-lg-2">Minimum Products <span class="text-danger">*</span></label>
              <div class="col-lg-4">
                <input type="number" name="min_product" id="min_product" class="form-control" placeholder="Min Products">
              </div>
          </span>

          </div>
          <div class="form-group ">
          <label class="control-label col-lg-2">Session Type <span class="text-danger">*</span></label>
          <div class="col-lg-4">
              <div class="icheck-inline">
                <label><input type="radio" name="type" data-radio="iradio_square-blue" value="Writing" checked>Writing</label>
                <label class="hide"><input type="radio" name="type" data-radio="iradio_square-blue" value="Enrich" class="model-option"> Enrich</label>
                <label><input type="radio" name="type" data-radio="iradio_square-blue" value="review" class="model-option"> Review</label>
                 <div class="text-danger">{{ $errors->first('sessionType') }}</div>
              </div>
            </div>
          </div>


          {{-- <h6 class="form-wizard-title">
            <span>Session Due</span>
            <small class="display-block">When is this session due?</small>
          </h6>
          <div class="form-group ">
            <label class="control-label col-lg-2">Session Deadline <span class="text-danger">*</span></label>
            <div class="col-lg-4">

              <div class="prepend-icon">
                <input type="text" name="session_deadline" class="datetimepicker form-control" placeholder="Choose a date..." value="{{old('session_deadline')}}">
                <i class="icon-clock"></i>
              </div>
              <div class="text-danger">{{ $errors->first('session_deadline') }}</div>
            </div>
            <label class="control-label col-lg-2">Auto submit on deadline <span class="text-danger">*</span></label>
            <div class="col-lg-4">
              <div class="icheck-inline">
                <label><input type="radio" name="hard_deadline" data-radio="iradio_square-blue" value="0" checked>No</label>
                <label><input type="radio" name="hard_deadline" data-radio="iradio_square-blue" value="1" class="model-option"> Yes</label>
                <div class="text-danger">{{ $errors->first('hard_deadline') }}</div>
              </div>
            </div>
          </div> --}}

          {{-- <h6 class="form-wizard-title">
            <span>Contributors</span>
            <small class="display-block">Who will be contributing to this session</small>
          </h6> --}}



          {{-- <div class="form-group ">
            <label class="control-label col-lg-2">Copywriting Agency <span class="text-danger">*</span></label>
            <div class="col-lg-4">
            <input type="hidden" name="old_agency_id" id="old_agency_id" value="{{old('agency_id')}}">
              <select name="agency_id" class="select2 agency-select" data-placeholder="Select an agency">
                <option value="">Select Agency</option>
                    @if(!empty($copywriting_agencies))
                    @foreach($copywriting_agencies as $agencies)

                        @php
                        $agency = new App\Models\Agency();
                        $users = $agency->GetCopywritingAgencyUsers($agencies->id);
                        @endphp

                        @if(count($users) > 0)
                        <option value="{{ $agencies->id }}" @if(old('agency_id')==$agencies->id) selected="selected" @endif>{{ $agencies->name }}
                        </option>
                        @endif
                    @endforeach
                    @else
                     <option value="">No Agency</option>
                    @endif
              </select>
              <div class="text-danger">{{ $errors->first('agency_id') }}</div>
            </div>
            <input type="hidden" name="old_agency_uid" id="old_agency_uid" value="{{old('agency_uid')}}">
            <label class="control-label col-lg-2">Copywriter </span></label>
            <div class="col-lg-4">
              <select name="agency_uid" id="agency_uid" class=" select2 " data-placeholder="Select a copywriter">
                <option value = "">Select Copywriter</option>
              </select>
              <div class="text-danger">{{ $errors->first('agency_uid') }}</div>
            </div>
          </div> --}}

          <h6 class="form-wizard-title">
            <span>Notes</span>
            <small class="display-block">Any notes you wish to add to this session</small>
          </h6>
          <div class="form-group pb-10">
            <label class="control-label col-lg-2">Notes</label>
            <div class="col-lg-4">
              <textarea name="notes" rows="3" cols="5" class="form-control" placeholder="Session notes...">{{old('notes')}}</textarea>
            </div>
          </div>

          <div class="form-wizard-actions">
            <input class="btn btn-default ui-wizard-content ui-formwizard-button reset-button" type="reset" id="reset">
            <a class="btn btn-info addbtn" onclick="addSession()">Add Session</a>
          </div>
        </form>
      </div>

    </div>

    <!--/Page Container-->
  @stop

  @section('page-scripts')

    <!-- Page scripts -->
    <script src="{{ URL::asset('js/forms/jquery.validate.js') }}"></script>
    <script src="{{ URL::asset('js/forms/switch.min.js') }}"></script>
    <script src="{{ URL::asset('js/pages/form_validations.js') }}"></script>
    <script src="{{ URL::asset('assets/global/plugins/timepicker/jquery-ui-timepicker-addon.min.js') }}"></script>
    <script src="{{ URL::asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>


    <!-- /page scripts -->

    <script type="text/javascript">

    $(".model-view").hide();
    // $('input[name="method"]').on('ifChecked', function(event){
    //     if($(this).attr("value")=="continuous"){
    //       $(".model-view").show();
    //       $("#min_product").attr('required', 'required');
    //     }else{
    //       $(".model-view").hide();
    //       $("#min_product").removeAttr('required');
    //     };

    // });

    $("input[type='reset']").click(function(){
      $("#form_submit").val("");
    });

    $(document).ready(function() {
      if($("#old_agency_id").val()!="") {
         $(".agency-select").change();
      }
    });

    $(document).on("change",".agency-select",function(){

     var csrfToken = $('[name=_token]').val();
     var agency_id = $(this).val();
     var old_agency_uid = $("#old_agency_uid").val();


     $.ajax({
      url: "/copywriting/session/agencyusers",
      headers: {
        'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{agency_id:agency_id},
      success:function(data){

        var model = $('#agency_uid');
        model.empty();
        model.append('<option value="0">Select a Copywriter</option>');
        $.each(data, function(index, element) {
          model.append("<option value='"+element.userid+"'>" + element.username + "</option>");
          if(old_agency_uid==element.userid) {
            $("#agency_uid").val(old_agency_uid).trigger('change');
          }
        });

        model.select2("destroy"); // destroy first next initialize again
        model.select2({
          placeholder: "Select a Copywriter"
        });

      }
    });

  });


    function addSession(){

      if($("#form_submit").val() == "1") {
        return false;
      }
      $(".addbtn").attr("disabled",true);
      $(".addbtn").css({cursor:"default"});
      $("input[type='reset']").attr("disabled",true);

      $( "#formaddSession" ).submit();
      // swal({
      //   title: "Add New Session",
      //   text: "Are you sure you want to add this session?",
      //   type: "warning",
      //   showCancelButton: true,
      //   confirmButtonClass: 'btn-success',
      //   confirmButtonText: 'Add Session',
      //   cancelButtonText: "Cancel",
      //   closeOnConfirm: true,
      //   closeOnCancel: true
      // },
      // function(isConfirm){
      //   if (isConfirm) {
      //
      //   } else {
      //     swal("Cancelled", "You have cancelled adding the session", "error");
      //   }
      // });

    };
    </script>
  @stop
