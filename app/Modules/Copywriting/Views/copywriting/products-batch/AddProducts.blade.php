@extends('layouts.body')


@section('main-content')

  <link rel="stylesheet" href="{{ URL::asset('js/lightbox2-master/src/css/lightbox.css') }}">

  <!--Page Container-->
  <div class="page-app page-content">


    <!--Main Left-->
    <div class="col-md-2 col-sm-4 secondary-sidebar">
      <div class="sidebar-content email-container" >
        <!--Top Bar-->
        <div class="col-xs-12 p-t-10 p-b-5 bg-white" >
          <div class="row">

            <div class="col-md-12 col-sm-12 p-0">
              <div class="text-center">

                <h3 class=" no-margin-bottom m-t-10">Session {{ $csession->id}} </h3>
                <div class="text-size-small text-info"><a href="{!! route('copywriting.session.view', ['id' => $csession->id]) !!}">go to Session page</a></div>
              </div>
              <div class="col-sm-12">
                <div class="col-xs-12 col-sm-6 text-center">
                  <h6 class="semi-bold">Products</h6>
                  <h1 class="m-xs "><span id="session-total-products">{{ $products_count }}</span></h1>
                </div>
                <div class="col-xs-12 col-sm-6 text-center">
                  <h6 class="semi-bold">Batches</h6>
                  <h1 class="m-xs" id="session-total-box"> </h1>
                </div>
              </div>
              <hr>
              <div class="col-sm-12 p-0">

                <div class="panel-group panel-accordion " id="accordion2">


                  <!-- <h6 class="form-wizard-title m-b-0">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false">
                      <span>Photography Session Filter</span>
                      <small class="display-block">Filter by Sessions </small>
                    </a>
                  </h6> -->

                  <div id="collapseOne2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="form-group p-10">
                      <select class="form-control select2" name="psession_id[]" id="psession_id" multiple="multiple" data-placeholder="Select Session(s)">
                        @if(!empty($psessions))
                          @foreach($psessions as $session)
                            <option value="{{ $session->psession_id }}">Session {{ $session->psession_id }}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>

                  <!-- <div class="pull-right p-5">
                    <button id="resetall" type="button" class=" btn btn-default">Reset </button>
                    <button id="filter-session" type="button" class=" btn btn-info ">Filter
                    </button>
                  </div> -->
                  
                </div>
              </div>
            </div>
          </div>
        </div>

        <!--/Top Bar-->
      </div>
    </div>
    <!--/Main Left-->
    <!--/Main Middle-->
    <div class="col-md-2 col-sm-3 secondary-sidebar">
      <div class="email-container">
        {{ csrf_field() }}
        <input type="hidden" name="csession_id" id="csession-id" value="{{$csession->id}}">
        <input type="hidden" name="channel_name" id="channel_name" value="">
        <input type="hidden" name="uid" id="csession-uid" value="{{ Auth::user()->id }}">


        <div class="p-5 text-center">

          <div class="clearfix m-t-10">
            <div class="pull-left">
              <button type="button" class=" btn btn-sm btn-default check-all-products">Check All</button>
            </div>
            <div class="pull-right">
              <button type="button" class=" btn btn-sm bg-blue addtosession">Add to Session</button>
            </div>
          </div>

        </div>

        <div class="">
            @include('copywriting.partials.batch.BatchScanImport')
          </div>

        <ul class="media-list media-list-linked media-list-bordered media-email new qc-master-list oldprodlist copy-scan-list">

        </ul>
      </div>
    </div>
    <!--/Main Middle-->

    <!--Main Right-->
    <div class="col-md-8 col-sm-4 no-padding">
      <div class="col-xs-12 no-margin-bottom no-border-bottom email-details">
        <div class="panel-heading">
          <div class="pull-right">
              <button type="button" class=" btn btn-sm btn-danger dropall">Drop All</button>
          </div>
          <h6><strong>Copywriting Products</strong></h6>
        </div>
        @include('copywriting.partials.CopyProducts')
      </div>
    </div>
    <!--/Main Right-->


  </div>
  <!--/Page Container-->
@include('partials.modals.flagProductModal')
@include('partials.modals.productFlags')
@stop


@section('page-scripts')

  <!-- Page scripts -->
  <script src="{{ URL::asset('assets/editors/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ URL::asset('js/pages/page_emails.js') }}"></script>
  <script src="{{ URL::asset('js/studiobridge/addproducts.js') }}"></script>
  <script src="{{ URL::asset('js/extensions/countdown/jquery.plugin.js') }}"></script>
  <script src="{{ URL::asset('js/extensions/countdown/jquery.countdown.js') }}"></script>
  <script src="{{ URL::asset('js/studiobridge/qc-product-operations.js') }}"></script>


  <script src="{{ URL::asset('js/lightbox2-master/src/js/lightbox.js') }}"></script>

  <!-- /page scripts -->

  <script>
  $('body').addClass('sidebar-collapsed')

  /* Check All Products */
  $(document).on("click", ".check-all-products", function(){
    $(this).removeClass('check-all-products').addClass('uncheck-all-products').html('Uncheck All');
    $('.todo-list li').removeClass('bounceInDown').addClass("done");
    $('input').iCheck('check');
  });

  /* Uncheck All Products */
  $(document).on("click", ".uncheck-all-products", function(){
    $(this).removeClass('uncheck-all-products').addClass('check-all-products').html('Check All');
    $('.todo-list li').removeClass("done");
    $('input').iCheck('uncheck');
  });

  /* Add product */
  //copywriting-product-check
  $(document).on("click", ".addtosession", function(){
    var csrfToken = $('[name=_token]').val();
    var csession = $('#csession-id').val();
    var checked_products = [];

    $("input:checkbox[name=check-product-id]:checked").each(function(){
        checked_products.push($(this).val());
    });

    if(checked_products.length == 0){
      swal({
        title: "No Products Selected",
        text: "You have not selected any products, please select atleast one product to add.",
        type: "warning"
      });
    } else{

      $.ajax({
        url: '/copywriting/session/products/store',
        method: 'POST',
        headers: {
          'X-CSRF-Token': csrfToken
        },
        data: {
          "products": checked_products,
          "csession": csession,

        },
        success: function (node) {

          swal({
            title: "Products added to Session",
            text: "Your selected products have been added to this copywriting session",
            timer: 1000,
            type: "success",
            showConfirmButton: false
          });

          refreshProductsContainer();
          $(".uncheck-all-products").click();
        },
        error: function(){
          alert('Failed! **');
        }

      });

    }
  });


  /*
  |--------------------------------------------------------------------------
  | Flag Product Function
  |--------------------------------------------------------------------------
  */
  //----------------------------------
      // To get product flags
  //----------------------------------

  $(document).on("click",".show-product-flags",function(){

    var product_id = $(this).attr('data-product-id');
    var csession_id = $('#csession-id').val();
    var csrfToken = $('[name=_token]').val();

    var $request = $.ajax({
      url: '/helper/productflags',
      type:'post',
      headers: {
        'X-CSRF-Token': csrfToken
      },
      data:{product_id: product_id, type:'csession', type_id:csession_id},
    });
    $request.done(function(data) { // success

      if(data.status == 'success'){
        var container = $('.item-comment')
        container.html(data.html)

        $('#product-flags').modal('show');
      }
    });
  });

  $(document).on('click', ".flagproduct", function (e) {
      var product_id = $(this).attr('prod_id');
      $('#flag-product-id').val(product_id);

  })

 /*
  |--------------------------------------------------------------------------
  | Filter Function
  |--------------------------------------------------------------------------
  */

  $(document).on('click', "#filter-session", function (e) {

    var psession_ids = [];
    $('#psession_id  option:selected').each(function() {
        psession_ids.push($(this).val());
     });

    if(psession_ids.length == 0){
        alert("Please select a session from the dropdown !!");
        return false;
    }

    var csrfToken = $('[name=_token]').val();
    var csession = $('#csession-id').val();

    $.ajax({
      url: '/copywriting/session/filter',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{psession_ids:psession_ids, csession_id:csession},
      success:function(data){
        //console.log(data);
        $('.oldprodlist').html(data.productlisthtml);
      }
    });
  })

  $(document).on('click', "#resetall", function (e) {

    $("#psession_id").val('').trigger('change');
    refreshProductsContainer();
  })

   /*
  |--------------------------------------------------------------------------
  | Drop Function
  |--------------------------------------------------------------------------
  */
  $(document).on('click', ".dropProd", function (e) {

    var prod_id = $(this).attr('prod_id');

    swal({
      title: "Drop Product",
      text: "Are you sure you want to drop this product?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: 'btn-success',
      confirmButtonText: 'Drop Product',
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm){
      if (isConfirm) {
        dropProduct(prod_id);
      } else {
        swal("Cancelled", "You have cancelled dropping this product", "error");
      }
    });

  })


  $(document).on('click', ".dropall", function (e) {
    var product_ids = [];
    $("#copyProductsTable tr").not(':eq(0)').each(function(ind,ele){
        product_ids.push($(ele).attr('data-id'));
     });

    if(product_ids=="") {
      NotifContent = '<div class="alert alert-danger media fade in">\
          <h4 class="alert-title">Drop Product</h4>\
          <p>No products to drop</p>\
          </div>';
      autoClose = true;
      type = 'warning';
      method = 3000;
      position = 'top';
      container ='';
      style = 'box';

      generate('bottomRight', '', NotifContent);

      return false;
    }
       swal({
          title: "Drop All Products",
          text: "Are you sure you want to drop all products ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: 'btn-success',
          confirmButtonText: 'Drop Products',
          cancelButtonText: "Cancel",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            dropProduct(product_ids);
          } else {
            swal("Cancelled", "You have cancelled dropping products", "error");
          }
        });
  });


  function dropProduct(prod_id) {

    var csrfToken = $('[name=_token]').val();
    var csession = $('#csession-id').val();

    $.ajax({
      url: '/copywriting/session/dropproduct',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{prod_id:prod_id},
      success:function(data){

        NotifContent = '<div class="alert alert-success media fade in">\
          <h4 class="alert-title">Drop Product</h4>\
          <p>Product has been dropped</p>\
          </div>';
        autoClose = true;
        type = 'success';
        method = 3000;
        position = 'top';
        container ='';
        style = 'box';

        generate('bottomRight', '', NotifContent);
        refreshProductsContainer();
      }
    });
  }

   /*
  |--------------------------------------------------------------------------
  | Refresh Product Container Function
  |--------------------------------------------------------------------------
  */

  function refreshProductsContainer(){

    var csession = $('#csession-id').val();
    var channel = $('#channel_name').val();

    var psession_ids = [];

    $('#psession_id  option:selected').each(function() {
        psession_ids.push($(this).val());
    });

    if(psession_ids.length == 0){
        psession_ids = [];
    }

    var csrfToken = $('[name=_token]').val();
    var $request = $.ajax({
      url: '/copywriting/session/refreshBatchProducts',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      type:'post',
      data:{csession_id:csession, psession_ids:psession_ids, channel:channel},
    });
    var $container = $('.asnProducts');
    $request.done(function(data) { // success
     // console.log(data);
     $('#session-total-box').html(data.total_products);
     $('.copyProducts').html(data.addedproductshtml);
     $('.oldprodlist').html(data.productlisthtml); // copy-scan-list rendered
     $('#session-total-products').html(data.products_count);
     handleiCheck();
    });

  }


    $(document).on('change', "#copy-channel", function (e) {
    
      var channel = $('#copy-channel').val();
      $('#channel_name').val(channel) ;
      var csession = $('#csession-id').val()

      if(channel!=0){
        //var $request = $.get("/getscancopyproducts/"+csession+"/"+channel); // make request

        var $container = $('.copy-scan-list');
        var $container_complete = $('.copy-complete-list');
        //console.log(data.total_products)
        var $request2 = $.get("/copywriting/session/"+csession+"/pending/refresh/?channel="+channel); // make request
        
        $container.html('');
        $request2.done(function(data2) { // success
          $('.copy-scan-list').append(data2.html);
            handleiCheck();
          $('#session-total-box').html(data2.total_products);
  
          $('li.pending-copy-product').each(function (i) {
  
            $('[id="' + this.id + '"]').slice(1).remove();
          });
  
          $('.copy-scan-list li:first').click();
        });
    
        var $request_complete = $.get("/getcompletecopyproducts/"+csession+"/"+channel); // make    request
        $request_complete.done(function(data) { // success
          $container_complete.html(data.html);
        });
        
        $('#tab-1').click();
      }
    });
  </script>
@stop
