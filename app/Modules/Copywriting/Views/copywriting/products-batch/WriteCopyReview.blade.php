@extends('layouts.body')

@section('main-content')

  <link rel="stylesheet" href="{{ URL::asset('js/lightbox2-master/src/css/lightbox.css') }}">

  <div class="app-content-body app-content-full">


    {{ csrf_field() }}
    <input type="hidden" name="csession_id" id="csession-id" value="{{$csession->id}}">
    <input type="hidden" name="uid" id="csession-uid" value="{{ Auth::user()->id }}">
    <!-- hbox layout -->
    <div class="hbox hbox-auto-xs bg-light ">
      <!-- column -->
      <div class="col w lter b-r secondary-sidebar" >
        <div class="vbox">
          <div class="wrapper b-b">
            <div class="font- thin h4 m-t-0 m-b-0">Write <strong>Copy</strong> </div>
            <div class="text-size-small text-info bold"><a href="/copywriting/session/view/{{$csession->id}}">go to session page</a></div>
          </div>
          <div class="">
            <!-- include => copywriting.partials.batch.BatchScanImport -->
          </div>
          <div class="nav-tabs-alt">
            <ul class="nav nav-tabs nav-justified m-b-0">
              <li class="active">
                <a data-target="#tab-1" role="tab" data-toggle="tab">Queue (<span class="queue-tab-count">0</span>) </a>
              </li>
              <li class="">
                  <a data-target="#tab-2" role="tab" data-toggle="tab">Complete (<span class="complete-tab-count">0</span>) </a>
              </li>
            </ul>
          </div>
          <div class="row-row">
            <div class="cell scrollable hover">
              <div class="cell-inner">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab-1">
                      <ul class="media-list media-list-linked media-list-bordered media-email copy-scan-list">
                        @include('copywriting.partials.batch.selectedBatchedProduct')
                      </ul>
                  </div>
                  <div class="tab-pane" id="tab-2">
                    <ul class="media-list media-list-linked media-list-bordered media-email copy-complete-list">
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col no-product-container">
        <div class="vbox">
          <div class="">
            <div class="text-center">
              <svg width="20em" height="16em" viewBox="0 0 300 240"><g fill="none"><ellipse fill="#000" opacity=".05" cx="150" cy="197" rx="135" ry="30"></ellipse><g transform="translate(191 129)"><rect fill="#9484E0" x=".47" y="12.26" width="76.05" height="51.72" rx="3.65"></rect><path d="M3.32 31.12H37.6v27.61a2.6 2.6 0 0 1-2.6 2.6H5.92a2.6 2.6 0 0 1-2.6-2.6V31.12zm36.45 0H74v27.61a2.6 2.6 0 0 1-2.6 2.6H42.37a2.6 2.6 0 0 1-2.6-2.6V31.12zM5.92 14.78h65.53a2.6 2.6 0 0 1 2.6 2.6v11.31H3.32V17.38a2.6 2.6 0 0 1 2.6-2.6z" fill="#A08EED"></path><path d="M19.17 16.95h8.64v6a3.25 3.25 0 0 1-3.25 3.25h-2.13a3.25 3.25 0 0 1-3.25-3.25v-6h-.01zm31.68 0h8.64v6a3.25 3.25 0 0 1-3.25 3.25h-2.13a3.25 3.25 0 0 1-3.25-3.25v-6h-.01z" fill="#44BCE6"></path><path d="M25.915 12.6A7.385 7.385 0 0 1 33.3 5.215h12a7.385 7.385 0 0 1 7.385 7.385v4.47h4.75V12.6C57.435 5.898 52.002.465 45.3.465h-12c-6.702 0-12.135 5.433-12.135 12.135v4.47h4.75V12.6z" fill="#44BCE6"></path></g><g transform="translate(33 117)"><path d="M61.6 7.37a.57.57 0 0 1-.57-.57v9.31a1.38 1.38 0 0 1-1.38 1.38H44.11a1.38 1.38 0 0 1-1.38-1.38V7.68a.31.31 0 0 1 .306-.31H61.6zm-25.29.31v8.43a7.8 7.8 0 0 0 7.8 7.8h15.54a7.8 7.8 0 0 0 7.8-7.8V6.8A5.85 5.85 0 0 0 61.6.95H43c-3.723.042-6.69 3.043-6.69 6.73z" fill="#00C79E"></path><rect fill="#00C79E" x=".29" y="10.71" width="102.82" height="69.92" rx="4.94"></rect><path d="M4.63 10.71h94.15a4.33 4.33 0 0 1 4.33 4.33v43.44a6.16 6.16 0 0 1-6.16 6.16H6.45a6.16 6.16 0 0 1-6.16-6.16V15a4.33 4.33 0 0 1 4.34-4.29z" fill="#00D4A8"></path><path d="M19.1 9.61H26v60a3.08 3.08 0 0 1-3 3.03h-.78a3.08 3.08 0 0 1-3.08-3.08v-60l-.04.05z" fill="#FFCE3E"></path><path d="M19.408 62.13h6.314c.138 0 .248.11.248.248v4.534c0 .14-.11.248-.248.248h-6.314a.246.246 0 0 1-.248-.248v-4.534c0-.14.11-.248.248-.248zm-1.988 4.782c0 1.1.89 1.988 1.988 1.988h6.314c1.1 0 1.988-.89 1.988-1.988v-4.534c0-1.1-.89-1.988-1.988-1.988h-6.314c-1.1 0-1.988.89-1.988 1.988v4.534z" fill="#F1F1F3"></path><path d="M77.12 9.61h6.94v60a3.08 3.08 0 0 1-3.08 3.08h-.78a3.08 3.08 0 0 1-3.08-3.08v-60z" fill="#FFCE3E"></path><path d="M77.438 62.13h6.314c.138 0 .248.11.248.248v4.534c0 .14-.11.248-.248.248h-6.314a.246.246 0 0 1-.248-.248v-4.534c0-.14.11-.248.248-.248zm-1.988 4.782c0 1.1.89 1.988 1.988 1.988h6.314c1.1 0 1.988-.89 1.988-1.988v-4.534c0-1.1-.89-1.988-1.988-1.988h-6.314c-1.1 0-1.988.89-1.988 1.988v4.534z" fill="#F1F1F3"></path></g><g transform="translate(79 95)"><path fill="#428AE0" d="M13.1 14.61h19.87v2.97H13.1zm99.81 0h19.87v2.97h-19.87zM62.418 9.87h21.894a1.93 1.93 0 0 1 1.928 1.93v10.35c0 1.07-.86 1.93-1.928 1.93H62.418a1.93 1.93 0 0 1-1.928-1.93V11.8c0-1.07.86-1.93 1.928-1.93zM51.47 22.15c0 6.047 4.9 10.95 10.948 10.95h21.894c6.052 0 10.948-4.9 10.948-10.95V11.8c0-6.047-4.9-10.95-10.948-10.95H62.418C56.366.85 51.47 5.75 51.47 11.8v10.35z"></path><rect fill="#428AE0" x=".89" y="17.35" width="144.4" height="98.19" rx="6.93"></rect><path d="M6.3 17.35h133.57a5.42 5.42 0 0 1 5.42 5.42v36c0 10.24-8.3 18.54-18.54 18.54H19.43C9.19 77.31.89 69.01.89 58.77v-36a5.42 5.42 0 0 1 5.41-5.42z" fill="#4996F2"></path><rect fill="#00D4A8" x="64.92" y="68.89" width="16.9" height="16.9" rx="2.47"></rect></g></g></svg>
              <p>Waiting for product selection</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col loading-product-container hidden">
        <div class="vbox">
          <div id="loading-wrapper-panel">
              <div id="loader">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>
                <div class="line4"></div>
                <div class="line5"></div>
                <div class="line6"></div>
              </div>
          </div>
        </div>
      </div>
      <!-- /column -->
      <div class="col w-4xl b-r product-details-container hidden">
        <div class="vbox">
          <div class="row-row">
            <div class="cell">
              <div class="cell-inner">
                <div class="panel-heading">
                  <h4>Product Details</h4>
                </div>
                @include('copywriting.partials.CopyProductDetails')
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- column -->
      <div class="col product-template-container hidden">
        <div class="vbox">
          <div class="row-row">
            <div class="cell">
              <div class="cell-inner">
                <div class="panel-heading">
                  <h4>Product Copy</h4>
                </div>
                @include('copywriting.partials.CopyProduct')
              </div>
            </div>
          </div>
        </div>
      </div>


      <!-- /hbox layout -->
    </div>
    <!--/Page Container-->
    <div class="copy-memory-container">
      @include('partials.modals.addCopyMemory')
    </div>

  </div>
  <input type="hidden" name="copyActiveProduct" id="copyActiveProduct" value="0">
  <input type="hidden" name="isLocked" id="isLocked" value="0">
  <input type="hidden" name="issubmit" id="issubmit" value="0">
@stop

@section('page-scripts')

  <!-- Page scripts -->
  <script src="{{ URL::asset('assets/editors/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ URL::asset('js/pages/page_emails.js') }}"></script>
  <script src="{{ URL::asset('js/studiobridge/addproducts.js') }}"></script>
  <script src="{{ URL::asset('js/extensions/countdown/jquery.plugin.js') }}"></script>
  <script src="{{ URL::asset('js/extensions/countdown/jquery.countdown.js') }}"></script>
  <script src="{{ URL::asset('js/studiobridge/qc-product-operations.js') }}"></script>
  <script src="{{ URL::asset('js/studiobridge/liveshoot-resequence.js') }}"></script>

  <script src="{{ URL::asset('js/lightbox2-master/src/js/lightbox.js') }}"></script>
  <!-- /page scripts -->

  <script src="{{ URL::asset('js/copydesk.js') }}"></script>
  <script src="{{ URL::asset('js/copymemory.js') }}"></script>
@stop
