<?php
namespace Modules\Copywriting\Controllers;

use App\Events\CopyLock;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessCopy;
use Modules\Copywriting\Models\CopyHeader;
use Modules\Copywriting\Models\CopyHeaderValue;
use Modules\Copywriting\Models\CopyRevisions;
use Modules\Copywriting\Models\CopyTemplates;
use Modules\Copywriting\Models\ProductCopywritingsession;
use App\Models\Ounass\Ounass;
use App\Models\Common\Product;
use App\Models\Common\ProductTime;
use App\Models\Category\SimpleCategory;
use App\Repositories\Agency\AgencyRepositoryInterface;
use App\Repositories\Copy\CopyRepositoryInterface;
use App\User;
use DateTime;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class CopywritingController extends Controller
{
    protected $copy;
    protected $agency;

    public function __construct(CopyRepositoryInterface $copy, AgencyRepositoryInterface $agency)
    {
        $this->copy   = $copy;
        $this->agency = $agency;
    }

    public function ViewAllSessions($status = 'all')
    {
        $user_id  = Auth::user()->id;
        $sessions = $this->copy->getCopySessions($user_id, $status);

        $open_session = $this->copy->getOpenCopywritingSession($user_id);

        return view('Copywriting::copywriting.sessions.ViewSessions', ['csessions' => $sessions, 'open_session' => $open_session]);
    }

    public function AddSession()
    {

        $user_id         = Auth::User()->id;
        $organization_id = $this->getOrganizationId(); // die ;

        $copywriting_agencies = $this->agency->findBy(['agency_type' => 'copywriting', 'organization_id' => $organization_id, 'status' => '1']);

        $csessions = $this->copy->countOpenSessionByUser($user_id, 'open');

        if ($csessions >= 1) {
            return back()->with('notification', ['title' => 'Existing session', 'message' => 'You already have an existing session in progress. You cannot create another one.', 'class' => 'bg-warning']);
        }

        return view('copywriting.sessions.AddSession')->with(['user' => $user_id, 'copywriting_agencies' => $copywriting_agencies]);
    }

    public function StoreSession(Request $request)
    {
        $this->validate($request, [
            'method' => 'required',
        ]);

        $user_id  = Auth::User()->id;
        $csession = $this->copy->create(['type' => $request->type, 'session_type' => $request->session_type, 'method' => $request->method, 'notes' => $request->notes, 'user_id' => $user_id, 'status' => 'open', 'min_product' => 0]);
        $csid     = $csession->id;

        return redirect('/copywriting/session/view/' . $csid);
    }

    public function ViewSession($id)
    {

        $session_data = $this->copy->getCopySessions_dtl($id);

        $total_batch_products = '';
        if ($session_data) {
            $cw = User::find($session_data->user_id);
        };

        if ($session_data->type == 'review') {
            $product_batched_collection = $this->copy->getReviewBatchedProductsList($id);
            $total_batch_products       = count($product_batched_collection);
        }

        if ($session_data->type == 'Writing') {
            $product_batched_collection = $this->copy->getPendingCopyProductsBatchList($id);
            $total_batch_products       = count($product_batched_collection);
        }

        $products               = $this->copy->getSessionProductList($session_data->id);
        $session_products_count = ProductCopywritingsession::where('copywritingsession_id', $session_data->id)->select('product_id')->count();
        $write_products_count   = ProductCopywritingsession::where('copywritingsession_id', $session_data->id)->where('copy_state', 1)->count();

        $return_data = [
            'csession'               => $session_data,
            'products_list'          => $products,
            'total_batch_products'   => $total_batch_products,
            'session_products_count' => $session_products_count,
            'write_products_count'   => $write_products_count,
        ];

        return view('Copywriting::copywriting.sessions.ViewSession', $return_data);
    }

    public function AddProducts($id)
    {

        $copy_ses = $this->copy->find($id);
        if ($copy_ses->status != "open") {
            return redirect('/copywriting/session/view/' . $copy_ses->id)->with('error', 'Access restricted to this section');
        }

        $return_products    = '';
        $product_collection = '';

        $product_collection = Product::with('states')
            ->whereHas('states', function ($q) {
                return $q->where('product_states.qc_done', 1)->where('product_states.qc_state', 1);
            })
            ->with(array('psessions' => function ($query) {
                return $query->where('psessions.status', 'closed')->orderBy('psessions.id', 'DESC');
            }))
            ->with('productcomment')
            ->whereHas('images')
            ->get();

        $product_ids     = array();
        $return_products = ProductCopywritingsession::where('copywritingsession_id', $copy_ses->id)->with('products')->get();

        foreach ($return_products as $product) {
            $product_ids[] = $product->products['id'];
        }

        $products_count = count($return_products);

        return view(
            'copywriting.products.AddProducts',
            [
                'csession'       => $copy_ses,
                'result'         => $product_collection,
                'product_list'   => $return_products,
                'product_ids'    => $product_ids,
                'products_count' => $products_count,
            ]
        );

    }

    public function CopywritingAddBatchProduct($id)
    {

        $copy_ses = $this->copy->find($id);
        if ($copy_ses->status != "open") {
            return redirect('/copywriting/session/view/' . $copy_ses->id)->with('error', 'Access restricted to this section');
        }

        $method  = $copy_ses->method;
        $channel = '';
        if (!empty($_REQUEST['channel'])) {
            $channel = $_REQUEST['channel'];
        }

        $product_collection = '';
        if ($method == "batch") {

            $product_collection = $this->copy->getPendingCopyProducts('DESC', $channel);
            $total_products     = count($product_collection);
        }

        $product_ids     = array();
        $return_products = ProductCopywritingsession::where('copywritingsession_id', $copy_ses->id)->with('products')->get();

        foreach ($return_products as $product) {
            $product_ids[] = $product->products['id'];
        }

        $products_count = count($return_products);

        return view(
            'copywriting.products-batch.AddProducts',
            [
                'csession'       => $copy_ses,
                'result'         => $product_collection,
                'product_list'   => $return_products,
                'product_ids'    => $product_ids,
                'products_count' => $products_count,
            ]
        );

    }

    public function StoreProducts(Request $request)
    {

        $product_ids = $request->products;
        $csession_id = $request->csession;
        $user_id     = Auth::user()->id;
        $data_insert = array();

        $copywritingsession = $this->copy->find($csession_id);

        foreach ($product_ids as $product) {

            $exist_products = ProductCopywritingsession::where('product_id', $product)->count();

            if ($exist_products > 0) {
                $state = 'exist';
            } else {
                $state = 'new';
            }

            if ($copywritingsession->type == 'review') {
                $copy_state = 1;
            } else {
                $copy_state = 0;
            }

            array_push($data_insert, array('product_id' => $product, 'copywritingsession_id' => $csession_id, 'user_id' => $user_id, 'state' => $state, 'copy_state' => $copy_state));

            $exists_check = ProductTime::whereNull('copy_start')->where('product_id', $product)->first();
            if (!empty($exists_check)) {
                $data_arr = array('copy_start' => date("Y-m-d H:i:s"));
                ProductTime::where('product_id', $pid)->update($data_arr);
            }
        }

        $products = new ProductCopywritingsession;
        $products->insert($data_insert);

        return response()->json(['status' => 'success', 200]);
    }

    public function WriteCopy($id)
    {

        $user_id = Auth::user()->id;

        $ses     = $this->copy;
        $copyses = $ses->find($id);

        if ($copyses->status != "open") {
            return redirect('/copywriting/session/view/' . $copyses->id)->with('error', 'Access restricted to this section');
        }
        $method = $copyses->method;

        if ($method == "continuous") {
            $product_collection = $ses->getPendingCopyProducts('DESC');
            $total_products     = count($product_collection);
        } else {
            $product_collection = $ses->getWriteProductsList($copyses->id);
            $total_products     = count($product_collection);
        }

        $contributers = $ses->getWriteContributers($copyses->id);

        ProductCopywritingsession::where(['lock_user_id' => $user_id])->update(['lock' => 0, 'lock_user_id' => null]);
        ProductCopywritingsession::whereNull('lock_user_id')->update(['lock' => 0, 'lock_user_id' => null]);

        return view(
            'Copywriting::copywriting.products.WriteCopy',
            [
                'csession'       => $copyses,
                'result'         => $product_collection,
                'total_products' => $total_products,
                'contributers'   => $contributers,
                'session_type'   => 'copy',
            ]
        );
    }

    public function WriteBatchCopy($id)
    {

        $user_id = Auth::user()->id;
        $ses     = $this->copy;
        $copyses = $ses->find($id);
        if ($copyses->status != "open") {
            return redirect('/copywriting/session/view/' . $copyses->id)->with('error', 'Access restricted to this section');
        }
        $method = $copyses->method;

        if ($method == "batch") {
            $product_collection = $ses->getPendingCopyProductsBatchList($id);
            $total_products     = count($product_collection);
        }

        $contributers = $ses->getWriteContributers($copyses->id);

        ProductCopywritingsession::where(['lock_user_id' => $user_id])->update(['lock' => 0, 'lock_user_id' => null]);
        ProductCopywritingsession::whereNull('lock_user_id')->update(['lock' => 0, 'lock_user_id' => null]);

        return view(
            'copywriting.products-batch.WriteCopy',
            [
                'csession'       => $copyses,
                'result'         => $product_collection,
                'total_products' => $total_products,
                'contributers'   => $contributers,
                'session_type'   => 'copy',
                'method'         => $method,
            ]
        );
    }

    public function GetCopywritingAgencyUsers(Request $request)
    {

        $agency_id          = $request->agency_id;
        $v                  = $this->agency;
        return $agencyUsers = $v->GetCopywritingAgencyUsers($agency_id);
    }

    public function GetProduct(Request $request)
    {

        $user_id     = Auth::user()->id;
        $product_id  = $request->product;
        $completion  = $request->completion;
        $csession_id = $request->csession;

        $session_type      = $request->session_type;
        $product_lock      = 0;
        $product_lock_user = '';

        $product_master = Product::where('id', $product_id)->first();

        $product_revisions = CopyRevisions::where('product_id', $product_id)->orderBy('id', 'DESC')->get();
        $latest_revision   = CopyRevisions::where('product_id', $product_id)->orderBy('id', 'DESC')->first();

        $product_channel = Ounass::leftJoin('simple_category', function ($join) {
            $join->on('ounass.Class', '=', 'simple_category.class');
            $join->on('ounass.SubClass', '=', 'simple_category.subclass');
        })
            ->select('ounass.*', 'simple_category.category')
            ->where('channel', $product_master->channel)
            ->where(function ($query) use ($product_master) {
                $query->where('Item', $product_master->identifier);
                $query->orWhere('Barcode', $product_master->identifier);
            })->first();

        $copy_header    = '';
        $latest_ch_val  = '';
        $copy_templates = '';

        if (!empty($product_channel->Channel) && !empty($product_channel->category)) {
            $copy_templates = CopyTemplates::where('channel', $product_channel->Channel)->where('simple_category', $product_channel->category)->first();
            if (!empty($copy_templates)) {
                $copy_header = CopyHeader::where(['is_visible' => 1, 'copy_template_id' => $copy_templates->id])->orderBy('weight', 'ASC')->get();
            }
            if (!empty($latest_revision)) {
                $latest_ch_val = CopyHeaderValue::where('copy_revision_id', $latest_revision->id)->orderBy('id', 'ASC')->get();
            }
        }

        $copywriting = ProductCopywritingsession::where([
            'product_id' => $product_id,
            'lock'       => 1,
        ])->first();

        event(new CopyLock($product_id, $user_id, 1, 0, 0));

        if (!empty($copywriting)) {
            if ($copywriting->lock_user_id == Auth::user()->id) {
                $auto_prod_data = $this->copy->GetParentCopyProducts($product_id);
                //dd($auto_prod_data) ;
                ProductCopywritingsession::where(['lock_user_id' => Auth::user()->id])->update(['lock' => 0, 'lock_user_id' => null]);
                ProductCopywritingsession::where(['product_id' => $product_id])->update(['lock' => 1, 'lock_user_id' => Auth::user()->id]);

                $product_lock      = 1;
                $product_lock_user = Auth::user()->id;
                $html              = view('Copywriting::copywriting.partials.CopyProduct', ['product_id' => $product_master, 'product' => $product_channel, 'revisions' => $product_revisions, 'latest_revision' => $latest_revision, 'session_type' => $session_type, 'copy_header' => $copy_header, 'latest_ch_val' => $latest_ch_val, 'template' => $copy_templates, 'copylock' => $product_lock, 'copylockuser' => $product_lock_user, 'auto_prod_data' => $auto_prod_data])->render();
            } else {
                $product_lock      = $copywriting->lock;
                $product_lock_user = $copywriting->lock_user_id;
                $html              = view('Copywriting::copywriting.partials.ViewCopyProduct', ['product_id' => $product_master, 'product' => $product_channel, 'revisions' => $product_revisions, 'latest_revision' => $latest_revision, 'session_type' => $session_type, 'copy_header' => $copy_header, 'latest_ch_val' => $latest_ch_val, 'template' => $copy_templates, 'copylock' => $product_lock, 'copylockuser' => $product_lock_user, 'completion' => $completion])->render();
            }

        } else {
            $auto_prod_data = $this->copy->GetParentCopyProducts($product_id);
            ProductCopywritingsession::where(['lock_user_id' => Auth::user()->id])->update(['lock' => 0, 'lock_user_id' => null]);
            ProductCopywritingsession::where(['product_id' => $product_id])->update(['lock' => 1, 'lock_user_id' => Auth::user()->id]);

            $product_lock      = 1;
            $product_lock_user = Auth::user()->id;

            if ($session_type == 'review') {
                $html = view('Copywriting::copywriting.partials.ViewCopyProduct', ['product_id' => $product_master, 'product' => $product_channel, 'revisions' => $product_revisions, 'latest_revision' => $latest_revision, 'session_type' => $session_type, 'copy_header' => $copy_header, 'latest_ch_val' => $latest_ch_val, 'template' => $copy_templates, 'copylock' => $product_lock, 'copylockuser' => $product_lock_user, 'completion' => $completion])->render();
            } else {
                $html = view('Copywriting::copywriting.partials.CopyProduct', ['product_id' => $product_master, 'product' => $product_channel, 'revisions' => $product_revisions, 'latest_revision' => $latest_revision, 'session_type' => $session_type, 'copy_header' => $copy_header, 'latest_ch_val' => $latest_ch_val, 'template' => $copy_templates, 'copylock' => $product_lock, 'copylockuser' => $product_lock_user, 'auto_prod_data' => $auto_prod_data])->render();
            }

        }
        $html_pd = view('Copywriting::copywriting.partials.CopyProductDetails', ['product_id' => $product_master, 'product' => $product_channel])->render();
        return response()->json(['copyProductDetails' => $html_pd, 'copyProduct' => $html]);
    }

    public function StoreDraft(Request $request)
    {

        $input_data = $request->all();

        $product_id  = $input_data['product_id'];
        $csession_id = $input_data['csession_id'];

        $copySession = $this->copy->find($csession_id);
        $user_id     = Auth::user()->id;

        $approval_qc_state = 0;
        if (!empty($input_data['qc_state'])) {
            $approval_qc_state = 1;
        }

        $approval_copy_state = 0;
        if ($input_data['copy_state'] == '1') {
            $approval_copy_state = 1;
        }

        if ($approval_qc_state == 1) {
            $session_products = ProductCopywritingsession::where('product_id', $product_id)->where('copy_state', 1)->orderBy('created_at', 'desc')->first();
            $user_id          = $session_products->user_id;
        }

        $product_state = ($input_data['copy_state'] != '' ? $input_data['copy_state'] : 0);

        $store_revision = false;
        foreach ($input_data as $key => $value) {
            if (preg_match('/copy_/i', $key)) {
                if (!empty($value) || $key == 'copy_state') {
                    if ($key == 'copy_state') {

                    } else {
                        $store_revision = true;
                    }
                    break;
                }
            }
        }

        if ($store_revision == true) {
            $product_copy                        = new CopyRevisions;
            $product_copy->copywritingsession_id = $csession_id;
            $product_copy->product_id            = $product_id;
            $product_copy->user_id               = $user_id;
            $product_copy->state                 = $product_state;
            $product_copy->save();

            $copydata = array();
            foreach ($input_data as $key => $value) {
                if (preg_match('/copy_/i', $key)) {
                    if (!empty($value) || $key == 'copy_state') {
                        $copydata[] = array('header_id' => str_replace('copy_', '', $key), 'product_id' => $product_id, 'value' => ltrim($value), 'copy_revision_id' => $product_copy->id);
                    }
                }
            }
            $chv = CopyHeaderValue::insert($copydata);
        }

        $data_arr = array('copy_end' => date("Y-m-d H:i:s"));
        ProductTime::where('product_id', $product_id)->update($data_arr);

        DB::table('product_completion')
            ->where('product_id', $product_id)
            ->update(['csession_id' => $csession_id, 'is_copy_complete' => 1, 'updated_at' => new DateTime]);

        $session_products_count = ProductCopywritingsession::where('copywritingsession_id', $csession_id)->where('product_id', $product_id)->count();

        if ($session_products_count == 0) {
            $exist_products = ProductCopywritingsession::where('product_id', $product_id)->count();
            $state          = ($exist_products > 0 ? 'exist' : 'new');

            $product_session                        = new ProductCopywritingsession;
            $product_session->copywritingsession_id = $csession_id;
            $product_session->product_id            = $product_id;
            $product_session->user_id               = Auth::user()->id;
            $product_session->qc_state              = $approval_qc_state;
            $product_session->copy_state            = $approval_copy_state;
            $product_session->save();

            if ($approval_qc_state == '1') {
                ProductCopywritingsession::where([
                    'product_id' => $product_id,
                ])->update([
                    'lock'         => 0,
                    'lock_user_id' => null,
                    'qc_state'     => 1,
                ]);

                $copy_job = (new ProcessCopy($product_id, $csession_id))
                    ->onConnection('redis') //database
                    ->onQueue('copyprocessing');
                dispatch($copy_job);

            }

        } else {
            $product_session             = ProductCopywritingsession::where('copywritingsession_id', $csession_id)->where('product_id', $product_id)->first();
            $product_session->qc_state   = $approval_qc_state;
            $product_session->copy_state = $approval_copy_state;
            $product_session->save();

            if ($approval_qc_state == '1') {
                ProductCopywritingsession::where([
                    'product_id' => $product_id,
                ])->update([
                    'lock'         => 0,
                    'lock_user_id' => null,
                    'qc_state'     => 1,
                ]);

                $copy_job = (new ProcessCopy($product_id, $csession_id))
                    ->onConnection('redis') //database
                    ->onQueue('copyprocessing');
                dispatch($copy_job);

            }

            if ($approval_copy_state == '1') {
                ProductCopywritingsession::where([
                    'product_id' => $product_id,
                ])->update([
                    'lock'         => 0,
                    'lock_user_id' => null,
                    'copy_state'   => 1,
                ]);

            }

        }

        event(new CopyLock($product_id, $user_id, 0, 1, 0));

        $product_master = Product::where('id', $product_id)->first();
        $return_product = Ounass::where('id', $product_master->ounass_id)->first();

        $product_revisions = CopyRevisions::where('product_id', $product_id)
            ->where('copywritingsession_id', $csession_id)->orderBy('id', 'DESC')->get();
        $latest_revision = CopyRevisions::where('product_id', $product_id)->where('copywritingsession_id', $csession_id)->orderBy('id', 'DESC')->first();
        //'copywriting.partials.batch.selectedBatchedProduct'
        $html = view('copywriting.partials.CopyProduct', [
            'product_id'      => $product_master,
            'product'         => $return_product,
            'revisions'       => $product_revisions,
            'copylockuser'    => '',
            'copylock'        => '',
            'latest_revision' => $latest_revision,
            'session_type'    => 'copy',
        ])->render();

        return response()->json(['html' => $html, 'copySessionMethod' => $copySession->method]);
    }

    public function dropProducts(Request $request)
    {

        $product_ids = $request->prod_id;

        if (is_array($product_ids)) {
            foreach ($product_ids as $id) {
                ProductCopywritingsession::where('id', $id)->delete();
            }
        } else {
            ProductCopywritingsession::where('id', $request->prod_id)->delete();
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function filterProducts(Request $request)
    {

        $psession_ids = $request->psession_ids;
        $csession_id  = $request->csession_id;

        if (is_array($psession_ids)) {

            $product_collection = Product::with('states')
                ->whereHas('states', function ($q) use ($psession_ids) {
                    return $q->whereIn('product_states.psession_id', $psession_ids)
                        ->where('product_states.qc_done', 1)->where('product_states.qc_state', 1)
                        ->groupBy('product_states.product_id')
                        ->orderBy('product_states.psession_id', 'DESC');
                })
                ->with('psessions')
                ->with('productcomment')
                ->whereHas('images')
                ->get();

            $product_array = array();

            $return_products = ProductCopywritingsession::where('copywritingsession_id', $csession_id)
                ->with('products')->get();

            foreach ($return_products as $product) {
                $product_array[] = $product->products['id'];
            }

            $productlisthtml = view('copywriting.partials.CopyList', ['result' => $product_collection,
                'product_ids'                                                      => $product_array])->render();

            return response()->json(['status' => 'success', 'productlisthtml' => $productlisthtml, 200]);
        }

    }

    public function refreshProducts(Request $request)
    {

        $csession_id  = $request->csession_id;
        $psession_ids = $request->psession_ids;

        $products_count = '0';

        if (!empty($psession_ids)) {

            $product_collection = Product::with('states')
                ->whereHas('states', function ($q) use ($psession_ids) {
                    return $q->whereIn('product_states.psession_id', $psession_ids)
                        ->where('product_states.qc_done', 1)->where('product_states.qc_state', 1)
                        ->groupBy('product_states.product_id')
                        ->orderBy('product_states.psession_id', 'DESC');
                })
                ->with('psessions')
                ->with('productcomment')
                ->whereHas('images')
                ->get();

        } else {

            $product_collection = Product::with('states')
                ->whereHas('states', function ($q) {
                    return $q->where('product_states.qc_done', 1)->where('product_states.qc_state', 1);
                })
                ->with(array('psessions' => function ($query) {
                    return $query->where('psessions.status', 'closed')->orderBy('psessions.id', 'DESC');
                }))
                ->with('productcomment')
                ->whereHas('images')
                ->get();

        }

        $product_array = array();

        $return_products = ProductCopywritingsession::where('copywritingsession_id', $csession_id)
            ->with('products')->get();

        foreach ($return_products as $product) {
            $product_array[] = $product->products['id'];
        }

        $productlisthtml   = view('copywriting.partials.CopyList', ['result' => $product_collection, 'product_ids' => $product_array])->render();
        $addedproductshtml = view('copywriting.partials.CopyProducts', ['product_list' => $return_products, 'csession' => $csession_id])->render();

        $products_count = count($return_products);

        return response()->json([
            'status'            => 'success',
            'productlisthtml'   => $productlisthtml,
            'addedproductshtml' => $addedproductshtml,
            'products_count'    => $products_count,
        ], 200);

    }

    public function refreshBatchedProducts(Request $request)
    {

        $csession_id  = $request->csession_id;
        $psession_ids = $request->psession_ids;
        $channel      = $request->channel;

        $products_count = '0';

        if (!empty($psession_ids)) {
            $product_collection = Product::with('states')
                ->whereHas('states', function ($q) use ($psession_ids) {
                    return $q->whereIn('product_states.psession_id', $psession_ids)
                        ->where('product_states.qc_done', 1)->where('product_states.qc_state', 1)
                        ->groupBy('product_states.product_id')
                        ->orderBy('product_states.psession_id', 'DESC');
                })
                ->with('psessions')
                ->with('productcomment')
                ->whereHas('images')
                ->get();
        } else {
            $product_collection = $this->copy->getBatchedPendingCopyProducts('DESC', $channel);
            $total_products     = count($product_collection);
        }

        $product_array = array();

        $return_products = ProductCopywritingsession::where('copywritingsession_id', $csession_id)
            ->with('products')->get();

        foreach ($return_products as $product) {
            $product_array[] = $product->products['id'];
        }

        $productlisthtml   = view('copywriting.partials.PendingCopyList', ['result' => $product_collection, 'product_ids' => $product_array, 'method' => 'batch'])->render();
        $addedproductshtml = view('copywriting.partials.CopyProducts', ['product_list' => $return_products, 'csession' => $csession_id])->render();

        $products_count = count($return_products);

        return response()->json([
            'status'            => 'success',
            'productlisthtml'   => $productlisthtml,
            'addedproductshtml' => $addedproductshtml,
            'products_count'    => $products_count,
            'total_products'    => $total_products,
        ], 200);
    }

    public function refreshBatchedReviewProducts(Request $request)
    {

        $csession_id  = $request->csession_id;
        $psession_ids = $request->psession_ids;
        $method       = $request->method;

        $products_count = '0';

        if (!empty($psession_ids)) {

            $product_collection = Product::with('states')
                ->whereHas('states', function ($q) use ($psession_ids) {
                    return $q->whereIn('product_states.psession_id', $psession_ids)
                        ->where('product_states.qc_done', 1)->where('product_states.qc_state', 1)
                        ->groupBy('product_states.product_id')
                        ->orderBy('product_states.psession_id', 'DESC');
                })
                ->with('psessions')
                ->with('productcomment')
                ->whereHas('images')
                ->get();

        } else {
            if ($method == "batch") {
                $product_collection = $this->copy->getReviewBatchProducts($csession_id);
            }
        }

        $product_array   = array();
        $return_products = ProductCopywritingsession::where('copywritingsession_id', $csession_id)->with('products')->get();

        foreach ($return_products as $product) {
            $product_array[] = $product->products['id'];
        }

        $productlisthtml   = view('copywriting.partials.batch.ReviewProducts', ['reviewproductlist' => $product_collection, 'method' => 'batch'])->render();
        $addedproductshtml = view('copywriting.partials.CopyProducts', ['product_list' => $return_products, 'csession' => $csession_id])->render();

        $products_count = count($return_products);

        return response()->json([
            'status'            => 'success',
            'productlisthtml'   => $productlisthtml,
            'addedproductshtml' => $addedproductshtml,
            'products_count'    => $products_count,
        ], 200);
    }

    public function statusUpdate(Request $request)
    {

        if ($request->status == 'copy-pending') {
            $status = 'copy pending';
        } elseif ($request->status == 'review-pending') {
            $status = 'review pending';
            ProductCopywritingsession::where('copywritingsession_id', $request->csession_id)->update(['copy_state' => '1']);
        } else {
            $status = $request->status;
        }

        /**
         * Update the Copywriting session status for ending session
         *
         */

        $this->copy->update(['status' => $status], $request->csession_id);

        /**
         * also as a fail safe, lets push all copy from the session when the session is closed
         *
         */

        $ids = ProductCopywritingsession::where('copywritingsession_id', '=', $request->csession_id)->where('qc_state', 1)->pluck('product_id');

        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $copy_job = (new ProcessCopy($id, $request->csession_id))
                    ->onConnection('redis') //database
                    ->onQueue('copyprocessing');

                dispatch($copy_job);
            }
        }

        return 'ok';
    }

    public function EndSession($id)
    {

        ProductCopywritingsession::where('copywritingsession_id', $id)->update(['lock' => 0, 'lock_user_id' => null]);
        $this->copy->update(['status' => 'closed'], $id);
        return redirect('/copyprocessing/' . $id);
    }

    public function StartReview($id)
    {

        $ses     = $this->copy;
        $copyses = $this->copy->find($id);

        if ($copyses->status != "open") {
            return redirect('/copywriting/session/view/' . $copyses->id)->with('error', 'Access restricted to this section');
        }

        if ($copyses->method == 'batch') {
            $products_list      = $ses->getReviewBatchedProductsList($copyses->id);
            $products_list_comp = $ses->getReviewBatchedProductsListCompleted($copyses->id);
        } else {
            $products_list      = $ses->getReviewProductsList($copyses->id);
            $products_list_comp = $ses->getReviewProductsListCompleted($copyses->id);
        }

        $total_products           = count($products_list);
        $total_products_completed = count($products_list_comp);

        return view(
            'copywriting.products.WriteReview',
            [
                'csession'                 => $copyses,
                'reviewproductlist'        => $products_list,
                'total_products'           => $total_products,
                'total_products_completed' => $total_products_completed,
                'session_type'             => 'review',
            ]
        );
    }

    public function getContinuousReviewProducts($id, $channel)
    {

        $ses     = $this->copy;
        $copyses = $this->copy->find($id);

        $products_list      = $ses->getReviewProductsList($id, $channel);
        $products_list_comp = $ses->getReviewProductsListCompleted($id, $channel);

        $total_products           = count($products_list);
        $total_products_completed = count($products_list_comp);

        $html = view(
            'copywriting.partials.ReviewProducts',
            [
                'csession'          => $copyses,
                'reviewproductlist' => $products_list,
                'session_type'      => 'review',
            ]
        )->render();

        $html_complete = view(
            'copywriting.partials.ReviewProducts',
            [
                'csession'          => $copyses,
                'reviewproductlist' => $products_list_comp,
                'session_type'      => 'review',
            ]
        )->render();

        return response()->json([
            'html'                     => $html,
            'html_complete'            => $html_complete,
            'total_products'           => $total_products,
            'total_products_completed' => $total_products_completed,
        ]);
    }

    public function StartReviewBatchCopy($id)
    {

        $copyses = $this->copy->find($id);
        if ($copyses->status != "open") {
            return redirect('/copywriting/session/view/' . $copyses->id)->with('error', 'Access restricted to this section');
        }

        if ($copyses->method == "batch") {
            $products_list  = $this->copy->getReviewBatchProducts($copyses->id);
            $total_products = count($products_list);
        }

        $product_ids     = array();
        $return_products = ProductCopywritingsession::where('copywritingsession_id', $copyses->id)->with('products')->get();
        foreach ($return_products as $product) {
            $product_ids[] = $product->products['id'];
        }
        $products_count = count($return_products);

        return view(
            'copywriting.products-batch.AddProductsReview',
            [
                'csession'               => $copyses,
                'reviewproductlist'      => $products_list,
                'pending_products_count' => $total_products,
                'session_type'           => 'review',
                'method'                 => $copyses->method,
                'product_list'           => $return_products,
                'product_ids'            => $product_ids,
                'products_count'         => $products_count,
            ]
        );
    }

    public function RenderProducts($id)
    {

        $ses     = $this->copy;
        $copyses = $ses->find($id);

        $products_list = $ses->getReviewProductsList($copyses->id);

        $html = view('copywriting.partials.ReviewProducts', ['reviewproductlist' => $products_list, 'qsession' => $copyses->id])->render();

        return response()->json(compact('html'));
    }

    public function PendingCopyListProducts($id)
    {

        $ses     = $this->copy;
        $copyses = $ses->find($id);

        $products_array = array();

        $method  = $copyses->method;
        $channel = '';
        if (!empty($_REQUEST['channel'])) {
            $channel = $_REQUEST['channel'];
        }

        if ($method == "batch") {

            $product_collection = $ses->getBatchedPendingCopyProducts('DESC', $channel);
            $total_products     = count($product_collection);

        } elseif ($method == "continuous") {

            $product_collection = $ses->getPendingCopyProducts('DESC', $channel);
            $total_products     = count($product_collection);
            // Get Complete products
            $product_completed      = $ses->getCompleteCopyProducts('DESC', $channel, $copyses->id);
            $total_compete_products = count($product_completed);
        } else {
            $products       = $ses->getWriteProductsList($copyses->id);
            $total_products = count($product_collection);
        }

        $html = view('Copywriting::copywriting.partials.PendingCopyList', [
            'result'     => $product_collection,
            'method'     => $method,
            'completion' => 0,
        ])->render();

        $html_complete = view('Copywriting::copywriting.partials.PendingCopyList', [
            'result'     => $product_completed,
            'method'     => $method,
            'completion' => 1,
        ])->render();

        return response()->json([
            'status'                 => 'success',
            'html'                   => $html,
            'html_complete'          => $html_complete,
            'total_products'         => $total_products,
            'total_compete_products' => $total_compete_products,
        ], 200);
    }

    public function RenderCopyListProducts($id)
    {

        $ses     = $this->copy;
        $copyses = $ses->find($id);

        $products_array  = array();
        $products_array1 = array();

        if ($copyses->method == "continuous") {

            $product_list = $ses->getSessionCompleteProductList($copyses->id);

            foreach ($product_list as $products) {
                $products_array[] = $products->product_id;
            }

            $product_collection = Product::with('states')
                ->whereHas('states', function ($q) {
                    return $q->where('product_states.qc_done', 1)->where('product_states.qc_state', 1);
                })
                ->with(array('psessions' => function ($query) {
                    //**for testing**
                    // return $query->where('psessions.status', 'closed')-orderBy('psessions.id', 'DESC');
                }))
                ->with('productcomment')
                ->whereHas('images')
                ->groupBy('products.id')
                ->get();

            foreach ($product_collection as $sid => $product) {
                $products_array1[] = $product->id;
            }

            $products       = array_diff($products_array1, $products_array);
            $total_products = count($products);

        } else {

            $product_collection = $ses->getWriteProductsList($copyses->id);
            $total_products     = count($product_collection);
        }

        $html = view('copywriting.partials.PendingCopyList', [
            'result'       => $product_collection,
            'product_list' => $products_array,
        ])->render();

        return response()->json([
            'status'         => 'success',
            'html'           => $html,
            'total_products' => $total_products],
            200);

    }

    public function RenderReviewProducts($id)
    {

        $ses     = $this->copy;
        $copyses = $ses->find($id);

        if ($copyses->method == 'batch') {
            $products_list      = $ses->getReviewBatchedProductsList($copyses->id);
            $products_list_comp = $ses->getReviewBatchedProductsListCompleted($copyses->id);
        } else {
            $products_list      = $ses->getReviewProductsList($copyses->id);
            $products_list_comp = $ses->getReviewProductsListCompleted($copyses->id);
        }

        $total_products           = count($products_list);
        $total_products_completed = count($products_list_comp);

        $html = view('copywriting.partials.ReviewProducts', [
            'reviewproductlist' => $products_list,
            'completion'        => 0,
        ])->render();

        $html_complete = view('copywriting.partials.ReviewProducts', [
            'reviewproductlist' => $products_list_comp,
            'completion'        => 1,
        ])->render();

        return response()->json([
            'status'                   => 'success',
            'html'                     => $html,
            'html_complete'            => $html_complete,
            'total_products'           => $total_products,
            'total_products_completed' => $total_products_completed,
        ],
            200);

    }

    public function ReviewStatusUpdate(Request $request)
    {

        $product_id  = $request->product;
        $btntype     = $request->btntype;
        $csession_id = $request->csession;

        if ($request->btntype == 'approve') {
            $copy_state = '1';
            $qc_state   = '1';

            CopyRevisions::where('product_id', $product_id)->where('copywritingsession_id', $csession_id)->update(['state' => '1']);

        } elseif ($request->btntype == 'reject') {
            CopyRevisions::where('product_id', $product_id)->where('copywritingsession_id', $csession_id)->update(['state' => '2']);
            $copy_state = '1';
            $qc_state   = '2';
        }

        ProductCopywritingsession::where('copywritingsession_id', $csession_id)
            ->where('product_id', $product_id)->update([
            'copy_state' => $copy_state,
            'qc_state'   => $qc_state,
        ]);

        return redirect('/copywriting/session/products/review/' . $csession_id);
    }

    public function PendingItemsCount($id)
    {

        $ses     = $this->copy;
        $copyses = $ses->find($id);

        $products_array  = array();
        $products_array1 = array();

        if ($copyses->method == "continuous") {

            $product_list = $ses->getSessionCompleteProductList($copyses->id);

            foreach ($product_list as $products) {
                $products_array[] = $products->product_id;
            }

            $product_collection = Product::with('states')
                ->whereHas('states', function ($q) {
                    return $q->where('product_states.qc_done', 1)->where('product_states.qc_state', 1);
                })
                ->with(array('psessions' => function ($query) {
                }))
                ->with('productcomment')
                ->whereHas('images')
                ->get();

            foreach ($product_collection as $sid => $product) {
                $products_array1[] = $product->id;
            }

            $products       = array_diff($products_array1, $products_array);
            $total_products = count($products);

        } else {

            $product_collection = $ses->getWriteProductsList($copyses->id);
            $total_products     = count($product_collection);
        }

        $channel = '';
        if (!empty($_REQUEST['channel'])) {
            $channel = $_REQUEST['channel'];
        }

        $product_completed       = $ses->getCompleteCopyProducts('DESC', $channel, $copyses->id);
        $total_complete_products = count($product_completed);

        return response()->json([
            'total_products'          => $total_products,
            'total_complete_products' => $total_complete_products,
        ]);
    }

    public function ExportCopyOld($id)
    {

        $ids = DB::table('copy_revisions')
            ->select(db::raw('max(id) as id'))
            ->where('copywritingsession_id', $id)
            ->groupBy('product_id')
            ->pluck('id');

        $items = DB::table('copy_revisions')
            ->join('products', 'copy_revisions.product_id', '=', 'products.id')
            ->select(['products.identifier', 'products.channel', 'copy_revisions.id'])
            ->whereIn('copy_revisions.id', $ids)
            ->get();

        $ch_array     = [];
        $ch_val_array = [];
        $i            = 0;
        foreach ($items as $payment) {
            //copy header
            $product_channel = Ounass::leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
                ->select('ounass.Channel', 'ounass.Item_Parent', 'ounass.GAP_Color_Code', 'ounass.ATG_Color_Code', 'ounass.VPN', 'simple_category.category')
                ->where('channel', $payment->channel)
                ->where(function ($query) use ($payment) {
                    $query->where('Item', $payment->identifier);
                    $query->orWhere('Barcode', $payment->identifier);
                })->first();

            $copy_header   = '';
            $latest_ch_val = '';
            //checking copy data section
            if (!empty($product_channel->Channel) && !empty($product_channel->category)) {
                $copy_templates = CopyTemplates::where(['channel' => $product_channel->Channel, 'simple_category' => $product_channel->category])->first();
                if (!empty($copy_templates->id)) {
                    $copy_header = CopyHeader::where(['copy_template_id' => $copy_templates->id])->orderBy('weight', 'ASC')->get();
                }
                if (!empty($payment->id)) {
                    $latest_ch_val = CopyHeaderValue::where('copy_revision_id', $payment->id)->orderBy('id', 'ASC')->get();
                }
            }

            $val = array();
            if (!empty($latest_ch_val)) {
                foreach ($latest_ch_val as $row) {
                    $val[$row->header_id] = $row->value;
                }
            }

            if (!empty($copy_header)) {
                $header_arr = array();
                foreach ($copy_header as $ch) {
                    $header_arr[] = ucwords(str_replace("_", " ", $ch->header_name));
                }
                $ch_array[0] = $header_arr;

                foreach ($copy_header as $ch) {
                    if (strtolower($ch->header_name) == "sku") {$ch_val_arr['sku'] = $payment->identifier;} else if (!empty($val[$ch->id])) {
                        /*
                        $value_to_send = str_replace("<ul>","",$val[$ch->id]);
                        $value_to_send = str_replace("</ul>","",$value_to_send);
                        $value_to_send = str_replace("<li>","<p>•",$value_to_send);
                        $value_to_send = str_replace("</li>","</p>",$value_to_send);
                        $value_to_send = str_replace("\n","",$value_to_send);
                         */
                        $value_to_send = strip_tags($val[$ch->id]);

                        $final_value                  = html_entity_decode($value_to_send);
                        $ch_val_arr[$ch->header_name] = $final_value;
                    } else { $ch_val_arr[$ch->header_name] = null;}
                }
                $i++;
                $ch_array[$i] = $ch_val_arr;
            }

        }

        $file_name_local = 'CopySession' . $id . '-Export';

        \Excel::create($file_name_local, function ($excel) use ($ch_array) {
            $excel->sheet('ExportFile', function ($sheet) use ($ch_array) {
                $sheet->fromArray($ch_array, null, 'A1', false, false);
            });
        })->store('xlsx', public_path('/exports'));

        // $headers = array(
        //   'Content-Type' => 'text/csv',
        // );

        ob_end_clean();
        $file_name = 'CopySession' . $id . '-Export.xlsx';
        return response()->download(public_path('/exports/' . $file_name), $file_name);
    }

    public function ExportCopy($id, $channel = 'Ounass')
    {

        $copyses = $this->copy->find($id);

        $ids = DB::table('copy_revisions')
            ->join('products', 'copy_revisions.product_id', '=', 'products.id')
            ->select(db::raw('max(copy_revisions.id) as crid'))
            ->where('products.channel', $channel)
            ->where('copy_revisions.copywritingsession_id', $id)
            ->groupBy('product_id')
            ->pluck('crid');

        $items = DB::table('copy_revisions')
            ->leftJoin('products', 'copy_revisions.product_id', '=', 'products.id')
            ->leftJoin('ounass', function ($join) {
                $join->on('products.identifier', '=', 'ounass.Item');
                $join->on('products.channel', '=', 'ounass.Channel');
            })
            ->leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->leftJoin('copy_templates', function ($join) {
                $join->on('copy_templates.channel', '=', 'ounass.Channel');
                $join->on('copy_templates.simple_category', '=', 'simple_category.category');
            })
            ->select(['products.identifier', 'products.id as product_id', 'products.channel', 'copy_revisions.id', 'simple_category.category', 'copy_templates.id as copy_template_id'])
            ->where('products.channel', $channel)
            ->whereIn('copy_revisions.id', $ids)
            ->get();

        $ch_array     = [];
        $ch_val_array = [];
        $i            = 0;
        $copy_header  = '';
        $val          = [];
        $sku_arr      = [];
        foreach ($items as $payment) {
            if (!empty($payment->copy_template_id)) {
                $copy_template_id[$payment->copy_template_id] = $payment->copy_template_id;
            }

            $sku_arr[$payment->product_id] = $payment->identifier;
        }

        $latest_ch_val = CopyHeaderValue::whereIn('copy_revision_id', $ids)->where('header_id', '!=', 0)->orderBy('product_id', 'DESC')->get();

        $copy_header = CopyHeader::whereIn('copy_template_id', $copy_template_id)->orderBy('weight', 'ASC')->get();

        $header_arr   = [];
        $header_names = [];
        if (!empty($copy_header)) {
            foreach ($copy_header as $ch) {
                //$header_arr[] = ucwords(str_replace("_", " ", $ch->header_name));
                $header_names[$ch->id] = ucwords(str_replace("_", " ", $ch->header_name));
            }

            if (!empty($latest_ch_val)) {
                foreach ($latest_ch_val as $row) {
                    $val[$row->product_id]['Sku'] = $sku_arr[$row->product_id];
                    if (!empty($header_names[$row->header_id])) {
                        $val[$row->product_id][$header_names[$row->header_id]] = $row->value;
                    }
                }
            }
        }

        //dd($val);

        $keys = [];

        foreach ($val as $productid) {
            $keys = array_unique(array_merge($keys, array_keys($productid)));

        }

        $userData = '<table border="1"><tr>';

        foreach ($keys as $tr) {

            $userData .= '<td>' . ucwords(str_replace("_", " ", $tr)) . '</td>';
            $header_arr[] = ucwords(str_replace("_", " ", $tr));

        }
        $ch_array[0] = $header_arr;

        $userData .= '</tr>';
        foreach ($val as $productid) {
            $userData .= '<tr>';

            foreach ($keys as $tr) {
                if (isset($productid[$tr])) {
                    $output_html = '';

                    $output_html = html_entity_decode($productid[$tr]);

                    $output        = strip_tags($output_html, '<ul><li><p><br>');
                    $value_to_send = str_replace("<ul>", "", $output);
                    $value_to_send = str_replace("</ul>", "", $value_to_send);
                    $value_to_send = str_replace("<li>", "<p>• ", $value_to_send);
                    $value_to_send = str_replace("</li>", "</p>", $value_to_send);
                    $value_to_send = str_replace("<p>&bull;", "<p>• ", $value_to_send);

                    $value_to_send = str_replace("\r\n", "", $value_to_send);
                    $value_to_send = str_replace("\n", "", $value_to_send);
                    $value_to_send = str_replace("&nbsp;", " ", $value_to_send);
                    $value_to_send = preg_replace("/<([^\s>]+)[^>]+\/>/i", "<\\1 />", $value_to_send);
                    $value_to_send = preg_replace("@<([^\s>]+)[^>]+[^\/]>@si", "<\\1>", $value_to_send);

                    $value_to_send = str_replace("<p><p>•", "<p>• ", $value_to_send);
                    $value_to_send = str_replace("</p></p>", "</p>", $value_to_send);
                    $value_to_send = str_replace("</p><p>", "</p>\n<p>", $value_to_send);
                    $value_to_send = str_replace(["<br />", "<br/>", "<br>"], "</p>\n<p>", $value_to_send);

                    $final_value = html_entity_decode($value_to_send);
                    $final_value = strip_tags($final_value, '<p>');
                    $final_value = trim($final_value);

                    $userData .= '<td>' . $final_value . '</td>';
                    $ch_val_arr[$tr] = $final_value;
                } else {
                    $userData .= '<td> </td>';
                    $ch_val_arr[$tr] = '';
                }
            }
            $i++;
            $ch_array[$i] = $ch_val_arr;
            $userData .= '</tr>';

        }

        $userData .= '</table>';

        ob_end_clean();
        //return view('excel', compact('userData'));
        //return $ch_array;

        $file_name_local = 'CopySession' . '-TRANSLATION-' . '-Export';

        Excel::create($file_name_local, function ($excel) use ($ch_array) {
            $excel->sheet('ExportFile', function ($sheet) use ($ch_array) {
                $sheet->fromArray($ch_array, null, 'A1', false, false);
            });
        })->store('xlsx', public_path('/exports'));

        //ob_end_clean();
        $file_name = 'CopySession' . '-TRANSLATION-' . '-Export.xlsx';
        return response()->download(public_path('/exports/' . $file_name), $file_name);

    }

    public function ExportCopyTestView()
    {
        return view('ounass.exportcopy');
    }

    public function ExportCopyTest(Request $request)
    {
        $in_ids = explode("\n", str_replace("\r", "", $request->ids));
        $ids    = DB::table('copy_revisions')
            ->leftJoin('products', 'products.id', '=', 'copy_revisions.product_id')
            ->select(db::raw('max(copy_revisions.id) as id'))
            ->whereIn('products.identifier', $in_ids)
            ->groupBy('product_id')
            ->pluck('copy_revisions.id');

        $items = DB::table('copy_revisions')
            ->leftJoin('products', 'copy_revisions.product_id', '=', 'products.id')
            ->leftJoin('ounass', function ($join) {
                $join->on('products.identifier', '=', 'ounass.Item');
                $join->on('products.channel', '=', 'ounass.Channel');
            })
            ->leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->leftJoin('copy_templates', function ($join) {
                $join->on('copy_templates.channel', '=', 'ounass.Channel');
                $join->on('copy_templates.simple_category', '=', 'simple_category.category');
            })
            ->select(['products.identifier', 'products.id as product_id', 'products.channel', 'copy_revisions.id', 'simple_category.category', 'copy_templates.id as copy_template_id'])
            ->whereIn('copy_revisions.id', $ids)
            ->get();

        $ch_array         = [];
        $ch_val_array     = [];
        $i                = 0;
        $copy_header      = '';
        $val              = [];
        $sku_arr          = [];
        $copy_template_id = [];
        foreach ($items as $payment) {
            if (!empty($payment->copy_template_id)) {
                $copy_template_id[$payment->copy_template_id] = $payment->copy_template_id;
            }

            $sku_arr[$payment->product_id] = $payment->identifier;
        }

        $latest_ch_val = CopyHeaderValue::whereIn('copy_revision_id', $ids)
            ->where('header_id', '!=', 0)
            ->orderBy('product_id', 'DESC')
            ->get();

        $copy_header = CopyHeader::whereIn('copy_template_id', $copy_template_id)->orderBy('weight', 'ASC')->get();

        $header_arr   = [];
        $header_names = [];
        if (!empty($copy_header)) {
            foreach ($copy_header as $ch) {
                //$header_arr[] = ucwords(str_replace("_", " ", $ch->header_name));
                $header_names[$ch->id] = ucwords(str_replace("_", " ", $ch->header_name));
            }

            if (!empty($latest_ch_val)) {
                foreach ($latest_ch_val as $row) {
                    $val[$row->product_id]['Sku'] = $sku_arr[$row->product_id];
                    if (!empty($header_names[$row->header_id])) {
                        $val[$row->product_id][$header_names[$row->header_id]] = $row->value;
                    }
                }
            }
        }

        //dd($val);

        $keys = [];

        foreach ($val as $productid) {
            $keys = array_unique(array_merge($keys, array_keys($productid)));

        }

        $userData = '<table border="1"><tr>';

        foreach ($keys as $tr) {

            $userData .= '<td>' . ucwords(str_replace("_", " ", $tr)) . '</td>';
            $header_arr[] = ucwords(str_replace("_", " ", $tr));

        }
        $ch_array[0] = $header_arr;

        $userData .= '</tr>';
        foreach ($val as $productid) {
            $userData .= '<tr>';

            foreach ($keys as $tr) {
                if (isset($productid[$tr])) {
                    $output_html = '';

                    $output_html = html_entity_decode($productid[$tr]);

                    $output        = strip_tags($output_html, '<ul><li><p><br>');
                    $value_to_send = str_replace("<ul>", "", $output);
                    $value_to_send = str_replace("</ul>", "", $value_to_send);
                    $value_to_send = str_replace("<li>", "<p>• ", $value_to_send);
                    $value_to_send = str_replace("</li>", "</p>", $value_to_send);
                    $value_to_send = str_replace("<p>&bull;", "<p>• ", $value_to_send);

                    $value_to_send = str_replace("\r\n", "", $value_to_send);
                    $value_to_send = str_replace("\n", "", $value_to_send);
                    $value_to_send = str_replace("&nbsp;", " ", $value_to_send);
                    $value_to_send = preg_replace("/<([^\s>]+)[^>]+\/>/i", "<\\1 />", $value_to_send);
                    $value_to_send = preg_replace("@<([^\s>]+)[^>]+[^\/]>@si", "<\\1>", $value_to_send);

                    $value_to_send = str_replace("<p><p>•", "<p>• ", $value_to_send);
                    $value_to_send = str_replace("</p></p>", "</p>", $value_to_send);
                    $value_to_send = str_replace("</p><p>", "</p>\n<p>", $value_to_send);

                    $value_to_send = str_replace(["<br />", "<br/>", "<br>"], "\n", $value_to_send);

                    $final_value = html_entity_decode($value_to_send);
                    $final_value = strip_tags($final_value, '<p>');
                    $final_value = trim($final_value);

                    $userData .= '<td>' . $final_value . '</td>';
                    $ch_val_arr[$tr] = $final_value;
                } else {
                    $userData .= '<td> </td>';
                    $ch_val_arr[$tr] = '';
                }
            }
            $i++;
            $ch_array[$i] = $ch_val_arr;
            $userData .= '</tr>';

        }

        $userData .= '</table>';

        ob_end_clean();
        //return view('excel', compact('userData'));
        //return $ch_array;

        $file_name_local = 'CopySession' . '-TRANSLATION-' . '-Export';

        Excel::create($file_name_local, function ($excel) use ($ch_array) {
            $excel->sheet('ExportFile', function ($sheet) use ($ch_array) {
                $sheet->fromArray($ch_array, null, 'A1', false, false);
            });
        })->store('xlsx', public_path('/exports'));

        //ob_end_clean();
        $file_name = 'CopySession' . '-TRANSLATION-' . '-Export.xlsx';
        return response()->download(public_path('/exports/' . $file_name), $file_name);

    }

// Move All these to CopyTemplateController

    public function ViewAllHeaders($copy_template_id = "")
    {
        $template_data    = '';
        $copy_header_data = '';
        if (!empty($copy_template_id)) {
            $template_data    = CopyTemplates::where('id', $copy_template_id)->first();
            $copy_header_data = CopyHeader::where('copy_template_id', $copy_template_id)->orderBy('weight', 'ASC')->get();
        }
        $template_all = CopyTemplates::get();

        $all_header = CopyHeader::select('header_name')->groupBy('header_name')->orderBy('header_name', 'ASC')->get();

        if (empty($copy_template_id)) {
            $copy_header_data = CopyHeader::orderBy('id', 'asc')->get();
        }

        return view('copywriting.headers.list', compact('copy_template_id', 'template_data', 'template_all', 'all_header', 'copy_header_data'));
    }

    public function ViewAllHeadersChannels()
    {
        $header_data = DB::table('copy_templates')
            ->leftjoin('copy_headers', 'copy_headers.copy_template_id', '=', 'copy_templates.id')
            ->select(DB::raw('count( copy_headers.id) as total_row, count(CASE WHEN copy_headers.is_required = 1 THEN 1 END ) as required_headers, count(CASE WHEN copy_headers.is_required = 0 THEN 1 END) as extra_headers, copy_templates.template_name, copy_templates.simple_category, copy_templates.id as copy_template_id, copy_headers.created_at, copy_templates.channel'))
            ->groupBy('copy_headers.copy_template_id')
            ->orderBy('copy_templates.id', 'asc')
            ->get();
        $cat_data = SimpleCategory::select(\DB::raw('DISTINCT(category)'))->get();

        $channel_data = CopyTemplates::groupBy('channel')->get();

        return view('copywriting.headers.template_list', compact('header_data', 'cat_data', 'channel_data'));
    }

    public function CopyHeaderData($copy_template_id = "")
    {
        if (!empty($copy_template_id)) {
            $header_data = CopyHeader::where('copy_template_id', $copy_template_id)->orderBy('id', 'asc')->get();

        } else {
            $header_data = CopyHeader::orderBy('id', 'asc')->get();
        }

        return DataTables::of($header_data)
            ->editColumn('created_at', function (CopyHeader $copyheader) {
                if ($copyheader->created_at != null || $copyheader->created_at != '') {
                    return $copyheader->created_at->diffForHumans();
                }

            })
            ->editColumn('required', function (CopyHeader $copyheader) {
                return $copyheader->is_required == '0' ? '' : '<span class="fa fa-check text-success></span>"';
            })
            ->addColumn('DT_RowId', function (CopyHeader $copyheader) {
                return "row_" . $copyheader->id;
            })
            ->addColumn('actions', function ($copyheader) {
                $actions = '<a href="javascript:void(0);" data-href="' . route('copywriting.headers.edit', $copyheader->id) . '"  class=" btn btn-xs btn-primary editmodal" data-toggle="modal" data-target="#edit_copyheader"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                $actions .= '  <a href="javascript:void(0);" data-href="' . route('copywriting.headers.delete', $copyheader->id) . '" class=" btn btn-xs btn-danger delmodal"> <i class="glyphicon glyphicon-trash"></i> Delete</a>';

                return $actions;
            })
            ->rawColumns(['actions', 'required'])
            ->make(true);
    }

    public function CopyHeaderDel($id)
    {
        CopyHeader::findOrFail($id)->delete();
        CopyHeaderValue::where('header_id', $id)->delete();
        return redirect('copywriting/header/list');
    }

    public function CopyHeaderEdit($id)
    {
        try {
            return CopyHeader::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['id' => '0']);
        }
    }

    public function CopyHeaderStore(Request $request)
    {
        $input = $request->all();
        if (!empty($input['id'])) {
            CopyHeader::where('id', $input['id'])->update(['header_name' => $input['header_name'], 'header_type' => $input['header_type'], 'default_value' => $input['default_value'], 'is_required' => $input['is_required'], 'is_visible' => $input['is_visible'], 'weight' => $input['weight']]);
        } else {

            $data                   = new CopyHeader;
            $data->header_name      = $input['header_name'];
            $data->header_type      = $input['header_type'];
            $data->default_value    = $input['default_value'];
            $data->is_required      = $input['is_required'];
            $data->is_visible       = $input['is_visible'];
            $data->weight           = $input['weight'];
            $data->copy_template_id = $input['copy_template_id'];
            $data->save();
            return $data;
        }
    }

    public function updateOrderCopyHeader(Request $request)
    {
        $tasks = CopyHeader::all();

        foreach ($tasks as $task) {
            $task->timestamps = false; // To disable update_at field updation
            $id               = $task->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $task->update(['weight' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }

    public function CopyTemplateStore(Request $request)
    {
        $input = $request->all();
        if (!empty($input['id'])) {
            CopyTemplates::where('id', $input['id'])->update(['template_name' => $input['template_name'], 'channel' => $input['channel'], 'simple_category' => $input['category']]);
        } else {

            $templae_name_check = CopyTemplates::where('template_name', $input['template_name'])->first();
            if (!empty($templae_name_check)) {
                return response()->json(['id' => '', 'error' => 'Please enter unique template name!']);
            }

            $exists_check = CopyTemplates::where(['simple_category' => $input['category'], 'channel' => $input['channel']])->first();
            if (!empty($exists_check)) {
                return response()->json(['id' => '', 'error' => 'Please select different category and channel!']);
            }

            $data                  = new CopyTemplates;
            $data->template_name   = $input['template_name'];
            $data->simple_category = $input['category'];
            $data->channel         = $input['channel'];
            $data->save();

            return $data;
        }

    }

    public function CopyTemplateGet($id)
    {
        $copy_data = CopyTemplates::findOrFail($id);
        return $copy_data;
    }

    public function RenderScanCopyListProducts($id, $channel)
    {

        $ses     = $this->copy;
        $copyses = $ses->find($id);

        $products_array = array();

        $product_collection = $ses->getWriteProductsList($copyses->id, $channel);

        $total_products = count($product_collection);

        $html = view('copywriting.partials.PendingCopyList', [
            'result' => $product_collection,
            //'product_list' => $products_array,
            'method' => $copyses->method,
        ])->render();

        return response()->json(['status' => 'success', 'html' => $html, 'total_products' => $total_products], 200);

    }

    public function RenderCompleteCopyListProducts($id, $channel)
    {

        $ses     = $this->copy;
        $copyses = $ses->find($id);

        $products_array = array();

        $product_collection = $ses->getWriteCompleteProductsList($copyses->id, $channel);

        $total_products = count($product_collection);

        $html = view('copywriting.partials.CompleteCopyList', [
            'result'       => $product_collection,
            'product_list' => $products_array,
        ])->render();

        return response()->json(['status' => 'success', 'html' => $html, 'total_products' => $total_products], 200);

    }

    public function lockProductSpecificUser(Request $request)
    {

        if ($request->type == 'lock-edit') {
            $html = '<div class="pull-left copywritinglock">' . '<i class="fa fa-lock lock-product-' . $request->product . '"></i>' . '</div>';

            $getlock = ProductCopywritingsession::where([
                'product_id' => $request->product,
            ])->first();

            if ($getlock) {

                if ($getlock->lock_user_id == Auth::user()->id) {
                    ProductCopywritingsession::where(['lock_user_id' => Auth::user()->id])->update(['lock' => 0, 'lock_user_id' => null]);
                    ProductCopywritingsession::where(['product_id' => $request->product])->update(['lock' => 1, 'lock_user_id' => Auth::user()->id]);
                }

                event(new CopyLock($request->product, Auth::user()->id, 1, 0, 0));

            } else {

                if (isset($request->reviewtype) && $request->reviewtype == 1) {
                    $copystate = 1;
                } else {
                    $copystate = 0;
                }

                ProductCopywritingsession::Create([
                    'product_id'            => $request->product,
                    'copywritingsession_id' => $request->csession,
                    'user_id'               => Auth::User()->id,
                    'state'                 => '',
                    'copy_state'            => $copystate,
                    'batch'                 => 0,
                    'qc_state'              => 0,
                    'lock'                  => 1,
                    'lock_user_id'          => Auth::User()->id,
                ]);
            }

        } elseif ($request->type == 'discard') {
            $html = 'discard';

            ProductCopywritingsession::where([
                'product_id' => $request->product,
            ])->update([
                'lock'         => 0,
                'lock_user_id' => null,
            ]);
            event(new CopyLock($request->product, Auth::user()->id, 0, 0, 0));
        } elseif ($request->type == 'drop') {
            $html = 'drop';

            ProductCopywritingsession::where([
                'product_id' => $request->product,
            ])->update([
                'lock'         => 0,
                'lock_user_id' => null,
                'is_dropped'   => 1,
            ]);
        }
        event(new CopyLock($request->product, Auth::user()->id, 0, 0, 0));

        return response()->json(['status' => 'success', 'html' => $html], 200);

    }

    public function GetParentCopyProducts($id)
    {
        $prod_data = $this->copy->GetParentCopyProducts($id);
        return response()->json(['status' => 'success', 'copy_html' => $prod_data], 200);
    }
}
