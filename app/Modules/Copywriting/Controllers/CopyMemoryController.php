<?php

namespace App\Http\Controllers\Copywriting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ounass\CopyMemory;

class CopyMemoryController extends Controller
{
  function loadFromMemory(Request $request){
    if($request->has('memory_type')){
      $memory_items = CopyMemory::where('memory_type', $request->memory_type)->get();

      $html = view('partials.modals.addCopyMemory', ['memory_items' => $memory_items, 'target_container' => $request->memory_type])->render();
      return response()->json(['status'=>'success', 'html'=>$html], 200);

    }
  }
}
