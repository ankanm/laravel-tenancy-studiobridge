<?php

Route::group(['module' => 'Ounass', 'middleware' => ['api'], 'namespace' => 'App\Modules\Ounass\Controllers'], function() {

    Route::resource('ounass', 'OunassController');

});
