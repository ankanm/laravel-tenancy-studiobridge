<?php

Route::group(['module' => 'Ounass', 'middleware' => ['web'], 'namespace' => 'App\Modules\Ounass\Controllers'], function() {

    Route::resource('ounass', 'OunassController');

});
