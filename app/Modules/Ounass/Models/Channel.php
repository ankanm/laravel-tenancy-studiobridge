<?php

namespace App\Modules\Ounass\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Channel extends Model
{
	use UsesTenantConnection;
	
    protected $table = 'channel';

    public function products()
    {
      return $this->hasMany('App\Modules\Product\Models\Product', 'id_channel', 'id_channel');
    }

    public function productsCount()
    {
      return $this->hasMany('App\Modules\Product\Models\Product', 'id_channel', 'id_channel')->count();
    }
}
