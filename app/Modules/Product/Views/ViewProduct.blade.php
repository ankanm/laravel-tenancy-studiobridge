@extends('layouts.body')


@section('main-content')
	{{ csrf_field() }}
	<link rel="stylesheet" href="{{ URL::asset('css/snippets.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/htimeline.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('assets/global/plugins/hover-effects/hover-effects.min.css') }}">
	<!--Page Container-->
	<div class="page-content">
		<!--Page Header-->
		<div class="header">
			<h2>View <strong>Product</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="#">Home</a>
					</li>
					<li><a href="#">Products</a>
					</li>
					<li class="active">View Product</li>
				</ol>
			</div>
		</div>
		<!--/Page Header-->

		<div class="">
			<div class="panel panel-flat">
				<div class="panel-body p-5 is-table-row">
					{{-- <div class="col-sm-2 text-center" style="width:160px;">
					<img src="https://www.julesb.ae/images/eton-classic-fit-shirt-p783028-2061243_medium.jpg" alt="" width="150px">
				</div> --}}
				<div class="col-sm-10">
					<div class=" col-sm-12">

						<h1 class="c-dark">{{$product->identifier}}</h1>
					</div>
					<div class="clearfix col-xs-12 col-sm-6">
						<p class="c-gray f-16  m-t-10 m-b-5 {{($product->type == 'mapped' ? 'text-success' : 'text-amber-dark')}}">This product is {{ucwords($product->type)}}</p>
						<p class="c-gray"><i class="fa fa-calendar p-r-10"></i> created 18 May, 2017 12:33 pm</p>
					</div>
				</div>

			</div>

		</div>
		<div class="tabbable">
			<ul class="nav nav-tabs ">
				<li class="active"><a href="#sessions" data-toggle="tab" aria-expanded="false">Images</a></li>
				<li><a href="#copy" data-toggle="tab" aria-expanded="false">Copy</a></li>
				{{-- <li ><a href="#" data-toggle="tab" aria-expanded="false">ASN</a></li>
				<li ><a href="#" data-toggle="tab" aria-expanded="false">Copywriting</a></li> --}}
			</ul>

			<div class="tab-content">
				<div class="tab-pane" id="copy">

					@php
					if($product_revisions && count($product_revisions) > 0){
						$product_id =  $product_revisions->product_id;
						$product_title = $product_revisions->product_title;
						$product_description = $product_revisions->product_description;
						$product_additionalinfo = $product_revisions->additional_details;
						$product_careinstructions = $product_revisions->care_instructions;
						$product_key_details = $product_revisions->key_details;
						$product_ingredients = $product_revisions->ingredients;
						$product_size_fit = $product_revisions->size_and_fit;
						$product_dimensions = $product_revisions->dimensions;
						$product_frame_type = $product_revisions->frame_type;
						$product_lens_type = $product_revisions->lens_type;
						$product_100ml_restriction = $product_revisions->hundredml_restriction;
						$product_hair_concern = $product_revisions->hair_concern;
						$product_heel_height = $product_revisions->heel_height;
						$product_heel_style = $product_revisions->heel_style;
						$product_metal_stone = $product_revisions->metal_stone;
						$product_skin_concern = $product_revisions->skin_concern;
					}
					@endphp
					@if($product_revisions && count($product_revisions) > 0)
						<div class="panel panel-body p-5">
							<div class="col-sm-6">
								<h5 class="media-heading text-navy text-bold m-t-5">Copy Session {{$product_revisions->copywritingsession_id}}</h5>
								Copywriter: <span class="bold">{{Auth::user($product_revisions->user_id)->name}}</span>
								<br>
								Modified Date: <span class="bold">{{$product_revisions->updated_at}}</span>
							</div>
						</div>
						<div class="col-sm-12">
							<form class="form-horizontal" >
								<div class="form-group">
									<label class="control-label col-sm-2">Title</label>
									<div class="col-sm-10">
										<h4 class="bold">{{$product_title}}</h4>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2">Description</label>
									<div class="col-sm-10">
										<h4 class="bold">{!! $product_description !!}</h4>
									</div>

								</div>
								<div class="form-group">
									<label class="control-label col-sm-2">Design Details</label>
									<div class="col-sm-10">
										<h4 class="bold">{!! $product_additionalinfo !!}</h4>
										{{-- <input type="text" class="form-control input-md additionalinfo" value="{{$product_additionalinfo}}" placeholder="Design Details"> --}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2">Size and Fit</label>
									<div class="col-sm-10">
										<h4 class="bold">{!! $product_size_fit !!}</h4>
										{{-- <input type="text" class="form-control input-md size_and_fit" value="{{$product_size_fit}}" placeholder="Size and Fit"> --}}
									</div>

								</div>
								<div class="form-group">
									<label class="control-label col-sm-2">Key Details</label>
									<div class="col-sm-10">
										<h4 class="bold">{!! $product_key_details !!}</h4>
										{{-- <input type="text" class="form-control input-md key_details" value="{{$product_key_details}}" placeholder="Key Details"> --}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2">Ingredients</label>
									<div class="col-sm-10">
										<h4 class="bold">{!! $product_ingredients !!}</h4>
										{{-- <input type="text" class="form-control input-md ingredients" value="{{$product_ingredients}}" placeholder="Ingredients"> --}}
									</div>

								</div>
								<div class="form-group">
									<label class="control-label col-sm-2">Dimensions</label>
									<div class="col-sm-10">
										<h4 class="bold">{!! $product_dimensions !!}</h4>
										{{-- <input type="text" class="form-control input-md dimensions" value="{{$product_dimensions}}" placeholder="Dimensions"> --}}
									</div>

								</div>
								<div class="form-group">
									<label class="control-label col-sm-2">Frame Type</label>
									<div class="col-sm-10">
										<h4 class="bold"><p>{!! $product_frame_type !!}</p></h4>
										</div>

									</div>

									<div class="form-group">
										<label class="control-label col-sm-2">Lens Type</label>
										<div class="col-sm-10">
											<h4 class="bold"><p>{!! $product_lens_type !!}</p></h4>
										</div>

									</div>

									<div class="form-group">
										<label class="control-label col-sm-2">100 ml Restriction</label>
										<div class="col-sm-10">
											<h4 class="bold"><p>{!! $product_100ml_restriction !!}</p></h4>
										</div>

									</div>

									<div class="form-group">
										<label class="control-label col-sm-2">Hair Concern</label>
										<div class="col-sm-10">
											<h4 class="bold"><p>{!! $product_hair_concern !!}</p></h4>
										</div>

									</div>
									<div class="form-group">
										<label class="control-label col-sm-2">Heel Height</label>
										<div class="col-sm-10">
											<h4 class="bold"><p>{!! $product_heel_height !!}</p></h4>
										</div>

									</div>

									<div class="form-group">
										<label class="control-label col-sm-2">Heel Style</label>
										<div class="col-sm-10">
											<h4 class="bold"><p>{!! $product_heel_style !!}</p></h4>
										</div>

									</div>

									<div class="form-group">
										<label class="control-label col-sm-2">Metal Stone</label>
										<div class="col-sm-10">
											<h4 class="bold"><p>{!! $product_metal_stone !!}</p></h4>
										</div>

									</div>

									<div class="form-group">
										<label class="control-label col-sm-2">Skin Concern</label>
										<div class="col-sm-10">
											<h4 class="bold"><p>{!! $product_skin_concern !!}</p></h4>
										</div>

									</div>
								</form>
						</div>

						@else
							<div id="quote-liveshoot" class="text-center text-black" style="min-height: 100%; padding-top: 150px; font-size: 25px; line-height: 50px; max-width: 400px; margin: auto;">
                  <i class="fa fa-pencil fa-4x text-grey-lighter"></i>
                  <br>
                  <h2 class="text-bold">This product has no copy</h2>
                  <h5>The latest revision of the copy will appear here once it is written</h5>
                </div>
						@endif

					</div>
					<div class="tab-pane active" id="sessions">
						<div class="tab_left">
							<ul class="nav nav-tabs nav-blue">


								@foreach($product_information as $product)
									@php
									$liclass = ($current_session == $product['session']->id ? 'active' : '');
									@endphp
									<li class="{{$liclass}}"><a href="#Session-{{$product['session']->id}}" data-toggle="tab" aria-expanded="false">Session {{$product['session']->id}}</a></li>
								@endforeach
							</ul>
							<div class="tab-content p-l-15">
								@foreach($product_information as $product)

									@php
									$liclass = ($current_session == $product['session']->id ? 'active' : '');
									@endphp
									<div class="tab-pane {{$liclass}}" id="Session-{{$product['session']->id}}">
										<div class="panel panel-body p-5">
											<div class="col-sm-6">
												<h5 class="media-heading text-navy text-bold m-t-5">Session {{$product['session']->id}}</h5>
												Session Date: <span class="bold">{{$product['session']->created_at}}</span>
											</div>
											<div class="col-sm-6 text-right">
												<a href="/psession/{{$product['session']->id}}" class="ripple btn btn-default bg-blue m-t-5">View Session</a>
											</div>
										</div>
										<div class=" no-margin-bottom no-border-bottom">

											<div id="imagecontainer-wrapper">
												@foreach($product['images'] as $key => $image)

													@php
													$naming_matrix = config('client.image_naming');

													//print_r($naming_matrix);
													$sequence_find = array_key_exists($key+1, $naming_matrix);
													if($sequence_find != null){
														$sequence = $naming_matrix[$key+1];
													} else{
														$sequence = $key+1;
													}
													@endphp

													<div class="bulkviewfiles ui-sortable-handle" id="warpper-img-{{$image->thumbpath}}">
														<div class="scancontainer">
															<div class="hovereffect">
																<div class="shadow">
																	<img src="/{{$image->thumbpath}}" class="scanpicture" data-imageid="{{$image->thumbpath}}" id="img-{{$image->id}}"></div>
																	<div class="overlay">

																	</div>
																</div>
																<div class="file-name">
																	<div id="tag-seq-img-{{$image->thumbpath}}" type="hidden">
																	</div>
																	<div class="row">
																		<div class="col col-sm-12">
																			<span id="{{$image->thumbpath}}">
																				<a class=" col-sm-4"><span class="sequence-number"><span id="seq-{{$image->thumbpath}}">{{$sequence}}</span></span>
																				</a>
																				<a class="col-sm-4 text-center" href="/image/view/{{$image->id}}" target="_blank"><i class="icon-magnifier image-ops"></i>
																				</a>

																			</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="studio-img-weight">
																<input type="hidden" value="{{$image->thumbpath}}"></div>
															</div>

														@endforeach
													</div>
												</div>
												@foreach($comments as $comment)
													@if($comment->productcomment->psession_id == $product['session']->id)
														<div class="row p-10">
															<div class="col-sm-2">
																<div class="thumbnail p-0">
																	<img class="img-responsive user-photo initials-profile comment-thumbnail" src="{{$comment->user->avatar}}" >
																	<br>
																	<span class="text-center block fs-12 " style="width:60%; margin:auto">{{$comment->user->name}}</span>
																</div><!-- /thumbnail -->
															</div><!-- /col-sm-1 -->

															<div class="col-sm-4 p-l-0">
																<div class="panel panel-default">
																	<div class="panel-heading" style="background-color: #fff !important;">
																		<span class="text-muted">added {{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</span>
																		<span class="p-t-10">{!! $comment->body !!}</span>
																	</div>

																	@php
																	$qc_operation_ids = App\Modules\Operation\Models\CommentQcOperation::
																	where('comment_id', $comment->id)
																	->limit(1)
																	->get(['qc_operation_id']);

																	foreach ($qc_operation_ids as $qc_operation_id) {
																		if($qc_operation_id != '' || $qc_operation_id != null){
																			$qc_ops = App\Models\QcOperation::where('id', $qc_operation_id->qc_operation_id)->get();
																			echo '<div class="panel-body" style="background-color:#f8f8fa;">';
																			if($qc_ops[0]->status == 1){
																				echo "QC Operation: " . "<span class='text-bold text-success'>Approved</span>
																				<br>
																				Session ID:" . $qc_ops[0]->psession_id;
																			} else{
																				echo "QC Operation: " . "<span class='text-bold text-danger'>Rejected</span>";
																			}
																			echo '</div>';
																		}
																	}

																	@endphp
																</div><!-- /panel panel-default -->
															</div><!-- /col-sm-5 -->
														</div>
													@endif
												@endforeach
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!--/Page Container-->

		@stop
