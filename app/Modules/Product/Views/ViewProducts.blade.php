@extends('layouts.body')

@section('main-content')
	<!--Page Container-->
	<div class="page-content">
		<!--Page Header-->
		<div class="header">
			@php
			$page_name = explode(" ", $screen_name)
			@endphp
			<h2>{{$page_name[0]}} <strong>{{$page_name[1]}}</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="#">Home</a>
					</li>
					<li><a href="#">Products</a>
					</li>
					<li class="active">{{$screen_name}}</li>
				</ol>
			</div>
		</div>
		<!--/Page Header-->


		<div class="">
			<div class="panel panel-flat">
				<table class="table datatable datatable-column-search-inputs">
					<thead>
						<tr>
							<th>Product ID</th>
							<th>Product</th>
							<th>Status</th>
							<th>Category</th>
							<th>QC Status</th>
							<th>Sessions</th>
							<th>Last User</th>
							<th class="text-center"></th>
						</tr>
					</thead>
					<tbody>

						@foreach ($products as $product)

							@php

							$statetext = '';
							$stateclass = '';
							$latest_session = '';
							$latest_user = '';

							//always use the last session state if its set.
							//reponse will be sorted by id so take the first one always
							if(count($product->states) >= 1){
								$latest_session = $product->states[0];
							}

							$state = -1;
							if(is_object($latest_session)){

								//Check the status of the product

								//$qc_done = $latest_session->qc_done;
								$qc_done = (isset($latest_session->qc_done) ? $latest_session->qc_done : '');
								$qc_state = (isset($latest_session->qc_state) ? $latest_session->qc_state : '');
								$latest_psession = (isset($latest_session->psession_id) ? $latest_session->psession_id : '');
								// $latest_psession = $latest_session->psession_id;
								$latest_user = $product->psessions[0]->user_id;

								if(isset($latest_user)){
									$user = App\Modules\User\Models\User::where('id', $latest_user)->first();
									if($user){
										$user_name = $user->name;
									} else {
										$user_name = '';
									}
								} else {
									continue;
								}

								if($qc_done == 0){
									$statetext = 'Pending';
									$stateclass = 'text-amber-darker';
								} else{
									if($qc_state == 0){
										$statetext = 'Rejected';
										$stateclass = 'text-danger';
									} else{
										$statetext = 'Approved';
										$stateclass = 'text-success';
									}

								}
							} else {
								continue;
							}

							if($screen_name == 'Rejected Products' && $statetext != 'Rejected'){ continue;}


							@endphp

							<tr>
								<td>{{$product->id}}</td>
								<td>
									<h6 class="no-margin">
										<span class="text-bold " href="">{{ $product->identifier }}</span>
										<small class="display-block text-muted" style="text-overflow: ellipsis;">{{($product->brand_name ? $product->brand_name : 'No brand') }}</small>
									</h6>
								</td>
								<td>
									<span class="{{ ($product->type == 'mapped' ? 'label label-success' : 'label bg-amber-darker') }} " href="">{{ ucwords($product->type) }}</span>
								</td>
								<td>
									{{ ($product->category ? $product->category : 'No Category') }}
								</td>
								<td>
									<span class="text-bold {{$stateclass}}">{{ $statetext }}</span>
								</td>
								<td>
									{{ count($product->psessions)  }}
								</td>

								<td>
									<h6 class="no-margin">
										<span class="text-bold " href="">{{ $user_name }}</span>
										<small class="display-block text-muted">Session {{$product->psessions[0]->id}}</small>
									</h6>
								</td>
								<td class="text-center">
									<ul class="icons-list">
										<li><a href="/product/{{ $latest_session->psession_id }}/{{ $product->id }}" ><i class="icon-eye"></i></a></li>
									</ul>
								</td>
							</tr>

						@endforeach


					</tbody>
				</table>
			</div>
		</div>


	</div>
	<!--/Page Container-->
@stop


@section('page-scripts')

	<!-- Page scripts -->
	<script src="{{ URL::asset('js/pnotify.min.js') }}"></script>
	<script src="{{ URL::asset('js/pages/extension_pnotify.js') }}"></script>

	<script src="{{ URL::asset('js/tables/datatables/datatables.min.js') }}"></script>
	<script src="{{ URL::asset('js/pages/datatable_basic.js') }}"></script>
	<!-- /page scripts -->

@stop
