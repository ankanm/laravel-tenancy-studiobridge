<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Hyn\Tenancy\Traits\UsesTenantConnection;
use Laravel\Scout\Searchable;
use Carbon\Carbon;

class Product extends Model {

    use Searchable, UsesTenantConnection;

    public function searchableAs(){
    	return 'Products';
  	}

	public function psessions(){
		return $this->belongsToMany('App\Modules\Psession\Models\Psession');
	}

	public function images(){
		return $this->hasMany('App\Models\Common\ImageTransfer');
	}

	public function asnboxes(){
		return $this->hasMany('App\Modules\Operations\Models\ASNBoxes');
	}

	public function channel(){
		return $this->belongsTo('App\Models\Ounass\Channel', 'id_channel', 'id_channel');
	}

	public function comments(){
		return $this->hasMany('App\Modules\Comment\Models\Comment')
	->join('comments', 'comments.id', '=', 'comment_product.comment_id');
	}

  	public function productcomment(){
	    return $this->hasMany('App\Modules\Product\Models\ProductComment')
	    ->join('comments', 'comments.id', '=', 'comment_product.comment_id')
	    ->join('users', 'users.id', '=', 'comments.user_id')
	    ->orderBy('comments.id', 'DESC');
  	}

  	public function states(){
    	return $this->hasMany('App\Modules\ProductState\Models\ProductState');
  	}

  	public function getProductInfoPD($sid, $pid){

    	return DB::table('product_states')
			->leftJoin('products', function($join){
			$join->on('products.id', '=', 'product_states.product_id');
			})
			->leftJoin('comment_product', 'products.id', '=', 'comment_product.product_id')
			->leftJoin('image_transfers', function($join) {
			$join->on('products.id', '=', 'image_transfers.product_id');
			$join->on('product_states.psession_id', '=', 'image_transfers.psession_id');
			})
			->leftJoin('images as img1', function($join) {
			$join->on('img1.id', '=', 'image_transfers.image_id')
			->whereNull('img1.deleted_at');
			})
			->leftJoin('images as img2', function($join) {
			$join->on('img2.id', '=', 'image_transfers.image_id')
			->whereNotNull('img2.deleted_at');
			})
			->select(
			DB::raw('COUNT(DISTINCT(img1.id)) as images'),
			DB::raw('COUNT(DISTINCT(img2.id)) as deleted_images'),
			DB::raw('COUNT(comment_product.id) as comments'),
			'products.id',
			'products.brand_name',
			'products.identifier',
			'products.base_product_id',
			'products.requires_extra_shot',
			'product_states.qc_done',
			'product_states.qc_state',
			'product_states.id_sequence_convention'
			)
			->where('product_states.psession_id',$sid)
			->where('product_states.product_id',$pid)
			->first();
  	}

  	public function getProductSequence($sid, $pid){

    	$match_collection = [];

	    $values =  DB::table('product_states')
	    ->leftJoin('products', function($join){
	      $join->on('products.id', '=', 'product_states.product_id');
	    })
	    ->leftJoin('psessions', 'psessions.id', '=', 'product_states.psession_id')
	    ->leftJoin('channel', 'channel.id_channel', '=', 'products.id_channel')
	    ->select(
	      'products.department',
	      'products.category',
	      'products.gender',
	      'psessions.shoot_type',
	      'channel.channel_code'
	    )
	    ->where('product_states.psession_id',$sid)
	    ->where('product_states.product_id',$pid)
	    ->first();

    	$match_collection = [strtolower($values->channel_code), strtolower($values->shoot_type), strtolower($values->gender), strtolower($values->category)];
    	return $match_collection;
  	}

  	public function getProductState($pid){
    	//Redundant Function, should be replaed with using state from Product_states
    	$psessions_count =  DB::table('product_psession')->where('product_id', $pid)->distinct()->get(['psession_id']);
    	$product_state = 'New';
	    if (count($psessions_count) > 1) {
	      	$product_state = 'Reshoot';
	    }
    	return $product_state;
  	}

  	public function getProductsCountInSession($psession){
    //Should leveraeg the COUNT function from eloquent ORM

    $products =  DB::table('product_psession')->where('psession_id', $psession)->distinct()->pluck('product_id');
    $products_count =  DB::table('product_psession')->where('psession_id', $psession)->distinct()->count();

    $unmapped_count= DB::table('products')
    ->where('type', '=', 'unmapped')
    ->whereIn('id', $products)
    ->count();

    $dropped_count =  DB::table('drop_products')
    ->where('status', '>', 0)
    ->whereIn('product_id', $products)
    ->count();


    return ['total' => $products_count, 'unmapped' => $unmapped_count, 'drop' => $dropped_count];
  }

  public function getImageCountOfProduct($pid, $sid){

    $total_count =  DB::table('image_transfers')
    ->where('product_id', $pid)
    ->where('psession_id', $sid)
    ->pluck('image_id');

    $trashed = Image::onlyTrashed()
    ->whereIn('id', $total_count)
    ->count();

    return ['total' => count($total_count), 'deleted' => $trashed];
  }

  public function calculateProductPeriodInSession($pid, $sid){

    $records = DB::table('product_psession')
    ->where('product_id', $pid)
    ->where('psession_id', $sid)
    ->orderBy('id', 'asc')
    ->get();
    $total = 0;
    foreach ($records as $record) {
      if ($record->updated_at) {
        $startTime = Carbon::parse($record->created_at);
        $finishTime = Carbon::parse($record->updated_at);
        $total += $finishTime->diffInSeconds($startTime);
      }
    }

    return $total;
  }

  public function calculateProductPeriodInAllSession($pid){

    $records = DB::table('product_psession')
    ->where('product_id', $pid)
    ->orderBy('id', 'asc')
    ->get();
    $total = 0;
    foreach ($records as $record) {
      if ($record->updated_at) {
        $startTime = Carbon::parse($record->created_at);
        $finishTime = Carbon::parse($record->updated_at);
        $total += $finishTime->diffInSeconds($startTime);
      }
    }

    return $total;
  }

  public function getProductsofSession($sid, $type, $with_dropped = 1){

    $products =  DB::table('product_psession')
    ->where('psession_id', $sid)
    ->distinct()
    ->get(['product_id']);

    $pids = [];
    foreach ($products as $pid) {
      $pids[$pid->product_id] = $pid->product_id;
    }

    unset($products);
    $dropped_pids = [];

    if (!$with_dropped) {
      $dropped_products =  DB::table('drop_products')
      ->where('psession_id', $sid)
      ->where('status', 1)
      ->distinct()
      ->get(['product_id']);

      $dropped_pids = [];
      foreach ($dropped_products as $pid) {
        $dropped_pids[] = $pid->product_id;
        if ($pids) {
          unset($pids[$pid->product_id]);
        }
      }
    }

    $products_handler = new ProductState();

    if($type =='all'){
      $products = $products_handler->where('psession_id', $sid)->wherein('product_id', $pids)
      ->with('products')
      ->orderBy('id', 'DESC')
      ->get();
    } else{
      $products = $products_handler->where('psession_id', $sid)->wherein('product_id', $pids)
      ->with('products')
      ->whereHas('products', function ($q) use ($type) {
        $q->where('type', '=', $type);
      })
      ->orderBy('id', 'DESC')
      ->get();
    }


    $block = [];

    foreach ($products as $product) {
      $image_handler = new ImageTransfer();
      $images = $image_handler->where(['psession_id' => $sid,'product_id' => $product->product_id])
      ->with('image')
      ->whereHas('image', function ($q) {
        $q->whereNull('deleted_at');
      })
      ->count();

      $thumb = $image_handler->where(['psession_id' => $sid,'product_id' => $product->product_id])
      ->with('image')
      ->whereHas('image', function ($q) {
        $q->whereNull('deleted_at');
      })
      ->first();


      $producttime = new Product;
      $time = $producttime->calculateProductPeriodInSession($product->product_id, $sid);

      $productcomments = new Comment;
      $comments = $productcomments->getComments($sid, $product->product_id);


      $block[] = ['product' => $product, 'images' => $images, 'time' => $time, 'comments' => $comments, 'thumb' => $thumb];
    }

    return $block;
  }

  public function getProductsofSession2($sid, $type, $with_dropped = 1){

    //Needs to return the product along with

    return DB::table('product_states')
      ->leftJoin('products', function($join) use ($type){
        $join->on('products.id', '=', 'product_states.product_id')
        ->where('products.type', '=', $type);
      })
      ->leftJoin('comment_product', 'products.id', '=', 'comment_product.product_id')
      ->leftJoin('channel', 'channel.id_channel', '=', 'products.id_channel')
      ->leftJoin('users', 'users.id', '=', 'product_states.model_uid')
      ->leftJoin('image_transfers', function($join) {
        $join->on('image_transfers.product_id', '=', 'products.id');
        $join->on('image_transfers.psession_id', '=', 'product_states.psession_id');
      })
      ->leftJoin('images', function($join) {
        $join->on('images.id', '=', 'image_transfers.image_id')
        ->whereNull('images.deleted_at');
      })
    ->select(
      DB::raw('MIN(images.thumbpath) as thumbpath'),
      DB::raw('COUNT(images.id) as images'),
      DB::raw('COUNT(comment_product.id) as comments'),
      'products.id',
      'products.brand_name',
      'products.identifier',
      'products.base_product_id',
      'product_states.qc_done',
      'product_states.qc_state',
      'users.avatar as model_avatar',
      'channel.channel_name as channel'
    )
    ->where('product_states.psession_id',$sid)
    ->whereNotNull('products.identifier')
    ->whereNotIn('products.id', DB::table('drop_products')->where('psession_id', $sid)->where('status', 1)->distinct()->pluck('product_id'))
    ->groupBy('products.id')
    ->orderBy('product_states.id', 'DESC')
    ->get();

  }

  public function getDroppedProductsOfSessions($sid){

    $products =  DB::table('drop_products')
    ->where('psession_id', $sid)
    ->where('status', 1)
    ->distinct()
    ->pluck('product_id');

    return DB::table('products')
    ->whereIn('id', $products)
    ->get();
  }

  public function getProductStatesofSession($sid, $state){

    $productstates = ProductState::where('psession_id', $sid)
    ->where('state', $state)
    ->get();

    return $productstates;

  }

  public function getProductStateofSession($sid, $qcstate){

    $productstates = ProductState::where('psession_id', $sid)
    ->where('qc_state', $qcstate)
    ->where('qc_done', 1)
    ->get();

    return $productstates;

  }

  public function checkProducts($type, $data){

    $product = DB::table('products')
    ->where('identifier', '=', $data['identifier'])
    ->first();

    if($type=="exception") {
      $product_asn_state = 'received exception';
      $checkin_quantity = '1';
    } else {
      $product_asn_state = 'packed';
      $checkin_quantity = '0';
    }


    if(count($product)==0) {

      $product_id = DB::table('products')->insertGetId(array(
        'identifier' => $data['identifier'],
        'import_at' => 'operations',
        'user_id' => $data['user_id'],
        'type' => 'unmapped',
        'created_at' => date("Y-m-d H:i:s"),
      ));

      DB::table('asnboxes_products')->insert([
        'asn_id' => $data['asn_id'],
        'box_id' => $data['box_id'],
        'product_id' => $product_id,
        'quantity' => '1',
        'checkin_quantity' => $checkin_quantity,
        'state' => 'new',
        'product_asn_state' => $product_asn_state,
      ]);

      $status = 1;

    } else {

      $product_id = $product->id;

      $product_asn = DB::table('asnboxes_products')
      ->where('asn_id', '=', $data['asn_id'])
      ->where('box_id', '=', $data['box_id'])
      ->where('product_id', '=', $product_id)
      ->count();

      if($product_asn == 0) {
        $status = 2;
      } else {
        $status = 3;
      }
    }

    return $status;

  }

  public function checkinProducts($data){

    $product = DB::table('products')
    ->join('asnboxes_products', 'asnboxes_products.product_id', '=', 'products.id')
    ->where('identifier', '=', $data['identifier'])
    ->where('asn_id', '=', $data['asn_id'])
    ->where('box_id', '=', $data['box_id'])
    ->select('quantity','checkin_quantity','asnboxes_products.id as asnpro_id')
    ->first();


    if(count($product)> 0) {

      $checkin_quantity = $product->checkin_quantity+1;

      if($checkin_quantity  > $product->quantity) {

        return $status = 2;

      } elseif($product->quantity ==1) {

        DB::table('asnboxes_products')->where('id', $product->asnpro_id)
        ->update(['product_asn_state'=>'checked in','checkin_quantity'=>'1']);
        $status = 1;

      } elseif($product->quantity > 1) {

        $checkin_quantity = $product->checkin_quantity+1;

        if($product->quantity==$checkin_quantity) {
          DB::table('asnboxes_products')->where('id', $product->asnpro_id)
          ->update(['product_asn_state'=>'checked in','checkin_quantity'=>$checkin_quantity]);
        } else if($product->quantity > $checkin_quantity) {
          DB::table('asnboxes_products')->where('id', $product->asnpro_id)
          ->update(['checkin_quantity'=>$checkin_quantity]);
        }

        $status = 1;
      }

    } else {
      $status = 0;
    }

    return $status;

  }

  public function addProductQuantity($type, $data){

    $product = DB::table('products')
    ->join('asnboxes_products', 'asnboxes_products.product_id', '=', 'products.id')
    ->where('identifier', '=', $data['identifier'])
    ->where('asn_id', '=', $data['asn_id'])
    ->where('box_id', '=', $data['box_id'])
    ->select('quantity','checkin_quantity','asnboxes_products.id as asnpro_id')
    ->first();


    if(count($product)> 0) {

      $checkin_quantity = $product->checkin_quantity+1;

      if($checkin_quantity > $product->quantity) {
        DB::table('asnboxes_products')->where('id', $product->asnpro_id)
        ->update(['checkin_quantity'=>$checkin_quantity,
        'product_asn_state' => 'received exception', 'exception' => '1']);
      }

      return ['msg' => 'success'];

    } else {
      return ['err' => 'missing data'];
    }
  }

  public function addProductException($data){

    $product = DB::table('products')
    ->where('identifier', '=', $data['identifier'])
    ->first();


    if(count($product)==0) {

      $product_id = DB::table('products')-> insertGetId(array(
        'identifier' => $data['identifier'],
        'import_at' => 'operations',
        'user_id' => $data['user_id'],
        'type' => 'unmapped',
        'created_at' => date("Y-m-d H:i:s"),
      ));

      DB::table('asnboxes_products')->insert([
        'asn_id' => $data['asn_id'],
        'box_id' => $data['box_id'],
        'product_id' => $product_id,
        'quantity' => '1',
        'checkin_quantity' => '1',
        'state' => 'new',
        'exception' => '1',
        'product_asn_state' => 'received exception',
      ]);

      $status = 1;

    } else {

      $product_id = $product->id;

      $product_asn = DB::table('asnboxes_products')
      ->where('asn_id', '=', $data['asn_id'])
      ->where('box_id', '=', $data['box_id'])
      ->where('product_id', '=', $product_id)
      ->count();

      if($product_asn == 0) {

        DB::table('asnboxes_products')->insert([
          'asn_id' => $data['asn_id'],
          'box_id' => $data['box_id'],
          'product_id' => $product_id,
          'quantity' => '1',
          'checkin_quantity' => '1',
          'state' => 'new',
          'exception' => '1',
          'product_asn_state' => 'received exception',
        ]);
      }
      $status = 1;
    }

    return $status;

  }

  public function addProducts($type, $data){


    $product = DB::table('products')->where('identifier', $data['identifier'])->first();


    if($data['addtype']==2) {

      $product_asn = DB::table('asnboxes_products')
      ->where('product_id', $product->id)
      ->first();

      if(count($product_asn) > 0 && $product_asn->asn_id==$data['asn_id']) {

        $state = 'new';
      } else {
        $state = 'existing';
      }


      DB::table('asnboxes_products')->insert([
        'asn_id' => $data['asn_id'],
        'box_id' => $data['box_id'],
        'product_id' => $product->id,
        'quantity' => '1',
        'state' => $state,
        'product_asn_state' => 'packed',
      ]);

    } else  if($data['addtype']==3) {

      $product_asn = DB::table('asnboxes_products')
      ->where('asn_id', $data['asn_id'])
      ->where('box_id', $data['box_id'])
      ->where('product_id', $product->id)
      ->first();

      $quantity = $product_asn->quantity+1;

      if($product_asn->product_asn_state=="dropped") {
        DB::table('asnboxes_products')
        ->where('id', $product_asn->id)
        ->update(
          ['state' => 'existing', 'quantity' => $quantity, 'product_asn_state'=>'packed']
        );
      } else {
        DB::table('asnboxes_products')
        ->where('id', $product_asn->id)
        ->update(
          ['state' => 'existing', 'quantity' => $quantity]
        );
      }
    }

    return 1;

  }

  public function getProductInfo($data){

    return $products =  DB::table('products')
    ->join('asnboxes_products', 'products.id', '=', 'asnboxes_products.product_id')
    ->where('identifier', $data['identifier'])
    ->where('asn_id', $data['asn_id'])
    ->get(['products.id as productid','identifier','color_name','brand_name','description','gender',
    'style_family','base_product_id','asnboxes_products.id as asnpro_id']);
  }

  public function updateProductsState($asn_id, $box_id ='all'){

    if($box_id == "all") {
      $products_asn = DB::table('asnboxes_products')
      ->where('asn_id', $asn_id)
      ->where('product_asn_state', '!=', 'checked in')
      ->where('product_asn_state', '!=', 'dropped')
      ->where('product_asn_state', '!=', 'received exception')
      ->get();
    } else {
      $products_asn = DB::table('asnboxes_products')
      ->where('asn_id', $asn_id)
      ->where('box_id', $box_id)
      ->where('product_asn_state', '!=', 'checked in')
      ->where('product_asn_state', '!=', 'dropped')
      ->where('product_asn_state', '!=', 'received exception')
      ->get();
    }


    if(count($products_asn) > 0) {
      foreach ($products_asn as $product) {
        DB::table('asnboxes_products')
        ->where('id', $product->id)
        ->update(['product_asn_state' => 'not received','exception' => '1']);
      }

    }

  }

  public function getRecentProductsofSession($sid){
    $products =  DB::table('product_psession')
    ->where('psession_id', $sid)
    ->distinct()
    ->pluck('product_id');

    $products_handler = new ProductState();

    $products = $products_handler->where('psession_id', $sid)->wherein('product_id', $products)
    ->with('products')
    ->orderBy('id', 'DESC')
    ->get();

    $block = [];

    foreach ($products as $product) {
      $image_handler = new ImageTransfer();
      $thumb = $image_handler->where(['psession_id' => $sid,'product_id' => $product->product_id])
      ->with('image')
      ->whereHas('image', function ($q) {
        $q->whereNull('deleted_at');
      })
      ->first();

      $block[] = ['product' => $product, 'thumb' => $thumb];
    }

    return $block;
  }
}
