<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class ProductComment extends Model
{
    use UsesTenantConnection;
    
    protected $table = 'comment_product';
    protected $fillable = ['comment_id', 'product_id', 'psession_id'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function comment()
    {
        return $this->belongsTo('App\Models\Comment');
    }
}
