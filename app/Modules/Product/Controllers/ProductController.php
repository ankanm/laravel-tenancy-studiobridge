<?php

namespace App\Modules\Product\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Modules\Product\Models\Product;
use App\Modules\Copywriting\Models\CopyRevisions;
use App\Modules\Image\Models\Image;
use App\Modules\Psession\Models\Psession;
use App\Modules\ProductState\Models\ProductState;
use App\Modules\Comment\Models\Comment;
use App\Modules\Ounass\Models\Ounass\Ounass;
//use App\Models\QcOperation;
//use App\Modules\Operation\Models\CommentQcOperation;
use App\Models\SequenceConvention;




class ProductController extends Controller
{

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function mapProducts(){
        //Get Unmapped Products
        $unmapped_products= DB::table('products')
        ->where('type', '=', 'unmapped')
        ->pluck('id');

        $mapped_products = 0;

        foreach($unmapped_products as $product) {
            $check_map = new \App\Http\Controllers\LiveshootRestController;
            $check_map_result = $check_map->mapProduct($product);

            $mapped_products = $mapped_products + $check_map_result;
        }

        return response()->json(['total' => count($unmapped_products), 'mapped' => $mapped_products]);

    }


    /* View All Sessions*/

    public function ViewProducts($type){

        $user_id = Auth::User()->id;


        switch ($type) {
            case 'all':
                $screen_name = 'All Products';
                $products = Product::with(['states' => function($query) {
                    return $query->orderBy('id', 'desc');
                    }])
                    ->with(['psessions' => function($query) {
                        return $query->distinct();
                    }])
                    ->orderBy('id', 'desc')
                    ->get();
            break;
            case 'my':
                $screen_name = 'My Products';
                     
                $products = Product::with(['states' => function($query) use ($user_id) {
                    return $query->where('psessions.user_id', $user_id)->distinct();
                }])
                ->with('states')
                ->orderBy('id', 'desc')
                ->get();
                
            break;

            case 'rejected':
                $screen_name = 'Rejected Products';
                $products = Product::with(['states' => function($query) {
                    return $query->orderBy('id', 'desc');
                }])
                ->with(['psessions' => function($query) {
                    return $query->orderBy('id', 'desc')->distinct();

                }])
                ->orderBy('id', 'desc')
                ->get();
            break;
            default:
                $screen_name = 'All Products';
                $products = Product::orderBy('id', 'desc')
                    ->with('psessions')
                    ->get();
        }

        if ($products) {
            $products_data = $products;
        } else {
            $products_data = '';
        }
        return view('Product::ViewProducts')->with(['products' => $products_data, 'screen_name' => $screen_name]);
    }

    public function productServerLookUp($identifier = 54491472){
        $base_uri = 'http://40.85.180.244/';
        $user_name = 'krishna';
        $pass = 1234;
        try {
            // Create a client with a base URI
            $client = new Client(['base_uri' => $base_uri]);
            // Send a request to https://foo.com/api/test
            $response = $client->request('GET', 'product/lookup/'.$identifier.'?_format=json', ['auth' => [$user_name, $pass]]);
            $result = (string) $response->getBody();
        } catch (\Exception $e) {
            $result = json_encode(['msg' => $e->getMessage()]);
        }

        return $result;
    }

    public function getProductImagesAllSessions($pid){
        //this function returns data for a given product for all sessions that product belongs to

        $block = [];
        if ($pid != null || $pid != '') {
            $block = [];
            $session_handler = new Product();
            $sessions = $session_handler->find($pid)->psessions()->distinct()->orderby('id', 'desc')->get();



            foreach ($sessions as $session) {
                $images_container = [];

                $image_handler = new Image();
                $images = $image_handler->getProductImagesOfSession($session->id, $pid);

                $product_info_handler = new Product();
                $product_information = $product_info_handler->where('id', $pid)->first();


                foreach ($images as $image) {
                    $images_container[] = $image;
                }

                $block[] = ['session' => $session, 'images' => $images_container, 'product' => $product_information];
            }
        }


        return $block;
    }

    function viewProduct($sid, $pid){
        //this function returns data for View Product page

        //Get product information based on Product ID
        $product_info_handler = new Product();
        $product = $product_info_handler->where('id', $pid)->first();

        $product_info = Product::where('id', $pid)
            ->with('states')
            ->whereHas('states', function ($q) use ($sid) {
            return $q->where('product_states.psession_id', $sid)->distinct();
        })
        ->get();

        $productcomments = new Comment;
        $comments = $productcomments->getComments($sid, $pid);

        //retrieves information for product from all other sessions
        $product_information = $this->getProductImagesAllSessions($pid);

        $product_revisions = CopyRevisions::where('product_id', $pid)->orderBy('id','DESC')->first();

        // return json_encode($comments);
        // die();
        //returns data to view
        return view('Product::ViewProduct',
        ['product_information' => $product_information, 'current_session' => $sid, 'product' => $product, 'comments' => $comments, 'product_revisions' => $product_revisions]);
    }

    function ViewAllProducts(){
        //this function returns data for View Product page

        //Get product information based on Product ID
        $product_info_handler = new Product();
        $products = $product_info_handler->all();


        //returns data to view
        return view('Product::ViewProducts', ['products' => $products]);
  }

  function getProductInfo($sid, $pid){

        $product = new Product();

        $product_info = $product->getProductInfoPD($sid, $pid);

        $sequence_convention =  $product_info->id_sequence_convention;

        if(isset($sequence_convention) && $sequence_convention != ''){
            $product_sequence = SequenceConvention::where('id_sequence_convention', $sequence_convention)->first();
        } else{
            //If sequence is not found, find and save
            $product_sequence_params = $product->getProductSequence($sid, $pid);
            $product_sequence = SequenceConvention::where('applies_when', implode(",",$product_sequence_params))->first();
            if(isset($product_sequence)){
                $sequence = $product_sequence->id_sequence_convention;
            } else {
                $sequence = 1;
            }

            $to_update = ProductState::where('psession_id', $sid)->where('product_id', $pid)->first();
            $to_update->id_sequence_convention = $sequence;
            $to_update->save();

            $product_sequence = SequenceConvention::where('id_sequence_convention', $sequence)->first();
        }

        return response()->json([$product_info, $product_sequence] ,200);

    }

    function stateExtraShot(Request $request){

        $type = $request->type;
        $sid = $request->sid;
        $pid = $request->pid;

        //Update in product
        if($type == 'add'){
            $product_update = Product::where('id', $pid)->first();
            $product_update->requires_extra_shot = '1';
            $product_update->extra_shot_session = $sid;
            $product_update->save();

            return response()->json('added', 200);
        } elseif($type == 'remove') {
            $product_update = Product::where('id', $pid)->first();
            $product_update->requires_extra_shot = '0';
            $product_update->extra_shot_session = '0';
            $product_update->save();

            return response()->json('removed', 200);
        } else {
            $product_update = Product::where('id', $pid)->first();
            $product_update->requires_extra_shot = '0';
            $product_update->extra_shot_session = '0';
            $product_update->save();
            //if request came from same session its a removal not completion
            if($product_update->extra_shot_session == $sid){
                return response()->json('removed', 200);
            } else {
                return response()->json('completed', 200);
            }
        }

        return response()->json('error', 200);

    }
}
