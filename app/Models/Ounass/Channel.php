<?php

namespace App\Models\Ounass;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'channel';

    public function products()
    {
      return $this->hasMany('App\Models\Product', 'id_channel', 'id_channel');
    }

    public function productsCount()
    {
      return $this->hasMany('App\Models\Product', 'id_channel', 'id_channel')->count();
    }
}
