<?php

namespace App\Models\Navigation;

use Illuminate\Database\Eloquent\Model;

class NavigationRole extends Model
{
	protected $table = 'navigation_role';

	protected $fillable = ['navigation_id','role_id'];
}
