<?php

namespace App\Models\Navigation;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class NavigationCache extends Model
{
 	use UsesTenantConnection;
 	protected $table = 'navigation_cache';
    protected $guarded = [];
}
