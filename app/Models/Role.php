<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function getUserRole($userid) 
    {
        return DB::table('role_user')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('user_id',$userid)
        ->pluck('role_id')->toArray();

    }

    public function getPrimaryRoles() 
    {
        return DB::table('roles')->where('is_premium','1')->get();
    }

    public function getAdditionalRoles() 
    {
        return DB::table('roles')->where('is_premium','0')
        ->where('name', '!=', 'Super Admin')
        ->where('name', '!=', 'Organization Admin')
        ->get();
    }

    public function getUserRoleName($userid) 
    {
        return DB::table('role_user')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('user_id',$userid)
        ->select('roles.id','roles.name')->get();

    }



   
}
