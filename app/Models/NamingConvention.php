<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NamingConvention extends Model
{
    protected $table = 'naming_conventions';
    protected $primaryKey = 'id_naming_convention';
}
