<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;
class QcOperation extends Model
{
    use UsesTenantConnection;

    /**
     * Get the comments/notes/flags for the product.
     */
    public function comments()
    {
        //1 QC operation has 1 comment
        return $this->hasOne('App\Modules\Comment\Models\Comment');
    }

    /**
     * Get the images associated with this qc operation
     */
    public function images()
    {
        //1 QC operation has 1 comment
        return $this->belongsTo('App\Modules\Image\Models\Image');
    }
}
