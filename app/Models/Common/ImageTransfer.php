<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class ImageTransfer extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'image_transfers';
    public $timestamps = false;
    protected $fillable = ['image_id','product_id','psession_id', 'container_id', 'weight'];


    public function image()
    {
        return $this->belongsTo('App\Models\Image', 'image_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
