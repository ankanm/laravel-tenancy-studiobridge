<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Imagesretouching extends Model
{
    protected $table = 'images_retouching';

    public function products()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
