<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    protected $table = 'comment_product';
    protected $fillable = ['comment_id', 'product_id', 'psession_id'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function comment()
    {
        return $this->belongsTo('App\Models\Comment');
    }
}
