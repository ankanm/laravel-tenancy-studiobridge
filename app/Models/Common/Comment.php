<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function psession()
    {
        return $this->belongsTo('App\Models\Psession');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function qcoperations()
    {
        return $this->belongsTo('App\Models\QcOperation');
    }

    public function productcomment()
    {
        return $this->hasOne('App\Models\ProductComment');
    }

    public function getComments($psid, $pid)
    {
        //Check if product has comments
        $comments_ids = DB::table('comment_product')
        ->where('product_id', $pid)
        ->orderBy('id', 'desc')
        ->pluck('comment_id');

        $comments = Comment::whereIn('id', $comments_ids)->with('productcomment')->with('user')->orderBy('id', 'desc')->get();

        return $comments;
    }

    public function getCommentsCount($psid, $pid)
    {
        //Check if product has comments
        $comments = DB::table('comment_product')
        ->where('product_id', $pid)
        ->orderBy('id', 'desc')
        ->count();

        return $comments;
    }

    public function recentComments($from = null, $to = null)
    {

        //Check if product has comments
        if ($from != null && $to != null) {
            $comments_ids = DB::table('comment_product')
            ->orderBy('id', 'desc')
            ->whereBetween(\DB::raw('date(created_at)'), [$from, $to])
            ->pluck('comment_id');
        } else {
            $comments_ids = DB::table('comment_product')
            ->orderBy('id', 'desc')
            ->pluck('comment_id');
        }

        $comments = Comment::whereIn('id', $comments_ids)
        ->with('productcomment')
        ->with('user')
        ->limit(5)
        ->orderBy('id', 'desc')->get();


        return $comments;
    }

    public function recentActivity($from = null, $to = null)
    {

        if ($from != null && $to != null) {
            $comments_ids = DB::table('comment_product')
            ->orderBy('id', 'desc')
            ->whereBetween(\DB::raw('date(created_at)'), [$from, $to])
            ->pluck('comment_id');
        } else {
            $comments_ids = DB::table('comment_product')
            ->orderBy('id', 'desc')
            ->pluck('comment_id');
        }

        $comments = Comment::whereIn('id', $comments_ids)->where('subject', '<>', 'Note')
        ->with('productcomment')
        ->with('user')
        ->limit(5)
        ->orderBy('id', 'desc')->get();


        return $comments;
    }
}
