<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class ProductTime extends Model
{
	protected $table = 'product_times';
    protected $fillable = ['product_id', 'check_in', 'photography_start', 'photography_end', 'copy_start', 'copy_end', 'retouch_start', 'retouch_end', 'check_out'];
    public $timestamps = false;
}
