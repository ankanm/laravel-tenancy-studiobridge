<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Image extends Model
{

    use SoftDeletes;

  /**
  * The attributes that should be mutated to dates.
  *
  * @var array
  */
    protected $dates = ['deleted_at'];

    public function imagetransfer()
    {
        return $this->hasOne('App\Models\Common\ImageTransfer', 'image_id');
    }

    public function qcoperation()
    {
        return $this->hasMany('App\Models\QcOperation', 'image_id');
    }


    public function getProductImagesOfSession($psession_id, $pid)
    {
        return DB::table('image_transfers')
          ->leftJoin('images', function($join) {
            $join->on('images.id', '=', 'image_transfers.image_id');
          })
          ->leftJoin('qc_operations', function($join) use ($psession_id) {
            $join->on('qc_operations.image_id', '=', 'image_transfers.image_id');
            $join->on('qc_operations.psession_id', '=', 'image_transfers.psession_id');
            $join->orderBy('qc_operations.id', 'DESC');
          })
          ->leftJoin('products', function($join) {
            $join->on('products.id', '=', 'image_transfers.product_id');
          })
        ->select(
          'image_transfers.sequence',
          'products.base_product_id',
          'products.color_family',
          'images.id',
          'images.deleted_at',
          'images.disk',
          'images.has_full_image',
          'images.id',
          'images.path',
          'images.updated_at',
          'qc_operations.status',
          \DB::raw('GROUP_CONCAT(qc_operations.status) as status_new'),
          \DB::raw('CONCAT(images.host, "/", images.thumbpath) as thumbpath')
        )
        ->where('image_transfers.psession_id',$psession_id)
        ->where('image_transfers.product_id',$pid)
        ->whereNull('images.deleted_at')
        ->orderBy('image_transfers.weight', 'asc')
        ->groupBy('image_transfers.image_id')
        ->get();
        return response()->json($images);
    }

    public function getSessionContactSheet($psession_id)
    {
        return DB::table('image_transfers')
          ->leftJoin('images', function($join) {
            $join->on('images.id', '=', 'image_transfers.image_id');
          })
          ->leftJoin('qc_operations', function($join) use ($psession_id) {
            $join->on('qc_operations.image_id', '=', 'image_transfers.image_id');
            $join->on('qc_operations.psession_id', '=', 'image_transfers.psession_id');
            $join->orderBy('qc_operations.id', 'DESC');
          })
          ->leftJoin('products', function($join) {
            $join->on('products.id', '=', 'image_transfers.product_id');
          })
          ->leftjoin('ounass', function($join){
                $join->on('ounass.Channel','=','products.channel');
                $join->on('ounass.Item','=','products.identifier');
          })
        ->select(
          'image_transfers.sequence',
          'products.base_product_id',
          'products.color_family',
          'products.brand_name',
          'ounass.Item_Parent',
          'ounass.ATG_Color_Code',
          'ounass.GAP_Color_Code',
          'ounass.VPN',
          'images.id',
          'images.deleted_at',
          'images.disk',
          'images.has_full_image',
          'images.id',
          'images.path',
          'images.updated_at',
          'image_transfers.sequence',
          'qc_operations.status',
          \DB::raw('GROUP_CONCAT(qc_operations.status) as status_new'),
          \DB::raw('CONCAT(images.host, "/", images.thumbpath) as thumbpath')
        )
        ->where('image_transfers.psession_id',$psession_id)
        ->whereNull('images.deleted_at')
        ->orderBy('products.id', 'desc')
        ->groupBy('image_transfers.image_id')
        ->get();
        return response()->json($images);
    }

    public function getProductImagesOfAllSessions($pid)
    {

        $images = ImageTransfer::where(
            ['product_id' => $pid]
        )->orderBy('weight', 'asc')->get();
        $iids = [];

        if ($images) {
            foreach ($images as $image) {
                $iids[] = $image['image_id'];
            }
            $imgs = Image::whereIn('id', $iids)->get();
            if ($imgs) {
                $imgs_array = $imgs->toArray();
                $tmp = [];
                if ($imgs_array) {
                    foreach ($imgs_array as $img) {
                        $tmp[$img['id']] = $img;
                    }

                    $final = [];
                    foreach ($iids as $iid) {
                        //$final[] = $tmp[$iid];
                        if (isset($tmp[$iid])) {
                            $final[] = $tmp[$iid];
                        }
                    }
                    return $final;
                }
            }
        }
        return [];
    }
}
