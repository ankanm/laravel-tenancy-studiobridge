<?php

namespace App\Models\;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Product;

class Psession extends Model
{

    //
    /**
    * The sessions that belong to the products.
    */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    public function productsCount()
    {
      return $this->products()->selectRaw('count(distinct(product_id)) as count');
    }

    public function product_states()
    {
          return $this->hasMany('App\Models\ProductState');
    }

    /**
    * Get the comments/notes/flags for the Psession.
    */
    public function comments()
    {
   //1 session can have many comments
        return $this->hasMany('App\Models\Comment');
    }


    public function getAllProductsOfSession($sid)
    {

        $products =  DB::table('product_psession')
        ->where('psession_id', $sid)
        ->distinct()
        ->pluck('product_id');

        return DB::table('products')
        ->whereIn('products.id', $products)
        ->orderBy('products.id', 'desc')
        ->get();
    }

    public function getAllProductsOfSessionExcludeQcProducts($sid)
    {

        $products =  DB::table('product_psession')
        ->where('psession_id', $sid)
        ->distinct()
        ->get(['product_id']);

        $pids = [];
        foreach ($products as $pid) {
            $qc_count = ProductState::where(['psession_id' => $sid,'product_id' => $pid->product_id, 'qc_done' => 1])->count();
            if ($qc_count == 0) {
                $pids[] = $pid->product_id;
            }
        }
        //                return $pids;


        return Product::whereIn('products.id', $pids)
        ->orderBy('products.id', 'desc')
        ->get();
    }
}
