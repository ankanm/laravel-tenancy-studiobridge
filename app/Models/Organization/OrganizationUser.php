<?php

namespace App\Models\Organization;

use Illuminate\Database\Eloquent\Model;

class OrganizationUser extends Model
{
	protected $table = 'organization_user';

	protected $fillable = ['organization_id','user_id', 'is_admin'];
}
