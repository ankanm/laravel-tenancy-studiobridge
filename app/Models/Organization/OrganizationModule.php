<?php

namespace App\Models\Organization;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationModule extends Model
{
	use SoftDeletes;

	protected $table = 'module_organization';

	protected $fillable = ['organization_id','module_id','deleted_at'];

	protected $dates = ['deleted_at'];
}
