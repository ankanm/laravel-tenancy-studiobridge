<?php

namespace App\Models\Organization;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Organization extends Model
{

	protected $fillable = ['name','type', 'address', 'city', 'state', 'country', 'postal_code', 'email', 'license_type','licenses',
	'status'];
	//

	//Get Organization Users
	public function getOrganizationUsers($orgid)
	{

		return DB::table('organization_user')
		->join('users', 'organization_user.user_id', '=', 'users.id')
		->join('role_user', 'organization_user.user_id', '=', 'role_user.user_id')
		->join('roles', 'roles.id', '=', 'role_user.role_id')
		->leftjoin('licenses', 'organization_user.user_id', '=', 'licenses.user_id')
		->where('organization_user.organization_id', $orgid)
		->select('users.*','roles.name as role_name','licenses.is_active')
		->groupBy('role_user.user_id')->get();
	}

	//Get Organization
	public function getOrganization($userid)
	{

		return DB::table('organization_user')
		->join('organizations', 'organization_user.organization_id', '=', 'organizations.id')
		->select('organizations.name','organizations.type', 'organizations.id',
		'organizations.licenses','organizations.status')
		->where('user_id',$userid)->get();

	}

	//Get Organization Admin
	public function getOrganizationAdmin($orgid)
	{
		return $org_users =  DB::table('organization_user')
		->join('users', 'organization_user.user_id', '=', 'users.id')
		->where('organization_id', $orgid)
		->where('is_admin','1')->select('user_id','name','email')->get();
	}

	public function getModules($orgid){
		return $modules = OrganizationModule::where('organization_id', $orgid)
		->join('module', 'module.id', '=', 'module_organization.module_id')
		->orderBy('module.weight')
		->get();

	}
	public function modules()
	{
		return $this->hasMany('App\Models\Module')->orderBy('weight');
	}


	public function user()
	{
		return $this->hasMany('App\User');
	}

	//Get Organization Locations
	public function getOrganizationLocations($orgid)
	{
		return DB::table('organizations')
		->join('locations', 'organizations.id', '=', 'locations.organization_id')
		->where('organizations.id', $orgid)
		->select('locations.id as loc_id', 'short_name','address1','address2','locations.city as city','province','locations.zipcode as zipcode',
			'locations.country as country','is_primary')
		->get();
	}

	//Get Organization Locations
	public function getOrganizationVendors($orgid)
	{
		return DB::table('vendors')
		->join('organization_vendors', 'vendors.id', '=', 'organization_vendors.vendor_id')
		->where('organization_vendors.organization_id', $orgid)
		->select('vendors.id as vendor_id', 'vendors.name as vendor_name','contact','is_private')
		->get();
	}

}
