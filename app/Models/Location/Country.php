<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Country extends Model
{

    public function states()
    {
        return $this->hasMany('App\Models\State', 'country_id', 'id');
    }



}
