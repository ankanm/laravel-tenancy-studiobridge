<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class State extends Model
{

    public function cities()
    {
        return $this->hasMany('App\Models\City', 'state_id', 'id');
    }



}
