<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Agency extends Model
{

	protected $fillable = ['name','contact','agency_type','agency_class','logo','status','organization_id'];


	public function agencies()
	{
	    return $this->belongsToMany('App\Models\Agency');
	}

	public function users()
    {
        return $this->hasMany('App\User','id');
    }

}
