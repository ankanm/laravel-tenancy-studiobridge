<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SequenceConvention extends Model
{
    protected $table = 'sequence_conventions';
    protected $primaryKey = 'id_sequence_convention';

    function getProductConvention($product, $psession_type){

      //Get Product


      return DB::table('product_states')
        ->leftJoin('products', function($join){
          $join->on('products.id', '=', 'product_states.product_id');
        })
        ->leftJoin('comment_product', 'products.id', '=', 'comment_product.product_id')
        ->leftJoin('image_transfers', 'products.id', '=', 'image_transfers.product_id')
        ->leftJoin('images', function($join) {
          $join->on('images.id', '=', 'image_transfers.image_id')
          ->where('images.deleted_at', '=', 'NULL');
        })
      ->select(
        DB::raw('COUNT(images.id) as images'),
        DB::raw('COUNT(comment_product.id) as comments'),
        'products.id',
        'products.brand_name',
        'products.identifier',
        'products.base_product_id',
        'product_states.qc_done',
        'product_states.qc_state'
      )
      ->where('product_states.psession_id',$sid)
      ->where('product_states.product_id',$pid);

    }
}
