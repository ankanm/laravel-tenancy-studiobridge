<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
	protected $table = 'module';


	public $fillable = ['name','parent_id'];


    public function organizations()
	{
	    return $this->belongsToMany('App\Models\Organization\Organization');
	}


	public function childs() {
        return $this->hasMany('App\Models\Module','parent_id','id') ;
    }


}
