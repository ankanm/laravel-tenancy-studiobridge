<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    //

    protected $table = 'locations';

    protected $fillable = ['organization_id', 'vendor_id', 'carrier_id', 'agencies_id','is_primary',
    'short_name','address1','address2','city','province','zipcode','country'];
}
