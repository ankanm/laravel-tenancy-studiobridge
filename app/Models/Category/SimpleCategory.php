<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;

class SimpleCategory extends Model
{
    protected $fillable = ['class','subclass','category_rms','category'];
    protected $table = 'simple_category';
}
