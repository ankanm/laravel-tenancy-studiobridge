<?php
namespace App\Traits\Custom;

trait CustomResponseTrait
{
	public function responseBindLocation($request)
	{
		return [
			'address1' => (isset($request['address1']) && !empty($request['address1'])) ? $request['address1'] : null ,
	        'address2' => (isset($request['address2']) && !empty($request['address2'])) ? $request['address2'] : null ,
	        'short_name' => (isset($request['short_name']) && !empty($request['short_name'])) ? $request['short_name']:null,
	        'city' => (isset($request['city']) && !empty($request['city'])) ? $request['city']:null , 
	        'province' =>(isset($request['province']) && !empty($request['province'])) ? $request['province']:null ,
	        'zipcode' => (isset($request['zipcode']) && !empty($request['zipcode'])) ? $request['zipcode']:null , 
	        'country' => (isset($request['country']) && !empty($request['country'])) ? $request['country']:null , 
	        'is_primary' => '1',
		];
	}

	public function responseBindOrganizationData($request)
	{
		return [
			'name' => (isset($request['name']) && !empty($request['name']))? $request['name'] : null ,
		    'type' => (isset($request['type']) && !empty($request['type']))? $request['type'] : null ,
		    'licenses' => (isset($request['licenses']) && !empty($request['licenses']))? $request['licenses'] : null ,
		    'email' => (isset($request['email']) && !empty($request['email']))? $request['email'] : null ,
		    'address' => (isset($request['address']) && !empty($request['address'])) ? trim($request->address) : null ,
		];
	}

	
}
