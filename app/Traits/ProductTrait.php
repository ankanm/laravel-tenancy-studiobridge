<?php
namespace App\Traits;

use App\Models\Ounass\Channel;
use App\Models\Ounass\Ounass;
use App\Models\Product;
use App\Models\WarehouseProduct; //for warehouse

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait ProductTrait
{
    public function importProduct($identifier, $channel, $csession_id, $import_at = 'liveshoot', $desk = 'Photography Desk')
    {
        $response = $this->productServerLookUp($identifier, $channel);
        $product  = new Product();
        $results  = (array) json_decode($response);
        if (empty($results)) {
            return response()->json(['error' => 'No template found'], 200);
        }

        /**
         *
         * Checking duplicate entry into the Product table:
         */

        $found = $this->checkProductExistInSystem($identifier, $channel);

        if (!empty($found['products'])) {
            return $found['products'] ;
        } 
        

        //getting id_final_sequence
        $id_final_sequence    = 0;
        $simple_category_data = DB::table('simple_category')->where('class', $results['Class'])
            ->where('subclass', $results['SubClass'])->first();
        if (!empty($simple_category_data)) {
            $simple_category = $simple_category_data->category;

            $finalSeq = DB::table('final_sequence')->where('channel', $channel)
                ->where(function ($query) use ($simple_category) {
                    $query->where('category', $simple_category);
                    $query->orWhere('category', null);
                })
                ->first();
            $id_final_sequence = $finalSeq->id;
        }

        /**
         * check for mapped or unmapped products
         *
         */

        if (array_key_exists('id', $results)) {

            $product->channel           = (isset($results['Channel']) ? $results['Channel'] : '');
            $product->identifier        = (isset($results['Item']) ? $results['Item'] : $identifier);
            $product->id_channel        = (isset($results['Channel']) ? $this->channelRecord($results['Channel']) : '');
            $product->ounass_id         = (isset($results['id']) ? $results['id'] : '');
            $product->barcode           = (isset($results['Barcode']) ? $results['Barcode'] : '');
            $product->base_product_id   = (isset($results['Item_Parent']) ? $results['Item_Parent'] : '');
            $product->color_name        = (isset($results['ATG_Color_Desc']) ? $results['ATG_Color_Desc'] : '');
            $product->color_family      = (isset($results['ATG_Color_Code']) ? $results['ATG_Color_Code'] : '');
            $product->color_variant_id  = (isset($results['Item_Parent']) ? $results['Item_Parent'] . $results['ATG_Color_Code'] : '');
            $product->brand_name        = (isset($results['Brand']) ? $results['Brand'] : '');
            $product->department        = (isset($results['Class']) ? $results['Class'] : '');
            $product->category          = (isset($results['Category']) ? $results['Category'] : '');
            $product->import_at         = $import_at;
            $product->size_name         = (isset($results['Size']) ? $results['Size'] : '');
            $product->gender            = (isset($results['Gender']) ? $results['Gender'] : '');
            $product->state             = '';
            $product->style_family      = '';
            $product->user_id           = Auth::user()->id;
            $product->description       = (isset($results['Item_Description']) ? $results['Item_Description'] : '');
            $product->type              = 'mapped';
            $product->id_final_sequence = $id_final_sequence;

        } else {

            $product->identifier = $identifier;
            $product->channel    = $channel;
            $product->type       = 'unmapped';

        }
        $product->save();

        //Its a new product, enter a new record in product state
        $warehouse_prod                   = new WarehouseProduct();
        $warehouse_prod->product_id       = $product->id;
        $warehouse_prod->user_checkin     = Auth::user()->id;
        $warehouse_prod->location_checkin = $desk;
        $warehouse_prod->check_in         = date('Y-m-d H:i:s');
        $warehouse_prod->save();
        
        return $product;
    }

    public static function productServerLookUp($identifier, $channel)
    {
        if (isset($identifier) && isset($channel)) {

            $item = Ounass::where('channel', $channel)
                ->where(function ($query) use ($identifier) {
                    $query->where('Item', $identifier);
                    $query->orWhere('Barcode', $identifier);
                    $query->orWhere('Item_Parent', $identifier);
                })
                ->first();

            if ($item != null || $item != '') {
                return $item;
            }
        } else if (isset($identifier) && !isset($channel)) {
            $item = Ounass::where(function ($query) use ($identifier) {
                $query->where('Item', $identifier);
                $query->orWhere('Barcode', $identifier);
                $query->orWhere('Item_Parent', $identifier);
            })->first();

            if ($item != null || $item != '') {
                return $item;
            }
        }

        return false;
    }

    public function checkProductExistInSystem($identifier, $channel)
    {
        //This function will check if product exists in the Photography Desk Module
        //Check the Product States table and confirm
        //To succeed this check, the product must not exist in states table

        $return_product = '';
        $rule_id        = 0;
        if (empty($channel)) {
            $item = Ounass::where(function ($query) use ($identifier) {
                $query->where('Item', $identifier);
                $query->orWhere('Barcode', $identifier);
                $query->orWhere('Item_Parent', $identifier);
            })
                ->first();
        } else {
            $item = Ounass::where('Channel', $channel)
                ->where(function ($query) use ($identifier) {
                    $query->where('Item', $identifier);
                    $query->orWhere('Barcode', $identifier);
                    $query->orWhere('Item_Parent', $identifier);
                })
                ->first();
        }

        if (empty($item) || empty($item->Item)) {
            return ['error' => 'No product found'];
            exit;
        }

        //check matching channel, category, gender with completion_rule table
        if (!empty($item)) {

            $channel      = $item->Channel;
            $gender       = $item->Gender;
            $rms_class    = $item->Class;
            $rms_subclass = $item->SubClass;
            $category     = DB::table('simple_category')
            //->select('category')->where('class',$rms_class)->where('subclass', $rms_subclass)->where('channel', $channel)->first();
                ->select('category')->where('class', $rms_class)->where('subclass', $rms_subclass)->first();
            if (!empty($category)) {
                $rule = DB::table('completion_rule')
                    ->select('id')->where('channel', $channel)->where('category', $category->category)
                    ->where(function ($query) use ($gender) {
                        $query->where('gender', $gender);
                        $query->orWhere('gender', 'NA');
                    })
                    ->first();
                if (!empty($rule)) {
                    $rule_id = $rule->id;
                }
            }

            //Check if product exists in product table
            $product = new Product();
            if (empty($channel)) {
                $products = $product->where('base_product_id', $item->Item_Parent)->where('color_family', $item->ATG_Color_Code)
                    ->first();
            } else {
                $products = $product->where('base_product_id', $item->Item_Parent)->where('color_family', $item->ATG_Color_Code)
                    ->where('channel', $channel)
                    ->first();
            }

            return ['in_ounass' => 1, 'rule_id' => $rule_id, 'products' => $products];
        } else {
            return ['in_ounass' => 0, 'rule_id' => $rule_id, 'products' => ''];
        }

    }

    public function mapProduct($id)
    {

        $product = Product::where('id', $id)->first();

        $response = $this->productServerLookUp($product->identifier);
        $results  = (array) json_decode($response);
        // check for mapped or unmapped products
        if (array_key_exists('id', $results)) {
            //check siblings
            $product->base_product_id  = (isset($results['Item_Parent']) ? $results['Item_Parent'] : '');
            $product->color_name       = (isset($results['Color']) ? $results['Color'] : '');
            $product->color_family     = (isset($results['Color']) ? $results['Color'] : '');
            $product->color_variant_id = (isset($results['ProductImageName']) ? $results['ProductImageName'] : '');
            $product->brand_name       = (isset($results['Brand']) ? $results['Brand'] : '');
            $product->department       = (isset($results['Class']) ? $results['Class'] : '');
            $product->category         = (isset($results['Category']) ? $results['Category'] : '');
            $product->size_name        = (isset($results['Size']) ? $results['Size'] : '');
            $product->description      = (isset($results['Item_Description']) ? $results['Item_Description'] : '');
            $product->type             = 'mapped';
            $product->save();
            return 1;
        }

        return 0;
    }

    /**
     *
     * fetching data from Channel table
     *
     * @param : identifier , channel , ATG_Color_Code
     * #response : array | false
     */

    public function channelRecord($channel)
    {
        if (!empty($channel)) {

            $item = Channel::where('channel_name', $channel)->first();

            if ($item != null || $item != '') {
                return $item['id_channel'];
            }
        }
        return false;
    }

}
