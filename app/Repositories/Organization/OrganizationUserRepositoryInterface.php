<?php

namespace App\Repositories\Organization;

use App\Repositories\Base\BaseRepositoryInterface;

interface OrganizationUserRepositoryInterface extends BaseRepositoryInterface
{
    public function create(array $attributes);
}
