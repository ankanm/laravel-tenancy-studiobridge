<?php

namespace App\Repositories\Organization;

use App\Repositories\Base\BaseRepositoryInterface;

interface OrganizationModuleRepositoryInterface extends BaseRepositoryInterface
{
    public function create(array $attributes);
}
