<?php

namespace App\Repositories\Organization;

use App\Repositories\Base\BaseRepositoryInterface;

interface OrganizationRepositoryInterface extends BaseRepositoryInterface
{
    public function create(array $attributes);

    public function findOrganizationModules(int $id);

    public function findOrganizationUser(int $id);
    
    public function organizationModuleDelete(array $condition);

}
