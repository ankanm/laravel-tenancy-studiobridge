<?php

namespace App\Repositories\Organization;

use App\Modules\Organization\Models\Organization;
use App\Modules\Organization\Models\OrganizationModule;
use App\Modules\Organization\Models\OrganizationUser;

use App\Repositories\Base\BaseRepository;
use App\Repositories\Organization\OrganizationRepositoryInterface;
use Illuminate\Support\Facades\DB;

class OrganizationRepository extends BaseRepository implements OrganizationRepositoryInterface
{
    protected $model;
    protected $modelOrgUser;
    protected $modelOrgModule;

    /**
     * OrganizationRepository constructor.
     * @param Organization $organization,OrganizationModule $organizationModule,
     * OrganizationUser $organizationUser
     */
    public function __construct(Organization $organization, OrganizationUser $organizationUser, OrganizationModule $organizationModule)
    {
        $this->model       = $organization;
        $this->modelUser   = $organizationUser;
        $this->modelModule = $organizationModule;
    }

    //Get Organization Admin
    public function getOrganizationAdmin($orgid)
    {
        return $this->modelUser->join('users', 'organization_user.user_id', '=', 'users.id')
            ->where('organization_id', $orgid)
            ->where('is_admin', '1')->select('user_id', 'name', 'email')->get();
    }

    //Get Organization Admin
    public function getOrganizationModulePlucked($orgid)
    {
        return $this->modelModule->where('organization_id', $orgid)->pluck('module_id')->toArray();
    }

    //Get Organization Locations
    public function getOrganizationLocations($orgid)
    {
        return
        $this->model->join('locations', 'organizations.id', '=', 'locations.organization_id')
            ->where('organizations.id', $orgid)
            ->select('locations.id as loc_id', 'short_name', 'address1', 'address2', 'locations.city as city', 'province', 'locations.zipcode as zipcode',
                'locations.country as country', 'is_primary')
            ->get();
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all($columns = array('*'), $condition = array(), string $orderBy = 'id', string $sortBy = 'desc')
    {
        if (count($condition) > 0) {
            return $this->model->where($condition)->orderBy($orderBy, $sortBy)->get($columns);
        }else{

            return $this->model->orderBy($orderBy, $sortBy)->get($columns);
        }
    }

    /**
     *
     * Override for Organization Module 
     *
     */    
    public function organizationModuleDelete(array $condition)
    {
        return $this->modelModule->deleteBy($condition);
    }

    /**
     *
     * Override for Create Method
     *
     */    
    public function organizationUserCreate(array $attributes)
    {
        return $this->modelUser->create($attributes);
    }

    /**
     *
     * Override for Organization Module 
     *
     */    
    public function organizationModuleCreate(array $attributes)
    {
        return $this->modelModule->create($attributes);
    }

    /*----------  Terting Find Operation  ----------*/

    /**
     * @param int val
     * @return mixed
     */
    public function findOrganizationUser(int $id)
    {
        return $this->modelUser->find($id);
    }

    /**
     * @param int val
     * @return mixed
     */
    public function findOrganizationModules(int $id)
    {
        return $this->modelModule->find($id);
    }

    //Get Organization Users
    public function getOrganizationUsers($orgid)
    {

        return DB::table('organization_user')
        ->join('users', 'organization_user.user_id', '=', 'users.id')
        ->join('role_user', 'organization_user.user_id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->leftjoin('licenses', 'organization_user.user_id', '=', 'licenses.user_id')
        ->where('organization_user.organization_id', $orgid)
        ->select('users.*','roles.name as role_name','licenses.is_active')
        ->groupBy('role_user.user_id')->get();
    }

    //Get Organization
    public function getOrganization($userid)
    {

        return DB::table('organization_user')
        ->join('organizations', 'organization_user.organization_id', '=', 'organizations.id')
        ->select('organizations.name','organizations.type', 'organizations.id',
        'organizations.licenses','organizations.status')
        ->where('user_id',$userid)->get();

    }

    
    public function getModules($orgid){
        return $modules = OrganizationModule::where('organization_id', $orgid)
        ->join('module', 'module.id', '=', 'module_organization.module_id')
        ->orderBy('module.weight')
        ->get();

    }

    //Get Organization Locations
    public function getOrganizationVendors($orgid)
    {
        return DB::table('vendors')
        ->join('organization_vendors', 'vendors.id', '=', 'organization_vendors.vendor_id')
        ->where('organization_vendors.organization_id', $orgid)
        ->select('vendors.id as vendor_id', 'vendors.name as vendor_name','contact','is_private')
        ->get();
    }

}
