<?php

namespace App\Repositories\Ounass;

use App\Models\Ounass\Ounass;
use App\Repositories\Ounass\OunassRepositoryInterface;
use App\Repositories\Base\BaseRepository;

class OunassRepository extends BaseRepository implements OunassRepositoryInterface
{
    protected $model;
  
    /**
     * ArticlesRepository constructor.
     * @param Article $article
     */
    public function __construct(Ounass $ounass)
    {
        $this->model = $ounass;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }
}