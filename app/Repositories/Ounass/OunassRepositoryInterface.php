<?php

namespace App\Repositories\Ounass;

use App\Repositories\Base\BaseRepositoryInterface;

interface OunassRepositoryInterface extends BaseRepositoryInterface
{
   public function create(array $attributes);
}