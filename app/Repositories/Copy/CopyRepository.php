<?php

namespace App\Repositories\Copy;

use Modules\Copywriting\Models\Copywritingsession;
use App\Repositories\Base\BaseRepository;
use App\Repositories\Copy\CopyRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CopyRepository extends BaseRepository implements CopyRepositoryInterface
{
    protected $model;

    public function __construct(Copywritingsession $copywritingsession)
    {
        $this->model = $copywritingsession;
    }

    //get copyseesion details by id
    public function getCopySessions_dtl($id)
    {
        $query = DB::table('copywritingsessions as cs')
            ->leftjoin('product_copywritingsessions as products', 'products.copywritingsession_id', '=', 'cs.id')
            ->leftjoin('products as product', 'product.id', '=', 'products.product_id')
            ->leftJoin('users', 'users.id', '=', 'cs.user_id')
            ->leftJoin('ounass', function ($join) {
                $join->on('product.identifier', '=', 'ounass.Item');
                $join->on('product.channel', '=', 'ounass.Channel');
            })
            ->leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->leftJoin('copy_templates', function ($join) {
                $join->on('copy_templates.channel', '=', 'ounass.Channel');
                $join->on('copy_templates.simple_category', '=', 'simple_category.category');
            })

            ->select(
                DB::raw('COUNT(products.id) as products'),
                DB::raw('group_concat(distinct(product.channel)) as channels'),
                DB::raw('group_concat(distinct(copy_templates.template_name)) as templates'),
                'cs.*',
                'users.avatar as user_avatar'
            )
            ->where('cs.id', $id)
            ->groupBy('cs.id')
            ->first();
        //->get();

        return $query;
    }

    /**
     *Get Copy Session By User
     *
     */
    public function getCopySessions($user_id, $status = 'all')
    {
        $query = DB::table('copywritingsessions as cs')
            ->leftjoin('product_copywritingsessions as products', 'products.copywritingsession_id', '=', 'cs.id')
            ->leftjoin('products as product', 'product.id', '=', 'products.product_id')
            ->leftJoin('users', 'users.id', '=', 'cs.user_id')
            ->select(
                DB::raw('COUNT(products.id) as products'),
                DB::raw('group_concat(distinct(product.channel)) as channels'),
                'cs.id',
                'cs.status',
                'cs.type',
                'cs.method',
                'cs.created_at as created_at',
                'cs.user_id',
                'users.avatar as user_avatar'
            )
            ->where(function ($query) use ($status, $user_id) {
                if ($status == "all") {
                    $query->where('cs.user_id', $user_id);
                }
                if ($status == "open") {
                    $query->where('cs.status', '=', 'open');
                    $query->where('cs.user_id', $user_id);
                }
                if ($status == "closed") {
                    $query->where('cs.status', '=', 'closed');
                    $query->where('cs.user_id', $user_id);
                }
            })
            ->groupBy('cs.id')
            ->orderBy('cs.id', 'DESC')
            ->get();

        return $query;

    }

    public function getPhotographySessions()
    {
        return DB::table('psessions')
            ->join('product_states', 'psessions.id', '=', 'product_states.psession_id')
            ->select('psessions.id as psession_id', 'qc_done', 'qc_state', 'psessions.status')
            ->where('psessions.status', 'closed')->where('product_states.qc_done', 1)->where('product_states.qc_state', 1)
            ->groupBy('psessions.id')
            ->orderBy('psessions.id', 'DESC')->toSql();
    }

    public function getCommentsCount($csession_id, $prod_id)
    {

        return DB::table('products')
            ->leftjoin('comment_product', 'comment_product.product_id', '=', 'products.id')
            ->select(DB::raw('count(comment_product.id) as commentcount'))
            ->where('comment_product.csession_id', $csession_id)
            ->where('comment_product.product_id', $prod_id)
            ->get();

    }

    public function getSessionProductList($csession_id)
    {

        $sql = DB::table('product_copywritingsessions')
            ->leftjoin('products', 'product_copywritingsessions.product_id', '=', 'products.id')
            ->leftJoin('product_states', function ($join) {
                $join->on('product_states.product_id', '=', 'products.id')
                    ->where('product_states.id', '=', DB::raw("(select max(`id`) from product_states where product_id = products.id)"));
            })
            ->leftJoin('image_transfers', function ($join) {
                $join->on('image_transfers.product_id', '=', 'products.id');
                $join->on('image_transfers.psession_id', '=', 'product_states.psession_id');
            })
            ->leftJoin('images', function ($join) {
                $join->on('images.id', '=', 'image_transfers.image_id')
                    ->whereNull('images.deleted_at');
            })
            ->leftJoin('ounass', function ($join) {
                $join->on('products.identifier', '=', 'ounass.Item');
                $join->on('products.channel', '=', 'ounass.Channel');
            })
            ->leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->select(
                DB::raw('DISTINCT(product_copywritingsessions.id)'),
                DB::raw('CONCAT(MIN(images.host),"/",MIN(images.thumbpath)) as thumbpath'),
                'ounass.Channel',
                'ounass.Item',
                'ounass.Item_Parent',
                'ounass.Brand',
                'ounass.ATG_Color_Code',
                'ounass.GAP_Color_Code',
                'ounass.VPN',
                'ounass.id as ounass_id',
                'products.created_at as created_at',
                'products.id as product_id',
                'product_copywritingsessions.id as cid',
                'product_copywritingsessions.copy_api_status as copy_api_status2',
                'product_copywritingsessions.copy_api_details as copy_api_details2',
                'products.copy_api_status',
                'products.copy_api_detail',
                'simple_category.category',
                'product_copywritingsessions.copy_state',
                'product_copywritingsessions.qc_state'
            )
            ->where('product_copywritingsessions.copywritingsession_id', $csession_id)
            ->where('product_copywritingsessions.is_dropped', '!=', 1)
            ->orderBy('product_copywritingsessions.id')
            ->groupBy('product_copywritingsessions.id')
            ->get();

        return $sql;
    }

    public function getWriteContributers($csession_id)
    {

        return DB::table('copy_revisions')
            ->join('users', 'users.id', '=', 'copy_revisions.user_id')
            ->select('user_id', 'name', 'avatar')
            ->where('copywritingsession_id', $csession_id)
            ->groupBy('copy_revisions.user_id')
            ->get();
    }

    public function getWriteProducts($csession_id)
    {

        $products = DB::table('copy_revisions')
            ->join('product_copywritingsessions', 'product_copywritingsessions.product_id', '=',
                'copy_revisions.product_id')
            ->select('copy_revisions.product_id')
            ->where('product_title', '!=', '')
            ->where('product_description', '!=', '')
            ->where('copy_revisions.copywritingsession_id', $csession_id)
            ->groupBy('copy_revisions.product_id')
            ->get();

        return $products_count = count($products);

    }

    public function getSessionCompleteProductList($csession_id, $channel = "")
    {

        return DB::table('copywritingsessions')
            ->join('product_copywritingsessions', 'product_copywritingsessions.copywritingsession_id', '=', 'copywritingsessions.id')
            ->leftjoin('products', 'product_copywritingsessions.product_id', '=', 'products.id')
            ->select(DB::raw('DISTINCT(product_copywritingsessions.id)'), 'identifier', 'products.brand_name', 'products.base_product_id', 'products.color_family', 'products.created_at as created_at', 'products.id as product_id', 'product_copywritingsessions.id as cid', 'copywritingsessions.status')
            ->where('product_copywritingsessions.copywritingsession_id', $csession_id)
            ->where('copywritingsessions.status', '!=', 'complete')
            ->where('product_copywritingsessions.copy_state', '1')
            ->orderBy('product_copywritingsessions.id', 'desc')
            ->groupBy('product_copywritingsessions.id')
            ->get();

    }

    public function getReviewProductsList($csession_id, $channel = "ounass")
    {

        $query = DB::table('products')
            ->join('product_copywritingsessions', function ($join) {
                $join->on('product_copywritingsessions.product_id', '=', 'products.id')
                    ->whereRaw("product_copywritingsessions.id=(select max(id) from product_copywritingsessions as t1 where t1.product_id=product_copywritingsessions.product_id)");
            })
            ->join('ounass', function ($join) {
                $join->on('ounass.Item', '=', 'products.identifier');
                $join->on('ounass.Channel', '=', 'products.channel');
            })
            ->Join('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->join('copy_header_values', 'copy_header_values.product_id', '=', 'products.id')
            ->Join('users', 'users.id', '=', 'product_copywritingsessions.user_id')
            ->select('products.*',
                'simple_category.category as simple_category',
                'users.name as copywriter',
                'product_copywritingsessions.copywritingsession_id as cid', 'product_copywritingsessions.qc_state', 'product_copywritingsessions.user_id as uid', 'product_copywritingsessions.lock', 'product_copywritingsessions.product_id')
            ->where('product_copywritingsessions.copy_state', '1')
            ->where('product_copywritingsessions.qc_state', '0')
            ->where('ounass.Channel', $channel)
            ->whereNotIn('products.id', DB::table('product_copywritingsessions')->select('product_copywritingsessions.product_id')->where('product_copywritingsessions.lock', '=', 1)->where('product_copywritingsessions.user_id', '!=', Auth::user()->id))
            ->orderBy('product_copywritingsessions.id', 'asc')
            ->orderBy('products.id', 'desc')
            ->groupBy('products.id')
            ->limit(300)
            ->get();
        //->toSql();
        //echo $query;exit;
        return $query;

    }

    /**
     *
     * fetch only completed review product:
     *
     */
    public function getReviewProductsListCompleted($csession_id, $channel = "ounass")
    {

        $query = DB::table('products')
            ->join('product_copywritingsessions', function ($join) {
                $join->on('product_copywritingsessions.product_id', '=', 'products.id')
                    ->whereRaw("product_copywritingsessions.id=(select max(id) from product_copywritingsessions as t1 where t1.product_id=product_copywritingsessions.product_id)");
            })
            ->join('ounass', function ($join) {
                $join->on('ounass.Item', '=', 'products.identifier');
                $join->on('ounass.Channel', '=', 'products.channel');
            })
            ->Join('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->Join('copy_revisions', function ($join) {
                $join->on('products.id', '=', 'copy_revisions.product_id');
                $join->on('product_copywritingsessions.copywritingsession_id', '=', 'copy_revisions.copywritingsession_id');
            })
            ->join('copy_header_values', 'copy_header_values.product_id', '=', 'products.id')
        //->Join('users', 'users.id', '=', 'product_copywritingsessions.user_id')
            ->Join('users', 'users.id', '=', 'copy_revisions.user_id')
            ->select('products.*',
                'simple_category.category as simple_category',
                'users.name as copywriter',
                'product_copywritingsessions.copywritingsession_id as cid', 'product_copywritingsessions.qc_state', 'product_copywritingsessions.user_id as uid', 'product_copywritingsessions.lock', 'product_copywritingsessions.product_id')
            ->where('product_copywritingsessions.copy_state', '1')
            ->where('product_copywritingsessions.user_id', Auth::user()->id)
            ->where('product_copywritingsessions.copywritingsession_id', $csession_id)
            ->where('product_copywritingsessions.qc_state', '1')
            ->where('ounass.Channel', $channel)
            ->orderBy('product_copywritingsessions.id', 'desc')
            ->groupBy('products.id')
            ->get();

        return $query;

    }

    /**
     *
     * Review section for batched product
     *
     */

    public function getReviewBatchProducts($csession_id)
    {

        $query = DB::table('products')
            ->join('product_copywritingsessions', function ($join) {
                $join->on('product_copywritingsessions.product_id', '=', 'products.id')
                    ->whereRaw("product_copywritingsessions.id=(select max(id) from product_copywritingsessions as t1 where t1.product_id=product_copywritingsessions.product_id)");
            })
        //->leftjoin('copywritingsessions', 'product_copywritingsessions.product_id', '=', 'products.id')
            ->join('ounass', function ($join) {
                //$join->on('ounass.Barcode','=','products.identifier'); // i want to join the users table with either of these columns
                $join->on('ounass.Item', '=', 'products.identifier');
            })

            ->Join('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->join('copy_header_values', 'copy_header_values.product_id', '=', 'products.id')
            ->Join('users', 'users.id', '=', 'product_copywritingsessions.user_id')
            ->select('products.*',
                'simple_category.category as simple_category',
                'users.name as copywriter',
                'product_copywritingsessions.copywritingsession_id as cid', 'product_copywritingsessions.qc_state', 'product_copywritingsessions.user_id as uid', 'product_copywritingsessions.lock', 'product_copywritingsessions.product_id')
            ->where('product_copywritingsessions.copy_state', '1')
            ->where('product_copywritingsessions.lock', '0')
            ->orderBy('product_copywritingsessions.id', 'desc')
            ->groupBy('products.id')
            ->limit(300)
            ->get();
        return $query;

    }

    public function getReviewBatchedProductsList($csession_id)
    {

        $query = DB::table('products')
            ->join('product_copywritingsessions', function ($join) {
                $join->on('product_copywritingsessions.product_id', '=', 'products.id')
                    ->whereRaw("product_copywritingsessions.id=(select max(id) from product_copywritingsessions as t1 where t1.product_id=product_copywritingsessions.product_id)");
            })
            ->join('ounass', function ($join) {
                $join->on('ounass.Item', '=', 'products.identifier');
            })
            ->Join('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->join('copy_header_values', 'copy_header_values.product_id', '=', 'products.id')
            ->Join('users', 'users.id', '=', 'product_copywritingsessions.user_id')
            ->select('products.*',
                'simple_category.category as simple_category',
                'users.name as copywriter',
                'product_copywritingsessions.copywritingsession_id as cid', 'product_copywritingsessions.qc_state', 'product_copywritingsessions.user_id as uid', 'product_copywritingsessions.lock', 'product_copywritingsessions.product_id')
            ->where('product_copywritingsessions.copywritingsession_id', $csession_id)
            ->where('product_copywritingsessions.copy_state', '1')
            ->where('product_copywritingsessions.qc_state', '0')
            ->orderBy('product_copywritingsessions.id', 'desc')
            ->groupBy('products.id')
            ->limit(300)
            ->get();

        return $query;
    }

    /**
     *
     * Total completed Batched review product.
     *
     */

    public function getReviewBatchedProductsListCompleted($csession_id)
    {

        $query = DB::table('products')
            ->join('product_copywritingsessions', function ($join) {
                $join->on('product_copywritingsessions.product_id', '=', 'products.id')
                    ->whereRaw("product_copywritingsessions.id=(select max(id) from product_copywritingsessions as t1 where t1.product_id=product_copywritingsessions.product_id)");
            })
            ->join('ounass', function ($join) {
                $join->on('ounass.Item', '=', 'products.identifier');
            })
            ->Join('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->join('copy_header_values', 'copy_header_values.product_id', '=', 'products.id')
            ->Join('users', 'users.id', '=', 'product_copywritingsessions.user_id')
            ->select('products.*',
                'simple_category.category as simple_category',
                'users.name as copywriter',
                'product_copywritingsessions.copywritingsession_id as cid', 'product_copywritingsessions.qc_state', 'product_copywritingsessions.user_id as uid', 'product_copywritingsessions.lock', 'product_copywritingsessions.product_id')
            ->where('product_copywritingsessions.copywritingsession_id', $csession_id)
            ->where('product_copywritingsessions.copy_state', '1')
            ->where('product_copywritingsessions.qc_state', '1')
            ->where('product_copywritingsessions.user_id', Auth::user()->id)
            ->orderBy('product_copywritingsessions.id', 'desc')
            ->groupBy('products.id')
            ->get();

        return $query;
    }

    public function getSessionClosedProductList($csession_id)
    {

        return DB::table('copywritingsessions')
            ->join('product_copywritingsessions', 'product_copywritingsessions.copywritingsession_id', '=', 'copywritingsessions.id')
            ->leftjoin('products', 'product_copywritingsessions.product_id', '=', 'products.id')
            ->select(DB::raw('DISTINCT(product_copywritingsessions.id)'), 'identifier', 'products.brand_name', 'products.base_product_id', 'products.color_family', 'products.created_at as created_at', 'products.id as product_id', 'product_copywritingsessions.id as cid', 'copywritingsessions.status')
            ->where('product_copywritingsessions.copywritingsession_id', '!=', $csession_id)
            ->where('copywritingsessions.status', 'closed')
            ->orWhere('product_copywritingsessions.copywritingsession_id', $csession_id)
            ->where('product_copywritingsessions.copy_state', '1')
            ->orderBy('product_copywritingsessions.id')
            ->groupBy('product_copywritingsessions.id')
            ->get();
    }

    public function getPendingCopyProducts($order_of_products, $channel = "ounass")
    {

        $query = DB::table('products')
            ->leftJoin('image_transfers', 'image_transfers.product_id', '=', 'products.id')
            ->leftJoin('ounass', function ($join) {
                $join->on('products.identifier', '=', 'ounass.Item');
                $join->on('products.channel', '=', 'ounass.Channel');
            })
            ->leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->leftJoin('product_copywritingsessions', 'product_copywritingsessions.product_id', '=', 'products.id')
            ->leftJoin('product_states', 'product_states.product_id', '=', 'products.id')
            ->select('ounass.*', 'products.id as product_id', 'product_copywritingsessions.lock_user_id as lock_user_id', 'product_copywritingsessions.id as cid', 'product_copywritingsessions.copy_state', 'product_copywritingsessions.user_id', 'product_copywritingsessions.lock')
            ->where('products.channel', $channel)
            ->where('products.import_at', 'liveshoot')
            ->where('product_copywritingsessions.qc_state', 0)
            ->where('product_copywritingsessions.copy_state', 0)
            ->whereNotIn('products.id', DB::table('product_copywritingsessions')->select('product_copywritingsessions.product_id')
                    ->where(function ($sql_query) {
                        return $sql_query->where('product_copywritingsessions.lock', '=', 1)
                            ->where('product_copywritingsessions.user_id', '!=', Auth::user()->id);
                    })
                    ->orWhere(function ($sql_query) {
                        return $sql_query->where('product_copywritingsessions.is_dropped', 1);
                    })
            )
            ->groupBy('products.id')
            ->orderBy('product_copywritingsessions.copy_state', 'asc')
            ->orderBy('products.id', 'asc')
            ->get();
        return $query;

    }

    /**
     *
     * Get complete products listing into queue after submitting the Product writing
     *
     */

    public function getCompleteCopyProducts($order_of_products, $channel = "ounass", $csession_id)
    {

        return DB::table('products')
            ->leftJoin('image_transfers', 'image_transfers.product_id', '=', 'products.id')
            ->leftJoin('ounass', function ($join) {
                $join->on('products.identifier', '=', 'ounass.Item');
                $join->on('products.channel', '=', 'ounass.Channel');
            })
            ->leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->leftJoin('product_copywritingsessions', 'product_copywritingsessions.product_id', '=', 'products.id')
            ->leftJoin('product_states', 'product_states.product_id', '=', 'products.id')
            ->select('ounass.*', 'products.id as product_id', 'product_copywritingsessions.id as cid', 'product_copywritingsessions.copy_state', 'product_copywritingsessions.lock_user_id as lock_user_id', 'product_copywritingsessions.user_id', 'product_copywritingsessions.lock')
            ->where('products.channel', $channel)
            ->where('products.import_at', 'liveshoot')
            ->where('product_copywritingsessions.qc_state', 0)
            ->where('product_copywritingsessions.copy_state', 1)
            ->where('product_copywritingsessions.copywritingsession_id', $csession_id)
            ->where('product_copywritingsessions.user_id', Auth::user()->id)
            ->groupBy('products.id')
            ->orderBy('product_copywritingsessions.id', 'desc')
            ->get();

    }

    /**
     *
     * getBatchedPendingCopyProducts : function only refresh and get Batched products
     * set sql_mode="";
     *
     */

    public function getBatchedPendingCopyProducts($order_of_products, $channel = "ounass")
    {

        $query = DB::table('products')
            ->leftJoin('image_transfers', 'image_transfers.product_id', '=', 'products.id')
            ->leftJoin('ounass', function ($join) {
                $join->on('products.identifier', '=', 'ounass.Item');
                $join->on('products.channel', '=', 'ounass.Channel');
            })
            ->leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->leftJoin('product_copywritingsessions', 'product_copywritingsessions.product_id', '=', 'products.id')
            ->leftJoin('product_states', 'product_states.product_id', '=', 'products.id')
            ->select('ounass.*', 'products.id as product_id', 'product_copywritingsessions.id as cid', 'product_copywritingsessions.copy_state', 'product_copywritingsessions.lock_user_id as lock_user_id', 'product_copywritingsessions.user_id', 'product_copywritingsessions.lock', 'product_copywritingsessions.state')
            ->where('product_states.qc_done', 1)
            ->where('product_states.qc_state', 1)
            ->where('products.channel', $channel)
            ->where('products.import_at', 'liveshoot')
            ->whereNotIn('products.id', DB::table('product_copywritingsessions')->select('product_copywritingsessions.product_id')->where('product_copywritingsessions.state', 'exist')
                    ->orWhere('product_copywritingsessions.qc_state', 1)->orWhere('product_copywritingsessions.copy_state', 1))
            ->groupBy('products.id')
            ->orderBy('products.id', 'asc')
            ->get();

        return $query;
    }

    /**
     *
     * getPendingCopyProductsBatchList : function only refresh and get Batched products
     * set sql_mode="";
     *
     */

    public function getPendingCopyProductsBatchList($session)
    {

        $query = DB::table('products')
            ->leftJoin('image_transfers', 'image_transfers.product_id', '=', 'products.id')
            ->leftJoin('ounass', function ($join) {
                $join->on('products.identifier', '=', 'ounass.Item');
                $join->on('products.channel', '=', 'ounass.Channel');
            })
            ->leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->leftJoin('product_copywritingsessions', 'product_copywritingsessions.product_id', '=', 'products.id')
            ->leftJoin('product_states', 'product_states.product_id', '=', 'products.id')
            ->select('ounass.*', 'products.id as product_id', 'product_copywritingsessions.id as cid', 'product_copywritingsessions.copy_state', 'product_copywritingsessions.user_id', 'product_copywritingsessions.lock', 'product_copywritingsessions.state')
            ->where('products.import_at', 'liveshoot')
            ->where('product_states.qc_done', 1)
            ->where('product_states.qc_state', 1)
            ->where('product_copywritingsessions.copywritingsession_id', $session)
            ->whereNotIn('products.id', DB::table('product_copywritingsessions')->select('product_copywritingsessions.product_id')->where('product_copywritingsessions.qc_state', 1)->orWhere('product_copywritingsessions.copy_state', 1))
            ->groupBy('products.id')
            ->orderBy('products.id', 'desc')
            ->get();

        return $query;
    }

    public function getWriteProductsList($csession_id, $channel = "")
    {

        if (!empty($channel)) {
            $sql_query = DB::table('copywritingsessions')
                ->join('product_copywritingsessions', 'product_copywritingsessions.copywritingsession_id', '=', 'copywritingsessions.id')
                ->join('products', 'product_copywritingsessions.product_id', '=', 'products.id')
                ->leftJoin('ounass', function ($join) {
                    $join->on('products.identifier', '=', 'ounass.Item');
                    $join->on('products.channel', '=', 'ounass.Channel');
                })
                ->leftJoin('copy_revisions', function ($join) {
                    $join->on('copy_revisions.product_id', '=', 'products.id')
                        ->where('copy_revisions.id', '=', DB::raw("(select max(`id`) from copy_revisions where product_id = products.id)"));
                })
                ->leftJoin('copy_header_values', function ($join) {
                    $join->on('copy_header_values.product_id', '=', 'products.id');
                    $join->on('copy_header_values.copy_revision_id', '=', 'copy_revisions.id');
                })
                ->select(DB::raw('DISTINCT(product_copywritingsessions.id)'), 'ounass.*', 'products.created_at as created_at', 'products.id as product_id',
                    'product_copywritingsessions.id as cid', 'products.channel', 'product_copywritingsessions.copy_state', 'product_copywritingsessions.lock_user_id as lock_user_id', 'copy_header_values.id as has_draft', 'product_copywritingsessions.user_id', 'product_copywritingsessions.lock', 'product_copywritingsessions.copywritingsession_id')
                ->where('copywritingsessions.id', $csession_id)
                ->where('products.channel', $channel)
                ->where('products.import_at', '!=', 'liveshoot')
                ->where('product_copywritingsessions.copy_state', 0)
                ->where('product_copywritingsessions.qc_state', 0)
                ->whereNotIn('products.id', DB::table('product_copywritingsessions')->select('product_copywritingsessions.product_id')
                        ->where(function ($sql_query) {
                            return $sql_query->where('product_copywritingsessions.lock', '=', 1)
                                ->where('product_copywritingsessions.lock_user_id', '!=', Auth::user()->id);
                        })
                        ->orWhere(function ($sql_query) {
                            return $sql_query->where('product_copywritingsessions.is_dropped', 1);
                        })
                )
                ->orderBy('product_copywritingsessions.id', 'desc')
                ->orderBy('products.id', 'desc')
                ->groupBy('products.identifier', 'products.channel')
                ->get();
            //->toSql();
            //dd($sql_query);
            return $sql_query;
        } else {
            return 'select a channel';
        }
    }

    public function getWriteCompleteProductsList($csession_id, $channel = "")
    {

        if (!empty($channel)) {
            return DB::table('copywritingsessions')
                ->join('product_copywritingsessions', 'product_copywritingsessions.copywritingsession_id', '=', 'copywritingsessions.id')
                ->join('products', 'product_copywritingsessions.product_id', '=', 'products.id')
                ->leftJoin('copy_revisions', function ($join) {
                    $join->on('copy_revisions.product_id', '=', 'products.id')
                        ->where('copy_revisions.id', '=', DB::raw("(select max(`id`) from copy_revisions where product_id = products.id)"));
                })
                ->leftJoin('copy_header_values', function ($join) {
                    $join->on('copy_header_values.product_id', '=', 'products.id');
                    $join->on('copy_header_values.copy_revision_id', '=', 'copy_revisions.id');
                })
                ->select(DB::raw('DISTINCT(product_copywritingsessions.id)'), 'identifier', 'products.brand_name', 'products.color_family', 'products.base_product_id', 'products.created_at as created_at', 'products.id as id',
                    'product_copywritingsessions.id as cid', 'products.channel', 'product_copywritingsessions.copy_state', 'copy_header_values.id as has_draft')
                ->where('copywritingsessions.id', $csession_id)
                ->where('products.channel', $channel)
                ->where('products.import_at', '!=', 'liveshoot')
                ->where('product_copywritingsessions.user_id', Auth::user()->id)
                ->where('product_copywritingsessions.copy_state', 1)
                ->orderBy('product_copywritingsessions.id', 'desc')
                ->groupBy('product_copywritingsessions.id')
                ->get();
        } else {
            return 'select a channel';
        }
    }

    public function getcopyStat($product_id)
    {
        $query = DB::table('copy_headers')
            ->leftjoin('copy_header_values', 'copy_header_values.header_id', '=', 'copy_headers.id')
            ->join('copy_revisions', 'copy_revisions.id', '=', 'copy_header_values.copy_revision_id')
            ->join('copy_templates', 'copy_templates.id', '=', 'copy_headers.copy_template_id')
            ->select('copy_header_values.value', 'copy_header_values.header_id', 'copy_headers.id', 'copy_headers.header_name', 'copy_headers.header_type', 'copy_headers.is_required', 'copy_templates.template_name')
            ->where('copy_header_values.product_id', $product_id)
            ->where('copy_headers.is_visible', 1)
            ->groupBy('copy_headers.id')
            ->orderBy('copy_revisions.id', 'asc')
            ->get();

        return $query;

    }

    public function getOpenCopywritingSession($user_id)
    {
        $open_session = Copywritingsession::where('user_id', $user_id)
            ->where(function ($open_session) {
                $open_session->where('status', '=', 'copy pending')
                    ->orWhere('status', '=', 'open')
                    ->orWhere('status', '=', 'enrich');
            })->first();
        return $open_session;
    }

    public function countOpenSessionByUser($user_id, $status)
    {
        return Copywritingsession::where('user_id', $user_id)->where('status', $status)->count();
    }

    public function getLatestCopyVal($product_id)
    {
        $hig_val_revision = DB::table('copy_revisions')
            ->where('product_id', $product_id)
            ->where('state', 1)
            ->orderBy('id', 'desc')
            ->first();
        if (!empty($hig_val_revision->id)) {
            $query = DB::table('copy_header_values')
                ->join('copy_headers', 'copy_header_values.header_id', '=', 'copy_headers.id')
                ->join('copy_templates', 'copy_templates.id', '=', 'copy_headers.copy_template_id')
                ->select('copy_header_values.value', 'copy_header_values.header_id', 'copy_headers.id', 'copy_headers.header_name', 'copy_headers.header_type', 'copy_headers.is_required', 'copy_templates.template_name', 'copy_header_values.copy_revision_id')
                ->where('copy_header_values.product_id', $product_id)
                ->where('copy_headers.is_visible', 1)
                ->where('copy_header_values.copy_revision_id', $hig_val_revision->id)
                ->orderBy('copy_header_values.id', 'asc')
                ->get();
            //->toSql();
            //dd($query);
            return $query;
        } else {
            return false;
        }
    }

    public function GetParentCopyProducts($id)
    {

        $prod_row = DB::table('products')
            ->leftJoin('ounass', function ($join) {
                $join->on('products.identifier', '=', 'ounass.Item');
                $join->on('products.channel', '=', 'ounass.Channel');
            })
            ->leftJoin('simple_category', function ($join) {
                $join->on('ounass.Class', '=', 'simple_category.class');
                $join->on('ounass.SubClass', '=', 'simple_category.subclass');
            })
            ->select('products.id', 'ounass.Item_Parent', 'ounass.Item')
            ->where('products.id', $id)->first();

        if (!empty($prod_row->Item_Parent)) {
            $parent_prod_result = DB::table('products')->leftJoin('ounass', function ($join) {
                $join->on('products.identifier', '=', 'ounass.Item');
                $join->on('products.channel', '=', 'ounass.Channel');
            })
                ->leftJoin('simple_category', function ($join) {
                    $join->on('ounass.Class', '=', 'simple_category.class');
                    $join->on('ounass.SubClass', '=', 'simple_category.subclass');
                })
                ->select('products.id')
                ->where('products.identifier', '=', $prod_row->Item)
                ->orwhere('products.base_product_id', '=', $prod_row->Item_Parent)
                ->orderBy('products.id', 'desc')
                ->get();
            //->toSql(); echo($parent_prod_result) ; die ;

            if (!empty($parent_prod_result)) {
                foreach ($parent_prod_result as $parent_prod_row) {
                    $copy_info = $this->getLatestCopyVal($parent_prod_row->id);
                    if (!empty($copy_info)) {
                        break;
                    }

                }

                return $copy_info;
            } else {
                return false;
            }
        }

    }
}
