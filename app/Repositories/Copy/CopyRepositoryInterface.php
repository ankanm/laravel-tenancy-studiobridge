<?php

namespace App\Repositories\Copy;
use App\Repositories\Base\BaseRepositoryInterface;

interface CopyRepositoryInterface extends BaseRepositoryInterface
{
	public function getCopySessions($user_id, $status);
}