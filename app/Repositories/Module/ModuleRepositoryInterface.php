<?php

namespace App\Repositories\Module;

use App\Repositories\Base\BaseRepositoryInterface;

interface ModuleRepositoryInterface extends BaseRepositoryInterface
{
    public function pluck(String $attributes);
}
