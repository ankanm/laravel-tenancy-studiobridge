<?php

namespace App\Repositories\Module;

use App\Models\Module;
use App\Repositories\Module\ModuleRepositoryInterface;
use App\Repositories\Base\BaseRepository;

class ModuleRepository extends BaseRepository implements ModuleRepositoryInterface
{
    protected $model;
  
    /**
     * ModuleRepository constructor.
     * @param Module $module
     */
    public function __construct(Module $module)
    {
        $this->model = $module;
    }


    /**
     * @param string $attributes
     * @return mixed
     */
    public function pluck(string $attribute1 , string $attribute2 = "")
    {
        return $this->model->pluck($attribute1,$attribute2)->all();
    }
}