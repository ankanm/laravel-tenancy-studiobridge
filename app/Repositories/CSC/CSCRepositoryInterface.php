<?php

namespace App\Repositories\CSC;

use App\Repositories\Base\BaseRepositoryInterface;

interface CSCRepositoryInterface extends BaseRepositoryInterface
{
	public function getCoutries();
	
	public function getStates();

	public function getCities();
}
