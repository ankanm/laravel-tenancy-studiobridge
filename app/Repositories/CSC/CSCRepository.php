<?php

namespace App\Repositories\CSC;

use App\Models\City;
use App\Models\Country;
use App\Models\State;
use App\Repositories\Base\BaseRepository;
use App\Repositories\CSC\CSCRepositoryInterface;

class CSCRepository extends BaseRepository implements CSCRepositoryInterface
{
    protected $modelCountry;
    protected $modelState;
    protected $modelCity;

    /**
     * CSCRepository constructor.
     * @param Country $Country, State $State, City $City
     */
    public function __construct(Country $Country, State $State, City $City)
    {
        $this->modelCountry = $Country;
        $this->modelState   = $State;
        $this->modelCity    = $City;
    }

    public function getCoutries()
    {
        return $this->modelCountry->all();
    }

    public function getStates()
    {
        return $this->modelState->all();
    }

    public function getCities()
    {
        return $this->modelCity->all();
    }
}
