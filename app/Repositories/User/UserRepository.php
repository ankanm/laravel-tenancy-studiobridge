<?php

namespace App\Repositories\User;

use App\User;
use Illuminate\Support\Facades\DB;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Base\BaseRepository;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    protected $model;
  
    /**
     * ArticlesRepository constructor.
     * @param Article $article
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    
    
}