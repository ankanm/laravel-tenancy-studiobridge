<?php

namespace App\Repositories\Locations;

use App\Models\Locations;
use App\Repositories\Base\BaseRepository;
use App\Repositories\Locations\LocationsRepositoryInterface;

class LocationsRepository extends BaseRepository implements LocationsRepositoryInterface
{
    protected $model;

    /**
     * LocationsRepository constructor.
     * @param Locations $location
     */
    public function __construct(Locations $location)
    {
        $this->model = $location;
    }
}
