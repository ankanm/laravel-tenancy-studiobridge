<?php

namespace App\Repositories\Agency;

use App\Models\Agency;
use Illuminate\Support\Facades\DB;
use App\Repositories\Agency\AgencyRepositoryInterface;
use App\Repositories\Base\BaseRepository;

class AgencyRepository extends BaseRepository implements AgencyRepositoryInterface
{
    protected $model;
  
    /**
     * ArticlesRepository constructor.
     * @param Article $article
     */
    public function __construct(Agency $agency)
    {
        $this->model = $agency;
    }

    //Get Agency Users
    public function getAgencyWithUsers($agencyid)
    {
        return DB::table('users')
        ->join('agency_users', 'users.id', '=', 'agency_users.user_id')
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('agency_users.agencies_id', $agencyid)
        ->where('roles.name', '!=', 'Super Admin')
        ->select('users.name as username', 'email','avatar','active','roles.name as role_name',
            'users.active as status', 'users.id as userid')
        ->groupBy('users.id')
        ->get();
    }

    //Get Agency Locations
    public function getAgencyWithLocation($agencyid)
    {
        return DB::table('agencies')
        ->join('locations', 'agencies.id', '=', 'locations.agencies_id')
        ->join('countries', 'countries.id', '=', 'locations.country')
        ->where('agencies.id', $agencyid)
        ->select('locations.id as loc_id', 'short_name','address1','address2','city','province',
            'zipcode','country','is_primary', 'agencies.name as agency_name','contact',
            'countries.name as country_name')
        ->get();
    }

    //Get Agency based User Roles
    public function getUserRoleByType($agencyid)
    {
        return DB::table('agencies')
        ->join('roles', 'agencies.agency_type', '=', 'roles.role_for')
        ->where('agencies.id', $agencyid)
        ->select('roles.id as role_id', 'roles.name as role_name')
        ->get();
    }

    //Get Copywriting Agency Users
    public function GetCopywritingAgencyUsers($agencyid)
    {
        return DB::table('users')
        ->join('agency_users', 'users.id', '=', 'agency_users.user_id')
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('agency_users.agencies_id', $agencyid)
        // ->where('roles.role_for', 'copywriting')
        ->select('users.name as username', 'email','avatar','active','roles.name as role_name',
            'users.active as status', 'users.id as userid')
        ->groupBy('users.id')
        ->get();
    }
    
}