<?php

namespace App\Repositories\Agency;

use App\Repositories\Base\BaseRepositoryInterface;

interface AgencyRepositoryInterface extends BaseRepositoryInterface
{
   public function create(array $attributes);
}