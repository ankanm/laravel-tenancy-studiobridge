<?php

namespace App\Repositories\Base;

interface BaseRepositoryInterface
{
    public function create(array $attributes);

    public function updateCreate(array $attributes);

    public function update(array $attributes, int $id);

    public function all($columns = array('*'), $condition = array(), string $orderBy = 'id', string $sortBy = 'asc');
    
    public function allWithLimit($columns = array('*'), $condition = array(), $limit =10 ,string $orderBy = 'id', string $sortBy = 'asc');

    public function find(int $id);
    
    public function findOneOrFail(int $id);

    public function findBy(array $data, string $orderBy = 'id', string $sortBy = 'asc');

    public function findOneBy(array $data);

    public function findOneByOrFail(array $data);

    public function paginateArrayResults(array $data, int $perPage = 50);

    public function delete(int $id);
    
    public function deleteBy(array $condition);
}